# qbe

Package qbe deals with the QBE intermediate language

### Installation

    $ go get [-u] modernc.org/qbe

### Documentation

[godoc.org/modernc.org/qbe](http://godoc.org/modernc.org/qbe)

[QBE language overview](https://c9x.me/compile/)

[QBE language reference](https://c9x.me/compile/doc/il.html)
