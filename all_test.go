// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package qbe // import "modernc.org/qbe"

import (
	"bytes"
	"flag"
	"fmt"
	"go/token"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"reflect"
	"regexp"
	"runtime"
	"runtime/debug"
	"strings"
	"testing"
	"time"
	"unicode"
	"unsafe"

	"github.com/pmezard/go-difflib/difflib"
	"modernc.org/cc/v3"
	"modernc.org/scannertest"
)

func caller(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	_, fn, fl, _ := runtime.Caller(2)
	fmt.Fprintf(os.Stderr, "# caller: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	_, fn, fl, _ = runtime.Caller(1)
	fmt.Fprintf(os.Stderr, "# \tcallee: %s:%d: ", path.Base(fn), fl)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func dbg(s string, va ...interface{}) {
	if s == "" {
		s = strings.Repeat("%v ", len(va))
	}
	pc, fn, fl, _ := runtime.Caller(1)
	f := runtime.FuncForPC(pc)
	fmt.Fprintf(os.Stderr, "# dbg %s:%d:%s: ", path.Base(fn), fl, f.Name())
	fmt.Fprintf(os.Stderr, s, va...)
	fmt.Fprintln(os.Stderr)
	os.Stderr.Sync()
}

func TODO(...interface{}) string { //TODOOK
	_, fn, fl, _ := runtime.Caller(1)
	return fmt.Sprintf("# TODO: %s:%d:\n", path.Base(fn), fl) //TODOOK
}

func stack() []byte { return debug.Stack() }

func use(...interface{}) {}

func init() {
	use(caller, dbg, TODO, stack, bits.dump, bits.len) //TODOOK
}

// ----------------------------------------------------------------------------

var (
	_ scannertest.Interface = (*testScanner)(nil)

	oCSmith = flag.Duration("csmith", time.Minute, "")
	oRE     = flag.String("re", "", "")
	oTrc    = flag.Bool("trc", false, "")
	re      *regexp.Regexp
	tempDir string

	astBlacklist = map[string]struct{}{
		"eucl.ssa":  {}, // :12:30: interdependent phi instructions within the same block not supported: %a
		"philv.ssa": {}, // :24:31: interdependent phi instructions within the same block not supported: %y0
		"abi1.ssa":  {}, // :28:14: void function result used as value: $alpha
		"abi4.ssa":  {}, // :25:14: void function result used as value: $alpha
		"dark.ssa":  {}, // :23:13: void function result used as value: $test
	}

	digits  = expand(unicode.Nd)
	letters = expand(unicode.L)

	ccBinary string
)

func TestMain(m *testing.M) {
	fmt.Fprintf(os.Stderr, "qbe: %v\n", os.Args)
	oGCC := flag.String("gcc", "", "")
	flag.Int("bestof", 0, "") // qbec/cc flags. Allows performing "go test -v ./... -bestof N" in qbe directory
	flag.BoolVar(&TraceC, "trcc", false, "")
	flag.Parse()
	if s := *oRE; s != "" {
		re = regexp.MustCompile(s)
	}
	if *oGCC == "" {
		ccBinary = os.Getenv("CC")
		if ccBinary == "" {
			ccBinary = "gcc"
		}
		var err error
		if ccBinary, err = exec.LookPath(ccBinary); err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		os.Exit(testMain(m))
	}

	var args []string
	for i, v := range os.Args {
		if v == "-gcc" {
			args = append(os.Args[:i], os.Args[i+2:]...)
		}
	}
	a := strings.Split(*oGCC, ",")
	rc := 0
	for _, suffix := range a {
		ccBinary = fmt.Sprintf("gcc-%s", suffix)
		var err error
		if ccBinary, err = exec.LookPath(ccBinary); err != nil {
			fmt.Fprintf(os.Stderr, "%s: %s\n", ccBinary, err)
			continue
		}

		os.Setenv("CC", ccBinary)
		cmd := exec.Command(args[0], args[1:]...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			rc = 1
		}
	}
	os.Exit(rc)
}

func gccVersion(bin string) string {
	out, err := exec.Command(bin, "--version").CombinedOutput()
	if err != nil {
		return ""
	}

	a := strings.Split(string(out), "\n")
	if len(a) != 0 {
		return a[0]
	}

	return ""
}

func expand(cat *unicode.RangeTable) (r []rune) {
	for _, v := range cat.R16 {
		for x := v.Lo; x <= v.Hi; x += v.Stride {
			r = append(r, rune(x))
		}
	}
	for _, v := range cat.R32 {
		for x := v.Lo; x <= v.Hi; x += v.Stride {
			r = append(r, rune(x))
		}
	}
	s := rand.NewSource(42)
	rn := rand.New(s)
	for i := range r {
		j := rn.Intn(len(r))
		r[i], r[j] = r[j], r[i]
	}
	return r
}

func testMain(m *testing.M) int {
	fmt.Fprintf(os.Stderr, "CC=%s\n%s\n", ccBinary, gccVersion(ccBinary))
	var err error
	tempDir, err = ioutil.TempDir("", "qbe-test-")
	if err != nil {
		panic(err) //TODOOK
	}

	defer os.RemoveAll(tempDir)

	return m.Run()
}

func TestSize(t *testing.T) {
	if sz := unsafe.Sizeof(CSTToken{}); sz > 24 {
		t.Errorf("CSTToken size > 24: %v", sz)
	}
	if sz := unsafe.Sizeof(Token{}); sz > 24 {
		t.Errorf("Token size > 24: %v", sz)
	}
}

func TestScan(t *testing.T) {
	t.Run("states", testScanStates)
	t.Run("testdata", testScanTestdata)
	t.Run("errchk", testScanErrChk)
}

type testScanner struct {
	inits int
	mod   int
	s     *Scanner
}

func newTestScanner() *testScanner {
	return &testScanner{}
}

func (s *testScanner) Init(name string, src []byte) (err error) {
	//trc("init %q", src)
	s.s, err = NewScanner([]byte(src), name, false)
	s.inits++
	return err
}

func (s *testScanner) Rune(c byte) (r rune, ok bool) {

	switch c {
	case 0:
		return -1, false
	case 0x80: // unicodeDigit
		r = digits[s.mod%len(digits)]
		s.mod++
		return r, true
	case 0x81: // unicodeLetter
		r = letters[s.mod%len(letters)]
		s.mod++
		return r, true
	}

	if c < 128 {
		return rune(c), true
	}

	return -1, false
}

func (s *testScanner) Scan() error {
	s.s.Tok.source = s.s.source
	s.s.scan()
	return s.s.Err()
}

func testScanStates(t *testing.T) {
	b, err := os.ReadFile(filepath.FromSlash("testdata/scanner/scanner.l"))
	if err != nil {
		t.Fatal(err)
	}

	s := newTestScanner()
	if err := scannertest.TestStates("scanner.l", bytes.NewReader(b), s); err != nil {
		t.Fatal(err)
	}

	t.Logf("%v test cases", s.inits)
}

func testScanTestdata(t *testing.T) {
	err := filepath.Walk("testdata", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		if strings.Contains(filepath.ToSlash(path), "/errchk/") {
			return nil
		}

		b, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		switch filepath.Ext(path) {
		case ".ssa", ".qbe":
			s, err := NewScanner(b, path, false)
			if err != nil {
				return err
			}

			toks := 0
			for s.Scan() {
				toks++
			}
			if err := s.Err(); err != nil {
				t.Errorf("%s: %v", path, err)
			}
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

func BenchmarkScanner(b *testing.B) {
	b.Run("15462 bytes", func(b *testing.B) { benchmarkScanner(b, "testdata/c9x.me/qbe/_bf99.ssa") })
	b.Run("41780 bytes", func(b *testing.B) { benchmarkScanner(b, "testdata/c9x.me/qbe/_bfmandel.ssa") })
	b.Run("196108 bytes", func(b *testing.B) { benchmarkScanner(b, "testdata/c9x.me/qbe/_slow.qbe") })
}

func benchmarkScanner(b *testing.B, nm string) {
	buf, err := ioutil.ReadFile(filepath.FromSlash(nm))
	if err != nil {
		b.Fatal(err)
	}

	b.SetBytes(int64(len(buf)))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		s, err := NewScanner(buf, "benchmarkScanner", false)
		if err != nil {
			b.Fatal(err)
		}

		for s.Scan() {
		}
		if err := s.Err(); err != nil {
			b.Fatal(err)
		}
	}
}

func testScanErrChk(t *testing.T) {
	err := filepath.Walk("testdata/errchk/scanner", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		b, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		switch filepath.Ext(path) {
		case ".ssa", ".qbe":
			s, err := NewScanner(b, path, true)
			if err != nil {
				return err
			}

			var toks []Node
			for s.Scan() {
				t := s.Tok
				toks = append(toks, &t)
			}
			errChk(t, s.source, s.errs, append(toks, &s.Tok))
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

func errChk(t *testing.T, s *source, errs []errItem, nodes []Node) {
	t.Helper()
	g := map[int]string{}
	var ga []errItem
	for _, v := range errs {
		p := v.position(s)
		if g[p.Line] == "" {
			g[p.Line] = fmt.Sprintf("%v: %v", p, v.err)
			ga = append(ga, v)
		}
	}
	e := map[int]string{}
	var ea []errChecker
	for _, v := range extractErrChecks(nodes) {
		if s := errChkPattern(v); s != "" {
			e[v.sepPosition().Line] = s
			ea = append(ea, v)
		}
	}
	fn := s.file.Name()
	for _, item := range ga {
		line := item.position(s).Line
		p := item.position(s)
		err := fmt.Sprintf("%v: %v", p, item.err)
		switch x, ok := e[line]; {
		case ok:
			re := regexp.MustCompile(x)
			switch {
			case re.MatchString(err):
				// ok
			default:
				t.Errorf("%v:%d: error `%s` does not match pattern `%s`", fn, line, err, x)
			}
			delete(e, line)
		default:
			t.Errorf("%v:%d: unexpected error `%s`", fn, line, err)
		}
	}
	for _, v := range ea {
		line := v.sepPosition().Line
		if _, ok := e[line]; ok {
			t.Errorf("%v:%d: missing error for pattern `%s`", s.file.Name(), line, e[line])
		}
	}
}

func errChkPattern(t errChecker) string {
	const tag = "#ERROR"
	s := strings.TrimSpace(string(t.Sep()))
	if !strings.HasPrefix(s, tag) {
		return ""
	}

	s = s[len(tag):]
	s = strings.Split(s, "\n")[0]
	s = strings.TrimSpace(s)
	switch {
	case s == "":
		panic(todo("")) //TODOOK
	case s[0] == '"':
		panic(todo("")) //TODOOK
	}
	return s
}

type errChecker interface {
	sepPosition() token.Position
	Sep() []byte
}

func extractErrChecks(nodes []Node) (r []errChecker) {
	for _, v := range nodes {
		r = append(r, extractErrCheckers0(v)...)
	}
	return r
}

func extractErrCheckers0(n interface{}) (r []errChecker) {
	if n == nil {
		return nil
	}

	switch x := n.(type) {
	case CSTToken:
		if !x.IsValid() {
			return nil
		}

		n = &x
	}
	if x, ok := n.(errChecker); ok {
		return []errChecker{x}
	}

	v := reflect.ValueOf(n)
	if v.IsZero() {
		return nil
	}

	if v.Kind() == reflect.Ptr {
		e := v.Elem()
		if e.IsZero() {
			return nil
		}

		v = reflect.ValueOf(e.Interface())
	}
	t := reflect.TypeOf(v.Interface())
	switch v.Kind() {
	case reflect.Struct:
		for i := 0; i < t.NumField(); i++ {
			fld := t.FieldByIndex([]int{i})
			if !token.IsExported(fld.Name) {
				continue
			}

			x := v.FieldByIndex([]int{i}).Interface()
			r = append(r, extractErrCheckers0(x)...)
		}
	case reflect.Slice:
		for i := 0; i < v.Len(); i++ {
			x := v.Index(i).Interface()
			r = append(r, extractErrCheckers0(x)...)
		}
	}
	return r
}

func TestParser(t *testing.T) {
	err := filepath.Walk("testdata", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		if strings.Contains(filepath.ToSlash(path), "/errchk/") {
			return nil
		}

		b, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		switch filepath.Ext(path) {
		case ".ssa", ".qbe":
			if _, err := Parse(b, path, false); err != nil {
				t.Errorf("%s: %v", path, err)
			}
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

func BenchmarkParser(b *testing.B) {
	b.Run("15462 bytes", func(b *testing.B) { benchmarkParser(b, "testdata/c9x.me/qbe/_bf99.ssa") })
	b.Run("41780 bytes", func(b *testing.B) { benchmarkParser(b, "testdata/c9x.me/qbe/_bfmandel.ssa") })
	b.Run("196108 bytes", func(b *testing.B) { benchmarkParser(b, "testdata/c9x.me/qbe/_slow.qbe") })
}

func benchmarkParser(b *testing.B, nm string) {
	buf, err := ioutil.ReadFile(filepath.FromSlash(nm))
	if err != nil {
		b.Fatal(err)
	}

	b.SetBytes(int64(len(buf)))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if _, err := Parse(buf, nm, false); err != nil {
			b.Fatalf("%s: %v", nm, err)
		}
	}
}

func TestParserErrChk(t *testing.T) {
	err := filepath.Walk("testdata/errchk/syntax", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		b, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		switch filepath.Ext(path) {
		case ".ssa", ".qbe":
			p, err := newParser(b, path, true)
			if err != nil {
				t.Fatal(err)
			}

			cst, _ := p.parse()
			errChk(t, p.source, p.errs, append(cst.Defs, &p.tok))
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

func TestAST(t *testing.T) {
	err := filepath.Walk("testdata", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		if strings.Contains(filepath.ToSlash(path), "/errchk/") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(path) {
				return nil
			}
		default:
			if _, ok := astBlacklist[filepath.Base(path)]; ok {
				return nil
			}
		}

		b, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		switch filepath.Ext(path) {
		case ".ssa", ".qbe":
			cst, err := Parse(b, path, false)
			if err != nil {
				t.Errorf("%s: %v", path, err)
				break
			}

			if _, err := cst.AST(runtime.GOOS, runtime.GOARCH); err != nil {
				t.Errorf("%s: %v", path, err)
			}
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

func BenchmarkAST(b *testing.B) {
	b.Run("15462 bytes", func(b *testing.B) { benchmarkAST(b, "testdata/c9x.me/qbe/_bf99.ssa") })
	b.Run("41780 bytes", func(b *testing.B) { benchmarkAST(b, "testdata/c9x.me/qbe/_bfmandel.ssa") })
	b.Run("196108 bytes", func(b *testing.B) { benchmarkAST(b, "testdata/c9x.me/qbe/_slow.qbe") })
}

func benchmarkAST(b *testing.B, nm string) {
	buf, err := ioutil.ReadFile(filepath.FromSlash(nm))
	if err != nil {
		b.Fatal(err)
	}

	b.SetBytes(int64(len(buf)))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		cst, err := Parse(buf, nm, false)
		if err != nil {
			b.Fatalf("%s: %v", nm, err)
			break
		}

		if _, err := cst.AST(runtime.GOOS, runtime.GOARCH); err != nil {
			b.Fatalf("%s: %v", nm, err)
		}
	}
}

func TestASTErrChk(t *testing.T) {
	blacklist := map[string]struct{}{}
	abi, err := cc.NewABI(runtime.GOOS, runtime.GOARCH)
	if err != nil {
		t.Fatal(err)
	}

	err = filepath.Walk("testdata/errchk/parser", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(path) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.ToSlash(path)]; ok {
				return nil
			}
		}

		b, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		switch filepath.Ext(path) {
		case ".ssa", ".qbe":
			p, err := newParser(b, path, true)
			if err != nil {
				t.Fatal(err)
			}

			cst, err := p.parse()
			if err != nil {
				t.Fatal(err)
			}

			ast, _ := cst.ast(&abi)
			errChk(t, ast.source, append(ast.errs, ast.warnings...), cst.Defs)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

type devNull struct{}

func (devNull) Write(b []byte) (int, error) { return len(b), nil }

func TestC(t *testing.T) {
	blacklist := map[string]struct{}{}
	if runtime.GOOS == "windows" {
		blacklist["conaddr.ssa"] = struct{}{} // #include <signal.h>
	}
	err := filepath.Walk("testdata", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		if strings.Contains(filepath.ToSlash(path), "/errchk/") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(path) {
				return nil
			}
		default:
			if _, ok := astBlacklist[filepath.Base(path)]; ok {
				return nil
			}

			if _, ok := blacklist[filepath.Base(path)]; ok {
				return nil
			}
		}

		b, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		switch filepath.Ext(path) {
		case ".ssa", ".qbe":
			cst, err := Parse(b, path, false)
			if err != nil {
				t.Errorf("%s: %v", path, err)
				break
			}

			ast, err := cst.AST(runtime.GOOS, runtime.GOARCH)
			if err != nil {
				t.Errorf("%s: %v", path, err)
				break
			}

			if err := ast.C(devNull{}, "", runtime.GOOS, runtime.GOARCH); err != nil {
				t.Errorf("%s: %v", path, err)
			}
			return nil
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

func TestC2(t *testing.T) {
	blacklist := map[string]struct{}{}
	if runtime.GOOS == "windows" {
		blacklist["queen.ssa"] = struct{}{} // error: conflicting types for 'calloc'
	}
	if runtime.GOARCH == "386" {
		blacklist["vararg1.ssa"] = struct{}{} //TODO va_list
		blacklist["vararg2.ssa"] = struct{}{} //TODO va_list
	}
	if runtime.GOARCH == "arm" {
		blacklist["vararg1.ssa"] = struct{}{} //TODO va_list
		blacklist["vararg2.ssa"] = struct{}{} //TODO va_list
	}
	if runtime.GOARCH == "arm64" {
		blacklist["vararg1.ssa"] = struct{}{} //TODO va_list
		blacklist["vararg2.ssa"] = struct{}{} //TODO va_list
	}
	err := filepath.Walk("testdata", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		if strings.Contains(filepath.ToSlash(path), "/errchk/") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(path) {
				return nil
			}
		default:
			if _, ok := astBlacklist[filepath.Base(path)]; ok {
				return nil
			}

			if _, ok := blacklist[filepath.Base(path)]; ok {
				return nil
			}
		}

		b, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		switch filepath.Ext(path) {
		case ".ssa", ".qbe":
			if *oTrc {
				fmt.Printf("%s\n", path)
			}
			cst, err := Parse(b, path, false)
			if err != nil {
				t.Errorf("%s: %v", path, err)
				break
			}

			ast, err := cst.AST(runtime.GOOS, runtime.GOARCH)
			if err != nil {
				t.Errorf("%s: %v", path, err)
				break
			}

			var headers string
			switch {
			case runtime.GOOS == "windows":
				// https://stackoverflow.com/a/58286937
				headers = `
#include <malloc.h>
#include <stdarg.h>
#include <stdint.h>
`
			default:
				headers = `
#include <alloca.h>
#include <stdarg.h>
#include <stdint.h>
`
			}
			var b bytes.Buffer
			if err := ast.C(&b, headers, runtime.GOOS, runtime.GOARCH); err != nil {
				t.Errorf("%s: %v", path, err)
				return nil
			}

			pth := filepath.Join(tempDir, "x.c")
			if err := ioutil.WriteFile(pth, b.Bytes(), 0660); err != nil {
				return err
			}

			out, err := exec.Command(
				ccBinary,
				"-c",
				"-o", os.DevNull,
				"-w",
				pth,
			).CombinedOutput()
			if err != nil {
				return fmt.Errorf("%s: %v\n%s\n----\n%s", path, err, out, b.Bytes())
			}

			if len(out) != 0 {
				t.Logf("%s: WARNING: %s", path, out)
			}
			return nil
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

func TestExec(t *testing.T) {
	blacklist := map[string]struct{}{}
	switch {
	case runtime.GOOS == "windows":
		// Other
		blacklist["conaddr.ssa"] = struct{}{} // error: unknown type name 'siginfo_t'
		blacklist["vararg1.ssa"] = struct{}{} //TODO va_list
		blacklist["vararg2.ssa"] = struct{}{} //TODO va_list
	case runtime.GOARCH == "s390x":
		blacklist["ldbits.ssa"] = struct{}{} //TODO recompute constants for big endian
	case runtime.GOARCH == "386":
		blacklist["vararg1.ssa"] = struct{}{} //TODO va_list
		blacklist["vararg2.ssa"] = struct{}{} //TODO va_list
	case runtime.GOARCH == "arm":
		blacklist["vararg1.ssa"] = struct{}{} //TODO va_list
		blacklist["vararg2.ssa"] = struct{}{} //TODO va_list
	}
	if runtime.GOARCH == "arm64" {
		blacklist["vararg1.ssa"] = struct{}{} //TODO va_list
		blacklist["vararg2.ssa"] = struct{}{} //TODO va_list
	}
	var files, ok int
	err := filepath.Walk(fmt.Sprintf("testdata/%s/%s", runtime.GOOS, runtime.GOARCH), func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(path) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(path)]; ok {
				return nil
			}
		}
		files++
		b, err := ioutil.ReadFile(path)
		if err != nil {
			return err
		}

		switch filepath.Ext(path) {
		case ".ssa", ".qbe":
			if *oTrc {
				fmt.Printf("%s\n", path)
			}
			cst, err := Parse(b, path, false)
			if err != nil {
				t.Errorf("%s: %v", path, err)
				break
			}

			ast, err := cst.AST(runtime.GOOS, runtime.GOARCH)
			if err != nil {
				t.Errorf("%s: %v", path, err)
				break
			}

			driver, want := extractDriverAndOutput(ast.EOF.Src())
			driverPath := filepath.Join(tempDir, "driver.c")
			if err := ioutil.WriteFile(driverPath, []byte(driver), 0660); err != nil {
				return fmt.Errorf("failed to write driver.c: %v", err)
			}

			bin := "a.out"
			var headers string
			switch {
			case runtime.GOOS == "windows":
				bin = "a.exe"
				// There's no alloca.h in windows: https://stackoverflow.com/a/58286937
				headers = `
#include <malloc.h>
#include <stdarg.h>
#include <stdint.h>
`
			default:
				headers = `
#include <alloca.h>
#include <stdarg.h>
#include <stdint.h>
`
			}
			var b bytes.Buffer
			if err := ast.C(&b, headers, runtime.GOOS, runtime.GOARCH); err != nil {
				t.Errorf("%s: %v", path, err)
				return nil
			}

			pth := filepath.Join(tempDir, "x.c")
			binPath := filepath.Join(tempDir, bin)
			if err := ioutil.WriteFile(pth, b.Bytes(), 0660); err != nil {
				return fmt.Errorf("failed to write x.c: %v", err)
			}

			out, err := exec.Command(
				ccBinary,
				"-O1",
				"-o", binPath,
				pth,
				driverPath,
			).CombinedOutput()
			if err != nil {
				return fmt.Errorf("%s: compile failed: %v\n%s\n---- x.c \n%s", path, err, out, b.Bytes())
			}

			if len(out) != 0 {
				t.Logf("%s: WARNING: %s", path, out)
			}

			if out, err = exec.Command(
				binPath,
				"a",
				"b",
				"c",
			).CombinedOutput(); err != nil {
				return fmt.Errorf("%s: execution failed: %v\n%s\n---- x.c\n%s", path, err, out, b.Bytes())
			}
			if len(want) != 0 {
				g, e := string(out), want
				g = strings.ReplaceAll(g, "\r", "")
				e = strings.ReplaceAll(e, "\r", "")
				switch {
				case strings.Contains(e, " #\n"):
					a := strings.Split(e, "\n")
					for i, v := range a {
						a[i] = v[:len(v)-1]
					}
					e = strings.Join(a, "\n")
				}
				if strings.HasSuffix(g, "\n") {
					g = g[:len(g)-1]
				}
				if g != e {
					diff := difflib.UnifiedDiff{
						A:        difflib.SplitLines(e),
						B:        difflib.SplitLines(g),
						FromFile: path + ".expect",
						ToFile:   path + ".got",
						Context:  3,
					}
					text, _ := difflib.GetUnifiedDiffString(diff)
					t.Errorf("%v: output differs:\n%s\n---- x.c\n%s", path, text, b.Bytes())
					return nil
				}
			}

			ok++
			return nil
		}
		return nil
	})
	t.Logf("files %v, ok %v", files, ok)
	if err != nil {
		t.Fatal(err)
	}
}

func extractDriverAndOutput(b []byte) (d, o string) {
	a := strings.SplitN(string(b), "<<<", 3)
	for _, v := range a {
		v = strings.TrimSpace(v)
		lines := strings.Split(v, "\n")
		lines = lines[:len(lines)-1]
		for i, v := range lines {
			if strings.HasPrefix(v, "# ") {
				lines[i] = v[2:]
			}
		}
		switch {
		case len(lines) == 0:
			continue
		case lines[0] == ">>> driver":
			d = strings.Join(lines[1:], "\n")
		case lines[0] == ">>> output":
			o = strings.Join(lines[1:], "\n")
		}
	}
	return d, o
}
