// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*TODO

  char x[42];
  intptr_t v42 = &x;
  v43 = *(char*)(v42);

  ->

  char x[42];

  v43 = *(char*)(&x);

*/

package qbe // import "modernc.org/qbe"

import (
	"bytes"
	"fmt"
	"io"
	"math"
	"sort"
	"strings"
)

const (
	typePrefix = "__qbe_t"
)

var (
	// TraceC enables printing the generated C code to stdout.
	TraceC bool // testing
)

// C renders an AST as the body of compilable C code prepended with includes.
func (a *AST) C(out io.Writer, includes, os, arch string) error {
	g, err := newCGen(out, a, os, arch)
	if err != nil {
		return err
	}

	g.ptr = a.ptr
	g.w("%s", includes)
	return g.main()
}

type cgen struct {
	arch                     string
	ast                      *AST
	declaredExternPrototypes map[string]struct{}
	errs                     msgList
	os                       string
	out                      io.Writer
	ptr                      Type
	vaInfo                   *VaInfo

	isClosed  bool
	allErrors bool
}

func newCGen(out io.Writer, ast *AST, os, arch string) (*cgen, error) {
	vaInfo, err := VaInfoFor(os, arch)
	if err != nil {
		return nil, err
	}

	r := &cgen{
		arch:                     arch,
		ast:                      ast,
		declaredExternPrototypes: map[string]struct{}{},
		os:                       os,
		out:                      out,
		vaInfo:                   vaInfo,
	}
	return r, nil
}

func (g *cgen) err(off int32, skip int, msg string, args ...interface{}) {
	if len(g.errs) == 10 && !g.allErrors {
		return
	}

	g.errs.err(off, skip+1, msg, args...)
}

func (g *cgen) w(s string, args ...interface{}) {
	if g.isClosed {
		return
	}

	if TraceC {
		fmt.Printf(s, args...)
	}
	if _, err := fmt.Fprintf(g.out, s, args...); err != nil {
		g.err(0, 1, "error writing output: %v", err)
		g.isClosed = true
	}
}

func (g *cgen) main() error {
	ctx := &ctx{types: g.types()}
	g.w("\n")
	g.extern(ctx)
	g.funcForwardDefs(ctx)
	g.dataForwardDefs()
	for _, v := range g.ast.Defs {
		switch x := v.(type) {
		case *FuncDef:
			g.funcDef(ctx, x)
		}
	}
	g.w("\n")
	return g.errs.Err(g.ast.source)
}

func (g *cgen) extern(ctx *ctx) {
	prototypes := map[string]string{}
	a := strings.Split(string(g.ast.FirstSeparator()), "\n")
	for _, v := range a {
		v = strings.TrimSpace(v)
		const tag = "#qbec:prototype "
		if strings.HasPrefix(v, tag) {
			v = v[len(tag):]
			x := strings.IndexByte(v, ' ')
			prototypes[v[:x]] = v[x+1:]
		}
	}
	a = a[:0]
	for k := range g.ast.ExternFuncs {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		if strings.HasPrefix(v, "__builtin_") ||
			v == "__qbe_va_end" || v == "__qbe_va_copy" ||
			g.os == "windows" && (v == "alloca" || v == "_alloca") {
			continue
		}

		if s, ok := prototypes[v]; ok {
			g.declaredExternPrototypes[v] = struct{}{}
			g.w("\n%s", s)
			continue
		}

		s := "void"
		if typ := g.ast.ExternFuncs[v]; typ != nil {
			s = g.typ(ctx, typ, true)
		}
		g.w("\nextern %s %s();", s, v)
	}
	a = a[:0]
	for k := range g.ast.ExternData {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		if s, ok := prototypes[v]; ok {
			g.declaredExternPrototypes[v] = struct{}{}
			g.w("\n%s", s)
			continue
		}

		g.w("\nextern char %s;", v)
	}
}

type global struct {
	global GlobalInitializer
	name   Name
	x      uintptr

	isIndex bool
}

func (g *cgen) dataForwardDefs() {
	done := map[*DataDef]struct{}{}
	first := true
	for _, def := range g.ast.Defs {
		switch x := def.(type) {
		case *DataDef:
			if x.Type == nil {
				continue
			}

			if len(x.Items) != 2 || x.Type != b {
				continue
			}

			if first {
				g.w("\n")
				first = false
			}
			g.w("\n")
			done[x] = struct{}{}
			g.singleTypeData(x, nil)
		}
	}
	var globals []global
	for _, def := range g.ast.Defs {
		switch x := def.(type) {
		case *DataDef:
			if _, ok := done[x]; !ok {
				globals = append(globals, g.dataDef(x, globals)...)
			}
		}
	}
	if len(globals) == 0 {
		return
	}

	g.w("\n\n__attribute__ ((constructor)) static void __qbe_init() {")
	for _, v := range globals {
		switch {
		case v.isIndex:
			g.w("\n\t%s[%d] = (intptr_t)(&%s)", v.name.cname(), v.x, v.global.Name.cname())
			if v.global.Offset != 0 {
				g.w("+%d", v.global.Offset)
			}
			g.w(";")
		default:
			panic(todo("%v:", v.name.Position()))
		}
	}
	g.w("\n}")
}

func (g *cgen) dataDef(n *DataDef, globals []global) []global {
	g.w("\n\n")
	switch {
	case n.Type != nil:
		return g.singleTypeData(n, globals)
	default:
		return g.mixedTypeData(n, globals)
	}
}

func (g *cgen) mixedTypeData(n *DataDef, globals []global) (r []global) {
	if !n.IsExported {
		g.w("static ")
	}
	g.w("struct {")
	for i, v := range n.Items {
		switch x := v.(type) {
		case IntInitializer:
			g.w("\n\t%s\tf%d;", g.typ(nil, x.Type, true), i)
		case ZeroInitializer:
			g.w("\n\t%s\tf%d[%d];", g.typ(nil, b, true), i, IntLit(x).Value())
		case GlobalInitializer:
			g.w("\n\tvoid\t*f%d;", i)
		case LongDoubleLitInitializer:
			g.w("\n\t%s\tf%d;", g.typ(nil, x.Type, true), i)
		default:
			panic(todo("%v: %T", v.Position(), x))
		}
	}
	g.w("\n} %s%s", n.Name.cname(), g.aligned(n.Align))
loop:
	for _, v := range n.Items {
		switch v.(type) {
		case
			IntInitializer,
			GlobalInitializer,
			LongDoubleLitInitializer:

			g.w(" = {")
			for i, v := range n.Items {
				switch x := v.(type) {
				case IntInitializer:
					g.w("\n\t.f%d = ", i)
					g.operand(nil, x.IntLit, x.Type, true)
					g.w(",")
				case ZeroInitializer:
					// nop
				case GlobalInitializer:
					switch {
					case x.Offset != 0:
						g.w("\n\t.f%d = (void*)((char*)(&%s)+%d),", i, x.Global.cname(), x.Offset)
					default:
						g.w("\n\t.f%d = &%s,", i, x.Global.cname())
					}
				case LongDoubleLitInitializer:
					g.w("\n\t.f%d = ", i)
					g.operand(nil, x.LongDoubleLit, x.Type, true)
					g.w(",")
				default:
					panic(todo("%v: %T", v.Position(), x))
				}
			}
			g.w("\n}")
			break loop
		}
	}
	g.w(";")
	return r
}

func (g *cgen) aligned(n uintptr) string {
	if n > 1 {
		return fmt.Sprintf(" __attribute__((aligned(%d)))", n)
	}

	return ""
}

func (g *cgen) singleTypeData(n *DataDef, globals []global) (r []global) {
	if !n.IsExported {
		g.w("static ")
	}
	if len(n.Items) == 2 && n.Type == b {
		if s, ok := n.Items[0].(StringInitializer); ok {
			ok = false
			switch x := n.Items[1].(type) {
			case ZeroInitializer:
				ok = true
			case IntInitializer:
				ok = x.IntLit.Value() == 0
			}
			if ok {
				switch {
				case n.IsReadOnly:
					g.w("char *%s = %s;", n.cname(), safeQuoteToASCII(string(s.Value())))
				default:
					g.w("char %s[] = %s;", n.cname(), safeQuoteToASCII(string(s.Value())))
				}
				return
			}
		}
	}

	g.w("%s %s[%d]%s", g.typ(nil, n.Type, true), n.cname(), n.Size/n.Type.size(g.ast.abi), g.aligned(n.Align))
	for _, v := range n.Attributes {
		g.w(" __attribute__((%s))", v)
	}
loop:
	for _, v := range n.Items {
		switch v.(type) {
		case
			IntInitializer,
			StringInitializer,
			GlobalInitializer,
			LongDoubleLitInitializer:

			g.w(" = {")
			var ix uintptr
			for _, v := range n.Items {
				switch x := v.(type) {
				case StringInitializer:
					val := x.Value()
					for _, v := range val {
						g.w("\n\t[%d] = %d,\t// %#U", ix, int8(v), v)
						ix++
					}
				case ZeroInitializer:
					val := IntLit(x).Value()
					ix += uintptr(val)
				case IntInitializer:
					g.w("\n\t[%d] = ", ix)
					ix++
					g.operand(nil, x.IntLit, x.Type, true)
					g.w(",")
				case GlobalInitializer:
					r = append(r, global{name: n.Name, global: x, x: ix, isIndex: true})
					ix++
				case LongDoubleLitInitializer:
					g.w("\n\t[%d] = ", ix)
					ix++
					g.operand(nil, x.LongDoubleLit, x.Type, true)
					g.w(",")
				default:
					panic(todo("%v: %T", v.Position(), x))
				}
			}
			g.w("\n}")
			break loop
		}
	}
	g.w(";")
	return r
}

func longDoubleCString(s string) string {
	switch s {
	case "nan":
		panic(todo(""))
	case "inf":
		panic(todo(""))
	case "-inf":
		panic(todo(""))
	default:
		return s + "L"
	}
}

var mainCName = []byte("main")

type ctx struct {
	f         *FuncDef
	types     map[string]int
	globalIDs map[string]uint32
}

func (ctx *ctx) isMain() bool {
	return ctx.f != nil && ctx.f.IsExported && bytes.Equal(ctx.f.Name.cname(), mainCName)
}

func (ctx *ctx) globalID(n Global) uint32 {
	if ctx.globalIDs == nil {
		ctx.globalIDs = map[string]uint32{}
	}
	nm := n.cname()
	if id, ok := ctx.globalIDs[string(nm)]; ok {
		return id
	}

	id := uint32(len(ctx.globalIDs))
	ctx.globalIDs[string(nm)] = id
	return id
}

func (g *cgen) funcForwardDefs(ctx *ctx) {
	g.w("\n")
	for _, def := range g.ast.Defs {
		switch x := def.(type) {
		case *FuncDef:
			ctx.f = x
			g.w("\n")
			g.funcSignature(ctx, x)
			g.w(";")
		}
	}
	ctx.f = nil
}

func (g *cgen) funcDef(ctx *ctx, n *FuncDef) {
	ctx.f = n
	g.w("\n\n")
	g.funcSignature(ctx, n)
	g.w(" {")
	var locals []*LocalInfo
	for _, v := range n.Scope.Nodes {
		if x, ok := v.(*LocalInfo); ok {
			if x.IsParameter {
				continue
			}

			locals = append(locals, x)
		}
	}
	sort.Slice(locals, func(a, b int) bool { return locals[a].N < locals[b].N })
	for _, v := range locals {
		g.w("\n\t%s __v%d; // %s", g.typ(ctx, v.Type, true), v.N, v.Name.Name())
	}
	labels := make(map[*Block]int, len(n.Blocks))
	for _, v := range n.Blocks {
		labels[v] = len(labels) + 1
	}
	produced := make(map[*Block]struct{}, len(n.Blocks))
	graph := n.NewCFG()
	g.block(ctx, graph, produced, labels, true)
	g.w("\n}")
	//TODO- f := newFunction(ctx, n, nil)        //TODO-
	//TODO- vm := newVM()                        //TODO-
	//TODO- budget := 1 << 16                    //TODO-
	//TODO- v, ok := vm.runFunc(&budget, f, nil) //TODO-
	//TODO- trc("%T(%[1]v), ok %v", v, ok)
}

func (g *cgen) funcSignature(ctx *ctx, n *FuncDef) {
	// trc("%v: %s, IsExported: %v", n.Position(), n.Name.Src(), n.IsExported)
	if !n.IsExported {
		g.w("static ")
	}
	isMain := ctx.isMain()
	pos := n.Position()
	g.w("\n#line %d %q\n", pos.Line, pos.Filename)
	for _, v := range n.Attributes {
		g.w("__attribute__((%s)) ", v)
	}
	switch {
	case n.Result == nil && isMain:
		g.w("int ")
	case n.Result == nil:
		g.w("void ")
	default:
		g.w("%s ", g.typ(ctx, n.Result, true))
	}
	g.w("\n%s(", n.Name.cname())
	for i, v := range n.Params {
		switch x := v.(type) {
		case *RegularParameter:
			info := n.Scope.node(x.Name).(*LocalInfo)
			if y, ok := info.Type.(TypeName); ok {
				if string(y.Name.Src()) == ":"+VaList {
					g.w("va_list __v%d", info.N)
					break
				}
			}

			switch {
			case isMain && i == 1:
				g.w("char** __v%d", info.N)
			default:
				g.w("%s __v%d", g.typ(ctx, x.Type, true), info.N)
			}
		default:
			panic(todo("%v: %T", v.Position(), x))
		}
		if i != len(n.Params)-1 {
			g.w(", ")
		}
	}
	if n.IsVariadic {
		g.w(", ...")
	}
	g.w(")")
}

func (g *cgen) typ(ctx *ctx, t Type, signed bool) string {
	switch x := t.(type) {
	case Int32:
		switch {
		case signed:
			return "int32_t"
		default:
			return "uint32_t"
		}
	case Int64:
		switch {
		case signed:
			return "int64_t"
		default:
			return "uint64_t"
		}
	case Int16:
		switch {
		case signed:
			return "int16_t"
		default:
			return "uint16_t"
		}
	case Int8:
		switch {
		case signed:
			return "int8_t"
		default:
			return "uint8_t"
		}
	case CharPointer:
		return "char*"
	case VoidPointer:
		return "void*"
	case Float64:
		return "double"
	case Float32:
		return "float"
	case LongDouble:
		return "long double"
	case TypeName:
		s := string(x.Name.Name())
		if id, ok := ctx.types[string(x.Name.Name())]; ok {
			return fmt.Sprintf("%s%d", typePrefix, id)
		}

		switch s[1:] {
		case VaList:
			return "va_list"
		case VaListPtr:
			return "*va_list"
		default:
			panic(todo(""))
		}
	case nil:
		return "void"
	default:
		panic(todo("%T", x))
	}
}

func (g *cgen) block(ctx *ctx, n *CFGNode, produced map[*Block]struct{}, labels map[*Block]int, isEntryBlock bool) {
	block := n.Block
	if _, ok := produced[block]; ok {
		return
	}

	produced[block] = struct{}{}
	g.w("\n")
	if !isEntryBlock {
		g.w("l%d: ", labels[block])
	}
	g.w("// %s", block.Name.Name())
	for _, inst := range block.Insts {
		g.inst(ctx, inst, isEntryBlock)
	}
	switch x := block.Jump.(type) {
	case Jmp, nil:
		g.setPhis(ctx, n, n.Out1, "")
		switch _, isProduced := produced[n.Out1.Block]; {
		case isProduced:
			g.w("\n\tgoto l%d;", labels[n.Out1.Block])
		default:
			g.block(ctx, n.Out1, produced, labels, false)
		}
	case Jnz:
		_, nzProduced := produced[n.Out1.Block]
		_, zProduced := produced[n.Out2.Block]
		switch {
		case nzProduced && zProduced:
			g.w("\n\tif (")
			g.operand(ctx, x.Value, nil, false)
			g.w(") {")
			g.setPhis(ctx, n, n.Out1, "\t")
			g.w("\n\t\tgoto l%d;\n\t} else {", labels[n.Out1.Block])
			g.setPhis(ctx, n, n.Out2, "\t")
			g.w("\n\t\tgoto l%d;\n\t}", labels[n.Out2.Block])
		case nzProduced && !zProduced:
			g.w("\n\tif (")
			g.operand(ctx, x.Value, nil, false)
			g.w(") {")
			g.setPhis(ctx, n, n.Out1, "\t")
			g.w("\n\t\tgoto l%d;\n\t}", labels[n.Out1.Block])
			g.setPhis(ctx, n, n.Out2, "")
			g.block(ctx, n.Out2, produced, labels, false)
		case !nzProduced && zProduced:
			g.w("\n\tif (!")
			g.operand(ctx, x.Value, nil, false)
			g.w(") {")
			g.setPhis(ctx, n, n.Out2, "\t")
			g.w("\n\t\tgoto l%d;\n\t}", labels[n.Out2.Block])
			g.setPhis(ctx, n, n.Out1, "")
			g.block(ctx, n.Out1, produced, labels, false)
		case !nzProduced && !zProduced:
			g.w("\n\tif (")
			g.operand(ctx, x.Value, nil, false)
			g.w(") {")
			g.setPhis(ctx, n, n.Out1, "\t")
			g.w("\n\t\tgoto l%d;\n\t}", labels[n.Out1.Block])
			g.setPhis(ctx, n, n.Out2, "")
			g.block(ctx, n.Out2, produced, labels, false)
			g.block(ctx, n.Out1, produced, labels, false)
		}
	case Ret:
		g.w("\n\treturn")
		switch {
		case x.Value != nil:
			g.w(" ")
			switch y := ctx.f.Result.(type) {
			case TypeName:
				switch z := x.Value.(type) {
				case Global:
					g.w("*((%s*)(&%s))", g.typ(ctx, ctx.f.Result, false), z.cname())
				case Local:
					info := ctx.f.Scope.node(z.Name).(*LocalInfo)
					if info.IsParameter {
						g.w("__v%d", info.N)
						break
					}

					switch {
					case info.IsStruct:
						g.w("__v%d", info.N)
					default:
						g.w("*((%s*)(__v%d))", g.typ(ctx, ctx.f.Result, false), info.N)
					}
				default:
					panic(todo("%v: %T %T", x.Position(), y, z))
				}
			default:
				g.operand(ctx, x.Value, ctx.f.Result, true)
			}
		case ctx.isMain():
			g.w(" 0")
		}
		g.w(";")
	default:
		panic(todo("%v: %T", block.Position(), x))
	}
}

func (g *cgen) setPhis(ctx *ctx, n, m *CFGNode, indent string) {
	for _, phi := range m.Block.Phis {
		g.setPhi(ctx, phi, n, indent)
	}
}

func (g *cgen) setPhi(ctx *ctx, n *Phi, from *CFGNode, indent string) {
	nm := from.Name.Name()
	for _, arg := range n.Args {
		if in := arg.Name.Name(); bytes.Equal(nm, in) {
			g.w("\n\t%s", indent)
			g.local(ctx, n.Dst)
			g.w(" = ")
			g.operand(ctx, arg.Value, n.DstType, true)
			g.w(";")
		}
	}
}

func (g *cgen) inst(ctx *ctx, inst Node, isEntryBlock bool) {
	pos := inst.Position()
	var t Type
	var rparen string
	indent := true
	switch inst.(type) {
	case *Alloc4, *Alloc8, *Alloc16:
		// nop
	case *Call, *VaArg:
		x := inst.(definer)
		g.w("\n#line %d %q\n\t", pos.Line, pos.Filename)
		indent = false
		t = x.preamble().DstType
		if _, ok := t.(TypeName); ok {
			info := ctx.f.Scope.node(x.dst().Name).(*LocalInfo)
			if _, ok := info.Type.(TypeName); !ok {
				g.w("*((%s*)__v%d) = ", g.typ(ctx, t, false), info.N)
				break
			}
		}

		g.local(ctx, x.dst())
		g.w(" = ")
	case *Copy:
		x := inst.(definer)
		t = x.preamble().DstType
		info := ctx.f.Scope.node(x.dst().Name).(*LocalInfo)
		g.w("\n#line %d %q\n\t", pos.Line, pos.Filename)
		indent = false
		g.local(ctx, x.dst())
		g.w(" = ")
		if g.isPtr(info.Type) {
			g.w("(void*)(")
			rparen = ")"
		}
	case *Declare:
		return
	default:
		if x, ok := inst.(definer); ok {
			g.w("\n#line %d %q\n\t", pos.Line, pos.Filename)
			indent = false
			t = x.preamble().DstType
			g.local(ctx, x.dst())
			g.w(" = ")
			info := ctx.f.Scope.node(x.dst().Name).(*LocalInfo)
			if g.isPtr(info.Type) {
				g.w("(void*)(")
				rparen = ")"
			}
		}
	}
	if indent {
		g.w("\n#line %d %q\n\t", pos.Line, pos.Filename)
	}
	switch x := inst.(type) {
	case *Copy:
		g.operand(ctx, x.Value, t, true)
	case *Sub:
		g.binary(ctx, x, "-", t, false)
	case *Add:
		g.binary(ctx, x, "+", t, false)
	case *And:
		g.binary(ctx, x, "&", t, false)
	case *Xor:
		g.binary(ctx, x, "^", t, false)
	case *Or:
		g.binary(ctx, x, "|", t, false)
	case *Mul:
		g.binary(ctx, x, "*", t, false)
	case *Div:
		g.binary(ctx, x, "/", t, true)
	case *Udiv:
		g.binary(ctx, x, "/", t, false)
	case *Rem:
		g.binary(ctx, x, "%", t, true)
	case *Urem:
		g.binary(ctx, x, "%", t, false)
	case *Alloc4:
		g.alloc(ctx, 4, &x.InstPreamble, x.Value, isEntryBlock)
	case *Alloc8:
		g.alloc(ctx, 8, &x.InstPreamble, x.Value, isEntryBlock)
	case *Alloc16:
		g.alloc(ctx, 16, &x.InstPreamble, x.Value, isEntryBlock)
	case *Call:
		g.call(ctx, &x.VoidCall, t)
	case *VoidCall:
		g.call(ctx, x, nil)
	case *Loadd:
		g.load(ctx, x.Value, d, false)
	case *Loadld:
		g.load(ctx, x.Value, ld, false)
	case *Loads:
		g.load(ctx, x.Value, s, false)
	case *Loadsb:
		g.load(ctx, x.Value, b, true)
	case *Loadsh:
		g.load(ctx, x.Value, h, true)
	case *Loadsw:
		g.load(ctx, x.Value, w, true)
	case *Loadl:
		g.load(ctx, x.Value, l, true)
	case *Loadub:
		g.load(ctx, x.Value, b, false)
	case *Loaduh:
		g.load(ctx, x.Value, h, false)
	case *Loaduw:
		g.load(ctx, x.Value, w, false)
	case *Storel:
		g.store(ctx, x.Dst, x.Src, l)
	case *Storeb:
		g.store(ctx, x.Dst, x.Src, b)
	case *Storeh:
		g.store(ctx, x.Dst, x.Src, h)
	case *Storew:
		g.store(ctx, x.Dst, x.Src, w)
	case *Stored:
		g.store(ctx, x.Dst, x.Src, d)
	case *Storeld:
		g.store(ctx, x.Dst, x.Src, ld)
	case *Stores:
		g.store(ctx, x.Dst, x.Src, s)
	case *Ceqd:
		g.binary(ctx, x, "==", d, true)
	case *Ceqld:
		g.binary(ctx, x, "==", ld, true)
	case *Ceql:
		g.binary(ctx, x, "==", l, true)
	case *Ceqs:
		g.binary(ctx, x, "==", s, false)
	case *Ceqw:
		g.binary(ctx, x, "==", w, false)
	case *Cged:
		g.binary(ctx, x, ">=", d, true)
	case *Cgeld:
		g.binary(ctx, x, ">=", ld, true)
	case *Cges:
		g.binary(ctx, x, ">=", s, false)
	case *Cgtd:
		g.binary(ctx, x, ">", d, true)
	case *Cgtld:
		g.binary(ctx, x, ">", ld, true)
	case *Cgts:
		g.binary(ctx, x, ">", s, false)
	case *Cled:
		g.binary(ctx, x, "<=", d, true)
	case *Cleld:
		g.binary(ctx, x, "<=", ld, true)
	case *Cles:
		g.binary(ctx, x, "<=", s, false)
	case *Cltd:
		g.binary(ctx, x, "<", d, false)
	case *Cltld:
		g.binary(ctx, x, "<", ld, false)
	case *Clts:
		g.binary(ctx, x, "<", s, false)
	case *Cned:
		g.binary(ctx, x, "!=", d, false)
	case *Cneld:
		g.binary(ctx, x, "!=", ld, false)
	case *Cnel:
		g.binary(ctx, x, "!=", l, false)
	case *Cnes:
		g.binary(ctx, x, "!=", s, false)
	case *Cnew:
		g.binary(ctx, x, "!=", w, false)
	case *Csgel:
		g.binary(ctx, x, ">=", l, true)
	case *Csgew:
		g.binary(ctx, x, ">=", w, true)
	case *Csgtl:
		g.binary(ctx, x, ">", l, true)
	case *Csgtw:
		g.binary(ctx, x, ">", w, true)
	case *Cslel:
		g.binary(ctx, x, "<=", l, true)
	case *Cslew:
		g.binary(ctx, x, "<=", w, true)
	case *Csltl:
		g.binary(ctx, x, "<", l, true)
	case *Csltw:
		g.binary(ctx, x, "<", w, true)
	case *Cugel:
		g.binary(ctx, x, ">=", l, false)
	case *Cugew:
		g.binary(ctx, x, ">=", w, false)
	case *Cugtl:
		g.binary(ctx, x, ">", l, false)
	case *Cugtw:
		g.binary(ctx, x, ">", w, false)
	case *Culel:
		g.binary(ctx, x, "<=", l, false)
	case *Culew:
		g.binary(ctx, x, "<=", w, false)
	case *Cultl:
		g.binary(ctx, x, "<", l, false)
	case *Cultw:
		g.binary(ctx, x, "<", w, false)
	case *Cuod:
		panic(todo(""))
	case *Cuos:
		panic(todo(""))
	case *Cod:
		panic(todo(""))
	case *Cos:
		panic(todo(""))
	case *Exts:
		g.ext(ctx, x.Value, t, s, false)
	case *Extd:
		g.ext(ctx, x.Value, t, d, false)
	case *Extsb:
		g.ext(ctx, x.Value, t, b, true)
	case *Extsh:
		g.ext(ctx, x.Value, t, h, true)
	case *Extsw:
		g.ext(ctx, x.Value, t, w, true)
	case *Extub:
		g.ext(ctx, x.Value, t, b, false)
	case *Extuh:
		g.ext(ctx, x.Value, t, h, false)
	case *Extuw:
		g.ext(ctx, x.Value, t, w, false)
	case *Sar:
		g.operand(ctx, x.A, t, true)
		g.w(" >> ")
		g.operand(ctx, x.B, nil, false)
	case *Shr:
		g.operand(ctx, x.A, t, false)
		g.w(" >> ")
		g.operand(ctx, x.B, nil, false)
	case *Shl:
		g.operand(ctx, x.A, t, false)
		g.w(" << ")
		g.operand(ctx, x.B, nil, false)
	case *Swtof:
		g.w("(%s)(", g.typ(ctx, t, false))
		g.operand(ctx, x.Value, w, true)
		g.w(")")
	case *Uwtof:
		g.w("(%s)(", g.typ(ctx, t, false))
		g.operand(ctx, x.Value, w, false)
		g.w(")")
	case *Sltof:
		g.w("(%s)(", g.typ(ctx, t, false))
		g.operand(ctx, x.Value, l, true)
		g.w(")")
	case *Ultof:
		g.w("(%s)(", g.typ(ctx, t, false))
		g.operand(ctx, x.Value, l, false)
		g.w(")")
	case *Stosi:
		g.w("(%s)(", g.typ(ctx, t, true))
		g.operand(ctx, x.Value, s, true)
		g.w(")")
	case *Stoui:
		g.w("(%s)(", g.typ(ctx, t, false))
		g.operand(ctx, x.Value, s, false)
		g.w(")")
	case *Dtosi:
		g.w("(%s)(", g.typ(ctx, t, true))
		g.operand(ctx, x.Value, d, true)
		g.w(")")
	case *Ldtosi:
		g.w("(%s)(", g.typ(ctx, t, true))
		g.operand(ctx, x.Value, d, true)
		g.w(")")
	case *Dtoui:
		g.w("(%s)(", g.typ(ctx, t, false))
		g.operand(ctx, x.Value, d, false)
		g.w(")")
	case *Ldtoui:
		g.w("(%s)(", g.typ(ctx, t, false))
		g.operand(ctx, x.Value, d, false)
		g.w(")")
	case *Cast:
		g.cast(ctx, x, t)
	case *Truncd:
		g.w("(float)(")
		g.operand(ctx, x.Value, d, true)
		g.w(")")
	case *Truncld:
		g.w("(%s)(", g.typ(ctx, t, true))
		g.operand(ctx, x.Value, ld, true)
		g.w(")")
	case *VaStart:
		if !ctx.f.IsVariadic {
			g.err(x.off, 0, "not a variadic function: %s", ctx.f.Name.Name())
			break
		}

		params := ctx.f.Params
		last := params[len(params)-1]
		lastInfo := ctx.f.Scope.node(last.(*RegularParameter).Name).(*LocalInfo)
		switch k, x := g.vaListType(ctx, x.Value); k {
		case vaListLocalPtr:
			g.w("va_start(*(va_list*)(__v%d), __v%d)", x.(*LocalInfo).N, lastInfo.N)
		case vaListLocal:
			g.w("va_start(__v%d, __v%d)", x.(*LocalInfo).N, lastInfo.N)
		case vaListGlobal:
			g.w("va_start(*(va_list*)(&%s), __v%d)", x.(Global).cname(), lastInfo.N)
		default:
			panic(todo("%v: %v %T", inst.Position(), k, x))
		}

		//TODO- params := ctx.f.Params
		//TODO- last := params[len(params)-1]
		//TODO- lastInfo := ctx.f.Scope.node(last.(*RegularParameter).Name).(*LocalInfo)
		//TODO- switch y := x.Value.(type) {
		//TODO- case Local:
		//TODO- 	switch info := ctx.f.Scope.node(y.Name).(*LocalInfo); {
		//TODO- 	case info.IsVaList:
		//TODO- 		g.w("va_start(__v%d, __v%d)", info.N, lastInfo.N)
		//TODO- 	case info.IsVaListPtr:
		//TODO- 		g.w("va_start(*(va_list*)(__v%d), __v%d)", info.N, lastInfo.N)
		//TODO- 	default:
		//TODO- 		panic(todo(""))
		//TODO- 	}
		//TODO- default:
		//TODO- 	panic(todo("%v: %T", x.Position(), y))
		//TODO- }
	case *VaArg:
		switch k, x := g.vaListType(ctx, x.Value); k {
		case vaListLocalPtr:
			g.w("va_arg(*(va_list*)(__v%d), %s)", x.(*LocalInfo).N, g.typ(ctx, t, true))
		case vaListLocal:
			g.w("va_arg(__v%d, %s)", x.(*LocalInfo).N, g.typ(ctx, t, true))
		case vaListGlobal:
			g.w("va_arg(*(va_list*)(&%s), %s)", x.(Global).cname(), g.typ(ctx, t, true))
		default:
			panic(todo("%v: %v %T", inst.Position(), k, x))
		}

		//TODO- switch y := x.Value.(type) {
		//TODO- case Local:
		//TODO- 	switch info := ctx.f.Scope.node(y.Name).(*LocalInfo); {
		//TODO- 	case info.IsVaList:
		//TODO- 		g.w("va_arg(__v%d, %s)", info.N, g.typ(ctx, t, true))
		//TODO- 	case info.IsVaListPtr:
		//TODO- 		g.w("va_arg(*(va_list*)(__v%d), %s)", info.N, g.typ(ctx, t, true))
		//TODO- 	default:
		//TODO- 		panic(todo(""))
		//TODO- 	}
		//TODO- case Global:
		//TODO- 	panic(todo("%v: %T", x.Position(), y))
		//TODO- default:
		//TODO- 	panic(todo("%v: %T", x.Position(), y))
		//TODO- }
	default:
		panic(todo("%v: %T", inst.Position(), x))
	}
	g.w("%s;", rparen)
}

const (
	vaListLocalPtr = iota
	vaListLocal
	vaListGlobal
)

func (g *cgen) vaListType(ctx *ctx, n Node) (int, interface{}) {
	switch x := n.(type) {
	case Local:
		info := ctx.f.Scope.node(x.Name).(*LocalInfo)
		switch y := info.Type.(type) {
		case Int32, Int64:
			return vaListLocalPtr, info
		case TypeName:
			switch string(y.Src()[1:]) {
			case VaList:
				if info.IsParameter {
					return vaListLocal, info
				}

				panic(todo("%v: %q", n.Position(), string(y.Src()[1:])))
			default:
				panic(todo("%v: %q", n.Position(), string(y.Src()[1:])))
			}
		case VoidPointer:
			return vaListLocalPtr, info
		default:
			panic(todo("%v: %T", n.Position(), y))
		}
	case Global:
		return vaListGlobal, x
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
}

func (g *cgen) isPtr(t Type) (r bool) {
	switch t.(type) {
	case CharPointer, VoidPointer:
		return true
	}

	return false
}

func (g *cgen) cast(ctx *ctx, n *Cast, t Type) {
	dst := ctx.f.Scope.node(n.Dst.Name).(*LocalInfo)
	switch x := n.Value.(type) {
	case Local:
		src := ctx.f.Scope.node(x.Name).(*LocalInfo)
		switch y := src.Type.(type) {
		case Float64:
			switch z := dst.Type.(type) {
			case Int64:
				g.w("(union {double d; int64_t i;}){__v%d}.i", src.N)
			default:
				panic(todo("%v: %T", n.Position(), z))
			}
		case Float32:
			switch z := dst.Type.(type) {
			case Int32:
				g.w("(union {float f; int32_t i;}){__v%d}.i", src.N)
			default:
				panic(todo("%v: %T", n.Position(), z))
			}
		case Int64:
			switch z := dst.Type.(type) {
			case Float64:
				g.w("(union {int64_t i; double d;}){__v%d}.d", src.N)
			default:
				panic(todo("%v: %T", n.Position(), z))
			}
		case Int32:
			switch z := dst.Type.(type) {
			case Float32:
				g.w("(union {int32_t i; float f;}){__v%d}.f", src.N)
			default:
				panic(todo("%v: %T", n.Position(), z))
			}
		default:
			panic(todo("%v: %T", n.Position(), y))
		}
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
}

func (g *cgen) ext(ctx *ctx, val Node, to, t Type, signed bool) {
	g.w("(%s)(", g.typ(ctx, to, true))
	g.operand(ctx, val, t, signed)
	g.w(")")
}

func (g *cgen) alloc(ctx *ctx, n int, pre *InstPreamble, val Node, isEntryBlock bool) {
	info := ctx.f.Scope.node(pre.Dst.Name).(*LocalInfo)
	switch {
	case isEntryBlock && info.Written == 1 && g.isNumericConstant(val):
		g.w("int8_t m")
		g.w("__v%d[", info.N)
		g.operand(ctx, val, nil, true)
		g.w("] __attribute__ ((aligned (%d)));", n)
		g.w("\n\t")
		g.local(ctx, pre.Dst)
		g.w(" = (intptr_t)&m")
		g.local(ctx, pre.Dst)
	default:
		g.local(ctx, pre.Dst)
		g.w(" = (intptr_t)alloca((size_t)(")
		g.operand(ctx, val, nil, true)
		g.w(" + %d));", n-1)
		g.w("\n\t")
		g.local(ctx, pre.Dst)
		g.w(" = ")
		g.local(ctx, pre.Dst)
		g.w("&%d ? ", n-1)
		g.local(ctx, pre.Dst)
		g.w(" + %d - (", n)
		g.local(ctx, pre.Dst)
		g.w("&%d) : ", n-1)
		g.local(ctx, pre.Dst)
	}
}

func (g *cgen) isNumericConstant(n Node) bool {
	switch n.(type) {
	case IntLit, Float32Lit, Float64Lit, LongDoubleLit:
		return true
	default:
		panic(todo("%v: %T", n.Position(), n))
	}
}

func (g *cgen) store(ctx *ctx, dst, src Node, t Type) {
	if local, ok := src.(Local); ok {
		info := ctx.f.Scope.node(local.Name).(*LocalInfo)
		if _, ok := info.Type.(TypeName); ok {
			g.w("*(intptr_t*)(")
			g.operand(ctx, dst, nil, true)
			g.w(") = ")
			g.operand(ctx, src, t, true)
			return
		}
	}

	g.w("*(%s*)((intptr_t)", g.typ(ctx, t, true))
	g.operand(ctx, dst, nil, true)
	g.w(") = ")
	g.operand(ctx, src, t, true)
}

func (g *cgen) load(ctx *ctx, mem Node, t Type, signed bool) {
	if local, ok := mem.(Local); ok {
		info := ctx.f.Scope.node(local.Name).(*LocalInfo)
		if _, ok := info.Type.(TypeName); ok {
			g.w("*(%s*)(&__v%d)", g.typ(ctx, t, false), info.N)
			return
		}
	}

	g.w("*(%s*)((intptr_t)", g.typ(ctx, t, signed))
	g.operand(ctx, mem, nil, false)
	g.w(")")
}

func (g *cgen) call(ctx *ctx, n *VoidCall, t Type) {
	args := g.callArgs(ctx, n)
	switch x := n.Value.(type) {
	case Global:
		nm := x.cname()
		s := string(nm)
		if g.os == "windows" && (s == "alloca" || s == "_alloca") {
			s = "__builtin_alloca"
		}
		if _, ok := g.declaredExternPrototypes[s]; ok {
			g.w("%s", nm)
			break
		}

		if strings.HasPrefix(s, "__builtin_") {
			g.w("%s", nm)
			break
		}

		if s == "__qbe_va_end" {
			g.w("va_end")
			break
		}

		if s == "__qbe_va_copy" {
			g.w("va_copy")
			break
		}

		if rt, ok := g.ast.ExternFuncs[s]; ok {
			g.w("((%s(*)(%s))(%s))", g.typ(ctx, rt, true), args, nm)
			break
		}

		if _, ok := g.ast.Funcs[s]; ok {
			g.w("%s", nm)
			break
		}

		g.w("((%s(*)(%s))(%s))", g.typ(ctx, t, true), args, nm)
	case Local:
		info := ctx.f.Scope.node(x.Name).(*LocalInfo)
		g.w("((%s(*)(%s))(__v%d))", g.typ(ctx, t, false), args, info.N)
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
	g.w("(")
	for i, arg := range n.Args {
		switch x := arg.(type) {
		case *RegularArg:
			switch y := x.Type.(type) {
			case TypeName:
				var isValist, isValistPtr bool
				switch y.String()[1:] {
				case VaList:
					isValist = true
				case VaListPtr:
					isValistPtr = true
				}
				switch z := x.Value.(type) {
				case Global:
					switch {
					case isValist:
						g.w("*(va_list*)(&%s)", z.cname())
					case isValistPtr:
						panic(todo(""))
					default:
						g.w("*((%s*)(&%s))", g.typ(ctx, x.Type, false), z.cname())
					}
				case Local:
					info := ctx.f.Scope.node(z.Name).(*LocalInfo)
					switch {
					case isValist:
						g.w("__v%d", info.N)
					case isValistPtr:
						g.w("*(va_list*)(__v%d)", info.N)
					default:
						if id, ok := ctx.types[string(y.Name.Name())]; ok {
							switch info.Type.(type) {
							case TypeName:
								g.w("__v%d", info.N)
							default:
								g.w("*((%s%d*)(__v%d))", typePrefix, id, info.N)
							}
							break
						}

						switch {
						case info.IsStruct:
							g.w("__v%d", info.N)
						case info.Type == c:
							panic(todo("", n.Position()))
						case g.isPtr(info.Type):
							g.w("(void*)(__v%d)", info.N)
						default:
							g.w("*((%s*)(__v%d))", g.typ(ctx, x.Type, false), info.N)
						}
					}
				default:
					panic(todo(""))
				}
			default:
				g.operand(ctx, x.Value, x.Type, true)
			}
		default:
			panic(todo("%v: %T", arg.Position(), x))
		}
		if i != len(n.Args)-1 {
			g.w(", ")
		}
	}
	g.w(")")
}

func (g *cgen) callArgs(ctx *ctx, n *VoidCall) string {
	if len(n.Args) == 0 {
		return "void"
	}

	var a []string
	for _, v := range n.Args {
		switch x := v.(type) {
		case *RegularArg:
			a = append(a, g.typ(ctx, x.Type, true))
		default:
			panic(todo("%v: %T", v.Position(), x))
		}
	}
	if n.IsVariadic {
		a = append(a, "...")
	}
	return strings.Join(a, ", ")
}

func (g *cgen) binary(ctx *ctx, n binaryOp, op string, t Type, signed bool) {
	a := g.operand(ctx, n.a(), t, signed)
	g.w(" %s ", op)
	b := g.operand(ctx, n.b(), t, signed)
	if a != nil && b != nil && !a.isCompatible(b) {
		g.err(n.off(), 0, "operand types don't match: %v, %v", a, b)
		return
	}
}

func (g *cgen) operand(ctx *ctx, n Node, t Type, signed bool) (localType Type) {
	switch x := n.(type) {
	case IntLit:
		v := x.Value()
		switch y := t.(type) {
		case Int32:
			switch {
			case signed:
				g.w("(int32_t)(%du)", uint32(v))
			default:
				g.w("%du", uint32(v))
			}
		case Int64:
			switch {
			case signed:
				g.w("(int64_t)(%dull)", v)
			default:
				g.w("%dull", v)
			}
		case Int16:
			switch {
			case signed:
				g.w("(int16_t)(%du)", uint16(v))
			default:
				g.w("(uint16_t)(%du)", uint16(v))
			}
		case Int8:
			switch {
			case signed:
				g.w("(int8_t)(%du)", uint8(v))
			default:
				g.w("(uint8_t)(%du)", uint8(v))
			}
		case CharPointer:
			g.w("(char*)(%du)", uint64(v))
		case VoidPointer:
			g.w("(void*)(%du)", uint64(v))
		case nil:
			g.w("%d", v)
		case Float64:
			g.doubleCString(math.Float64frombits(v))
		case Float32:
			g.floatCString(math.Float32frombits(uint32(v)))
		case LongDouble:
			g.w("%d", v)
		default:
			panic(todo("%v: %T %q", n.Position(), y, v))
		}
	case Local:
		nm := x.Name.Name()
		info := ctx.f.Scope.node(x.Name).(*LocalInfo)
		switch t.(type) {
		case TypeName:
			switch x := info.Type.(type) {
			case Int32, Int64:
				panic(todo("", n.Position()))
				g.w("*((%s*)(__v%d))", g.typ(ctx, t, false), info.N)
			case TypeName:
				panic(todo("", n.Position()))
				g.w("__v%d", info.N)
			default:
				panic(todo("%v: %T -> %T", n.Position(), x, t))
			}
			panic(todo("%v: %T -> %T", n.Position(), x, t))
			return
		}

		switch x := info.Type.(type) {
		case TypeName:
			switch y := t.(type) {
			case Int32, Int64:
				g.w("(intptr_t)(&__v%d)", info.N)
				return g.ptr
			case TypeName:
				panic(todo("", n.Position()))
				g.w("__v%d", info.N)
				return g.ptr
			case nil: // lhs
				g.w("(intptr_t)(&__v%d)", info.N)
				return g.ptr
			case VoidPointer, CharPointer:
				g.w("(void*)(&__v%d)", info.N)
				return g.ptr
			default:
				panic(todo("%v: %T -> %T", n.Position(), x, y))
			}
		}

		switch {
		case signed:
			switch {
			case t == nil:
				g.w("__v%d", info.N)
			case t != info.Type:
				g.w("(%s)(__v%d)", g.typ(ctx, t, true), info.N)
			default:
				g.w("__v%d", info.N)
			}
		default:
			switch {
			case t == nil:
				g.w("__v%d", info.N)
			case t != info.Type:
				g.w("(%s)(__v%d)", g.typ(ctx, t, false), info.N)
			default:
				g.w("(%s)(__v%d)", g.typ(ctx, info.Type, false), info.N)
			}
		}
		g.w(" /* %s */", nm)
		return info.Type
	case Global:
		switch {
		case t != nil:
			g.w("(%s)(&%s)", g.typ(ctx, t, true), x.cname())
		default:
			g.w("(&%s)", x.cname())
		}
	case Float64Lit:
		g.doubleCString(x.Value())
	case Float32Lit:
		g.floatCString(x.Value())
	case LongDoubleLit:
		g.w("%s", longDoubleCString(x.Value()))
	case StringLit:
		switch t.(type) {
		case Int32, Int64:
			g.w("(%s)", g.typ(ctx, t, false))
		case VoidPointer, CharPointer, nil:
			// ok
		default:
			panic(todo("%T", t))
		}
		g.w("%s", safeQuoteToASCII(string(x.Value())))
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
	return nil
}

func (g *cgen) doubleCString(v float64) {
	switch {
	case math.IsInf(v, -1):
		g.w("(-1.0/0.0)")
	case math.IsInf(v, 1):
		g.w("(1.0/0.0)")
	case math.IsNaN(v):
		g.w("(1.0/0.0 - 1.0/0.0)")
	default:
		g.w("%x", v)
	}
}

func (g *cgen) floatCString(v float32) {
	switch v64 := float64(v); {
	case math.IsInf(v64, -1):
		g.w("(-1.0f/0.0f)")
	case math.IsInf(v64, 1):
		g.w("(1.0f/0.0f)")
	case math.IsNaN(v64):
		g.w("(1.0f/0.0f - 1.0f/0.0f)")
	default:
		g.w("%x", v)
	}
}

func (g *cgen) local(ctx *ctx, n Local) {
	nm := n.Name.Name()
	info := ctx.f.Scope.node(n.Name).(*LocalInfo)
	g.w("__v%d /* %s */", info.N, nm)
}

func (g *cgen) types() (r map[string]int) {
	ctx := &ctx{}
	for _, def := range g.ast.Defs {
		switch x := def.(type) {
		case *TypeDef:
			if r == nil {
				r = make(map[string]int, len(g.ast.Defs))
				ctx.types = r
			}
			r[string(x.Name.Name())] = len(r) + 1
			if len(x.Fields) == 1 {
				if y, ok := x.Fields[0].(UnionField); ok {
					g.w("\n\ntypedef struct { int8_t f[%d]; } %s%d", IntLit(y).Value(), typePrefix, len(r))
					if x.Align != 0 {
						g.w(" __attribute__ ((aligned (%d)))", x.Align)
					}
					g.w(";")
					continue
				}
			}

			g.w("\n\ntypedef ")
			g.structType(ctx, x.Fields)
			g.w(" %s%d", typePrefix, len(r))
			if x.Align != 0 {
				g.w(" __attribute__ ((aligned (%d)))", x.Align)
			}
			g.w(";")
		}
	}
	return r
}

func (g *cgen) structType(ctx *ctx, fields []Node) {
	g.w("struct {")
	for i, v := range fields {
		switch x := v.(type) {
		case Field:
			g.w("\n\t%s f%d;", g.typ(ctx, x.Type, true), i)
		case StructField:
			g.w("\n\t")
			g.structType(ctx, x.Fields)
			g.w(" f%d;", i)
		case ArrayField:
			g.w("\n\t%s f%d[%d];", g.typ(ctx, x.Type, true), i, x.Len)
		default:
			panic(todo("%v: %T", v.Position(), x))
		}
	}
	g.w("\n}")
}
