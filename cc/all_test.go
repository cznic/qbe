// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//TODO Csmith

package cc // import "modernc.org/qbe/cc"

import (
	"bufio"
	"bytes"
	"context"
	"encoding/hex"
	"flag"
	"fmt"
	"go/build"
	"io"
	"io/ioutil"
	"math"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"runtime/debug"
	"sort"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/pmezard/go-difflib/difflib"
	"modernc.org/cc/v3"
	"modernc.org/ccgo/v3/lib"
	"modernc.org/ccorpus"
	"modernc.org/qbe"
)

var (
	fs                   = ccorpus.FileSystem()
	includePaths         []string
	initIncludePathsOnce sync.Once
	oBenchO              = flag.Int("BO", 3, "")
	oBestOf              = flag.Int("bestof", 5, "TestBenchmarksGame: best of N")
	oCSmith              = flag.Duration("csmith", time.Minute, "")
	oFF                  = flag.Bool("ff", false, "fail fast")
	oFullPaths           = flag.Bool("full-paths", false, "")
	oInstall             = flag.Bool("install", false, "install qbec and ecc binaries")
	oKeepTmp             = flag.Bool("keep-tmp", false, "")
	oMarch               = flag.String("march", "native", "")
	oMtune               = flag.String("mtune", "native", "")
	oO                   = flag.Int("O", 1, "")
	oOpenmp              = flag.Bool("fopenmp", false, "")
	oRE                  = flag.String("re", "", "")
	oTrc                 = flag.Bool("trc", false, "")
	oTrc2                = flag.Bool("trc2", false, "")
	oTrco                = flag.Bool("trco", false, "")
	overlayDir           string
	predefined           string
	re                   *regexp.Regexp
	sysIncludePaths      []string
	systemCC             string
	systemCCVersion      string
	tempDir              string
)

func init() {
	use(use)
}

func use(...interface{}) {}

func initIncludePaths(cpp string) error {
	var err error
	predefined, includePaths, sysIncludePaths, err = cc.HostConfig(cpp)
	if err != nil {
		return err
	}

	includePaths = append(includePaths, "@")
	includePaths = append(includePaths, sysIncludePaths...)
	return nil
}

func TestMain(m *testing.M) {
	fmt.Fprintf(os.Stderr, "qbe/cc: %v\n", os.Args)
	oGCC := flag.String("gcc", "", "")
	flag.BoolVar(&qbe.TraceC, "trcc", false, "")
	flag.BoolVar(&oTrcw, "trcw", false, "")
	flag.Parse()
	if s := *oRE; s != "" {
		re = regexp.MustCompile(s)
	}
	if *oGCC == "" {
		var err error
		initIncludePathsOnce.Do(func() { err = initIncludePaths("") })
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		if systemCC, err = exec.LookPath(env("QBEC_CC", env("CC", "gcc"))); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		fmt.Fprintf(os.Stderr, "QBEC_CC=%s\n", systemCC)
		out, err := exec.Command(systemCC, "--version").CombinedOutput()
		if err == nil {
			if a := strings.Split(string(out), "\n"); len(a) > 0 {
				systemCCVersion = a[0]
				fmt.Fprintf(os.Stderr, "%s\n", systemCCVersion)
			}
		}

		os.Exit(testMain(m))
	}

	var args []string
	for i, v := range os.Args {
		if v == "-gcc" {
			args = append(os.Args[:i], os.Args[i+2:]...)
		}
	}
	a := strings.Split(*oGCC, ",")
	rc := 0
	for _, suffix := range a {
		systemCC = fmt.Sprintf("gcc-%s", suffix)
		systemCPP := fmt.Sprintf("cpp-%s", suffix)
		var err error
		if systemCC, err = exec.LookPath(systemCC); err != nil {
			fmt.Fprintf(os.Stderr, "%s: %s\n", systemCC, err)
			continue
		}

		if systemCPP, err = exec.LookPath(systemCPP); err != nil {
			fmt.Fprintf(os.Stderr, "%s: %s\n", systemCPP, err)
			continue
		}

		os.Setenv("QBEC_CC", systemCC)
		os.Setenv("ECC_CPP", systemCPP)
		cmd := exec.Command(args[0], args[1:]...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			rc = 1
		}
	}
	os.Exit(rc)

}

func testMain(m *testing.M) int {
	var err error
	tempDir, err = ioutil.TempDir("", "qbe-cc-test-")
	if err != nil {
		panic(err) //TODOOK
	}

	switch {
	case *oKeepTmp:
		fmt.Fprintf(os.Stderr, "keeping temporary directory %s\n", tempDir)
	default:
		defer os.RemoveAll(tempDir)
	}

	s := filepath.FromSlash("testdata/overlay")
	if overlayDir, err = filepath.Abs(s); err != nil {
		panic(err) //TODOOK
	}

	return m.Run()
}

func mustEmptyDir(t *testing.T, s string) {
	if err := emptyDir(s); err != nil {
		t.Fatal(err)
	}
}

func emptyDir(s string) error {
	m, err := filepath.Glob(filepath.FromSlash(s + "/*"))
	if err != nil {
		return err
	}

	for _, v := range m {
		fi, err := os.Stat(v)
		if err != nil {
			return err
		}

		switch {
		case fi.IsDir():
			if err = os.RemoveAll(v); err != nil {
				return err
			}
		default:
			if err = os.Remove(v); err != nil {
				return err
			}
		}
	}
	return nil
}

type runResult struct {
	ccTime    time.Duration
	csmithSrc []byte
	eccTime   time.Duration
	err       error
	name      string
	out       []byte
}

type skipErr string

func (e skipErr) Error() string { return "skipped: " + string(e) }

type runTask struct {
	args      []string
	c         chan *runResult
	cmd       string
	csmithSrc []byte
	opts      []string
	src       string

	ccCanFail       bool
	doNotExec       bool
	hasBinaryOutput bool
}

func (t *runTask) run() {
	r := &runResult{name: t.src}
	r.out, r.err, r.ccTime, r.eccTime = t.run0()
	t.c <- r
}

func (t *runTask) runCSmith() {
	r := &runResult{name: t.src, csmithSrc: t.csmithSrc}
	r.out, r.err, r.ccTime, r.eccTime = t.run0CSmith()
	t.c <- r
}

func (t *runTask) run0() (_ []byte, err error, ccTime, eccTime time.Duration) {
	const outLimit = 1 << 16
	defer func() {
		if e := recover(); e != nil {
			switch {
			case err == nil:
				err = fmt.Errorf("PANIC: %v\n%s", e, debug.Stack())
			default:
				err = fmt.Errorf("%v\nPANIC: %v\n%s", err, e, debug.Stack())
			}
		}
	}()

	overlay := filepath.Join(overlayDir, t.src)
	b, err := ioutil.ReadFile(overlay)
	if err != nil {
		if !os.IsNotExist(err) {
			return nil, err, ccTime, eccTime
		}

		f, err := fs.Open(t.src)
		if err != nil {
			return nil, err, ccTime, eccTime
		}

		if b, err = ioutil.ReadAll(f); err != nil {
			return nil, err, ccTime, eccTime
		}

		if err = f.Close(); err != nil {
			return nil, err, ccTime, eccTime
		}
	}

	overlay = filepath.Join(overlayDir, t.src+".expectrc")
	b2, err := ioutil.ReadFile(overlay)
	if err != nil {
		f, err := fs.Open(t.src + ".expectrc")
		if err == nil {
			if b2, err = ioutil.ReadAll(f); err != nil {
				return nil, err, ccTime, eccTime
			}

			if err = f.Close(); err != nil {
				return nil, err, ccTime, eccTime
			}
		}
	}
	var expectRC int
	if len(b2) != 0 {
		s := strings.TrimSpace(string(b2))
		n, err := strconv.ParseUint(s, 10, 32)
		if err != nil {
			return nil, err, ccTime, eccTime
		}

		expectRC = int(n)
	}

	baseName := filepath.Base(t.src)
	if err := ioutil.WriteFile(baseName, b, 0600); err != nil {
		return nil, err, ccTime, eccTime
	}

	args, err := getArgs(t.src)
	if err != nil {
		return nil, err, ccTime, eccTime
	}

	ccArgs := append([]string{"-lm"}, t.opts...)
	ok := true
	for _, v := range t.opts {
		if strings.HasPrefix(v, "-O") {
			ok = false
			break
		}
	}
	if ok {
		if o := *oO; o >= 0 {
			ccArgs = append(ccArgs, fmt.Sprintf("-O%d", o))
		}
	}
	if t.doNotExec {
		ccArgs = append(ccArgs, "-c")
	}
	binary, err := makeCCBinary(baseName, t.doNotExec, ccArgs...)
	if err != nil {
		return nil, skipErr(err.Error()), ccTime, eccTime
	}

	const (
		ccOut  = "cc.out"
		eccOut = "ecc.out"
	)
	var binaryBytes, binaryBytes2 int
	var expected []byte
	if !t.doNotExec {
		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Minute)
		defer cancel()
		if t.cmd != "" {
			binary = t.cmd
		}
		if len(t.args) != 0 {
			args = t.args
		}
		t0 := time.Now()
		if *oTrc2 {
			fmt.Fprintf(os.Stderr, "%v: started CC binary for %s: %v %v\n", t0, baseName, binary, args)
		}
		switch {
		case t.hasBinaryOutput:
			binaryBytes, err = execute(ctx, binary, ccOut, args)
			defer os.Remove(ccOut)
		default:
			expected, err = exec.CommandContext(ctx, binary, args...).CombinedOutput()
			if len(expected) > outLimit {
				panic(todo("", t.src, len(expected)))
			}
		}
		ccTime = time.Since(t0)
		if *oTrc2 {
			switch {
			case t.hasBinaryOutput:
				fmt.Fprintf(os.Stderr, "%v: CC binary for %s returned: %v bytes, err %v\n", time.Now(), baseName, binaryBytes, err)
			default:
				fmt.Fprintf(os.Stderr, "%v: CC binary for %s returned: err %v\n%s\n", time.Now(), baseName, err, expected)
			}
		}
		if err != nil {
			switch {
			case t.ccCanFail:
				expected = nil
				expectRC = 0
			default:
				rc := err.(*exec.ExitError).ProcessState.ExitCode()
				if rc != expectRC {
					return nil, skipErr(fmt.Sprintf("executing CC binary %v %v: %v (rc %v, expected %v)\n%s", binary, args, err, rc, expectRC, expected)), ccTime, eccTime
				}

				err = nil
			}
		}

		if *oTrco {
			switch {
			case t.hasBinaryOutput:
				fmt.Fprintf(os.Stderr, "%v %q: %d bytes\n", ccTime, args, binaryBytes)
			default:
				fmt.Fprintf(os.Stderr, "%v %q: %s\n", ccTime, args, expected)
			}
		}
	}

	if t.cmd == "" {
		if err := os.Remove(binary); err != nil {
			return nil, fmt.Errorf("removing %v: %v", binary, err), ccTime, eccTime
		}
	}

	eccArgs := append([]string{"-lm"}, t.opts...)
	ok = true
	for _, v := range t.opts {
		if strings.HasPrefix(v, "-O") {
			ok = false
			break
		}
	}
	if ok {
		if o := *oO; o >= 0 {
			eccArgs = append(eccArgs, fmt.Sprintf("-O%d", o))
		}
	}
	if *oFullPaths {
		eccArgs = append(eccArgs, "-full-paths")
	}
	if *oKeepTmp {
		eccArgs = append(eccArgs, "-keep-tmp")
	}
	if t.doNotExec {
		eccArgs = append(ccArgs, "-c")
	}
	if binary, err = makeBinary(t.src, t.doNotExec, eccArgs...); err != nil {
		return nil, err, ccTime, eccTime
	}

	var got []byte
	if !t.doNotExec {
		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Minute)
		defer cancel()
		if t.cmd != "" {
			binary = t.cmd
		}
		if len(t.args) != 0 {
			args = t.args
		}
		t0 := time.Now()
		if *oTrc2 {
			fmt.Fprintf(os.Stderr, "%v: started ecc binary for %s: %v %v\n", t0, baseName, binary, args)
		}
		switch {
		case t.hasBinaryOutput:
			binaryBytes2, err = execute(ctx, binary, eccOut, args)
			defer os.Remove(eccOut)
		default:
			got, err = exec.CommandContext(ctx, binary, args...).CombinedOutput()
			if len(got) > outLimit {
				panic(todo("", t.src, len(expected)))
			}
		}
		eccTime = time.Since(t0)
		if *oTrc2 {
			switch {
			case t.hasBinaryOutput:
				fmt.Fprintf(os.Stderr, "%v: ecc binary for %s returned: %v bytes, err %v\n", time.Now(), baseName, binaryBytes2, err)
			default:
				fmt.Fprintf(os.Stderr, "%v: ecc binary for %s returned: err %v\n%s\n", time.Now(), baseName, err, got)
			}
		}
		if err != nil {
			rc := err.(*exec.ExitError).ProcessState.ExitCode()
			if rc != expectRC {
				return nil, fmt.Errorf("executing ecc binary %v %v: %v (rc %v, expected %v)\n%s", binary, args, err, rc, expectRC, got), ccTime, eccTime
			}

			err = nil
		}

		if *oTrco {
			switch {
			case t.hasBinaryOutput:
				fmt.Fprintf(os.Stderr, "%v %q: %d bytes\n", eccTime, args, binaryBytes2)
			default:
				fmt.Fprintf(os.Stderr, "%v %q: %s\n", eccTime, args, got)
			}
		}
		switch {
		case t.hasBinaryOutput:
			if err := fileEqual(eccOut, ccOut); err != nil {
				return nil, fmt.Errorf("binary output: %s", err), ccTime, eccTime
			}
		default:
			got := string(got)
			expected := string(expected)
			got = strings.ReplaceAll(got, "\r", "")
			got = lineTrim(strings.TrimSpace(got))
			expected = strings.ReplaceAll(expected, "\r", "")
			expected = lineTrim(strings.TrimSpace(expected))
			if got != expected {
				diff := difflib.UnifiedDiff{
					A:        difflib.SplitLines(expected),
					B:        difflib.SplitLines(got),
					FromFile: "expected",
					ToFile:   "got",
					Context:  3,
				}
				text, _ := difflib.GetUnifiedDiffString(diff)
				return nil, fmt.Errorf(
					"%v: text output differs:\n%s\n---- x.c\ngot\n%s\nexp\n%s\ngot\n%s\nexp\n%s",
					t.src, text,
					hex.Dump([]byte(got)), hex.Dump([]byte(expected)),
					got, expected,
				), ccTime, eccTime
			}
		}
	}
	return got, err, ccTime, eccTime
}

func (t *runTask) run0CSmith() (_ []byte, err error, ccTime, eccTime time.Duration) {
	timeLimit := 10 * time.Second
	const outLimit = 1 << 16
	defer func() {
		if e := recover(); e != nil {
			switch {
			case err == nil:
				err = fmt.Errorf("PANIC: %v\n%s", e, debug.Stack())
			default:
				err = fmt.Errorf("%v\nPANIC: %v\n%s", err, e, debug.Stack())
			}
		}
	}()

	baseName := filepath.Base(t.src)
	ccArgs := append([]string(nil), t.opts...)
	ok := true
	for _, v := range t.opts {
		if strings.HasPrefix(v, "-O") {
			ok = false
			break
		}
	}
	if ok {
		if o := *oO; o >= 0 {
			ccArgs = append(ccArgs, fmt.Sprintf("-O%d", o))
		}
	}
	binary, err := makeCCBinary(baseName, t.doNotExec, ccArgs...)
	if err != nil {
		return nil, skipErr(err.Error()), ccTime, eccTime
	}

	const (
		ccOut  = "cc.out"
		eccOut = "ecc.out"
	)
	var binaryBytes, binaryBytes2 int
	var expected []byte
	ctx, cancel := context.WithTimeout(context.Background(), timeLimit)
	defer cancel()
	t0 := time.Now()
	if *oTrc2 {
		fmt.Fprintf(os.Stderr, "%v: started CC binary for %s: %v\n", t0, baseName, binary)
	}
	switch {
	case t.hasBinaryOutput:
		binaryBytes, err = execute(ctx, binary, ccOut, nil)
		defer os.Remove(ccOut)
	default:
		expected, err = exec.CommandContext(ctx, binary).CombinedOutput()
		if len(expected) > outLimit {
			panic(todo("", t.src, len(expected)))
		}
	}
	ccTime = time.Since(t0)
	if *oTrc2 {
		switch {
		case t.hasBinaryOutput:
			fmt.Fprintf(os.Stderr, "%v: CC binary for %s returned: %v bytes, err %v\n", time.Now(), baseName, binaryBytes, err)
		default:
			fmt.Fprintf(os.Stderr, "%v: CC binary for %s returned: err %v\n%s\n", time.Now(), baseName, err, expected)
		}
	}
	if err != nil {
		return nil, skipErr(fmt.Sprintf("executing CC binary %v: %v\n%s", binary, err, expected)), ccTime, eccTime
	}

	if *oTrco {
		fmt.Fprintf(os.Stderr, "%v: %s\n", ccTime, expected)
	}

	if err := os.Remove(binary); err != nil {
		return nil, fmt.Errorf("removing %v: %v", binary, err), ccTime, eccTime
	}

	eccArgs := append([]string(nil), t.opts...)
	ok = true
	for _, v := range t.opts {
		if strings.HasPrefix(v, "-O") {
			ok = false
			break
		}
	}
	if ok {
		if o := *oO; o >= 0 {
			eccArgs = append(eccArgs, fmt.Sprintf("-O%d", o))
		}
	}
	if *oFullPaths {
		eccArgs = append(eccArgs, "-full-paths")
	}
	if *oKeepTmp {
		eccArgs = append(eccArgs, "-keep-tmp")
	}
	if binary, err = makeBinary(t.src, t.doNotExec, eccArgs...); err != nil {
		return nil, err, ccTime, eccTime
	}

	var got []byte
	ctx, cancel = context.WithTimeout(context.Background(), timeLimit)
	defer cancel()
	t0 = time.Now()
	if *oTrc2 {
		fmt.Fprintf(os.Stderr, "%v: started ecc binary for %s: %v\n", t0, baseName, binary)
	}
	got, err = exec.CommandContext(ctx, binary).CombinedOutput()
	if len(got) > outLimit {
		panic(todo("", t.src, len(expected)))
	}
	eccTime = time.Since(t0)
	if *oTrc2 {
		switch {
		case t.hasBinaryOutput:
			fmt.Fprintf(os.Stderr, "%v: ecc binary for %s returned: %v bytes, err %v\n", time.Now(), baseName, binaryBytes2, err)
		default:
			fmt.Fprintf(os.Stderr, "%v: ecc binary for %s returned: err %v\n%s\n", time.Now(), baseName, err, got)
		}
	}
	if err != nil {
		return nil, fmt.Errorf("executing ecc binary %v: %v\n%s", binary, err, got), ccTime, eccTime
	}

	if *oTrco {
		fmt.Fprintf(os.Stderr, "%v: %s\n", eccTime, got)
	}
	{
		got := string(got)
		expected := string(expected)
		got = strings.ReplaceAll(got, "\r", "")
		got = lineTrim(strings.TrimSpace(got))
		expected = strings.ReplaceAll(expected, "\r", "")
		expected = lineTrim(strings.TrimSpace(expected))
		if got != expected {
			diff := difflib.UnifiedDiff{
				A:        difflib.SplitLines(expected),
				B:        difflib.SplitLines(got),
				FromFile: "expected",
				ToFile:   "got",
				Context:  3,
			}
			text, _ := difflib.GetUnifiedDiffString(diff)
			return nil, fmt.Errorf(
				"%v: text output differs:\n%s\n---- x.c\ngot\n%s\nexp\n%s\ngot\n%s\nexp\n%s",
				t.src, text,
				hex.Dump([]byte(got)), hex.Dump([]byte(expected)),
				got, expected,
			), ccTime, eccTime
		}
	}
	return got, err, ccTime, eccTime
}

func fileEqual(g, e string) error {
	fig, err := os.Stat(g)
	if err != nil {
		return err
	}

	fie, err := os.Stat(e)
	if err != nil {
		return err
	}

	if g, e := fig.Size(), fie.Size(); g != e {
		return fmt.Errorf("files sizes differ, got %v, expected %v", g, e)
	}

	rem := fig.Size()
	if rem == 0 {
		return nil
	}

	var bg, be [4096]byte
	fg, err := os.Open(g)
	if err != nil {
		return err
	}

	defer fg.Close()

	fe, err := os.Open(e)
	if err != nil {
		return err
	}

	defer fe.Close()

	for rem != 0 {
		n, err := io.ReadFull(fg, bg[:])
		if n == 0 {
			if err == io.EOF {
				err = nil
			}
			return err
		}

		n2, err := io.ReadFull(fe, be[:])
		if n == 0 {
			if err == io.EOF {
				err = nil
			}
			return err
		}

		if n != n2 {
			panic(todo("", n, n2))
		}

		if !bytes.Equal(bg[:n], be[:n]) {
			return fmt.Errorf("files are different")
		}

		rem -= int64(n)
	}
	return nil
}

type countingWriter struct {
	written int
	w       *bufio.Writer
}

func (c *countingWriter) Write(b []byte) (int, error) {
	n, err := c.w.Write(b)
	c.written += n
	return n, err
}

var _ io.Writer = (*countingWriter)(nil)

// err = execute(ctx, executable, args, ccOut)
func execute(ctx context.Context, executable, out string, args []string) (n int, err error) {
	cmd := exec.CommandContext(ctx, executable, args...)
	f, err := os.Create(out)
	if err != nil {
		return 0, err
	}

	defer func() {
		if e := f.Close(); e != nil && err == nil {
			err = e
		}
	}()

	w := &countingWriter{w: bufio.NewWriter(f)}

	defer func() {
		if e := w.w.Flush(); e != nil && err == nil {
			err = e
		}
	}()

	cmd.Stdout = w
	err = cmd.Run()
	return w.written, err
}

func run(src string, binaryOut, ccCanFail, doNotExec bool, c chan *runResult, opts ...string) {
	(&runTask{
		c:               c,
		ccCanFail:       ccCanFail,
		doNotExec:       doNotExec,
		hasBinaryOutput: binaryOut,
		opts:            opts,
		src:             src,
	}).run()
}

func makeCCBinary(src string, obj bool, args ...string) (executable string, err error) {
	ext := ""
	if obj {
		ext = ".o"
	}
	src = filepath.Base(src)
	executable = "./" + src[:len(src)-len(filepath.Ext(src))]
	if runtime.GOOS == "windows" && !obj {
		ext = ".exe"
	}
	executable += ext
	os.Remove(executable)
	b, err := exec.Command(systemCC, append([]string{"-o", executable, src}, args...)...).CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("%v %v -o %v %v: system C compiler: %v\n%s", systemCC, args, executable, src, err, b)
	}

	return executable, nil
}

func makeBinary(src string, obj bool, args ...string) (executable string, err error) {
	ext := ""
	if obj {
		ext = ".o"
	}
	src = filepath.Base(src)
	executable = "./" + src[:len(src)-len(filepath.Ext(src))]
	if runtime.GOOS == "windows" {
		ext = ".exe"
	}
	executable += ext
	os.Remove(executable)
	var out bytes.Buffer
	task := NewTask("ecc", append([]string{"-o", executable, src}, args...), &out, &out)
	if err := task.Main(); err != nil {
		return "", fmt.Errorf("%v: ecc compiler: %v\n%s", src, err, out.Bytes())
	}

	if b := out.Bytes(); len(b) != 0 { //TODO-
		s := string(b)
		s = strings.ReplaceAll(s, "‘", "'")
		s = strings.ReplaceAll(s, "’", "'")
		switch {
		case
			strings.Contains(s, "warning: 'return' with no value, in function returning non-void"),
			strings.Contains(s, "warning: conflicting types for built-in function '__fprintf_chk'"),
			strings.Contains(s, "warning: conflicting types for built-in function '__printf_chk'"),
			strings.Contains(s, "warning: conflicting types for built-in function '__vfprintf_chk'"),
			strings.Contains(s, "warning: conflicting types for built-in function '__vprintf_chk'"),
			strings.Contains(s, "warning: conflicting types for built-in function 'cbrtl'"),
			strings.Contains(s, "warning: conflicting types for built-in function 'memcmp'"),
			strings.Contains(s, "warning: conflicting types for built-in function 'rintf'"),
			strings.Contains(s, "warning: floating constant exceeds range of 'long double'"),
			strings.Contains(s, "warning:") && strings.Contains(s, "may not be initialized"):
			// ok
		default:
			fmt.Printf("WARNING %s\n", s)
		}
	}
	return executable, nil
}

func getArgs(src string) (args []string, err error) {
	src = src[:len(src)-len(filepath.Ext(src))] + ".arg"
	overlay := filepath.Join(overlayDir, src)
	b, err := ioutil.ReadFile(overlay)
	if err != nil {
		if !os.IsNotExist(err) {
			return nil, err
		}

		f, err := fs.Open(src)
		if err != nil {
			return nil, nil
		}

		if b, err = ioutil.ReadAll(f); err != nil {
			return nil, err
		}

		if err = f.Close(); err != nil {
			return nil, err
		}
	}

	a := strings.Split(strings.TrimSpace(string(b)), "\n")
	for _, v := range a {
		switch {
		case strings.HasPrefix(v, "\"") || strings.HasPrefix(v, "`"):
			w, err := strconv.Unquote(v)
			if err != nil {
				return nil, fmt.Errorf("%s: %v: %v", src, v, err)
			}

			args = append(args, w)
		default:
			args = append(args, v)
		}
	}
	return args, nil
}

func lineTrim(s string) string {
	a := strings.Split(s, "\n")
	for i, v := range a {
		a[i] = strings.TrimSpace(v)
	}
	return strings.Join(a, "\n")
}

func walk(dir string, f func(pth string, fi os.FileInfo) error) error {
	if !strings.HasSuffix(dir, "/") {
		dir += "/"
	}
	root, err := fs.Open(dir)
	if err != nil {
		return err
	}

	fi, err := root.Stat()
	if err != nil {
		return err
	}

	if !fi.IsDir() {
		return fmt.Errorf("%s: not a directory", fi.Name())
	}

	fis, err := root.Readdir(-1)
	if err != nil {
		return err
	}

	for _, v := range fis {
		switch {
		case v.IsDir():
			if err = walk(v.Name(), f); err != nil {
				return err
			}
		default:
			if err = f(v.Name(), v); err != nil {
				return err
			}
		}
	}
	return nil
}

func TestTCC(t *testing.T) {
	const root = "/tcc-0.9.27/tests/tests2"
	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	needFiles(t, root, []string{
		"18_include.h",
		"95_bitfields.c",
	})
	blacklist := map[string]struct{}{
		"60_errors_and_warnings.c":  {}, // no main
		"73_arm64.c":                {}, // does not work properly on any gcc tested (7-11)
		"77_push_pop_macro.c":       {}, // unsupported push/pop macro
		"78_vla_label.c":            {}, //MAYBE
		"79_vla_continue.c":         {}, //MAYBE
		"80_flexarray.c":            {}, //MAYBE
		"83_utf8_in_identifiers.c":  {}, // No support before gcc 10.
		"85_asm-outside-function.c": {}, // asm
		"90_struct-init.c":          {}, // 90_struct-init.c:168:25: `...`: expected ]
		"94_generic.c":              {}, // 94_generic.c:36:18: `int`: expected primary-expression
		"96_nodata_wanted.c":        {}, // no main
		"98_al_ax_extend.c":         {}, // asm
		"99_fastcall.c":             {}, // asm

		"95_bitfields_ms.c": {}, //TODO
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "s390x" {
		blacklist["95_bitfields.c"] = struct{}{} //TODO
	}
	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]struct{}{}
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				ok++
				delete(failed, r.name)
			case skipErr:
				delete(failed, r.name)
				t.Logf("%v: %v\n%s", r.name, r.err, r.out)
			default:
				t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			failed[pth] = struct{}{}
			go run(pth, false, false, false, results)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			delete(failed, r.name)
		case skipErr:
			delete(failed, r.name)
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s", v)
	}
}

func needFiles(t *testing.T, root string, a []string) {
	for _, v := range a {
		overlay := filepath.Join(overlayDir, filepath.FromSlash(root), v)
		b, err := ioutil.ReadFile(overlay)
		if err != nil {
			if !os.IsNotExist(err) {
				t.Fatal(err)
			}

			f, err := fs.Open(path.Join(root, v))
			if err != nil {
				t.Fatal(err)
			}

			if b, err = ioutil.ReadAll(f); err != nil {
				t.Fatal(err)
			}

			if err = f.Close(); err != nil {
				t.Fatal(err)
			}
		}
		if dir, _ := filepath.Split(v); dir != "" {
			if err := os.MkdirAll(dir, 0700); err != nil {
				t.Fatal(err)
			}
		}

		if err := ioutil.WriteFile(v, b, 0600); err != nil {
			t.Fatal(err)
		}
	}
}

func TestCCGo(t *testing.T) {
	const root = "/ccgo/bug"
	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	blacklist := map[string]struct{}{}
	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]struct{}{}
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				ok++
				delete(failed, r.name)
			case skipErr:
				delete(failed, r.name)
				t.Logf("%v: %v\n%s", r.name, r.err, r.out)
			default:
				t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			failed[pth] = struct{}{}
			go run(pth, false, false, false, results)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			delete(failed, r.name)
		case skipErr:
			delete(failed, r.name)
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s", v)
	}
}

func TestMirBenchmarks(t *testing.T) {
	const root = "/github.com/vnmakarov/mir/c-benchmarks"
	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	needFiles(t, root, []string{
		"simple_hash.h",
	})
	blacklist := map[string]struct{}{}
	if runtime.GOOS == "windows" && runtime.GOARCH == "amd64" {
		blacklist["except.c"] = struct{}{} //TODO
	}
	binary := map[string]bool{
		"mandelbrot.c": true,
	}
	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]struct{}{}
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				ok++
				delete(failed, r.name)
			case skipErr:
				delete(failed, r.name)
				t.Logf("%v: %v\n%s", r.name, r.err, r.out)
			default:
				t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			failed[pth] = struct{}{}
			go run(pth, binary[filepath.Base(pth)], false, false, results)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			delete(failed, r.name)
		case skipErr:
			delete(failed, r.name)
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s", v)
	}
}

func TestMirAndrewChambers(t *testing.T) {
	const root = "/github.com/vnmakarov/mir/c-tests/andrewchambers_c"
	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	blacklist := map[string]struct{}{}
	binary := map[string]bool{}
	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]struct{}{}
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				ok++
				delete(failed, r.name)
			case skipErr:
				delete(failed, r.name)
				t.Logf("%v: %v\n%s", r.name, r.err, r.out)
			default:
				t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			failed[pth] = struct{}{}
			go run(pth, binary[filepath.Base(pth)], false, false, results)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			delete(failed, r.name)
		case skipErr:
			delete(failed, r.name)
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s", v)
	}
}

func TestMirLacc(t *testing.T) {
	const root = "/github.com/vnmakarov/mir/c-tests/lacc"
	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	needFiles(t, root, []string{
		"hello.c",
		"header.h",
	})
	blacklist := map[string]struct{}{
		"function-incomplete.c": {}, // Calls int foo(char*) without argument, no intent to support.

		"macro-repeat-expand.c": {}, //TODO
		"pointer-immediate.c":   {}, //TODO
		"string-addr.c":         {}, //TODO
		"string-conversion.c":   {}, //TODO
		"strings.c":             {}, //TODO
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "s390x" {
		blacklist["assignment-type.c"] = struct{}{}         //TODO
		blacklist["bitfield-basic.c"] = struct{}{}          //TODO
		blacklist["bitfield-load.c"] = struct{}{}           //TODO
		blacklist["bitfield-pack-next.c"] = struct{}{}      //TODO
		blacklist["bitfield-packing.c"] = struct{}{}        //TODO
		blacklist["bitfield-types-init.c"] = struct{}{}     //TODO
		blacklist["bitfield-types.c"] = struct{}{}          //TODO
		blacklist["cast-immediate-truncate.c"] = struct{}{} //TODO
		blacklist["comment.c"] = struct{}{}                 //TODO
		blacklist["convert-unsigned-float.c"] = struct{}{}  //TODO
		blacklist["function-char-args.c"] = struct{}{}      //TODO
		blacklist["return-bitfield.c"] = struct{}{}         //TODO
		blacklist["stringify.c"] = struct{}{}               //TODO
		blacklist["struct-comma-call.c"] = struct{}{}       //TODO
		blacklist["token.c"] = struct{}{}                   //TODO
		blacklist["union-bitfield.c"] = struct{}{}          //TODO
	}
	if runtime.GOOS == "windows" && runtime.GOARCH == "amd64" {
		blacklist["bitfield-basic.c"] = struct{}{}         //TODO
		blacklist["bitfield-pack-next.c"] = struct{}{}     //TODO
		blacklist["bitfield-trailing-zero.c"] = struct{}{} //TODO
		blacklist["bitfield-types-init.c"] = struct{}{}    //TODO
		blacklist["constant-expression.c"] = struct{}{}    //TODO
		blacklist["ptrdiff.c"] = struct{}{}                //TODO
		blacklist["signed-division.c"] = struct{}{}        //TODO
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "386" {
		blacklist["bitfield-pack-next.c"] = struct{}{}  //TODO
		blacklist["bitfield-types-init.c"] = struct{}{} //TODO
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "arm" {
		blacklist["assignment-type.c"] = struct{}{}         //TODO
		blacklist["bitfield-basic.c"] = struct{}{}          //TODO
		blacklist["bitfield-trailing-zero.c"] = struct{}{}  //TODO
		blacklist["bitfield-types-init.c"] = struct{}{}     //TODO
		blacklist["cast-immediate-truncate.c"] = struct{}{} //TODO
		blacklist["function-char-args.c"] = struct{}{}      //TODO
		blacklist["stringify.c"] = struct{}{}               //TODO
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "arm64" {
		blacklist["assignment-type.c"] = struct{}{}         //TODO
		blacklist["bitfield-basic.c"] = struct{}{}          //TODO
		blacklist["bitfield-trailing-zero.c"] = struct{}{}  //TODO
		blacklist["bitfield-types-init.c"] = struct{}{}     //TODO
		blacklist["cast-immediate-truncate.c"] = struct{}{} //TODO
		blacklist["function-char-args.c"] = struct{}{}      //TODO
		blacklist["stringify.c"] = struct{}{}               //TODO
	}
	binary := map[string]bool{}
	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]struct{}{}
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				ok++
				delete(failed, r.name)
			case skipErr:
				delete(failed, r.name)
				t.Logf("%v: %v\n%s", r.name, r.err, r.out)
			default:
				t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			failed[pth] = struct{}{}
			go run(pth, binary[filepath.Base(pth)], false, false, results, "-trigraphs") // comment.c needs trigraphs
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			delete(failed, r.name)
		case skipErr:
			delete(failed, r.name)
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s", v)
	}
}

func TestMirNew(t *testing.T) {
	const root = "/github.com/vnmakarov/mir/c-tests/new"
	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	blacklist := map[string]struct{}{
		// 1: /github.com/vnmakarov/mir/c-tests/new/endif.c
		//     all_test.go:1045: /github.com/vnmakarov/mir/c-tests/new/endif.c: /usr/bin/gcc: system C compiler: exit status 1
		//         endif.c:1:2: error: #endif without #if
		//          #endif
		//           ^~~~~
		"endif.c": {}, // No intent to support.

		// 1: /github.com/vnmakarov/mir/c-tests/new/fermian-2.c
		//     all_test.go:1051: /github.com/vnmakarov/mir/c-tests/new/fermian-2.c: /usr/bin/gcc: system C compiler: exit status 1
		//         fermian-2.c:1:3: error: expected ‘=’, ‘,’, ‘;’, ‘asm’ or ‘__attribute__’ before ‘{’ token
		//          a {
		//            ^
		"fermian-2.c": {}, // No intent to support.
		"fermian.c":   {}, // No main.

		"array-elem-addr-in-initializer.c": {}, //TODO
		"issue117.c":                       {}, //TODO
		"issue23.c":                        {}, //TODO
	}
	if runtime.GOOS == "windows" && runtime.GOARCH == "amd64" {
		blacklist["setjmp2.c"] = struct{}{} //TODO
	}
	binary := map[string]bool{}
	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]struct{}{}
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				ok++
				delete(failed, r.name)
			case skipErr:
				delete(failed, r.name)
				t.Logf("%v: %v\n%s", r.name, r.err, r.out)
			default:
				t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			failed[pth] = struct{}{}
			go run(pth, binary[filepath.Base(pth)], false, false, results)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			delete(failed, r.name)
		case skipErr:
			delete(failed, r.name)
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s", v)
	}
}

func TestCompCert(t *testing.T) {
	const root = "/github.com/AbsInt/CompCert/test/c/"
	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	needFiles(t, root, []string{
		"Results/knucleotide-input.txt",
		"endian.h",
	})
	blacklist := map[string]struct{}{}
	if runtime.GOOS == "linux" && runtime.GOARCH == "s390x" {
		blacklist["aes.c"] = struct{}{} //TODO
	}
	if runtime.GOOS == "windows" && runtime.GOARCH == "amd64" {
		blacklist["qsort.c"] = struct{}{} //TODO
	}
	binary := map[string]bool{
		"mandelbrot.c": true,
	}
	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]struct{}{}
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				ok++
				delete(failed, r.name)
			case skipErr:
				delete(failed, r.name)
				t.Logf("%v: %v\n%s", r.name, r.err, r.out)
			default:
				t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			failed[pth] = struct{}{}
			go run(pth, binary[filepath.Base(pth)], false, false, results)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			delete(failed, r.name)
		case skipErr:
			delete(failed, r.name)
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s", v)
	}
}

func TestGCCExecute(t *testing.T) {
	const root = "/github.com/gcc-mirror/gcc/gcc/testsuite/gcc.c-torture/execute"
	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	// Prepare testdata
	needFiles(t, root, []string{
		"20040709-2.c",
	})
	blacklist := map[string]struct{}{
		"eeprof-1.c": {}, // Requires -finstrument-functions
		"pr64242.c":  {}, // Depends on specific stack corruption.

		// assembler
		"20001009-2.c": {}, //TODO
		"20020107-1.c": {}, //TODO
		"20050203-1.c": {}, //TODO
		"20061031-1.c": {}, //TODO
		"20071211-1.c": {}, //TODO
		"20071220-1.c": {}, //TODO
		"20071220-2.c": {}, //TODO
		"20080122-1.c": {}, //TODO
		"960312-1.c":   {}, //TODO
		"990130-1.c":   {}, //TODO
		"990413-2.c":   {}, //TODO
		"bitfld-5.c":   {}, //TODO
		"pr38533.c":    {}, //TODO
		"pr40022.c":    {}, //TODO
		"pr40657.c":    {}, //TODO
		"pr41239.c":    {}, //TODO
		"pr43385.c":    {}, //TODO
		"pr43560.c":    {}, //TODO
		"pr44852.c":    {}, //TODO
		"pr45695.c":    {}, //TODO
		"pr46309.c":    {}, //TODO
		"pr47925.c":    {}, //TODO
		"pr49218.c":    {}, //TODO
		"pr49279.c":    {}, //TODO
		"pr49390.c":    {}, //TODO
		"pr51581-1.c":  {}, //TODO
		"pr51581-2.c":  {}, //TODO
		"pr51877.c":    {}, //TODO
		"pr51933.c":    {}, //TODO
		"pr52286.c":    {}, //TODO
		"pr56205.c":    {}, //TODO
		"pr56866.c":    {}, //TODO
		"pr56982.c":    {}, //TODO
		"pr57344-1.c":  {}, //TODO
		"pr57344-2.c":  {}, //TODO
		"pr57344-3.c":  {}, //TODO
		"pr57344-4.c":  {}, //TODO
		"pr58277-1.c":  {}, //TODO
		"pr58419.c":    {}, //TODO
		"pr63641.c":    {}, //TODO
		"pr65053-1.c":  {}, //TODO
		"pr65053-2.c":  {}, //TODO
		"pr65648.c":    {}, //TODO
		"pr65956.c":    {}, //TODO
		"pr68328.c":    {}, //TODO
		"pr69320-2.c":  {}, //TODO
		"pr69691.c":    {}, //TODO
		"pr78438.c":    {}, //TODO
		"pr78726.c":    {}, //TODO
		"pr79354.c":    {}, //TODO
		"pr79737-2.c":  {}, //TODO
		"pr80421.c":    {}, //TODO
		"pr81588.c":    {}, //TODO
		"pr82954.c":    {}, //TODO
		"pr84478.c":    {}, //TODO
		"pr84524.c":    {}, //TODO
		"pr85156.c":    {}, //TODO
		"pr85756.c":    {}, //TODO
		"pr88904.c":    {}, //TODO
		"pr93945.c":    {}, //TODO
		"pr94130.c":    {}, //TODO
		"stkalign.c":   {}, //TODO

		// variable type size
		"20010209-1.c":    {}, //TODO
		"20020412-1.c":    {}, //TODO
		"20040308-1.c":    {}, //TODO
		"20040411-1.c":    {}, //TODO
		"20040423-1.c":    {}, //TODO
		"20040811-1.c":    {}, //TODO
		"20041218-2.c":    {}, //TODO
		"20070919-1.c":    {}, //TODO
		"920721-2.c":      {}, //TODO
		"920929-1.c":      {}, //TODO
		"970217-1.c":      {}, //TODO
		"align-nest.c":    {}, //TODO
		"pr22061-1.c":     {}, //TODO
		"pr41935.c":       {}, //TODO
		"pr43220.c":       {}, //TODO
		"pr77767.c":       {}, //TODO
		"pr82210.c":       {}, //TODO
		"vla-dealloc-1.c": {}, //TODO

		// vector
		"20050316-1.c":   {}, //TODO
		"20050316-2.c":   {}, //TODO
		"20050316-3.c":   {}, //TODO
		"20050604-1.c":   {}, //TODO
		"20050607-1.c":   {}, //TODO
		"20060420-1.c":   {}, //TODO
		"pr23135.c":      {}, //TODO
		"pr53645-2.c":    {}, //TODO
		"pr53645.c":      {}, //TODO
		"pr60960.c":      {}, //TODO
		"pr65427.c":      {}, //TODO
		"pr70903.c":      {}, //TODO
		"pr71626-1.c":    {}, //TODO
		"pr71626-2.c":    {}, //TODO
		"pr85169.c":      {}, //TODO
		"pr85331.c":      {}, //TODO
		"pr92618.c":      {}, //TODO
		"pr94412.c":      {}, //TODO
		"pr94524-1.c":    {}, //TODO
		"pr94524-2.c":    {}, //TODO
		"pr94591.c":      {}, //TODO
		"scal-to-vec1.c": {}, //TODO
		"scal-to-vec2.c": {}, //TODO
		"scal-to-vec3.c": {}, //TODO
		"simd-1.c":       {}, //TODO
		"simd-2.c":       {}, //TODO
		"simd-4.c":       {}, //TODO
		"simd-5.c":       {}, //TODO
		"simd-6.c":       {}, //TODO

		// #pragma push_macro("_")
		"pushpop_macro.c": {}, //TODO

		// __attribute__ ((alias("a")))
		"alias-2.c": {}, //TODO
		"alias-3.c": {}, //TODO
		"alias-4.c": {}, //TODO

		// assignment to expression with array type
		"pr64979.c": {}, //TODO

		// argument 3 in call to function ‘__builtin_mul_overflow’ does not have pointer to integral type
		"pr64006.c":   {}, //TODO
		"pr68381.c":   {}, //TODO
		"pr71554.c":   {}, //TODO
		"pr85095.c":   {}, //TODO
		"pr89434.c":   {}, //TODO
		"pr90311.c":   {}, //TODO
		"pr91450-1.c": {}, //TODO
		"pr91450-2.c": {}, //TODO
		"pr91635.c":   {}, //TODO
		"pr93494.c":   {}, //TODO

		// __builtin_va_arg_pack
		"va-arg-pack-1.c": {}, //TODO

		// __complex__ types
		"20010605-2.c": {}, //TODO
		"20020227-1.c": {}, //TODO
		"20020411-1.c": {}, //TODO
		"20030910-1.c": {}, //TODO
		"20041124-1.c": {}, //TODO
		"20041201-1.c": {}, //TODO
		"20050121-1.c": {}, //TODO
		"20070614-1.c": {}, //TODO
		"960512-1.c":   {}, //TODO
		"complex-1.c":  {}, //TODO
		"complex-2.c":  {}, //TODO
		"complex-4.c":  {}, //TODO
		"complex-5.c":  {}, //TODO
		"complex-6.c":  {}, //TODO
		"complex-7.c":  {}, //TODO
		"pr38151.c":    {}, //TODO
		"pr38969.c":    {}, //TODO
		"pr42248.c":    {}, //TODO
		"pr49644.c":    {}, //TODO
		"pr56837.c":    {}, //TODO

		// &&label or __label__
		"20040302-1.c":  {},
		"20041214-1.c":  {},
		"20071210-1.c":  {},
		"920302-1.c":    {},
		"920415-1.c":    {},
		"920428-2.c":    {},
		"920501-4.c":    {},
		"920501-5.c":    {},
		"920501-7.c":    {},
		"920721-4.c":    {},
		"930406-1.c":    {},
		"980526-1.c":    {},
		"990208-1.c":    {},
		"comp-goto-1.c": {},
		"comp-goto-2.c": {},
		"pr24135.c":     {},
		"pr51447.c":     {},
		"pr70460.c":     {},
		"pr71494.c":     {},

		// local func def
		"20000822-1.c":   {},
		"20010605-1.c":   {},
		"20030501-1.c":   {},
		"20040520-1.c":   {},
		"20061220-1.c":   {},
		"20090219-1.c":   {},
		"920612-2.c":     {},
		"921017-1.c":     {},
		"921215-1.c":     {},
		"931002-1.c":     {},
		"nest-align-1.c": {},
		"nest-stdar-1.c": {},
		"nestfunc-1.c":   {},
		"nestfunc-2.c":   {},
		"nestfunc-3.c":   {},
		"nestfunc-5.c":   {},
		"nestfunc-6.c":   {},
		"nestfunc-7.c":   {},
		"pr22061-3.c":    {},
		"pr22061-4.c":    {},

		// goto * expr;
		"920501-3.c": {},

		// Flexible array member
		"20010924-1.c": {},
		"20030109-1.c": {},
		"20050613-1.c": {},
		"pr28865.c":    {},
		"pr33382.c":    {},

		// __builtin_classify_type
		"20040709-1.c": {},
		"20040709-2.c": {},
		"20040709-3.c": {},

		// LabeledStatementRange
		"pr34154.c": {},

		// __builtin_apply
		"pr47237.c": {},

		// 128 bit int/uint
		"pr54471.c":   {},
		"pr61375.c":   {},
		"pr63302.c":   {},
		"pr65170.c":   {},
		"pr84169.c":   {},
		"pr84748.c":   {},
		"pr85582-2.c": {},
		"pr85582-3.c": {},
		"pr92904.c":   {},
		"pr93213.c":   {},
		"pr98474.c":   {},

		// _Decimal64
		"pr80692.c": {},

		"builtin-types-compatible-p.c": {}, //TODO
		"call-trap-1.c":                {}, //TODO
		"pr70602.c":                    {}, //TODO
		"pr71700.c":                    {}, //TODO

		"vfprintf-chk-1.c": {}, //TODO va_list (nuc_64 only)
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "s390x" {
		blacklist["20020201-1.c"] = struct{}{} //TODO
		blacklist["20020226-1.c"] = struct{}{} //TODO
		blacklist["20020508-1.c"] = struct{}{} //TODO
		blacklist["20020508-2.c"] = struct{}{} //TODO
		blacklist["20020508-3.c"] = struct{}{} //TODO
		blacklist["20020911-1.c"] = struct{}{} //TODO
		blacklist["20030128-1.c"] = struct{}{} //TODO
		blacklist["20050215-1.c"] = struct{}{} //TODO
		blacklist["20070724-1.c"] = struct{}{} //TODO
		blacklist["20180921-1.c"] = struct{}{} //TODO
		blacklist["bf-pack-1.c"] = struct{}{}  //TODO
		blacklist["bitfld-6.c"] = struct{}{}   //TODO
		blacklist["bitfld-7.c"] = struct{}{}   //TODO
		blacklist["memchr-1.c"] = struct{}{}   //TODO
		blacklist["memset-1.c"] = struct{}{}   //TODO
		blacklist["memset-2.c"] = struct{}{}   //TODO
		blacklist["memset-3.c"] = struct{}{}   //TODO
		blacklist["packed-1.c"] = struct{}{}   //TODO
		blacklist["pr19606.c"] = struct{}{}    //TODO
		blacklist["pr23467.c"] = struct{}{}    //TODO
		blacklist["pr29695-2.c"] = struct{}{}  //TODO
		blacklist["pr40386.c"] = struct{}{}    //TODO
		blacklist["pr42269-2.c"] = struct{}{}  //TODO
		blacklist["pr43438.c"] = struct{}{}    //TODO
		blacklist["pr44164.c"] = struct{}{}    //TODO
		blacklist["pr58574.c"] = struct{}{}    //TODO
		blacklist["pr77766.c"] = struct{}{}    //TODO
		blacklist["pr81503.c"] = struct{}{}    //TODO
		blacklist["pr81555.c"] = struct{}{}    //TODO
		blacklist["pr83383.c"] = struct{}{}    //TODO
		blacklist["pr94567.c"] = struct{}{}    //TODO
		blacklist["pr98366.c"] = struct{}{}    //TODO
	}
	if runtime.GOOS == "darwin" && runtime.GOARCH == "amd64" {
		blacklist["pr44164.c"] = struct{}{} //TODO
		blacklist["pr98366.c"] = struct{}{} //TODO
	}
	if runtime.GOOS == "windows" && runtime.GOARCH == "amd64" {
		blacklist["20000519-1.c"] = struct{}{}     //TODO va_list
		blacklist["20001108-1.c"] = struct{}{}     //TODO
		blacklist["20071213-1.c"] = struct{}{}     //TODO va_list
		blacklist["20101011-1.c"] = struct{}{}     //TODO
		blacklist["930513-1.c"] = struct{}{}       //TODO
		blacklist["950607-2.c"] = struct{}{}       //TODO
		blacklist["960416-1.c"] = struct{}{}       //TODO
		blacklist["fprintf-2.c"] = struct{}{}      //TODO
		blacklist["fprintf-chk-1.c"] = struct{}{}  //TODO va_list
		blacklist["loop-2f.c"] = struct{}{}        //TODO
		blacklist["loop-2g.c"] = struct{}{}        //TODO
		blacklist["multdi-1.c"] = struct{}{}       //TODO
		blacklist["pr34456.c"] = struct{}{}        //TODO
		blacklist["pr78622.c"] = struct{}{}        //TODO
		blacklist["pr98366.c"] = struct{}{}        //TODO
		blacklist["printf-2.c"] = struct{}{}       //TODO
		blacklist["printf-chk-1.c"] = struct{}{}   //TODO va_list
		blacklist["stdarg-1.c"] = struct{}{}       //TODO va_list
		blacklist["stdarg-2.c"] = struct{}{}       //TODO va_list
		blacklist["stdarg-4.c"] = struct{}{}       //TODO va_list
		blacklist["user-printf.c"] = struct{}{}    //TODO
		blacklist["va-arg-10.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-13.c"] = struct{}{}      //TODO
		blacklist["va-arg-14.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-20.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-21.c"] = struct{}{}      //TODO
		blacklist["va-arg-9.c"] = struct{}{}       //TODO va_list
		blacklist["vfprintf-1.c"] = struct{}{}     //TODO va_list
		blacklist["vfprintf-chk-1.c"] = struct{}{} //TODO va_list
		blacklist["vprintf-1.c"] = struct{}{}      //TODO va_list
		blacklist["vprintf-chk-1.c"] = struct{}{}  //TODO va_list
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "386" {
		blacklist["20000519-1.c"] = struct{}{}     //TODO va_list
		blacklist["20071213-1.c"] = struct{}{}     //TODO va_list
		blacklist["960416-1.c"] = struct{}{}       //TODO
		blacklist["960830-1.c"] = struct{}{}       //TODO
		blacklist["floatunsisf-1.c"] = struct{}{}  //TODO
		blacklist["fprintf-chk-1.c"] = struct{}{}  //TODO va_list
		blacklist["pr23467.c"] = struct{}{}        //TODO
		blacklist["pr97073.c"] = struct{}{}        //TODO
		blacklist["pr98366.c"] = struct{}{}        //TODO
		blacklist["printf-chk-1.c"] = struct{}{}   //TODO va_list
		blacklist["stdarg-1.c"] = struct{}{}       //TODO va_list
		blacklist["stdarg-2.c"] = struct{}{}       //TODO va_list
		blacklist["stdarg-4.c"] = struct{}{}       //TODO va_list
		blacklist["user-printf.c"] = struct{}{}    //TODO va_list
		blacklist["va-arg-10.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-13.c"] = struct{}{}      //TODO
		blacklist["va-arg-14.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-20.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-21.c"] = struct{}{}      //TODO
		blacklist["va-arg-9.c"] = struct{}{}       //TODO va_list
		blacklist["vfprintf-1.c"] = struct{}{}     //TODO va_list
		blacklist["vfprintf-chk-1.c"] = struct{}{} //TODO va_list
		blacklist["vprintf-1.c"] = struct{}{}      //TODO va_list
		blacklist["vprintf-chk-1.c"] = struct{}{}  //TODO va_list
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "arm" {
		blacklist["20000519-1.c"] = struct{}{}     //TODO va_list
		blacklist["20071213-1.c"] = struct{}{}     //TODO va_list
		blacklist["960416-1.c"] = struct{}{}       //TODO
		blacklist["fprintf-chk-1.c"] = struct{}{}  //TODO va_list
		blacklist["pr23467.c"] = struct{}{}        //TODO
		blacklist["pr44164.c"] = struct{}{}        //TODO
		blacklist["pr98366.c"] = struct{}{}        //TODO
		blacklist["printf-chk-1.c"] = struct{}{}   //TODO va_list
		blacklist["stdarg-1.c"] = struct{}{}       //TODO va_list
		blacklist["stdarg-2.c"] = struct{}{}       //TODO va_list
		blacklist["stdarg-4.c"] = struct{}{}       //TODO va_list
		blacklist["strct-pack-1.c"] = struct{}{}   //TODO
		blacklist["user-printf.c"] = struct{}{}    //TODO va_list
		blacklist["va-arg-10.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-13.c"] = struct{}{}      //TODO
		blacklist["va-arg-14.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-20.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-21.c"] = struct{}{}      //TODO
		blacklist["va-arg-9.c"] = struct{}{}       //TODO va_list
		blacklist["vfprintf-1.c"] = struct{}{}     //TODO va_list
		blacklist["vfprintf-chk-1.c"] = struct{}{} //TODO va_list
		blacklist["vprintf-1.c"] = struct{}{}      //TODO va_list
		blacklist["vprintf-chk-1.c"] = struct{}{}  //TODO va_list
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "arm64" {
		blacklist["20000519-1.c"] = struct{}{}     //TODO va_list
		blacklist["20071213-1.c"] = struct{}{}     //TODO va_list
		blacklist["fprintf-chk-1.c"] = struct{}{}  //TODO va_list
		blacklist["pr44164.c"] = struct{}{}        //TODO
		blacklist["pr98366.c"] = struct{}{}        //TODO
		blacklist["pr98681.c"] = struct{}{}        //TODO
		blacklist["pr98853-2.c"] = struct{}{}      //TODO
		blacklist["printf-chk-1.c"] = struct{}{}   //TODO va_list
		blacklist["stdarg-1.c"] = struct{}{}       //TODO va_list
		blacklist["stdarg-2.c"] = struct{}{}       //TODO va_list
		blacklist["stdarg-4.c"] = struct{}{}       //TODO va_list
		blacklist["user-printf.c"] = struct{}{}    //TODO va_list
		blacklist["va-arg-10.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-13.c"] = struct{}{}      //TODO
		blacklist["va-arg-14.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-20.c"] = struct{}{}      //TODO va_list
		blacklist["va-arg-21.c"] = struct{}{}      //TODO
		blacklist["va-arg-9.c"] = struct{}{}       //TODO va_list
		blacklist["vfprintf-1.c"] = struct{}{}     //TODO va_list
		blacklist["vfprintf-chk-1.c"] = struct{}{} //TODO
		blacklist["vprintf-1.c"] = struct{}{}      //TODO va_list
		blacklist["vprintf-chk-1.c"] = struct{}{}  //TODO va_list
	}
	binary := map[string]bool{}
	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]struct{}{}
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if strings.Contains(pth, "/ieee/") || !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				ok++
				delete(failed, r.name)
			case skipErr:
				delete(failed, r.name)
				t.Logf("%v: %v\n%s", r.name, r.err, r.out)
			default:
				t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			base := filepath.Base(pth)
			var opts []string
			switch base {
			case
				"20010122-1.c", //TODO
				"20101011-1.c", //TODO
				"pr44164.c",    //TODO
				"pr97386-1.c",  //TODO
				"pr97386-2.c",  //TODO
				"pr98366.c":    //TODO

				opts = []string{"-O0"}
			}
			failed[pth] = struct{}{}
			go run(pth, binary[base], true, false, results, opts...)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			delete(failed, r.name)
		case skipErr:
			delete(failed, r.name)
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s", v)
	}
}

func TestGCCExecuteIEEE(t *testing.T) {
	const root = "/github.com/gcc-mirror/gcc/gcc/testsuite/gcc.c-torture/execute/ieee"
	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	blacklist := map[string]struct{}{
		"builtin-nan-1.c": {}, //TODO
		"copysign1.c":     {}, //TODO
		"copysign2.c":     {}, //TODO
		"fp-cmp-3.c":      {}, //TODO
		"fp-cmp-4l.c":     {}, //TODO
		"fp-cmp-8l.c":     {}, //TODO
		"pr50310.c":       {}, //TODO
		"pr72824-2.c":     {}, //TODO
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "386" {
		blacklist["20030331-1.c"] = struct{}{} //TODO
		blacklist["pr67218.c"] = struct{}{}    //TODO
	}
	binary := map[string]bool{}
	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]struct{}{}
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				ok++
				delete(failed, r.name)
			case skipErr:
				delete(failed, r.name)
				t.Logf("%v: %v\n%s", r.name, r.err, r.out)
			default:
				t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			failed[pth] = struct{}{}
			go run(pth, binary[filepath.Base(pth)], true, false, results)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			delete(failed, r.name)
		case skipErr:
			delete(failed, r.name)
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s", v)
	}
}

var installOnce sync.Once

func installEcc(t *testing.T) bool {
	if !*oInstall && os.Getenv("INSTALL_ECC") != "1" {
		return false
	}

	var out []byte
	var err error
	installOnce.Do(func() {
		t.Log("installing binaries")
		out, err = ccgo.Shell("go", "install", "-v", "../...")
		s := os.Getenv("GOPATH")
		if s == "" {
			s = build.Default.GOPATH
		}
		s = filepath.Join(s, "bin")
		p := os.Getenv("PATH")
		if !strings.Contains(p, s) {
			p = s + string(os.PathListSeparator) + p
			t.Logf("setting PATH=%s", p)
			os.Setenv("PATH", p)
		}
		if os.Getenv("QBEC_CC") == "" {
			if s := os.Getenv("CC"); s == "" || s == "ecc" {
				t.Logf("setting QBEC_CC=gcc")
				os.Setenv("QBEC_CC", "gcc")
			}
		}
	})
	if err != nil {
		t.Fatalf("%s\n%s", err, out)
	}

	t.Logf("ecc %v installed", Version())
	return true
}

func TestZlib(t *testing.T) {
	if !installEcc(t) {
		t.Skip("set INSTALL_ECC=1 or add -install to execute this test")
	}

	const root = "/github.com/madler/zlib.tar.gz"
	archive, err := fs.Open(root)
	if err != nil {
		t.Fatal(err)
	}

	mustEmptyDir(t, tempDir)
	if err := ccgo.Untar(tempDir, archive, nil); err != nil {
		t.Fatal(err)
	}

	if err := ccgo.InDir(filepath.Join(tempDir, "zlib"), func() error {
		switch {
		case
			runtime.GOOS == "darwin" && runtime.GOARCH == "amd64",
			//TODO runtime.GOOS == "linux" && runtime.GOARCH == "386",
			runtime.GOOS == "linux" && runtime.GOARCH == "amd64",
			//TODO runtime.GOOS == "linux" && runtime.GOARCH == "arm",
			//TODO runtime.GOOS == "linux" && runtime.GOARCH == "arm64",
			runtime.GOOS == "linux" && runtime.GOARCH == "s390x":

			out, err := ccgo.Shell("sh", "-c", "CC=ecc ./configure && make check")
			if err != nil || strings.Contains(string(out), "FAILED") {
				return errorf("err: %v\n%s", err, out)
			}
		default:
			// windows/amd64
			//TODO
			t.Skip("TODO") //TODO
		}

		return nil
	}); err != nil {
		t.Fatal(err)
	}
}

func TestBenchmarksGame(t *testing.T) {
	const root = "/benchmarksgame-team.pages.debian.net/"

	if testing.Short() {
		t.Skip("skipped: -short")
	}

	if runtime.GOOS != "linux" || runtime.GOARCH != "amd64" {
		t.Skip("skipped: supported currently only on linux/amd64") //TODO
	}

	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	// Prepare testdata
	needFiles(t, root, []string{
		"Include/khash.h",
		"fasta.c",
	})
	bin, err := makeCCBinary("fasta.c", false)
	if err != nil {
		t.Fatal(err)
	}

	defer os.Remove(bin)

	for _, size := range []int{5e6, 1e8, 25e6} {
		cmd := exec.Command(bin, fmt.Sprint(size))
		fn := fmt.Sprintf("%d.txt", size)
		f, err := os.Create(fn)
		if err != nil {
			t.Fatal(err)
		}

		w := bufio.NewWriter(f)
		cmd.Stdout = w
		if err := cmd.Run(); err != nil {
			t.Fatal(err)
		}

		if err := w.Flush(); err != nil {
			t.Fatal(err)
		}

		if err := f.Close(); err != nil {
			t.Fatal(err)
		}
	}

	blacklist := map[string]struct{}{
		"binary-trees-2.c":  {}, //TODO
		"binary-trees-3.c":  {}, //TODO
		"fannkuchredux-4.c": {}, //TODO
		"fannkuchredux-5.c": {}, //TODO
		"fannkuchredux.c":   {}, //TODO
		"fasta-2.c":         {}, //TODO
		"fasta-5.c":         {}, //TODO
		"fasta-6.c":         {}, //TODO
		"fasta-7.c":         {}, //TODO
		"k-nucleotide.c":    {}, //TODO
		"mandelbrot-3.c":    {}, //TODO
		"mandelbrot-4.c":    {}, //TODO
		"mandelbrot-6.c":    {}, //TODO
		"mandelbrot-7.c":    {}, //TODO
		"mandelbrot-8.c":    {}, //TODO
		"mandelbrot-9.c":    {}, //TODO
		"mandelbrot.c":      {}, //TODO
		"nbody-4.c":         {}, //TODO
		"nbody-5.c":         {}, //TODO
		"nbody-8.c":         {}, //TODO
		"nbody-9.c":         {}, //TODO
		"pidigits-6.c":      {}, //TODO
		"regex-redux-4.c":   {}, //TODO
		"regex-redux-5.c":   {}, //TODO
		"spectral-norm-4.c": {}, //TODO
		"spectral-norm-5.c": {}, //TODO
		"spectral-norm-6.c": {}, //TODO
		"spectral-norm.c":   {}, //TODO
	}

	m := map[string]*runResult{}
	var names []string
	collect := func(r *runResult) {
		nm := filepath.Base(r.name)
		ex := m[nm]
		if ex == nil {
			m[nm] = r
			names = append(names, nm)
			return
		}

		if ex.ccTime > r.ccTime {
			ex.ccTime = r.ccTime
		}
		if ex.eccTime > r.eccTime {
			ex.eccTime = r.eccTime
		}
	}
	binary := map[string]bool{
		"fasta-3.c":              true,
		"fasta-4.c":              true,
		"fasta-8.c":              true,
		"fasta-9.c":              true,
		"fasta.c":                true,
		"mandelbrot-2.c":         true,
		"mandelbrot-3.c":         true,
		"mandelbrot-4.c":         true,
		"mandelbrot-6.c":         true,
		"mandelbrot-7.c":         true,
		"mandelbrot-8.c":         true,
		"mandelbrot-9.c":         true,
		"mandelbrot.c":           true,
		"reverse-complement-2.c": true,
		"reverse-complement-4.c": true,
		"reverse-complement-5.c": true,
		"reverse-complement-6.c": true,
	}
	var rq, res, ok int
	limit := 1
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}

			fallthrough
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

		for i := 0; i < *oBestOf; i++ {
		more:
			select {
			case r := <-results:
				res++
				<-limiter
				switch r.err.(type) {
				case nil:
					ok++
					collect(r)
				case skipErr:
					t.Logf("%v: %v\n%s", r.name, r.err, r.out)
				default:
					t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
				}
				goto more
			case limiter <- struct{}{}:
				rq++
				if *oTrc {
					fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
				}
				opts := []string{
					"-D_GNU_SOURCE",
					"-I/usr/include/apr-1.0",
					"-IInclude",
					"-lapr-1",
					"-lgmp",
					"-lm",
					"-lpcre",
					"-lpcre2-8",
					"-pthread",
					fmt.Sprintf("-O%d", *oBenchO),
				}
				if s := *oMarch; s != "" {
					opts = append(opts, fmt.Sprintf("-march=%s", s))
				}
				if s := *oMtune; s != "" {
					opts = append(opts, fmt.Sprintf("-mtune=%s", s))
				}
				if *oOpenmp {
					opts = append(opts, "-fopenmp")
				}
				base := filepath.Base(pth)
				task := &runTask{
					c:               results,
					hasBinaryOutput: binary[base],
					opts:            opts,
					src:             pth,
				}
				switch {
				case strings.HasPrefix(base, "regex-redux"):
					switch runtime.GOOS {
					case "windows":
						panic(todo(""))
					default:
						task.cmd = "sh"
						task.args = []string{"-c", fmt.Sprintf("./%s < 5000000.txt", base[:len(base)-2])}
					}
				case strings.HasPrefix(base, "k-nucleotide"):
					switch runtime.GOOS {
					case "windows":
						panic(todo(""))
					default:
						task.cmd = "sh"
						task.args = []string{"-c", fmt.Sprintf("./%s < 25000000.txt", base[:len(base)-2])}
					}
				case strings.HasPrefix(base, "reverse-complement"):
					switch runtime.GOOS {
					case "windows":
						panic(todo(""))
					default:
						task.cmd = "sh"
						task.args = []string{"-c", fmt.Sprintf("./%s < 100000000.txt", base[:len(base)-2])}
					}
				}
				go task.run()
			}
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			collect(r)
		case skipErr:
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	benchmarkResults(t, rq, ok, names, m)
}

func benchmarkResults(t *testing.T, rq, ok int, names []string, m map[string]*runResult) {
	t.Logf("files %v, ok %v", rq, ok)
	if ok == 0 {
		return
	}

	sort.Strings(names)
	var ccDurations, eccDurations time.Duration
	t.Logf("  os/arch: %s/%s", runtime.GOOS, runtime.GOARCH)
	t.Logf("       cc: %s", systemCC)
	t.Logf("--version: %s", systemCCVersion)
	t.Logf("       -O: %d", *oBenchO)
	t.Logf("   -march: %s", *oMarch)
	t.Logf("   -mtune: %s", *oMtune)
	t.Logf(" -fopenmp: %v", *oOpenmp)
	t.Logf("  best of: %d", *oBestOf)
	t.Logf("                       src   ecc      cc      ecc/cc")
	var min, max float64
	geomean := 1.0
	for i, nm := range names {
		r := m[nm]
		coeff := float64(r.eccTime) / float64(r.ccTime)
		ccDurations += r.ccTime
		eccDurations += r.eccTime
		t.Logf("%25s: %7.3fs %7.3fs  %.3f%s", nm, r.eccTime.Seconds(), r.ccTime.Seconds(), coeff, star(coeff))
		switch {
		case i == 0:
			min, max = coeff, coeff
		default:
			if coeff < min {
				min = coeff
			}
			if coeff > max {
				max = coeff
			}
		}
		geomean *= coeff
	}
	geomean = math.Pow(geomean, 1/float64(len(names)))
	coeff := float64(eccDurations) / float64(ccDurations)
	t.Logf(
		"                   overall %7.3fs %7.3fs  %.3f%s (min %.3f%s max %.3f%s)",
		eccDurations.Seconds(),
		ccDurations.Seconds(),
		float64(eccDurations)/float64(ccDurations), star(coeff),
		min, star(min),
		max, star(max),
	)
	t.Logf("                   geomean                  %7.3f%s", geomean, star(geomean))
}

func star(n float64) string {
	if n < 1 {
		return "*"
	}

	return ""
}

func TestBenchmarksMir(t *testing.T) {
	const root = "/github.com/vnmakarov/mir/c-benchmarks"

	if testing.Short() {
		t.Skip("skipped: -short")
	}

	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	needFiles(t, root, []string{
		"simple_hash.h",
	})
	blacklist := map[string]struct{}{}
	if runtime.GOOS == "windows" && runtime.GOARCH == "amd64" {
		blacklist["except.c"] = struct{}{} //TODO
	}
	m := map[string]*runResult{}
	var names []string
	collect := func(r *runResult) {
		nm := filepath.Base(r.name)
		ex := m[nm]
		if ex == nil {
			m[nm] = r
			names = append(names, nm)
			return
		}

		if ex.ccTime > r.ccTime {
			ex.ccTime = r.ccTime
		}
		if ex.eccTime > r.eccTime {
			ex.eccTime = r.eccTime
		}
	}
	binary := map[string]bool{
		"mandelbrot.c": true,
	}
	var rq, res, ok int
	limit := 1
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}

			fallthrough
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

		for i := 0; i < *oBestOf; i++ {
		more:
			select {
			case r := <-results:
				res++
				<-limiter
				switch r.err.(type) {
				case nil:
					ok++
					collect(r)
				case skipErr:
					t.Logf("%v: %v\n%s", r.name, r.err, r.out)
				default:
					t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
				}
				goto more
			case limiter <- struct{}{}:
				rq++
				if *oTrc {
					fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
				}
				opts := []string{
					fmt.Sprintf("-O%d", *oBenchO),
				}
				if s := *oMarch; s != "" {
					opts = append(opts, fmt.Sprintf("-march=%s", s))
				}
				if s := *oMtune; s != "" {
					opts = append(opts, fmt.Sprintf("-mtune=%s", s))
				}
				go run(pth, binary[filepath.Base(pth)], true, false, results, opts...)
			}
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			collect(r)
		case skipErr:
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	benchmarkResults(t, rq, ok, names, m)
}

func TestBenchmarksCompCert(t *testing.T) {
	const root = "/github.com/AbsInt/CompCert/test/c/"

	if testing.Short() {
		t.Skip("skipped: -short")
	}

	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	needFiles(t, root, []string{
		"Results/knucleotide-input.txt",
		"endian.h",
	})
	blacklist := map[string]struct{}{}
	if runtime.GOOS == "linux" && runtime.GOARCH == "s390x" {
		blacklist["aes.c"] = struct{}{} //TODO
	}
	if runtime.GOOS == "windows" && runtime.GOARCH == "amd64" {
		blacklist["bisect.c"] = struct{}{} //TODO va_list
		blacklist["fftw.c"] = struct{}{}   //TODO va_list
		blacklist["perlin.c"] = struct{}{} //TODO va_list
		blacklist["qsort.c"] = struct{}{}  //TODO

	}
	binary := map[string]bool{
		"mandelbrot.c": true, //TODO
	}

	m := map[string]*runResult{}
	var names []string
	collect := func(r *runResult) {
		nm := filepath.Base(r.name)
		ex := m[nm]
		if ex == nil {
			m[nm] = r
			names = append(names, nm)
			return
		}

		if ex.ccTime > r.ccTime {
			ex.ccTime = r.ccTime
		}
		if ex.eccTime > r.eccTime {
			ex.eccTime = r.eccTime
		}
	}
	var rq, res, ok int
	limit := 1
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}

			fallthrough
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

		for i := 0; i < *oBestOf; i++ {
		more:
			select {
			case r := <-results:
				res++
				<-limiter
				switch r.err.(type) {
				case nil:
					ok++
					collect(r)
				case skipErr:
					t.Logf("%v: %v\n%s", r.name, r.err, r.out)
				default:
					t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
				}
				goto more
			case limiter <- struct{}{}:
				rq++
				if *oTrc {
					fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
				}
				opts := []string{
					fmt.Sprintf("-O%d", *oBenchO),
				}
				if s := *oMarch; s != "" {
					opts = append(opts, fmt.Sprintf("-march=%s", s))
				}
				if s := *oMtune; s != "" {
					opts = append(opts, fmt.Sprintf("-mtune=%s", s))
				}
				if *oOpenmp {
					opts = append(opts, "-fopenmp")
				}
				go run(pth, binary[filepath.Base(pth)], true, false, results, opts...)
			}
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			collect(r)
		case skipErr:
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	benchmarkResults(t, rq, ok, names, m)
}

func TestPCRE(t *testing.T) {
	if !installEcc(t) {
		t.Skip("set INSTALL_ECC=1 or add -install to execute this test")
	}

	const root = "/ftp.pcre.org/pub/pcre.tar.gz"
	archive, err := fs.Open(root)
	if err != nil {
		t.Fatal(err)
	}

	mustEmptyDir(t, tempDir)
	if err := ccgo.Untar(tempDir, archive, nil); err != nil {
		t.Fatal(err)
	}

	if err := ccgo.InDir(filepath.Join(tempDir, "pcre"), func() error {
		switch {
		case
			//TODO runtime.GOOS == "darwin" && runtime.GOARCH == "amd64",
			//TODO runtime.GOOS == "linux" && runtime.GOARCH == "386",
			runtime.GOOS == "linux" && runtime.GOARCH == "amd64":
			//TODO runtime.GOOS == "linux" && runtime.GOARCH == "arm",
			//TODO runtime.GOOS == "linux" && runtime.GOARCH == "arm64",
			//TODO runtime.GOOS == "linux" && runtime.GOARCH == "s390x":

			//TODO --enable-pcre16 fails
			//TODO --enable-jit need assembler support
			out, err := ccgo.Shell("sh", "-c", "CC=ecc ./configure --disable-cpp --enable-shared=no --enable-pcre16 --enable-pcre32 --enable-utf --enable-unicode-properties && make check")
			if err != nil {
				return errorf("err: %v\n%s", err, out)
			}
		default:
			// windows/amd64
			//TODO
			t.Skip("TODO") //TODO
		}

		return nil
	}); err != nil {
		t.Fatal(err)
	}
}

func TestCxgo(t *testing.T) {
	const root = "/github.com/cxgo"
	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	needFiles(t, root, []string{})
	blacklist := map[string]struct{}{
		"function forward decl 2.c": {}, //TODO
		"inet.c":                    {}, //TODO
	}
	if runtime.GOOS == "darwin" && runtime.GOARCH == "amd64" {
		blacklist["function forward decl 2.c"] = struct{}{} //TODO
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "s390x" {
		blacklist["function forward decl 2.c"] = struct{}{} //TODO
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "arm64" {
		blacklist["function forward decl 2.c"] = struct{}{} //TODO
	}
	if runtime.GOOS == "linux" && runtime.GOARCH == "386" {
		blacklist["enum fixed.c"] = struct{}{}              //TODO
		blacklist["enum no zero 2.c"] = struct{}{}          //TODO
		blacklist["enum no zero.c"] = struct{}{}            //TODO
		blacklist["enum start.c"] = struct{}{}              //TODO
		blacklist["enum zero.c"] = struct{}{}               //TODO
		blacklist["extern var.c"] = struct{}{}              //TODO
		blacklist["forward enum.c"] = struct{}{}            //TODO
		blacklist["function forward decl 2.c"] = struct{}{} //TODO
		blacklist["go ints.c"] = struct{}{}                 //TODO
		blacklist["named enum.c"] = struct{}{}              //TODO
		blacklist["return enum.c"] = struct{}{}             //TODO
		blacklist["struct and func.c"] = struct{}{}         //TODO
		blacklist["typedef enum.c"] = struct{}{}            //TODO
		blacklist["typedef primitive.c"] = struct{}{}       //TODO
		blacklist["unnamed enum.c"] = struct{}{}            //TODO
	}
	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]struct{}{}
	err = walk(root, func(pth string, fi os.FileInfo) error {
		if !strings.HasSuffix(pth, ".c") {
			return nil
		}

		switch {
		case re != nil:
			if !re.MatchString(pth) {
				return nil
			}
		default:
			if _, ok := blacklist[filepath.Base(pth)]; ok {
				return nil
			}
		}

	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				ok++
				delete(failed, r.name)
			case skipErr:
				delete(failed, r.name)
				t.Logf("%v: %v\n%s", r.name, r.err, r.out)
			default:
				t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			failed[pth] = struct{}{}
			go run(pth, false, false, true, results)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			ok++
			delete(failed, r.name)
		case skipErr:
			delete(failed, r.name)
			t.Logf("%v: %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s", v)
	}
}

func TestCSmith(t *testing.T) {
	return //TODO-

	if *oCSmith == 0 {
		return
	}

	fixedBugs := []string{
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 169375684",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 1833258637",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 1885311141",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 2205128324",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 2273393378",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 241244373",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 2517344771",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 2648215054",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 2876930815",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3043990076",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3100949894",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3126091077",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3130410542",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3329111231",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3363122597",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3365074920",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3578720023",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3645367888",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3919255949",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 4058772172",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 4101947480",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 4130344133",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 4146870674",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 517639208",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 56498550",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 890611563",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 963985971",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 1341005347",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 1346606946",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 1410729753",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 1473217979",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 1485689586",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 1872452393",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 1947158672",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 2232359541",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 2295327168",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 231122029",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 2452101147",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 251697819",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 2545154538",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 2551936195",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 3394478498",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 3688696230",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 4151780743",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 4275869822",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 541090131",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 828263634",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 885721321",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 964049230",
		"--bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-volatile-pointers --no-volatiles --paranoid -s 980538434",
		"--bitfields --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid --max-nested-struct-level 10 -s 1906742816",
		"--bitfields --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid --max-nested-struct-level 10 -s 3629008936",
		"--bitfields --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid --max-nested-struct-level 10 -s 612971101",
		"--no-bitfields --max-nested-struct-level 10 --no-const-pointers --no-consts --no-packed-struct --no-volatile-pointers --no-volatiles --paranoid -s 3720922579",
	}

	csmithDefaultArgs := strings.Join([]string{
		"--bitfields",                     // --bitfields | --no-bitfields: enable | disable full-bitfields structs (disabled by default).
		"--max-nested-struct-level", "10", // --max-nested-struct-level <num>: limit maximum nested level of structs to <num>(default 0). Only works in the exhaustive mode.
		"--no-const-pointers",    // --const-pointers | --no-const-pointers: enable | disable const pointers (enabled by default).
		"--no-consts",            // --consts | --no-consts: enable | disable const qualifier (enabled by default).
		"--no-volatile-pointers", // --volatile-pointers | --no-volatile-pointers: enable | disable volatile pointers (enabled by default).
		"--no-volatiles",         // --volatiles | --no-volatiles: enable | disable volatiles (enabled by default).
		"--paranoid",             // --paranoid | --no-paranoid: enable | disable pointer-related assertions (disabled by default).
	}, " ")

	csmith, err := exec.LookPath("csmith")
	if err != nil {
		t.Skip("csmith executable not found")
		return
	}

	var csp string
	switch runtime.GOOS {
	case "linux", "darwin":
		csp = "/usr/include/csmith"
	}
	if s := os.Getenv("CSMITH_PATH"); s != "" {
		csp = s
	}
	if csp != "" {
		csp = fmt.Sprintf("-I%s", csp)
	}

	argCh := make(chan string, 100)
	doneCh := make(chan struct{})

	go func() {
		for _, v := range fixedBugs {
			select {
			case <-doneCh:
				return
			default:
				argCh <- v
			}
		}
		for {
			select {
			case <-doneCh:
				return
			default:
				argCh <- csmithDefaultArgs
			}
		}
	}()

	go func() {
		<-time.After(*oCSmith)
		close(doneCh)
	}()

	mustEmptyDir(t, tempDir)
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if err := os.Chdir(tempDir); err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	blacklist := map[string]struct{}{}

	var rq, res, ok int
	limit := runtime.GOMAXPROCS(0)
	limiter := make(chan struct{}, limit)
	results := make(chan *runResult, limit)
	failed := map[string]string{}
	iFile := 0
loop:
	for {
		var args string
		select {
		case args = <-argCh:
			// ok
		case <-doneCh:
			break loop
		}

		switch {
		case re != nil:
			if !re.MatchString(args) {
				continue
			}

			close(doneCh)
		default:
			if _, ok := blacklist[filepath.Base(args)]; ok {
				continue
			}
		}

		pth := fmt.Sprintf("%06d.c", iFile)
		iFile++
	more:
		select {
		case r := <-results:
			res++
			<-limiter
			switch r.err.(type) {
			case nil:
				os.Remove(r.name)
				ok++
				delete(failed, r.name)
			case skipErr:
				os.Remove(r.name)
				delete(failed, r.name)
				t.Errorf("%v: ERROR %v\n%s", r.name, r.err, r.out)
				if *oFF {
					break loop
				}
			default:
				t.Errorf("%v: ERROR %v\n%s", r.name, r.err, r.out)
				if *oFF {
					break loop
				}
			}
			goto more
		case limiter <- struct{}{}:
			rq++
			if *oTrc {
				fmt.Fprintf(os.Stderr, "%v: %s\n", rq, pth)
			}
			csOut, err := exec.Command(csmith, strings.Split(args, " ")...).Output()
			if err != nil {
				t.Fatalf("%v\n%s", err, csOut)
			}

			if err := os.WriteFile(pth, csOut, 0660); err != nil {
				t.Fatalf("%s, %s: %v", pth, args, err)
			}

			failed[pth] = string(csOut)
			go (&runTask{
				c:         results,
				csmithSrc: csOut,
				opts:      []string{csp},
				src:       pth,
			}).runCSmith()
		}
	}
	for res != rq {
		r := <-results
		res++
		<-limiter
		switch r.err.(type) {
		case nil:
			os.Remove(r.name)
			ok++
			delete(failed, r.name)
		case skipErr:
			os.Remove(r.name)
			delete(failed, r.name)
			t.Errorf("%v: ERROR %v\n%s", r.name, r.err, r.out)
		default:
			t.Errorf("%v: ERROR %v\n%s", r.name, r.err, r.out)
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	if len(failed) == 0 {
		return
	}

	var a []string
	for k := range failed {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		t.Logf("FAIL %s\n%s", v, failed[v])
		if *oFF {
			break
		}
	}
	t.Logf("files %v, ok %v, failed %v", rq, ok, len(failed))
	for _, v := range a {
		t.Logf("FAIL %s: %s", v, extractCSmithOptions(failed[v]))
		if *oFF {
			break
		}
	}
}

func extractCSmithOptions(s string) string {
	const (
		tag  = "* Options:"
		tag2 = "* Seed:"
	)
	x := strings.Index(s, tag)
	s = s[x+len(tag):]
	x = strings.Index(s, "\n")
	s2 := s[x:]
	s = s[:x]
	s = strings.TrimSpace(s)
	if strings.Contains(s, " -s ") {
		return s
	}

	x = strings.Index(s2, tag2)
	s2 = s2[x+len(tag):]
	x = strings.Index(s2, "\n")
	s2 = s2[:x]
	return fmt.Sprintf("%s -s %s", s, strings.TrimSpace(s2))
}
