// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

#ifdef __PTRDIFF_TYPE__
typedef __PTRDIFF_TYPE__ ptrdiff_t;
#else
#error __PTRDIFF_TYPE__ undefined
#endif

#ifdef __SIZE_TYPE__
typedef __SIZE_TYPE__ size_t;
#else
#error __SIZE_TYPE__ undefined
#endif

#ifdef __WCHAR_TYPE__
typedef __WCHAR_TYPE__ wchar_t;
#else
#error __WCHAR_TYPE__ undefined
#endif

#ifdef __SIZEOF_INT128__
typedef struct { __INT64_TYPE__ lo, hi; } __int128_t;
typedef struct { __UINT64_TYPE__ lo, hi; } __uint128_t;
#endif;

#define __FUNCTION__ __func__
#define __PRETTY_FUNCTION__ __func__
#define __asm __asm__
#define __builtin_constant_p(x) __builtin_constant_p_impl(0, x)
#define __builtin_offsetof(type, member) ((__SIZE_TYPE__)&(((type*)0)->member))
#define __builtin_va_arg(ap, type) ((type)__ecc_va_arg(ap))
#define __builtin_va_copy(dst, src) __qbe_va_copy(dst, src)
#define __builtin_va_end(ap) __qbe_va_end(ap)
#define __builtin_va_start(ap, v) __ecc_va_start(&ap)
#define __extension__
#define __has_include(...) __has_include_impl(#__VA_ARGS__)
#define __has_include_impl(x)
#define __inline__ inline
#define __signed signed
#define asm __asm__

// OS https://sourceforge.net/p/predef/wiki/OperatingSystems/
// Architecture https://sourceforge.net/p/predef/wiki/Architectures/
#ifdef __gnu_linux__

	// linux/amd64
	#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)
	// https://stackoverflow.com/a/4958507
	typedef struct {
		unsigned int gp_offset;
		unsigned int fp_offset;
		void *overflow_arg_area;
		void *reg_save_area;
	} __builtin_va_list[1];

	// linux/386
	#elif defined(i386) || defined(__i386) || defined(__i386__)
	// Only by trial and error.
	// TODO need authoritative source.
	typedef char *__builtin_va_list;
	typedef long double __float128;


	// linux/arm
	#elif defined(__arm__)
	// Procedure Call Standard for the Arm Architecture, Release 2020Q2, Document number: IHI 0042J
	// https://documentation-service.arm.com/static/5fa187c2b1a7c5445f29028a?token=
	// 8.1.4 Additional Types, pg. 33
	typedef struct {
		void *__ap;
	} __builtin_va_list;

	// linux/arm64
	#elif defined(__aarch64__)
	// https://developer.arm.com/documentation/ihi0055/latest
	// Table 6, Definition of va_list
	typedef struct {
		void *__stack;
		void *__gr_top;
		void *__vr_top;
		int   __gr_offs;
		int   __vr_offs;
	} __builtin_va_list;

	// linux/s390x
	#elif defined(__s390x__)
	// Only by trial and error.
	// TODO need authoritative source.
	typedef struct {
		void *a[4];
	} __builtin_va_list[1];

	#else
	#error unknown/usupported linux architecture
	typedef void *__builtin_va_list;
	#endif

#elif defined(__APPLE__) && defined(__MACH__)

	// darwin/amd64
	#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)
	// According to https://en.wikipedia.org/wiki/X86_calling_conventions#List_of_x86_calling_conventions
	// the ABI is the same as for linux/amd64
	typedef struct {
		unsigned int gp_offset;
		unsigned int fp_offset;
		void *overflow_arg_area;
		void *reg_save_area;
	} __builtin_va_list[1];

	#else
	#error unknown/usupported darwin architecture
	typedef void *__builtin_va_list;
	#endif
	
#elif defined(__MINGW64__)

	// windows/amd64
	#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)
	// Only by trial and error.
	// TODO need authoritative source.
	typedef char *__builtin_va_list;
	typedef __builtin_va_list va_list;

	#else
	#error unknown/usupported windows architecture
	typedef void *__builtin_va_list;
	#endif

#else
#error unknown/usupported operating system
typedef void *__builtin_va_list;
#endif

#if defined(__MINGW32__) || defined(__MINGW64__)
int gnu_printf(const char *format, ...);
int gnu_scanf(const char *format, ...);
int ms_printf(const char *format, ...);
int ms_scanf(const char *format, ...);
#define _VA_LIST_DEFINED
#define __extension__
#endif

__UINT16_TYPE__ __builtin_bswap16(__UINT16_TYPE__ x);
__UINT32_TYPE__ __builtin_bswap32(__UINT32_TYPE__ x);
__UINT64_TYPE__ __builtin_bswap64(__UINT64_TYPE__ x);
char *__builtin___strcat_chk(char *dest, const char *src, size_t os);
char *__builtin___strcpy_chk(char *dest, const char *src, size_t os);
char *__builtin___strncpy_chk(char *dest, char *src, size_t n, size_t os);
char *__builtin_strchr(const char *s, int c);
char *__builtin_strcpy(char *dest, const char *src);
char *__builtin_strncpy(char *dest, const char *src, size_t n);
double __builtin_copysign( double x, double y );
double __builtin_fabs(double x);
double __builtin_huge_val(void);
double __builtin_inf(void);
double __builtin_nan(const char *str);
float __builtin_copysignf( float x, float y );
float __builtin_huge_valf(void);
float __builtin_inff(void);
float __builtin_nanf(const char *str);
int __builtin___snprintf_chk(char *s, size_t maxlen, int flag, size_t os, const char *fmt, ...);
int __builtin___sprintf_chk(char *s, int flag, size_t os, const char *fmt, ...);
int __builtin___vsnprintf_chk(char *s, size_t maxlen, int flag, size_t os, const char *fmt, __builtin_va_list ap);
int __builtin__snprintf_chk(char * str, size_t maxlen, int flag, size_t strlen, const char * format);
int __builtin_abs(int j);
int __builtin_add_overflow();
int __builtin_clrsb(int x);
int __builtin_clrsbl(long);
int __builtin_clrsbll(long long);
int __builtin_clz(unsigned int x);
int __builtin_clzl(unsigned long);
int __builtin_clzll(unsigned long long);
int __builtin_constant_p_impl();
int __builtin_ctz(unsigned int x);
int __builtin_ctzl(unsigned long);
int __builtin_ctzll(unsigned long long);
int __builtin_ffs(int i);
int __builtin_ffsl(long int i);
int __builtin_ffsll(long long);
int __builtin_isgreater();
int __builtin_isgreaterequal();
int __builtin_isinf(x);
int __builtin_isinff(float x);
int __builtin_isinfl(long double x);
int __builtin_isless();
int __builtin_islessequal();
int __builtin_islessgreater();
int __builtin_isnan(double);
int __builtin_isprint(int c);
int __builtin_isunordered();
int __builtin_memcmp(const void *s1, const void *s2, size_t n);
int __builtin_mul_overflow();
int __builtin_parity(unsigned int x);
int __builtin_parityl(unsigned long);
int __builtin_parityll(unsigned long long);
int __builtin_popcount(unsigned int x);
int __builtin_popcountl(unsigned long);
int __builtin_popcountll(unsigned long long);
int __builtin_printf(const char *format, ...);
int __builtin_setjmp();
int __builtin_signbit();
int __builtin_snprintf(char *str, size_t size, const char *format, ...);
int __builtin_sprintf(char *str, const char *format, ...);
int __builtin_strcmp(const char *s1, const char *s2);
int __builtin_sub_overflow();
long __builtin_expect(long exp, long c);
long double __builtin_huge_vall(void);
long double __builtin_infl(void);
long double __builtin_nan(const char *str);
long long __builtin_llabs(long long j);
size_t __builtin_object_size(void * ptr, int type);
size_t __builtin_strlen(const char *s);
void *__builtin___memcpy_chk(void *dest, const void *src, size_t n, size_t os);
void *__builtin___memmove_chk(void *dest, const void *src, size_t n, size_t os);
void *__builtin___memset_chk(void *dstpp, int c, size_t len, size_t dstlen);
void *__builtin_alloca(size_t size);
void *__builtin_frame_address (unsigned int level);
void *__builtin_malloc(size_t size);
void *__builtin_memcpy(void *dest, const void *src, size_t n);
void *__builtin_memmove(void *dest, const void *src, size_t n);
void *__builtin_memset(void *s, int c, size_t n);
void *__builtin_mmap(void *addr, size_t length, int prot, int flags, int fd, __INTPTR_TYPE__ offset);
void *__builtin_return_address(unsigned int level);
void *__ecc_va_arg(__builtin_va_list ap);
void __builtin_abort(void);
void __builtin_exit(int status);
void __builtin_free(void *ptr);
void __builtin_longjmp();
void __builtin_prefetch(const void *addr, ...);
void __builtin_trap(void);
void __builtin_unreachable(void);
void __ecc_va_start(__builtin_va_list *ap);
void __qbe_va_copy(__builtin_va_list dst, __builtin_va_list src);
void __qbe_va_end(__builtin_va_list ap);
