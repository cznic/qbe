rm -f log-benchmarks-*
for v in {7,8,9,10,11} ; do \
	date ; \
	go clean -testcache ; \
	CC=gcc-$v ECC_CPP=cpp-$v go test -v -timeout 24h -run TestBenchmarksGame |& tee log-benchmarks-$v ; \
done
