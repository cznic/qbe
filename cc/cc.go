// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate assets -package cc

package cc // import "modernc.org/qbe/cc"

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"modernc.org/cc/v3"
	"modernc.org/opt"
	"modernc.org/qbe"
)

const (
	version = "0.0.8-20210725"
)

// Version reports the version of the qbe package.
func Version() string { return fmt.Sprintf("%v %v/%v", version, runtime.GOOS, runtime.GOARCH) }

// Task represents a compilation job.
type Task struct {
	D               []string // -D argument. Read only.
	I               []string // -I argument. Read only.
	O               string   // -O argument. Read only.
	U               []string // -U argument. Read only.
	arch            string
	args            []string // As passed to NewTask.
	cpp             string
	includePaths    []string
	name            string // As passed to NewTask.
	o               string // -o argument.
	os              string
	predefined      string
	stderr          io.Writer
	stdout          io.Writer
	sysIncludePaths []string

	E         bool // -E, read only.
	fullPaths bool // -full-paths
	keepTmp   bool // -keep-tmp
	trigraphs bool // -trigraphs
}

// NewTask returns a newly created Task.
func NewTask(name string, args []string, stdout, stderr io.Writer) *Task {
	return &Task{
		arch:   env("TARGET_ARCH", env("GOARCH", runtime.GOARCH)),
		args:   args,
		cpp:    env("ECC_CPP", env("CPP", "cpp")),
		name:   name,
		os:     env("TARGET_OS", env("GOOS", runtime.GOOS)),
		stderr: stderr,
		stdout: stdout,
	}
}

// Main executes task.
func (t *Task) Main() (err error) {
	if dmesgs {
		dmesg("%v: %v %v, os %s, arch %s, cpp %s", origin(1), t.name, t.args, t.os, t.arch, t.cpp)

		defer func() {
			if err != nil {
				dmesg("%v: returning from Task.Main: %v", origin(1), err)
			}
		}()

	}

	t.predefined, t.includePaths, t.sysIncludePaths, err = cc.HostConfig(t.cpp)
	if err != nil {
		return err
	}

	stop := false
	opts := opt.NewSet()
	opts.Arg("D", true, func(arg, value string) error { t.D = append(t.D, value); return nil })
	opts.Arg("I", true, func(opt, arg string) error { t.I = append(t.I, arg); return nil })
	opts.Arg("O", true, func(opt, arg string) error { t.O = arg; return nil })
	opts.Arg("U", true, func(arg, value string) error { t.U = append(t.U, value); return nil })
	opts.Arg("o", false, func(opt, arg string) error { t.o = arg; return nil })
	opts.Opt("E", func(opt string) error { t.E = true; return nil })
	opts.Opt("full-paths", func(opt string) error { t.fullPaths = true; return nil })
	opts.Opt("keep-tmp", func(opt string) error { t.keepTmp = true; return nil })
	opts.Opt("trigraphs", func(opt string) error { t.trigraphs = true; return nil })
	opts.Opt("version", func(opt string) error {
		fmt.Fprintln(t.stderr, Version())
		stop = true
		return nil
	})
	opts.Opt("-version", func(opt string) error {
		fmt.Fprintln(t.stderr, Version())
		stop = true
		return nil
	})

	var in, replace []string
	if err := opts.Parse(t.args, func(s string) error {
		if strings.HasPrefix(s, "-") {
			return nil
		}

		if strings.HasSuffix(s, ".c") {
			in = append(in, s)
		}

		return nil
	}); err != nil {
		return errorf("%s: %v", t.name, err)
	}
	if stop {
		return nil
	}

	// https://pubs.opengroup.org/onlinepubs/9699919799/utilities/c99.html
	//
	// Headers whose names are enclosed in double-quotes ( "" ) shall be
	// searched for first in the directory of the file with the #include
	// line, then in directories named in -I options, and last in the usual
	// places
	includePaths := append([]string{"@"}, t.I...)
	includePaths = append(includePaths, t.includePaths...)
	includePaths = append(includePaths, t.sysIncludePaths...)

	// For headers whose names are enclosed in angle brackets ( "<>" ), the
	// header shall be searched for only in directories named in -I options
	// and then in the usual places.
	sysIncludePaths := append(t.I, t.sysIncludePaths...)

	if len(in) != 0 {
		produceQBE := false
		if strings.HasSuffix(t.o, ".qbe") {
			if len(in) != 1 {
				return errorf("%s: output set to a QBE file with multiple C input files", t.name)
			}

			produceQBE = true
		}

		var tmpDir, qbePath string
		if !produceQBE {
			tmpDir, err = ioutil.TempDir("", "ecc-")
			if err != nil {
				return errorf("%s: cannot create temporary directory: %v", t.name, err)
			}

			defer func() {
				if t.keepTmp {
					fmt.Fprintf(os.Stderr, "keeping temporary directory %s\n", tmpDir)
					return
				}

				os.RemoveAll(tmpDir)
			}()
		}

		qbeNames := map[string]struct{}{}
		for i, v := range in {
			baseName := filepath.Base(v)
			switch {
			case produceQBE:
				qbePath = t.o
			default:
				qbeName := baseName[:len(baseName)-len(".c")] + ".qbe"
				dir := tmpDir
				if _, ok := qbeNames[qbeName]; ok {
					dir = filepath.Join(tmpDir, fmt.Sprint(i))
					if err := os.Mkdir(dir, 0700); err != nil {
						return errorf("mkdir(%s): %v", dir, err)
					}
				}
				qbeNames[qbeName] = struct{}{}
				qbePath = filepath.Join(dir, qbeName)
			}
			replace = append(replace, qbePath)
			b, err := ioutil.ReadFile(v)
			if err != nil {
				return errorf("%s: cannot open input file: %v", t.name, err)
			}

			cfg := newDefaultConfig()
			if !t.trigraphs {
				cfg.Config3.DisableTrigraphs = true
			}
			if cfg.ABI, err = cc.NewABI(runtime.GOOS, runtime.GOARCH); err != nil {
				return err
			}

			sources := []cc.Source{
				{Name: "<predefined>", Value: t.predefined, DoNotCache: false},
				{Name: "<builtin>", Value: assets["/builtin.h"], DoNotCache: false},
			}

			if len(t.D) != 0 {
				var a []string
				for _, v := range t.D {
					if i := strings.IndexByte(v, '='); i > 0 {
						a = append(a, fmt.Sprintf("#define %s %s", v[:i], v[i+1:]))
						continue
					}

					a = append(a, fmt.Sprintf("#define %s 1", v))
				}
				a = append(a, "\n")
				sources = append(sources, cc.Source{Name: "<defines>", Value: strings.Join(a, "\n"), DoNotCache: true})
			}

			if len(t.U) != 0 {
				var a []string
				for _, v := range t.U {
					a = append(a, fmt.Sprintf("#undef %s", v))
				}
				a = append(a, "\n")
				sources = append(sources, cc.Source{Name: "<undefines>", Value: strings.Join(a, "\n"), DoNotCache: true})
			}

			sources = append(sources, cc.Source{Name: v, Value: string(b), DoNotCache: true})
			if t.E {
				cfg.PreprocessOnly = true
				if err := cc.Preprocess(cfg, includePaths, sysIncludePaths, sources, t.stdout); err != nil {
					return err
				}

				continue
			}

			ast, err := cc.Translate(cfg, includePaths, sysIncludePaths, sources)
			if err != nil {
				return err
			}

			f, err := os.Create(qbePath)
			if err != nil {
				return errorf("%s: cannot create file: %v", t.name, err)
			}

			w := bufio.NewWriter(f)
			gen, err := newGen(t, w, baseName, ast, cfg)
			if err != nil {
				return err
			}

			if err = gen.main(); err != nil {
				return err
			}

			if err = w.Flush(); err != nil {
				return err
			}

			if err = f.Close(); err != nil {
				return err
			}

			if produceQBE {
				return nil
			}
		}
	}
	if t.E {
		return nil
	}

	var args []string
	if t.O == "" {
		args = append(args, "-O1")
	}
	for _, v := range t.args {
		if v == "-full-paths" {
			continue
		}

		if len(in) != 0 && in[0] == v {
			v = replace[0]
			in = in[1:]
			replace = replace[1:]
		}
		args = append(args, v)
	}
	return qbe.NewTask(t.name, args, t.stdout, t.stderr).Main()
}

func newDefaultConfig() *cc.Config {
	return &cc.Config{
		Config3: cc.Config3{
			DisableTrigraphs: true,
			GCCStructs:       true,
			MaxSourceLine:    1 << 20,
			UnsignedEnums:    true,
		},
		CheckExternInlineFnBodies:             true,
		DoNotTypecheckAsm:                     true,
		EnableAssignmentCompatibilityChecking: true,
	}
}
