// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cc // import "modernc.org/qbe/cc"

import (
	"fmt"
	"math"
	"sort"
	"strings"

	"modernc.org/cc/v3"
	"modernc.org/qbe"
)

var (
	idAlloca = cc.String("alloca")
	idMain   = cc.String("main")
)

type declaratorInfo struct {
	d  *cc.Declarator
	op operand

	aggregate      bool // struct/union, array
	declared       bool
	escapes        bool
	hasInitializer bool
	static         bool
}

func (g *gen) externalDeclarationPrototypeAttrs(n *cc.ExternalDeclaration) {
	switch n.Case {
	case cc.ExternalDeclarationDecl: // Declaration
		g.tldDeclarationPrototypeAttrs(n.Declaration)
	}
}

func (g *gen) tldDeclarationPrototypeAttrs(n *cc.Declaration) {
	for list := n.InitDeclaratorList; list != nil; list = list.InitDeclaratorList {
		g.tldInitDeclaratorPrototypeAttrs(list.InitDeclarator)
	}
}

func (g *gen) tldInitDeclaratorPrototypeAttrs(n *cc.InitDeclarator) {
	d := n.Declarator
	t := d.Type()
	switch {
	case t.Kind() == cc.Function:
		declaratorAttrs := g.attrSpecList(d.AttributeSpecifierList)
		g.prototypeAttrs[d.Name()] = append(g.prototypeAttrs[d.Name()], declaratorAttrs...)
	}
}

func (g *gen) externalDeclaration(n *cc.ExternalDeclaration) {
	switch n.Case {
	case cc.ExternalDeclarationFuncDef: // FunctionDefinition
		g.functionDefinition(n.FunctionDefinition)
	case cc.ExternalDeclarationDecl: // Declaration
		g.tldDeclaration(n.Declaration)
	case cc.ExternalDeclarationAsm: // AsmFunctionDefinition
		// ignore
	case cc.ExternalDeclarationAsmStmt: // AsmStatement
		// ignore
	case cc.ExternalDeclarationEmpty: // ';'
		// ignore
	case cc.ExternalDeclarationPragma: // PragmaSTDC
		// ignore
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) tldDeclaration(n *cc.Declaration) {
	for list := n.InitDeclaratorList; list != nil; list = list.InitDeclaratorList {
		g.tldInitDeclarator(list.InitDeclarator, g.declarationSpecifiersAttrs(n.DeclarationSpecifiers), g.attrSpecList(list.InitDeclarator.AttributeSpecifierList))
	}
}

func (g *gen) line(n cc.Node) (r string) {
	// defer func() { //TODO-
	// 	r += fmt.Sprintf("\t%v: %v", origin(2), origin(3))
	// }()
	p := n.Position()
	return fmt.Sprintf("#line %d %q", p.Line, p.Filename)
}

func (g *gen) lineNL(n cc.Node) (r string) {
	// defer func() { //TODO-
	// 	r += fmt.Sprintf("\t%v: %v", origin(2), origin(3))
	// }()
	p := n.Position()
	return fmt.Sprintf("#line %d %q\n", p.Line, p.Filename)
}

func attrNodes(a []*cc.AttributeSpecifier) []cc.Node {
	r := make([]cc.Node, len(a))
	for i, v := range a {
		r[i] = v
	}
	return r
}

func (g *gen) tldInitDeclarator(n *cc.InitDeclarator, typeAttrs, varAttrs []*cc.AttributeSpecifier) {
	d := n.Declarator
	declaratorAttrs := g.attrSpecList(d.AttributeSpecifierList)
	t := d.Type()
	if !g.isStaticSize(d, t) {
		panic(todo("%v: variable type size %v %v", d.Position(), t, t.Kind()))
	}

	switch {
	case d.IsExtern():
		return // Defined elsewhere.
	case t.Kind() == cc.Function:
		return // Function prototype.
	case d.IsTypedefName:
		return
	case d.Linkage == cc.None:
		return
	case d.Linkage == cc.Internal && d.Read == 0 && d.Write == 0 && !d.AddressTaken:
		return
	}

	if _, ok := g.ast.TLD[d]; !ok {
		return
	}

	g.w("\n")
	if len(typeAttrs) != 0 {
		g.dumpAttrs(typeAttrs)
	}
	if len(varAttrs) != 0 {
		panic(todo("%v: %s", d.Position(), nodeSource(attrNodes(varAttrs)...)))
	}
	if len(declaratorAttrs) != 0 {
		g.dumpAttrs(declaratorAttrs)
	}
	g.w("\n%s", g.lineNL(d))
	if d.Linkage == cc.External {
		g.w("export ")
	}
	switch n.Case {
	case cc.InitDeclaratorDecl: // Declarator AttributeSpecifierList
		g.w("data $%s = align %d { z %d }\t# %v:", d.Name(), g.align(n, t.Align()), t.Size(), g.pos(d))
	case cc.InitDeclaratorInit: // Declarator AttributeSpecifierList '=' Initializer
		if !g.isConstInitializer(n.Initializer) {
			panic(todo("", n.Position(), t))
		}

		if n.Initializer.IsZero() {
			g.w("data $%s = align %d { z %d }\t# %v:", d.Name(), g.align(n, t.Align()), t.Size(), g.pos(d))
			break
		}

		if t.IsArithmeticType() {
			g.w("data $%s = align %d { %s ", d.Name(), g.align(n, t.Align()), g.baseType(n, t))
			e := n.Initializer.AssignmentExpression
			op := e.Operand
			switch {
			case t.IsIntegerType():
				g.w("%d }", op.Value())
			case t.Kind() == cc.Double:
				switch x := op.Value().(type) {
				case cc.Float32Value:
					g.w("%d }", math.Float64bits(float64(x)))
				case cc.Float64Value:
					g.w("%d }", math.Float64bits(float64(x)))
				case cc.Int64Value:
					g.w("%d }", math.Float64bits(float64(x)))
				case cc.Uint64Value:
					g.w("%d }", math.Float64bits(float64(x)))
				default:
					panic(todo("", d.Position()))
				}
			case t.Kind() == cc.Float:
				switch x := op.Value().(type) {
				case cc.Float32Value:
					g.w("%d }", math.Float32bits(float32(x)))
				case cc.Float64Value:
					g.w("%d }", math.Float32bits(float32(x)))
				case cc.Int64Value:
					g.w("%d }", math.Float32bits(float32(x)))
				case cc.Uint64Value:
					g.w("%d }", math.Float32bits(float32(x)))
				default:
					panic(todo("", d.Position()))
				}
			case t.Kind() == cc.LongDouble:
				switch x := op.Value().(type) {
				case cc.Int64Value:
					g.w("ld_%d }", x)
				case *cc.Float128Value:
					g.w("%s }", g.longDoubleString(d, x))
				default:
					panic(todo("%v: %T", d.Position(), x))
				}
			default:
				panic(todo("", n.Position(), t.Kind()))
			}
			g.w("\t# %v", g.pos(d))
			break
		}

		list := n.Initializer.List()
		if len(list) == 1 {
			e := list[0].AssignmentExpression
			op := e.Operand
			if g.isCharPtr(e, t) {
				if s, ok := op.Value().(cc.StringValue); ok && op.Offset() == 0 {
					g.w("ro data $%s = align %d { b %s, b 0 }", d.Name(), t.Align(), safeQuoteToASCII(cc.StringID(s).String()))
					break
				}
			}
		}

		in := g.newInitializer(t, "")
		g.initializer(in, list, 0)
		g.w("data $%s = align %d {", d.Name(), in.t.Align())
		in.emit()
		g.w(" }\t# %v:", g.pos(d))
		var a []*initializer
		a = append(a, in.more...)
		for len(a) != 0 {
			in := a[0]
			a = a[1:]
			g.w("\n\ndata %s = align %d {", in.nm, in.t.Align())
			in.emit()
			g.w(" }")
			a = append(a, in.more...)
		}
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) attrSpecList(n *cc.AttributeSpecifierList) (r []*cc.AttributeSpecifier) {
	for ; n != nil; n = n.AttributeSpecifierList {
		r = append(r, n.AttributeSpecifier)
	}
	return r
}

func (g *gen) isStringLit(v cc.Value) (r bool) {
	_, r = v.(cc.StringValue)
	return r
}

func (g *gen) isCharPtr(n cc.Node, t cc.Type) bool {
	if t.Kind() != cc.Ptr {
		return false
	}

	switch t.Elem().Kind() {
	case cc.Char, cc.SChar, cc.UChar, cc.Int8, cc.UInt8:
		return true
	}

	return false
}

func (g *gen) isCharArray(n cc.Node, t cc.Type) bool {
	if t.Kind() != cc.Array {
		return false
	}

	switch t.Elem().Kind() {
	case cc.Char, cc.SChar, cc.UChar, cc.Int8, cc.UInt8:
		return true
	}

	return false
}

func (g *gen) functionDefinition(n *cc.FunctionDefinition) {
	d := n.Declarator
	if d.IsExtern() && d.Type().Inline() {
		// https://gcc.gnu.org/onlinedocs/gcc/Inline.html
		//
		// If you specify both inline and extern in the function definition, then the
		// definition is used only for inlining. In no case is the function compiled on
		// its own, not even if you refer to its address explicitly. Such an address
		// becomes an external reference, as if you had only declared the function, and
		// had not defined it.
		//
		// This combination of inline and extern has almost the effect of a macro. The
		// way to use it is to put a function definition in a header file with these
		// keywords, and put another copy of the definition (lacking inline and extern)
		// in a library file. The definition in the header file causes most calls to
		// the function to be inlined. If any uses of the function remain, they refer
		// to the single copy in the library.
		return
	}

	if d.Linkage != cc.External && d.Read == 0 && !d.AddressTaken {
		return
	}

	g.f = n
	g.nextLabel = 0
	g.nextLocal = 0
	g.declaratorInfos = map[*cc.Declarator]*declaratorInfo{}
	g.compositeLiterals = map[*cc.PostfixExpression]operand{}

	defer func() {
		g.f = nil
		g.nextLabel = -1
		g.nextLocal = -1
		g.declaratorInfos = nil
		g.compositeLiterals = nil
	}()

	g.w("\n\n")
	g.fnAttrs(n)
	g.dumpAttrs(g.prototypeAttrs[d.Name()])
	g.w("\n%s", g.lineNL(d))
	if d.Linkage == cc.External {
		g.w("export ")
	}
	var rt string
	t := d.Type().Result()
	if g.isMain(d) {
		t = g.cInt
	}
	if t.Kind() != cc.Void {
		rt = g.abiType(n, t)
	}
	g.w("function %s $%s(", rt, d.Name())
	params := d.Type().Parameters()
out:
	switch {
	case g.isMain(d) && len(params) != 2:
		g.w("%s %s, %s %s", g.abiType(n, g.cInt), g.newVal(nil), g.abiType(n, g.cPtr), g.newVal(nil))
	default:
		for i, v := range params {
			if v.Type().Kind() == cc.Void {
				break out
			}

			d := v.Declarator()
			if !g.isStaticSize(d, d.Type()) {
				panic(todo("%v: variable type size %v %v", d.Position(), d.Type(), d.Type().Kind()))
			}

			info := g.registerObject(d, d, false)
			switch {
			case g.isVaList(d.Type()):
				g.w(":%s %s", qbe.VaList, info.op)
			case info.escapes:
				g.w("%s %%0%d", g.abiType(d, v.Type()), info.op.(*pval).n)
			default:
				g.w("%s %s", g.abiType(d, v.Type()), info.op)
			}
			if i != len(params)-1 {
				g.w(", ")
			}
		}
	}
	if d.Type().IsVariadic() {
		g.w(", ...")
	}
	g.w(") {\n@%d", g.newLabel())
	for _, v := range n.InitDeclarators {
		d := v.Declarator
		g.registerObject(d, d, v.Case == cc.InitDeclaratorInit)
	}
	var a []*cc.Declarator
	for k := range g.declaratorInfos {
		a = append(a, k)
	}
	sort.Slice(a, func(i, j int) bool {
		x, y := a[i], a[j]
		return x.Position().Offset < y.Position().Offset
	})
	for _, v := range a {
		info := g.declaratorInfos[v]
		d := info.d
		g.w("\n#qbec:map %s %s declared at %s:", info.op, d.Name(), d.Position())
	}
	for _, v := range a {
		info := g.declaratorInfos[v]
		d := info.d
		g.w("\n%s", g.line(d))
		t := d.Type()
		if g.isVaList(t) {
			if d.IsParameter {
				continue
			}

			g.w("\n\t%s =%s alloc%d %d\t# %v: %s", info.op, g.ptr, g.align(d, t.Align()), t.Size(), g.pos(d), d.Name())
			info.declared = true
			continue
		}

		if !info.static && (info.escapes || info.aggregate) {
			switch {
			case d.IsParameter:
				switch {
				case g.isRecordType(t):
					// nop
				case t.Kind() == cc.Array:
					g.w("\n\t%s =%s alloc%d %d\t# %v: %s", info.op, g.ptr, g.align(d, g.cPtr.Align()), g.cPtr.Size(), g.pos(d), d.Name())
					g.w("\n%s", g.line(d))
					g.w("\n\tstore%s %%0%d, %s", g.ptr, info.op.(*pval).n, info.op)
				default:
					g.w("\n\t%s =%s alloc%d %d\t# %v: %s", info.op, g.ptr, g.align(d, t.Align()), t.Size(), g.pos(d), d.Name())
					g.w("\n%s", g.line(d))
					g.w("\n\tstore%s %%0%d, %s", g.baseType(n, t), info.op.(*pval).n, info.op)
				}
			default:
				g.w("\n\t%s =%s alloc%d %d\t# %v: %s", info.op, g.ptr, g.align(d, t.Align()), t.Size(), g.pos(d), d.Name())
				info.declared = true
			}
		}
	}
	sort.Slice(n.CompositeLiterals, func(i, j int) bool {
		return n.CompositeLiterals[i].Token.Seq() < n.CompositeLiterals[j].Token.Seq()
	})
	for _, v := range n.CompositeLiterals {
		switch t := v.TypeName.Type(); {
		case g.isAggregateType(t):
			op := g.newPVal(nil)
			g.compositeLiterals[v] = op
			g.w("\n\t%s =%s copy 0\t# %v:", op, g.ptr, g.pos(v))
		default:
			panic(todo("", v.Position(), t))
		}
	}
	g.w("\n%s", g.line(&n.CompoundStatement.Token2))
	switch returns, _ := g.compoundStatement(n.CompoundStatement, nil, -1, -1); {
	case returns:
		g.w("\n\tret")
	default:
		switch ret := d.Type().Result(); ret.Kind() {
		case cc.Void:
			g.w("\n\tret")
		default:
			if g.isMain(d) {
				g.w("\n\tret 0")
				break
			}

			g.w("\n\tret %s", g.zero(n, ret))
		}
	}
	g.w("\n}")
	g.emitStaticLocal(n)
	g.emitInitializers()
}

func (g *gen) fnAttrs(n *cc.FunctionDefinition) {
	g.dumpAttrs(append(g.declarationSpecifiersAttrs(n.DeclarationSpecifiers)))
}

func (g *gen) dumpAttrs(a []*cc.AttributeSpecifier) {
	for _, v := range a {
		for list := v.AttributeValueList; list != nil; list = list.AttributeValueList {
			switch av := list.AttributeValue; av.Case {
			case cc.AttributeValueIdent: // IDENTIFIER
				g.w("\n#qbec:attribute %s", av.Token.Value)
			case cc.AttributeValueExpr: // IDENTIFIER '(' ExpressionList ')'
				g.w("\n#qbec:attribute %s%s", av.Token.Value, flatString(nodeSource(&av.Token2, av.ExpressionList, &av.Token3)))
			default:
				panic(todo("", av.Position(), strings.TrimSpace(nodeSource(av))))
			}
		}
	}
}

func (g *gen) declarationSpecifiersAttrs(ds *cc.DeclarationSpecifiers) (r []*cc.AttributeSpecifier) {
	for ; ds != nil; ds = ds.DeclarationSpecifiers {
		if ds.Case == cc.DeclarationSpecifiersAttribute {
			r = append(r, ds.AttributeSpecifier)
		}
	}
	return r
}

func (g *gen) emitInitializers() {
	if len(g.initializers) == 0 {
		return
	}

	g.w("\n")
	for _, v := range g.initializers {
		//TODO line info
		g.w("\ndata %s = align %d {", v.nm, v.t.Align())
		v.emit()
		g.w("}")
	}
	g.initializers = g.initializers[:0]
}

func (g *gen) emitStaticLocal(n *cc.FunctionDefinition) {
	var s []*declaratorInfo
	for _, v := range g.declaratorInfos {
		if v.static && !v.hasInitializer {
			s = append(s, v)
		}
	}
	if len(s) == 0 {
		return
	}

	sort.Slice(s, func(i, j int) bool {
		x, y := s[i], s[j]
		return x.d.Position().Offset < y.d.Position().Offset
	})
	for _, v := range s {
		t := v.d.Type()
		g.w("\n%s", g.line(v.d))
		g.w("\ndata %s = align %d { z %d }\t# %s/%s", v.op, g.align(v.d, t.Align()), t.Size(), n.Declarator.Name(), v.d.Name())
	}
}

func (g *gen) registerObject(nd cc.Node, d *cc.Declarator, hasInitializer bool) (r *declaratorInfo) {
	if x, ok := g.declaratorInfos[d]; ok {
		return x
	}

	if !d.IsParameter && g.isFunctionPrototype(d) {
		return nil
	}

	r = &declaratorInfo{
		d:              d,
		aggregate:      g.isAggregateDeclarator(d),
		hasInitializer: hasInitializer,
	}
	switch {
	case d.StorageClass == cc.Automatic && d.Linkage == cc.None:
		// function-local C variable
		switch {
		case g.localDeclaratorEscapes(d):
			r.escapes = true
			r.op = g.newPVal(nil)
		default:
			// Non escaping locals are non addressable scalars.
			r.op = g.newVal(nil)
		}
	case d.StorageClass == cc.Static && d.Linkage == cc.None:
		// function-local C static variable
		if d.Read == 0 && d.Write == 0 && !d.AddressTaken {
			return nil // dead variable
		}

		r.static = true
		switch {
		case r.aggregate:
			r.op = g.newStaticGlobalArray()
		default:
			r.op = g.newStaticGlobal()
		}
	}
	g.declaratorInfos[d] = r
	return r
}

func (g *gen) localDeclaratorEscapes(d *cc.Declarator) (r bool) {
	if d.StorageClass != cc.Automatic {
		panic(todo("%v: internal error: %s %v", d.Position(), d.Name(), d.StorageClass))
	}

	switch d.Type().Kind() {
	case cc.Array:
		return d.AddressTaken && d.IsParameter
	case cc.Struct, cc.Union:
		return false
	default:
		// Any local that must be addressable escapes.
		return d.AddressTaken
	}
}

func (g *gen) isFunctionPrototype(d *cc.Declarator) bool {
	return d.Type().Kind() == cc.Function && d.FunctionDefinition() == nil
}

func (g *gen) isMain(d *cc.Declarator) bool {
	return d.Linkage == cc.External && d.Name() == idMain
}

func (g *gen) declaration(n *cc.Declaration) {
	for list := n.InitDeclaratorList; list != nil; list = list.InitDeclaratorList {
		g.initDeclarator(list.InitDeclarator)
	}
}

func (g *gen) initDeclarator(n *cc.InitDeclarator) {
	d := n.Declarator
	t := d.Type()
	if !g.isStaticSize(d, t) {
		panic(todo("%v: variable type size %v %v", d.Position(), t, t.Kind()))
	}

	if d.Read == 0 && d.Write == 0 && !d.AddressTaken || t.Kind() == cc.Function {
		return
	}

	info := g.declaratorInfos[d]
	switch n.Case {
	case cc.InitDeclaratorDecl: // Declarator AttributeSpecifierList
		if info.escapes || info.aggregate || info.d.StorageClass == cc.Static || info.declared {
			break
		}

		if t.IsScalarType() {
			g.w("\n%s", g.line(d))
			g.w("\n\t%s =%s declare\t# %v: %s %s, %s", info.op, g.baseType(d, t), g.pos(d), d.Name(), t, t.Kind())
			break
		}

		panic(todo("%v: %v", n.Position(), t))
	case cc.InitDeclaratorInit: // Declarator AttributeSpecifierList '=' Initializer
		if d.Read == 0 && d.Write == 1 && n.Initializer.IsConst() {
			return
		}

		if d.StorageClass == cc.Static {
			switch {
			case g.isConstInitializer(n.Initializer):
				in := g.newInitializer(t, info.op.String())
				g.initializer(in, n.Initializer.List(), 0)
				g.initializers = append(g.initializers, in)
				return
			default:
				panic(todo("", d.Position()))
			}
		}

		list := n.Initializer.List()
		if t.IsScalarType() {
			e := list[0].AssignmentExpression
			src := g.assignmentExpression(nil, e, !g.isAggregateType(e.Operand.Type()), t, nil, -1, -1, -1)
			g.store(n, info.op, src, t, t, false)
			g.w("\t# %v: %s %s, %s", g.pos(d), d.Name(), t, t.Kind())
			break
		}

		if g.isConstInitializer(n.Initializer) {
			if !g.isAggregateType(t) {
				panic(todo("", d.Position()))
			}

			list := n.Initializer.List()
			if len(list) == 1 {
				e := list[0].AssignmentExpression
				if d := e.Declarator(); d != nil {
					g.w("\n%s", g.line(d))
					src := g.primaryExpressionDeclarator(nil, d, false, t, t)
					g.w("\n\tcall $memcpy(p %s, p %s, %s %d)", info.op, src, g.abiType(n, g.cSizeT), d.Type().Size())
					break
				}
			}

			op := g.newStaticGlobal()
			in := g.newInitializer(t, op.String())
			g.initializer(in, list, 0)
			g.initializers = append(g.initializers, in)
			g.w("\n%s", g.line(d))
			g.w("\n\tcall $memcpy(p %s, p %s, %s %d)", info.op, op, g.abiType(n, g.cSizeT), d.Type().Size())
			break
		}

		// Aggregate type, non-constant initializer.
		p := g.newPVal(nil)
		for _, in := range list {
			e := in.AssignmentExpression
			et := e.Operand.Type()
			off := in.Offset
			g.w("\n%s", g.line(e))
			switch {
			case off == 0:
				g.w("\n\t%s =%s copy %s", p, g.ptr, info.op)
			default:
				g.w("\n\t%s =%s add %s, %d", p, g.ptr, info.op, off)
			}
			if f := in.Field; f != nil {
				if f.IsBitField() {
					p.setBitField(&bitField{f, t})
				}
			}
			src := g.assignmentExpression(nil, e, !g.isAggregateType(e.Operand.Type()), et, nil, -1, -1, -1)
			switch {
			case et.Decay().IsScalarType():
				g.store(n, p, src, in.Type(), et, false)
			default:
				g.w("\n\tcall $memcpy(p %s, p %s, %s %d)", p, src, g.abiType(n, g.cSizeT), et.Size())
			}
		}
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) zero(n cc.Node, t cc.Type) string {
	if t.Decay().IsScalarType() {
		return "0"
	}

	r := g.newVal(nil)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s alloc%d %d", r, g.ptr, g.align(n, t.Align()), t.Size())
	return r.String()
}
