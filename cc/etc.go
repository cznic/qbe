// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cc // import "modernc.org/qbe/cc"

import (
	"fmt"
	gotoken "go/token"
	"os"
	"path/filepath"
	"runtime"
	"sort"
	"strings"

	"modernc.org/cc/v3"
)

var (
	_ = origin
	_ = trc
)

func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	fn = filepath.Base(fn)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
	}
	return fmt.Sprintf("%s:%d:%s", fn, fl, fns)
}

func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	pc, fn, fl, _ := runtime.Caller(1)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
	}
	r := fmt.Sprintf("%s:%d:%s: TODOTODO %s", fn, fl, fns, s) //TODOOK
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

func errorf(s string, args ...interface{}) error {
	e := fmt.Errorf(s, args...)
	return fmt.Errorf("%w (%s)", e, origin(2))
}

func env(name, deflt string) (r string) {
	r = deflt
	if s := os.Getenv(name); s != "" {
		r = s
	}
	return r
}

func (g *gen) align(nd cc.Node, n int) int {
	switch {
	case n <= 4:
		return 4
	case n <= 8:
		return 8
	case n <= 16:
		return 16
	case n <= 32:
		return 32
	default:
		g.errList.err(nd, "invalid/unsupported alignment: %d", n)
		return 32
	}
}

func (g *gen) pos(n cc.Node) (r gotoken.Position) {
	if n != nil {
		r = gotoken.Position(n.Position())
		if !g.task.fullPaths {
			r.Filename = filepath.Base(r.Filename)
		}
	}
	return r
}

func nodeSource(n ...cc.Node) (r string) {
	if len(n) == 0 {
		return ""
	}

	var a []*cc.Token
	for _, v := range n {
		cc.Inspect(v, func(n cc.Node, entry bool) bool {
			if !entry {
				return true
			}

			if x, ok := n.(*cc.Token); ok && x.Seq() != 0 {
				a = append(a, x)
			}
			return true
		})
	}
	sort.Slice(a, func(i, j int) bool {
		return a[i].Seq() < a[j].Seq()
	})
	w := 0
	seq := -1
	for _, v := range a {
		if n := v.Seq(); n != seq {
			seq = n
			a[w] = v
			w++
		}
	}
	a = a[:w]
	var b strings.Builder
	for _, v := range a {
		b.WriteString(v.Sep.String())
		b.WriteString(v.Src.String())
	}
	return b.String()
}

func flatString(s string) (r string) {
	r = strings.ReplaceAll(s, "\r", "")
	return strings.ReplaceAll(r, "\n", " ")
}
