// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cc // import "modernc.org/qbe/cc"

import (
	"fmt"
	"strings"

	"modernc.org/cc/v3"
	"modernc.org/qbe"
)

var (
	idAligned              = cc.String("aligned")
	idBuiltinConstantPImpl = cc.String("__builtin_constant_p_impl")
	idVaArg                = cc.String("__ecc_va_arg")
	idVaCopy               = cc.String("__ecc_va_copy")
	idVaStart              = cc.String("__ecc_va_start")
)

func (g *gen) expression(ret operand, n *cc.Expression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.ExpressionAssign: // AssignmentExpression
		return g.assignmentExpression(ret, n.AssignmentExpression, value, t, cases, brk, cont, zero)
	case cc.ExpressionComma: // Expression ',' AssignmentExpression
		g.expression(nil, n.Expression, false, t, cases, brk, cont, -1)
		return g.assignmentExpression(ret, n.AssignmentExpression, value, t, cases, brk, cont, zero)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) assignmentExpression(ret operand, n *cc.AssignmentExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.AssignmentExpressionCond: // ConditionalExpression
		return g.conditionalExpression(ret, n.ConditionalExpression, value, t, cases, brk, cont, zero)
	case cc.AssignmentExpressionAssign: // UnaryExpression '=' AssignmentExpression
		if g.isRecordType(n.UnaryExpression.Operand.Type()) {
			switch {
			case g.isRecordType(n.AssignmentExpression.Operand.Type()):
				dst := g.unaryExpression(nil, n.UnaryExpression, false, nil, cases, brk, cont, -1)
				src := g.assignmentExpression(nil, n.AssignmentExpression, false, n.AssignmentExpression.Operand.Type(), cases, brk, cont, -1)
				//TODO extend QBE copy to allow abi types
				g.w("\n\tcall $memcpy(p %s, p %s, %s %d)", dst, src, g.abiType(n, g.cSizeT), n.UnaryExpression.Operand.Type().Size())
				if zero > 0 {
					panic(todo("", n.Position()))
				}

				return dst
			default:
				panic(todo("%v:", n.Position()))
			}
		}

		dst := g.unaryExpression(nil, n.UnaryExpression, false, nil, cases, brk, cont, -1)
		e := n.AssignmentExpression
		src := g.assignmentExpression(nil, e, !g.isAggregateType(e.Operand.Type()), e.Operand.Type(), cases, brk, cont, -1)
		r := g.store(n, dst, src, n.UnaryExpression.Operand.Type(), e.Operand.Type(), value)
		if !value {
			return nil
		}

		if zero > 0 {
			return g.zeroMode(n, r, n.UnaryExpression.Operand.Type(), zero)
		}

		return g.convert(n, r, n.UnaryExpression.Operand.Type(), t)
	case cc.AssignmentExpressionMul: // UnaryExpression "*=" AssignmentExpression
		return g.assop(n, "mul", t, value, cases, brk, cont, zero)
	case cc.AssignmentExpressionDiv: // UnaryExpression "/=" AssignmentExpression
		return g.assop(n, g.u(n.Promote())+"div", t, value, cases, brk, cont, zero)
	case cc.AssignmentExpressionMod: // UnaryExpression "%=" AssignmentExpression
		return g.assop(n, g.u(n.Promote())+"rem", t, value, cases, brk, cont, zero)
	case cc.AssignmentExpressionAdd: // UnaryExpression "+=" AssignmentExpression
		return g.assop(n, "add", t, value, cases, brk, cont, zero)
	case cc.AssignmentExpressionSub: // UnaryExpression "-=" AssignmentExpression
		return g.assop(n, "sub", t, value, cases, brk, cont, zero)
	case cc.AssignmentExpressionLsh: // UnaryExpression "<<=" AssignmentExpression
		return g.assop(n, "shl", t, value, cases, brk, cont, zero)
	case cc.AssignmentExpressionRsh: // UnaryExpression ">>=" AssignmentExpression
		switch {
		case g.isSigned(n.Operand.Type()):
			return g.assop(n, "sar", t, value, cases, brk, cont, zero)
		default:
			return g.assop(n, "shr", t, value, cases, brk, cont, zero)
		}
	case cc.AssignmentExpressionAnd: // UnaryExpression "&=" AssignmentExpression
		return g.assop(n, "and", t, value, cases, brk, cont, zero)
	case cc.AssignmentExpressionXor: // UnaryExpression "^=" AssignmentExpression
		return g.assop(n, "xor", t, value, cases, brk, cont, zero)
	case cc.AssignmentExpressionOr: // UnaryExpression "|=" AssignmentExpression
		return g.assop(n, "or", t, value, cases, brk, cont, zero)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) assop(n *cc.AssignmentExpression, op string, t cc.Type, value bool, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	if zero > 0 {
		defer g.zeroModePost(n, &r, t, zero)
	}

	// UnaryExpression "*=" AssignmentExpression
	lprom := n.Promote()
	switch op {
	case "shl", "shr", "sar":
		lprom = n.UnaryExpression.Operand.Type()
	}
	rprom := n.Promote()
	lhs := g.unaryExpression(nil, n.UnaryExpression, false, nil, cases, brk, cont, -1)
	e := n.AssignmentExpression
	rhs := g.assignmentExpression(nil, e, !g.isAggregateType(e.Operand.Type()), lprom, cases, brk, cont, -1)
	if uet := n.UnaryExpression.Operand.Type().Decay(); uet.Kind() == cc.Ptr {
		if sz := uet.Elem().Size(); sz != 1 {
			v := g.newVal(nil)
			g.w("\n%s\n\t%s =%s mul %s, %d", g.line(n), v, g.baseType(n, uet), rhs, sz)
			rhs = v
		}
	}
	switch x := lhs.(type) {
	case *val:
		a := g.convert(n, x, n.UnaryExpression.Operand.Type(), lprom)
		b := g.convert(n, rhs, n.AssignmentExpression.Operand.Type(), rprom)
		r := g.newVal(nil)
		g.w("\n%s\n\t%s =%s %s %s, %s", g.line(n), r, g.baseType(n, lprom), op, a, b)
		g.w("\n%s\n\t%s =%s copy %s", g.line(n), x, g.baseType(n, n.UnaryExpression.Operand.Type()), r)
		if !value {
			return r
		}

		return g.convert(n, r, lprom, t)
	case *pval:
		r := g.newVal(nil)
		v := g.load(n, x, n.UnaryExpression.Operand.Type())
		v = g.convert(n, v, n.UnaryExpression.Operand.Type(), lprom)
		g.w("\n%s\n\t%s =%s %s %s, %s", g.line(n), r, g.baseType(n, lprom), op, v, rhs)
		g.store(n, x, r, n.UnaryExpression.Operand.Type(), lprom, false)
		if !value {
			return r
		}

		r = g.reduceAssignOp(n, r, n.UnaryExpression.Operand.Type(), n.AssignmentExpression.Operand.Type())
		return g.convert(n, r, lprom, t)
	default:
		panic(todo("%v: %T %v", n.Position(), x, x))
	}
}

func (g *gen) reduceAssignOp(n cc.Node, op *val, lt, rt cc.Type) *val {
	var bits int
	if lt.IsBitFieldType() {
		bits = lt.BitField().BitFieldWidth()
	}
	if rt.IsBitFieldType() {
		if bits2 := rt.BitField().BitFieldWidth(); bits2 > bits {
			bits = bits2
		}
	}
	if bits == 0 {
		return op
	}

	if !rt.IsSignedType() {
		return op
	}

	r := g.newVal(nil)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s and %s, %d", r, g.baseType(nil, lt), op, uint64(1)<<uint(bits)-1)
	return r
}

func (g *gen) load(n cc.Node, p operand, t cc.Type) operand {
	if g.isVaList(t) {
		switch vaInfo := g.vaInfo; {
		case vaInfo.IsPointer:
			return p
		case vaInfo.IsStruct:
			panic(todo("", n.Position()))
		case vaInfo.IsStructArray:
			panic(todo("", n.Position()))
		default:
			panic(todo("", n.Position()))
		}
	}

	if !t.IsScalarType() {
		switch {
		case t.Kind() == cc.Array:
			t = g.cPtr
		default:
			panic(todo("%v: %v (%v) %v", n.Position(), t, t.Alias(), t.Alias().Kind()))
		}
	}

	if bf := p.bitField(); bf != nil {
		return g.loadBitfield(n, p, t, bf)
	}

	r := g.newVal(nil)
	g.w("\n%s\n\t%s =%s load%s %s", g.line(n), r, g.baseType(n, t), g.loadType(n, t), p)
	return r
}

func (g *gen) loadBitfield(n cc.Node, p operand, t cc.Type, bf *bitField) operand {
	if bf.t.IsPacked() {
		return g.loadPackedBitfield(n, p, t, bf)
	}

	f := bf.f
	r0 := g.newVal(nil)
	bt := g.baseType(n, f.Type())
	w := f.BitFieldWidth()
	g.w("\n%s\n\t%s =%s load%s %s", g.line(n), r0, bt, g.loadType(n, f.Type()), p)
	sz := int(f.Type().Size()) * 8
	if f.Type().Size() < g.cInt.Size() {
		sz = int(g.cInt.Size()) * 8
	}
	r1 := g.newVal(nil)
	g.w("\n%s\n\t%s =%s shl %s, %d", g.line(n), r1, bt, r0, sz-w-f.BitFieldOffset())
	r := g.newVal(nil)
	switch {
	case g.isSigned(t):
		g.w("\n%s\n\t%s =%s sar %s, %d", g.line(n), r, bt, r1, sz-w)
	default:
		g.w("\n%s\n\t%s =%s shr %s, %d", g.line(n), r, bt, r1, sz-w)
	}
	return r

}

func (g *gen) loadPackedBitfield(n cc.Node, p operand, t cc.Type, bf *bitField) operand {
	f := bf.f
	st := bf.t
	w := f.BitFieldWidth()
	off := f.Offset()
	boff := f.BitFieldOffset()
	accessBits := boff + w
	accessBytes := ((accessBits + 7) &^ 7) >> 3
	switch {
	case accessBytes > 9 || off+uintptr(accessBytes) > st.Size():
		panic(todo("%v: internal error", n.Position()))
	case accessBytes > 8:
		m := f.Mask()
		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l loadl %s", r, p)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l and %[1]s, %d\t# %#[2]x", r, m)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l shr %[1]s, %d", r, boff)
		// Now get the overflow byte at dst+8
		p2 := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s add %s, 8", p2, g.ptr, p)
		r2 := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l loadub %s", r2, p2)
		w2 := w + boff - 64
		m = 1<<w2 - 1
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l and %[1]s, %[2]d\t# %#x", r2, m, m)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l shl %[1]s, %d", r2, w-w2)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l or %[1]s, %s", r, r2)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l shl %[1]s, %d", r, 64-w)
		switch {
		case g.isSigned(t):
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =l sar %[1]s, %d", r, 64-w)
		default:
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =l shr %[1]s, %d", r, 64-w)
		}
		return r
	default:
		loadType, _ := packedTypes(accessBytes)
		r0 := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s load%s %s", r0, g.baseType(n, f.Type()), loadType, p)
		sz := int(f.Type().Size()) * 8
		if f.Type().Size() < g.cInt.Size() {
			sz = int(g.cInt.Size()) * 8
		}
		r1 := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s shl %s, %d", r1, g.baseType(n, f.Type()), r0, sz-w-boff)
		r := g.newVal(nil)
		switch {
		case g.isSigned(t):
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s sar %s, %d", r, g.baseType(n, t), r1, sz-w)
		default:
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s shr %s, %d", r, g.baseType(n, t), r1, sz-w)
		}
		return r
	}
}

func (g *gen) store(n cc.Node, dest, source operand, dType, sType cc.Type, value bool) operand {
	if g.isRecordType(dType) {
		panic(todo("%v: %v %v <- %v %v", n.Position(), dType, dType.Kind(), sType, sType.Kind()))
	}

	if g.isRecordType(sType) {
		panic(todo("%v: %v %v <- %v %v", n.Position(), dType, dType.Kind(), sType, sType.Kind()))
	}

	if bf := dest.bitField(); bf != nil {
		return g.storeBitfield(n, dest, source, dType, sType, value, bf)
	}

	switch dst := dest.(type) {
	case *val:
		source = g.convert(n, source, sType, dType)
		g.w("\n\t%s =%s copy %s", dst, g.baseType(n, dType), source)
		return dest
	case *pval:
		source = g.convert(n, source, sType, dType)
		g.w("\n\tstore%s %s, %s", g.storeType(n, dType), source, dst)
		return source
	default:
		panic(todo("%v: %T", n.Position(), dst))
	}
}

func (g *gen) storeBitfield(n cc.Node, dest, source operand, dType, sType cc.Type, value bool, bf *bitField) operand {
	if bf.t.IsPacked() {
		return g.storePackedBitfield(n, dest, source, dType, sType, value, bf)
	}

	f := bf.f
	v := g.newVal(nil)
	bt := g.baseType(n, f.Type())
	m := f.Mask()
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s load%s %s", v, bt, g.loadType(n, f.Type()), dest)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s and %[1]s, %[3]d\t# ^%#x", v, bt, ^m, m)
	rhs := g.convert(n, source, sType, f.Type())
	g.w("\n%s", g.line(n))
	rhs2 := g.newVal(nil)
	g.w("\n\t%s =%s shl %s, %d", rhs2, bt, rhs, f.BitFieldOffset())
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s and %s, %d\t# %#[4]x", rhs2, bt, rhs2, m)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s or %[1]s, %[3]s", v, bt, rhs2)
	g.w("\n%s", g.line(n))
	g.w("\n\tstore%s %s, %s", g.storeType(n, f.Type()), v, dest)
	if !value {
		return nil
	}

	return g.load(n, dest, dType)
}

func (g *gen) storePackedBitfield(n cc.Node, dest, source operand, dType, sType cc.Type, value bool, bf *bitField) operand {
	f := bf.f
	st := bf.t
	w := f.BitFieldWidth()
	off := f.Offset()
	boff := f.BitFieldOffset()
	ft := f.Type()
	bt := g.baseType(n, ft)
	m := f.Mask()
	accessBits := boff + w
	accessBytes := ((accessBits + 7) &^ 7) >> 3
	switch {
	case accessBytes > 9 || off+uintptr(accessBytes) > st.Size():
		panic(todo("%v: internal error", n.Position()))
	case accessBytes > 8:
		v := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l loadl %s", v, dest)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l and %[1]s, %[2]d\t# ^%#x", v, ^m, m)
		rhs := g.convert(n, source, sType, ft)
		rhs2 := g.newVal(nil)
		rhs3 := g.newVal(nil)
		w2 := w + boff - 64
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l shr %s, %d", rhs2, rhs, w-w2)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =w copy %s", rhs3, rhs2)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l shl %[1]s, %d", rhs, boff)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l and %[1]s, %d\t# %#[2]x", rhs, m)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =l or %[1]s, %s", v, rhs)
		g.w("\n%s", g.line(n))
		g.w("\n\tstorel %s, %s", v, dest)
		// Now handle the overflow byte at dst+8
		m = 1<<w2 - 1
		p2 := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s add %s, 8", p2, g.ptr, dest)
		v2 := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =w loadub %s", v2, p2)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =w and %[1]s, %[2]d\t# ^%#x", v2, ^m, m)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =w and %[1]s, %d\t# %#[2]x", rhs3, m)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =w or %[1]s, %s", v2, rhs3)
		g.w("\n%s", g.line(n))
		g.w("\n\tstoreb %s, %s", v2, p2)
	default:
		loadType, storeType := packedTypes(accessBytes)
		v := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s load%s %s", v, bt, loadType, dest)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s and %[1]s, %[3]d\t# ^%#x", v, bt, ^m, m)
		rhs := g.convert(n, source, sType, ft)
		g.w("\n%s", g.line(n))
		rhs2 := g.newVal(nil)
		g.w("\n\t%s =%s shl %s, %d", rhs2, bt, rhs, boff)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s and %s, %d\t# %#[4]x", rhs2, bt, rhs2, m)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s or %[1]s, %[3]s", v, bt, rhs2)
		g.w("\n%s", g.line(n))
		g.w("\n\tstore%s %s, %s", storeType, v, dest)
	}
	if !value {
		return nil
	}

	return g.load(n, dest, dType)
}

func packedTypes(accessBytes int) (loadType, storeType string) {
	switch {
	case accessBytes == 1:
		return "ub", "b"
	case accessBytes == 2:
		return "uh", "h"
	case accessBytes <= 4:
		return "uw", "w"
	default:
		return "l", "l"
	}
}

func (g *gen) conditionalExpression(ret operand, n *cc.ConditionalExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.ConditionalExpressionLOr: // LogicalOrExpression
		return g.logicalOrExpression(ret, n.LogicalOrExpression, value, t, cases, brk, cont, zero)
	case cc.ConditionalExpressionCond: // LogicalOrExpression '?' Expression ':' ConditionalExpression
		return g.ternary(n, value, t, cases, brk, cont, zero)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) ternary(n *cc.ConditionalExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	if value && zero <= 0 {
		defer func() {
			r = g.convert(n, r, n.Operand.Type(), t)
		}()
	}
	if n.LogicalOrExpression.Operand.IsZero() {
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		return g.conditionalExpression(nil, n.ConditionalExpression, true, n.Operand.Type(), cases, brk, cont, -1)
	}

	if n.LogicalOrExpression.Operand.IsNonZero() {
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		return g.expression(nil, n.Expression, !g.isAggregateType(n.Expression.Operand.Type()), n.Operand.Type(), cases, brk, cont, -1)
	}

	if n.Expression == nil {
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		// https://gcc.gnu.org/onlinedocs/gcc/Conditionals.html
		//
		// 6.8 Conditionals with Omitted Operands
		//
		//	x ? : y => x ? x : y

		// LogicalOrExpression '?' ':' ConditionalExpression
		//
		//		%v = LogicalOrExpression
		//		jnz %v, @a, @b
		//	@a
		//		%r = %v
		//		jmp @z
		//	@b
		//		%r = ConditionalExpression
		//	@z
		r = g.newVal(nil)
		a := g.newLabel()
		b := g.newLabel()
		z := g.newLabel()
		v := g.logicalOrExpression(nil, n.LogicalOrExpression, true, n.LogicalOrExpression.Operand.Type(), cases, brk, cont, -1)
		g.w("\n%s", g.line(n.LogicalOrExpression))
		g.w("\n\tjnz %s, @%d, @%d", v, a, b)
		g.w("\n@%d", a)
		g.w("\n%s", g.line(n.LogicalOrExpression))
		g.w("\n\t%s =%s copy %s", r, g.baseType(n, n.Operand.Type()), v)
		g.w("\n%s", g.line(n.LogicalOrExpression))
		g.w("\n\tjmp @%d", z)
		g.w("\n@%d", b)
		v = g.conditionalExpression(nil, n.ConditionalExpression, !g.isAggregateType(n.ConditionalExpression.Operand.Type()), n.Operand.Type(), cases, brk, cont, -1)
		g.w("\n%s", g.line(n.ConditionalExpression))
		g.w("\n\t%s =%s copy %s", r, g.baseType(n, n.Operand.Type()), v)
		g.w("\n@%d", z)
		return r
	}

	if n.Expression.Operand.Type().Kind() != cc.Void {
		if n.ConditionalExpression.Operand.Type().Kind() == cc.Void {
			panic(todo("", n.Position()))
		}

		r = g.newVal(nil)
	}

	if zero > 0 {
		//		LogicalOrExpression(a)
		//		Expression(zero)
		//		jmp @b
		//	@a
		//		ConditionalExpression(zero)
		//	@b
		a := g.newLabel()
		b := g.newLabel()
		g.logicalOrExpression(nil, n.LogicalOrExpression, true, n.LogicalOrExpression.Operand.Type(), cases, brk, cont, a)
		g.expression(nil, n.Expression, !g.isAggregateType(n.Expression.Operand.Type()), n.Operand.Type(), cases, brk, cont, zero)
		g.w("\n%s", g.line(n.ConditionalExpression))
		g.w("\n\tjmp @%d", b)
		g.w("\n@%d", a)
		g.w("\n%s", g.line(n.ConditionalExpression))
		g.conditionalExpression(nil, n.ConditionalExpression, !g.isAggregateType(n.ConditionalExpression.Operand.Type()), n.Operand.Type(), cases, brk, cont, zero)
		g.w("\n@%d", b)
		g.w("\n%s", g.line(n))
		return nil
	}

	// LogicalOrExpression '?' Expression ':' ConditionalExpression
	//
	//		%v = LogicalOrExpression
	//		jnz %v, @a, @b
	//	@a
	//		%r = Expression
	//		jmp @z
	//	@b
	//		%r = ConditionalExpression
	//	@z
	a := g.newLabel()
	b := g.newLabel()
	z := g.newLabel()
	v := g.logicalOrExpression(nil, n.LogicalOrExpression, true, n.LogicalOrExpression.Operand.Type(), cases, brk, cont, -1)
	g.w("\n%s", g.line(n.LogicalOrExpression))
	g.w("\n\tjnz %s, @%d, @%d", v, a, b)
	g.w("\n@%d", a)
	g.w("\n%s", g.line(n.Expression))
	v = g.expression(nil, n.Expression, !g.isAggregateType(n.Expression.Operand.Type()), n.Operand.Type(), cases, brk, cont, -1)
	if r != nil {
		g.w("\n%s", g.line(n.Expression))
		g.w("\n\t%s =%s copy %s", r, g.baseType(n, n.Operand.Type()), v)
	}
	g.w("\n\tjmp @%d", z)
	g.w("\n@%d", b)
	v = g.conditionalExpression(nil, n.ConditionalExpression, !g.isAggregateType(n.ConditionalExpression.Operand.Type()), n.Operand.Type(), cases, brk, cont, -1)
	if r != nil {
		g.w("\n%s", g.line(n.ConditionalExpression))
		g.w("\n\t%s =%s copy %s", r, g.baseType(n, n.Operand.Type()), v)
	}
	g.w("\n@%d", z)
	return r
}

func (g *gen) logicalOrExpression(ret operand, n *cc.LogicalOrExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.LogicalOrExpressionLAnd: // LogicalAndExpression
		return g.logicalAndExpression(ret, n.LogicalAndExpression, value, t, cases, brk, cont, zero)
	case cc.LogicalOrExpressionLOr: // LogicalOrExpression "||" LogicalAndExpression
		if zero > 0 {
			//		lhs(a)
			//		jmp @nonZero
			//	@a
			//		rhs(zero)
			//	@nonZero
			a := g.newLabel()
			nonZero := g.newLabel()
			g.logicalOrExpression(nil, n.LogicalOrExpression, true, n.LogicalOrExpression.Operand.Type(), cases, brk, cont, a)
			g.w("\n%s", g.line(n.LogicalOrExpression))
			g.w("\n\tjmp @%d", nonZero)
			g.w("\n@%d", a)
			g.w("\n%s", g.line(n.LogicalAndExpression))
			g.logicalAndExpression(nil, n.LogicalAndExpression, true, n.LogicalAndExpression.Operand.Type(), cases, brk, cont, zero)
			g.w("\n@%d", nonZero)
			g.w("\n%s", g.line(n.LogicalAndExpression))
			return nil
		}

		//		%v = lhs
		//		%r = cne(t) %v, 0
		//		jnz %r, @z, @a
		//	@a
		//		%v2 = rhs
		//		%r = cne(t) %v2, 0
		//	@z
		a := g.newLabel()
		z := g.newLabel()
		v := g.logicalOrExpression(nil, n.LogicalOrExpression, true, n.LogicalOrExpression.Operand.Type(), cases, brk, cont, -1)
		r := g.newVal(nil)
		g.w("\n%s", g.line(n.LogicalOrExpression))
		g.w("\n\t%s =w cne%s %s, %s", r, g.baseType(n, n.LogicalOrExpression.Operand.Type()), v, g.zero(n, n.LogicalOrExpression.Operand.Type()))
		g.w("\n%s", g.line(n.LogicalOrExpression))
		g.w("\n\tjnz %s, @%d, @%d", r, z, a)
		g.w("\n@%d", a)
		g.w("\n%s", g.line(n.LogicalAndExpression))
		v = g.logicalAndExpression(nil, n.LogicalAndExpression, true, n.LogicalAndExpression.Operand.Type(), cases, brk, cont, -1)
		g.w("\n%s", g.line(n.LogicalAndExpression))
		g.w("\n\t%s =w cne%s %s, %s", r, g.baseType(n, n.LogicalAndExpression.Operand.Type()), v, g.zero(n, n.LogicalAndExpression.Operand.Type()))
		g.w("\n@%d", z)
		return g.convert(n, r, g.cInt, t)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) logicalAndExpression(ret operand, n *cc.LogicalAndExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.LogicalAndExpressionOr: // InclusiveOrExpression
		return g.inclusiveOrExpression(ret, n.InclusiveOrExpression, value, t, cases, brk, cont, zero)
	case cc.LogicalAndExpressionLAnd: // LogicalAndExpression "&&" InclusiveOrExpression
		if zero > 0 {
			//		lhs(zero)
			//		rhs(zero)
			g.logicalAndExpression(nil, n.LogicalAndExpression, true, n.LogicalAndExpression.Operand.Type(), cases, brk, cont, zero)
			g.inclusiveOrExpression(nil, n.InclusiveOrExpression, true, n.InclusiveOrExpression.Operand.Type(), cases, brk, cont, zero)
			return nil
		}

		//		%v = lhs
		//		%r = cne(t) %v, 0
		//		jnz %r, @a, @z
		//	@a
		//		%v2 = rhs
		//		%r = cne(t) %v2, 0
		//	@z
		a := g.newLabel()
		z := g.newLabel()
		v := g.logicalAndExpression(nil, n.LogicalAndExpression, true, n.LogicalAndExpression.Operand.Type(), cases, brk, cont, -1)
		r := g.newVal(nil)
		g.w("\n%s", g.line(n.LogicalAndExpression))
		g.w("\n\t%s =w cne%s %s, %s", r, g.baseType(n, n.LogicalAndExpression.Operand.Type()), v, g.zero(n, n.LogicalAndExpression.Operand.Type()))
		g.w("\n%s", g.line(n.LogicalAndExpression))
		g.w("\n\tjnz %s, @%d, @%d", r, a, z)
		g.w("\n@%d", a)
		v = g.inclusiveOrExpression(nil, n.InclusiveOrExpression, true, n.InclusiveOrExpression.Operand.Type(), cases, brk, cont, -1)
		g.w("\n%s", g.line(n.InclusiveOrExpression))
		g.w("\n\t%s =w cne%s %s, %s", r, g.baseType(n, n.InclusiveOrExpression.Operand.Type()), v, g.zero(n, n.InclusiveOrExpression.Operand.Type()))
		g.w("\n@%d", z)
		return g.convert(n, r, g.cInt, t)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) inclusiveOrExpression(ret operand, n *cc.InclusiveOrExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.InclusiveOrExpressionXor: // ExclusiveOrExpression
		return g.exclusiveOrExpression(ret, n.ExclusiveOrExpression, value, t, cases, brk, cont, zero)
	case cc.InclusiveOrExpressionOr: // InclusiveOrExpression '|' ExclusiveOrExpression
		prom := n.Promote()
		return g.arith(n, g.inclusiveOrExpression(nil, n.InclusiveOrExpression, true, prom, cases, brk, cont, -1), g.exclusiveOrExpression(nil, n.ExclusiveOrExpression, true, prom, cases, brk, cont, -1), "or", prom, t, zero)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) arith(n cc.Node, a, b operand, op string, opType, t cc.Type, zero int) (r operand) {
	r = g.newVal(nil)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s %s %s, %s", r, g.baseType(n, opType), op, a, b)
	if t == nil {
		return g.zeroMode(n, r, opType, zero)
	}

	return g.zeroMode(n, g.convert(n, r, opType, t), t, zero)
}

func (g *gen) exclusiveOrExpression(ret operand, n *cc.ExclusiveOrExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.ExclusiveOrExpressionAnd: // AndExpression
		return g.andExpression(ret, n.AndExpression, value, t, cases, brk, cont, zero)
	case cc.ExclusiveOrExpressionXor: // ExclusiveOrExpression '^' AndExpression
		prom := n.Promote()
		return g.arith(n, g.exclusiveOrExpression(nil, n.ExclusiveOrExpression, true, prom, cases, brk, cont, -1), g.andExpression(nil, n.AndExpression, true, prom, cases, brk, cont, -1), "xor", prom, t, zero)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) andExpression(ret operand, n *cc.AndExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.AndExpressionEq: // EqualityExpression
		return g.equalityExpression(ret, n.EqualityExpression, value, t, cases, brk, cont, zero)
	case cc.AndExpressionAnd: // AndExpression '&' EqualityExpression
		prom := n.Promote()
		return g.arith(n, g.andExpression(nil, n.AndExpression, true, prom, cases, brk, cont, -1), g.equalityExpression(nil, n.EqualityExpression, true, prom, cases, brk, cont, -1), "and", n.Promote(), t, zero)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) equalityExpression(ret operand, n *cc.EqualityExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.EqualityExpressionRel: // RelationalExpression
		return g.relationalExpression(ret, n.RelationalExpression, value, t, cases, brk, cont, zero)
	case cc.EqualityExpressionEq: // EqualityExpression "==" RelationalExpression
		return g.eq(n, "eq", t, cases, brk, cont, zero)
	case cc.EqualityExpressionNeq: // EqualityExpression "!=" RelationalExpression
		return g.eq(n, "ne", t, cases, brk, cont, zero)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) eq(n *cc.EqualityExpression, op string, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	prom := n.Promote()
	lhs := g.equalityExpression(nil, n.EqualityExpression, true, prom, cases, brk, cont, -1)
	rhs := g.relationalExpression(nil, n.RelationalExpression, true, prom, cases, brk, cont, -1)
	r = g.newVal(nil)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =w c%s%s %s, %s", r, op, g.baseType(n, prom), lhs, rhs)
	if zero > 0 {
		return g.zeroMode(n, r, g.cInt, zero)
	}

	return g.convert(n, r, g.cInt, t)
}

func (g *gen) relationalExpression(ret operand, n *cc.RelationalExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.RelationalExpressionShift: // ShiftExpression
		return g.shiftExpression(ret, n.ShiftExpression, value, t, cases, brk, cont, zero)
	case cc.RelationalExpressionLt: // RelationalExpression '<' ShiftExpression
		return g.rel(n, "lt", t, cases, brk, cont, zero)
	case cc.RelationalExpressionGt: // RelationalExpression '>' ShiftExpression
		return g.rel(n, "gt", t, cases, brk, cont, zero)
	case cc.RelationalExpressionLeq: // RelationalExpression "<=" ShiftExpression
		return g.rel(n, "le", t, cases, brk, cont, zero)
	case cc.RelationalExpressionGeq: // RelationalExpression ">=" ShiftExpression
		return g.rel(n, "ge", t, cases, brk, cont, zero)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) rel(n *cc.RelationalExpression, op string, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	prom := n.Promote()
	lhs := g.relationalExpression(nil, n.RelationalExpression, true, prom, cases, brk, cont, -1)
	rhs := g.shiftExpression(nil, n.ShiftExpression, true, prom, cases, brk, cont, -1)
	r = g.newVal(nil)
	g.w("\n%s", g.line(n))
	g.w("\n\t%v =w c%s%s%s %v, %v", r, g.su(prom), op, g.baseType(n, prom), lhs, rhs)
	if zero > 0 {
		return g.zeroMode(n, r, g.cInt, zero)
	}

	return g.convert(n, r, g.cInt, t)
}

func (g *gen) zeroMode(n cc.Node, r operand, rt cc.Type, zero int) operand {
	if zero <= 0 {
		return r
	}

	switch rt.Kind() {
	case cc.Int, cc.UInt:
		// ok
	default:
		g.w("\n%s", g.line(n))
		t := g.newVal(nil)
		g.w("\n\t%s =w cne%s %s, %s", t, g.baseType(n, rt), r, g.zero(n, rt))
		r = t
	}

	a := g.newLabel()
	g.w("\n%s", g.line(n))
	g.w("\n\tjnz %s, @%d, @%d", r, a, zero)
	g.w("\n@%d", a)
	g.w("\n%s", g.line(n))
	return nil
}

func (g *gen) shiftExpression(ret operand, n *cc.ShiftExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.ShiftExpressionAdd: // AdditiveExpression
		return g.additiveExpression(ret, n.AdditiveExpression, value, t, cases, brk, cont, zero)
	case cc.ShiftExpressionLsh: // ShiftExpression "<<" AdditiveExpression
		r = g.shift(n, "shl", cases, brk, cont)
	case cc.ShiftExpressionRsh: // ShiftExpression ">>" AdditiveExpression
		op := "shr"
		if g.isSigned(n.ShiftExpression.Operand.Type()) {
			op = "sar"
		}
		r = g.shift(n, op, cases, brk, cont)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
	r = g.reduceShift(n, r, n.ShiftExpression.Operand.Type())
	if !value {
		return r
	}

	return g.zeroMode(n, g.convert(n, r, n.Operand.Type(), t), t, zero)
}

func (g *gen) reduceShift(n cc.Node, op operand, t cc.Type) operand {
	if !t.IsBitFieldType() {
		return op
	}

	bits := t.BitField().BitFieldWidth()
	if bits < 32 {
		bits = 32
	}
	r := g.newVal(nil)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s and %s, %d", r, g.baseType(nil, t), op, uint64(1)<<uint(bits)-1)
	return r
}

func (g *gen) shift(n *cc.ShiftExpression, op string, cases map[*cc.LabeledStatement]int, brk, cont int) operand {
	a := g.shiftExpression(nil, n.ShiftExpression, true, n.ShiftExpression.Operand.Type(), cases, brk, cont, -1)
	b := g.additiveExpression(nil, n.AdditiveExpression, true, g.cInt, cases, brk, cont, -1)
	r := g.newVal(nil)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s %s %s, %s", r, g.baseType(n, n.ShiftExpression.Operand.Type()), op, a, b)
	return r
}

func (g *gen) additiveExpression(ret operand, n *cc.AdditiveExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.AdditiveExpressionMul: // MultiplicativeExpression
		return g.multiplicativeExpression(ret, n.MultiplicativeExpression, value, t, cases, brk, cont, zero)
	case cc.AdditiveExpressionAdd: // AdditiveExpression '+' MultiplicativeExpression
		r = g.add(n, t, cases, brk, cont)
	case cc.AdditiveExpressionSub: // AdditiveExpression '-' MultiplicativeExpression
		r = g.sub(n, t, cases, brk, cont)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
	return g.zeroMode(n, g.reduceBinOp(n, r, n.Operand.Type(), n.AdditiveExpression.Operand.Type(), n.MultiplicativeExpression.Operand.Type()), n.Operand.Type(), zero)
}

func (g *gen) reduceBinOp(n cc.Node, op operand, t, lt, rt cc.Type) operand {
	if t != nil && (!t.IsIntegerType() || t.IsSignedType()) {
		return op
	}

	if !rt.IsBitFieldType() {
		return op
	}

	if !lt.IsBitFieldType() {
		return op
	}

	bits := lt.BitField().BitFieldWidth()
	if bits2 := rt.BitField().BitFieldWidth(); bits2 > bits {
		bits = bits2
	}
	r := g.newVal(nil)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s and %s, %d", r, g.baseType(nil, t), op, uint64(1)<<uint(bits)-1)
	return r
}

func (g *gen) add(n *cc.AdditiveExpression, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont int) (r operand) {
	// AdditiveExpression '+' MultiplicativeExpression
	switch lhs, rhs := n.AdditiveExpression.Operand.Type(), n.MultiplicativeExpression.Operand.Type(); {
	case n.Operand.Type().IsArithmeticType():
		prom := n.Promote()
		return g.arith(n, g.additiveExpression(nil, n.AdditiveExpression, true, prom, cases, brk, cont, -1), g.multiplicativeExpression(nil, n.MultiplicativeExpression, true, prom, cases, brk, cont, -1), "add", prom, t, -1)
	case lhs.Decay().Kind() == cc.Ptr && rhs.IsArithmeticType():
		prom := g.cPtr
		p := g.additiveExpression(nil, n.AdditiveExpression, false, prom, cases, brk, cont, -1)
		switch x := p.(type) {
		case *val:
			// ok
		case *pval:
			if lhs.Kind() == cc.Array {
				break
			}

			p = g.val(n, p, g.cPtr)
		default:
			panic(todo("%v: %T %s", n.Token.Position(), x, x))
		}
		off := g.multiplicativeExpression(nil, n.MultiplicativeExpression, true, prom, cases, brk, cont, -1)
		if lhs.Elem().Size() != 1 {
			v := g.newVal(nil)
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s mul %s, %d", v, g.ptr, off, lhs.Elem().Size())
			off = v
		}
		return g.arith(n, p, off, "add", prom, nil, -1)
	case rhs.Decay().Kind() == cc.Ptr && lhs.IsArithmeticType():
		prom := g.cPtr
		off := g.additiveExpression(nil, n.AdditiveExpression, true, prom, cases, brk, cont, -1)
		if rhs.Elem().Size() != 1 {
			v := g.newVal(nil)
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s mul %s, %d", v, g.ptr, off, rhs.Elem().Size())
			off = v
		}
		p := g.multiplicativeExpression(nil, n.MultiplicativeExpression, false, prom, cases, brk, cont, -1)
		switch x := p.(type) {
		case *val:
			// ok
		case *pval:
			// ok
		default:
			panic(todo("%v: %T %s", n.Token.Position(), x, x))
		}
		return g.arith(n, p, off, "add", prom, nil, -1)
	default:
		panic(todo("", n.Position(), lhs, rhs, n.Operand.Type()))
	}
}

func (g *gen) sub(n *cc.AdditiveExpression, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont int) (r operand) {
	// AdditiveExpression '-' MultiplicativeExpression
	prom := n.Promote()
	switch lhs, rhs := n.AdditiveExpression.Operand.Type(), n.MultiplicativeExpression.Operand.Type(); {
	case lhs.IsArithmeticType() && n.Operand.Type().IsArithmeticType():
		return g.arith(n, g.additiveExpression(nil, n.AdditiveExpression, true, prom, cases, brk, cont, -1), g.multiplicativeExpression(nil, n.MultiplicativeExpression, true, prom, cases, brk, cont, -1), "sub", n.Promote(), t, -1)
	case lhs.Decay().Kind() == cc.Ptr && rhs.IsArithmeticType():
		prom := g.cPtr
		p := g.additiveExpression(nil, n.AdditiveExpression, false, prom, cases, brk, cont, -1)
		switch x := p.(type) {
		case *val:
			// ok
		case *pval:
			if lhs.Kind() == cc.Array {
				break
			}

			p = g.val(n, p, lhs)
		default:
			panic(todo("%v: %T %s", n.Token.Position(), x, x))
		}
		rhs := g.multiplicativeExpression(nil, n.MultiplicativeExpression, true, prom, cases, brk, cont, -1)
		if lhs.Elem().Size() != 1 {
			v := g.newVal(nil)
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s mul %s, %d", v, g.ptr, rhs, lhs.Elem().Size())
			rhs = v
		}
		return g.arith(n, p, rhs, "sub", prom, nil, -1)
	case lhs.Decay().Kind() == cc.Ptr && rhs.Decay().Kind() == cc.Ptr:
		r := g.arith(n, g.additiveExpression(nil, n.AdditiveExpression, true, prom, cases, brk, cont, -1), g.multiplicativeExpression(nil, n.MultiplicativeExpression, true, prom, cases, brk, cont, -1), "sub", n.Promote(), t, -1)
		if sz := lhs.Decay().Elem().Size(); sz != 1 {
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s div %s, %d", r, g.ptr, r, sz)
		}
		return r
	default:
		panic(todo("", n.Position(), lhs, rhs))
	}
}

func (g *gen) multiplicativeExpression(ret operand, n *cc.MultiplicativeExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	prom := n.Promote()
	switch n.Case {
	case cc.MultiplicativeExpressionCast: // CastExpression
		return g.castExpression(ret, n.CastExpression, value, t, cases, brk, cont, zero)
	case cc.MultiplicativeExpressionMul: // MultiplicativeExpression '*' CastExpression
		r = g.arith(n, g.multiplicativeExpression(nil, n.MultiplicativeExpression, true, prom, cases, brk, cont, -1), g.castExpression(nil, n.CastExpression, true, prom, cases, brk, cont, -1), "mul", n.Promote(), t, -1)
	case cc.MultiplicativeExpressionDiv: // MultiplicativeExpression '/' CastExpression
		r = g.arith(n, g.multiplicativeExpression(nil, n.MultiplicativeExpression, true, prom, cases, brk, cont, -1), g.castExpression(nil, n.CastExpression, true, prom, cases, brk, cont, -1), g.u(prom)+"div", prom, t, -1)
	case cc.MultiplicativeExpressionMod: // MultiplicativeExpression '%' CastExpression
		r = g.arith(n, g.multiplicativeExpression(nil, n.MultiplicativeExpression, true, prom, cases, brk, cont, -1), g.castExpression(nil, n.CastExpression, true, prom, cases, brk, cont, -1), g.u(prom)+"rem", prom, t, -1)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}

	r = g.reduceBinOp(n, r, n.Operand.Type(), n.MultiplicativeExpression.Operand.Type(), n.CastExpression.Operand.Type())
	return g.zeroMode(n, g.convert(n, r, prom, t), t, zero)
}

func (g *gen) castExpression(ret operand, n *cc.CastExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.CastExpressionUnary: // UnaryExpression
		return g.unaryExpression(ret, n.UnaryExpression, value, t, cases, brk, cont, zero)
	case cc.CastExpressionCast: // '(' TypeName ')' CastExpression
		from := n.CastExpression.Operand.Type()
		to := n.TypeName.Type()
		switch ce := g.castExpression(nil, n.CastExpression, true, from, cases, brk, cont, -1); {
		case ce.isVaArg():
			switch {
			case g.isRecordType(to):
				r = g.newVal(ret)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =%s vaarg %s", r, g.abiType(n, to), ce)
			default:
				r = g.newVal(ret)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =%s vaarg %s", r, g.baseType(n, to), ce)
			}
		default:
			r = g.convert(n, ce, from, to)
		}
		if !value {
			return r
		}

		if zero > 0 {
			panic(todo("", n.Position()))
		}

		return g.convert(n, r, to, t)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) unaryExpression(ret operand, n *cc.UnaryExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.UnaryExpressionPostfix: // PostfixExpression
		return g.postfixExpression(ret, n.PostfixExpression, value, t, cases, brk, cont, zero)
	case cc.UnaryExpressionInc: // "++" UnaryExpression
		return g.preInc(n, "add", t, value, cases, brk, cont, zero)
	case cc.UnaryExpressionDec: // "--" UnaryExpression
		return g.preInc(n, "sub", t, value, cases, brk, cont, zero)
	case cc.UnaryExpressionAddrof: // '&' CastExpression
		p := g.castExpression(nil, n.CastExpression, false, nil, cases, brk, cont, -1)
		switch x := p.(type) {
		case *val:
			if zero > 0 {
				panic(todo("", n.Position()))
			}

			return x
		case *pval:
			return g.zeroMode(n, x.val, g.cPtr, zero)
		default:
			panic(todo("%v: %T %s", n.Token.Position(), x, x))
		}
	case cc.UnaryExpressionDeref: // '*' CastExpression
		p := g.castExpression(nil, n.CastExpression, false, nil, cases, brk, cont, -1)
		if !value {
			switch x := p.(type) {
			case *val:
				if n.Operand.Type().Kind() == cc.Function {
					return p
				}

				return g.pval(n, p)
			case *pval:
				if n.Operand.Type().Kind() == cc.Function {
					return p
				}

				p = g.load(n, p, g.cPtr)
				return g.pval(n, p)
			default:
				panic(todo("%v: %T %s", n.Token.Position(), x, x))
			}
		}

		p = g.val(n, p, g.cPtr)
		if n.Operand.Type().Kind() == cc.Function {
			if zero > 0 {
				panic(todo("", n.Position()))
			}

			return p
		}

		r := g.load(n, p, n.CastExpression.Operand.Type().Elem())
		return g.zeroMode(n, g.convert(n, r, n.CastExpression.Operand.Type().Elem(), t), t, zero)
	case cc.UnaryExpressionPlus: // '+' CastExpression
		return g.zeroMode(n, g.castExpression(nil, n.CastExpression, true, t, cases, brk, cont, -1), t, zero)
	case cc.UnaryExpressionMinus: // '-' CastExpression
		ot := n.Operand.Type()
		e := g.castExpression(nil, n.CastExpression, true, ot, cases, brk, cont, -1)
		if zero > 0 {
			return g.zeroMode(n, e, ot, zero)
		}

		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		switch {
		case ot.IsIntegerType():
			g.w("\n\t%s =%s sub 0, %v", r, g.baseType(n, ot), e)
		case ot.Kind() == cc.Float:
			g.w("\n\t%s =%s mul s_-1, %v", r, g.baseType(n, ot), e)
		case ot.Kind() == cc.Double:
			g.w("\n\t%s =%s mul d_-1, %v", r, g.baseType(n, ot), e)
		case ot.Kind() == cc.LongDouble:
			g.w("\n\t%s =%s mul ld_-1, %v", r, g.baseType(n, ot), e)
		default:
			panic(todo("%v: %v", n.Position(), ot))
		}
		return g.convert(n, r, n.Operand.Type(), t)
	case cc.UnaryExpressionCpl: // '~' CastExpression
		v := g.castExpression(nil, n.CastExpression, true, n.Operand.Type(), cases, brk, cont, -1)
		if zero > 0 {
			return g.zeroMode(n, v, n.Operand.Type(), zero)
		}

		v = g.convert(n, v, n.Operand.Type(), t)
		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s xor %s, -1", r, g.baseType(n, t), v)
		return r
	case cc.UnaryExpressionNot: // '!' CastExpression
		ct := n.CastExpression.Operand.Type()
		v := g.castExpression(nil, n.CastExpression, true, ct, cases, brk, cont, -1)
		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =w ceq%s %s, %s", r, g.baseType(n, ct), v, g.zero(n, ct))
		if zero > 0 {
			return g.zeroMode(n, r, g.cInt, zero)
		}

		return g.convert(n, r, n.Operand.Type(), t)
	case cc.UnaryExpressionSizeofExpr: // "sizeof" UnaryExpression
		return g.sizeofExpr(n, t, cases, brk, cont, zero)
	case cc.UnaryExpressionSizeofType: // "sizeof" '(' TypeName ')'
		return g.sizeofType(n, n.TypeName.Type(), t, cases, brk, cont, zero)
	case cc.UnaryExpressionLabelAddr: // "&&" IDENTIFIER
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		panic(todo("%v: %v", n.Position(), n.Case))
	case cc.UnaryExpressionAlignofExpr: // "_Alignof" UnaryExpression
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		al := n.UnaryExpression.Operand.Type().Align()
		if d := n.UnaryExpression.Declarator(); d != nil {
			sc := d.LexicalScope()
			a := sc[d.Name()]
			for _, v := range a {
				d := v.(*cc.Declarator)
				attrs := g.attrSpecList(d.AttributeSpecifierList)
			next:
				for _, attr := range attrs {
					for list := attr.AttributeValueList; list != nil; list = list.AttributeValueList {
						switch av := list.AttributeValue; av.Case {
						case cc.AttributeValueExpr: // IDENTIFIER '(' ExpressionList ')'
							if av.Token.Value != idAligned {
								continue
							}

							switch x := av.ExpressionList.AssignmentExpression.Operand.Value().(type) {
							case cc.Int64Value:
								if v := int(x); v > al {
									al = v
								}
							case cc.Uint64Value:
								if v := int(x); v > al {
									al = v
								}
							default:
								continue next
							}
						}
					}
				}
			}
		}

		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s copy %d", r, g.baseType(n, t), al)
		return r
	case cc.UnaryExpressionAlignofType: // "_Alignof" '(' TypeName ')'
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s copy %d", r, g.baseType(n, t), n.TypeName.Type().Align())
		return r
	case cc.UnaryExpressionImag: // "__imag__" UnaryExpression
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		panic(todo("%v: %v", n.Position(), n.Case))
	case cc.UnaryExpressionReal: // "__real__" UnaryExpression
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		panic(todo("%v: %v", n.Position(), n.Case))
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) zeroModePost(n cc.Node, r *operand, t cc.Type, zero int) {
	op := *r
	if op == nil {
		return
	}

	a := g.newLabel()
	x := g.newVal(nil)
	g.w("\n\t%s =w cne%s %s, %s", x, g.baseType(n, t), op, g.zero(n, t))
	g.w("\n\tjnz %s, @%d, @%d", x, a, zero)
	g.w("\n@%d", a)
	*r = nil
}

func (g *gen) sizeofExpr(n *cc.UnaryExpression, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	if zero > 0 {
		defer g.zeroModePost(n, &r, t, zero)
	}

	st := n.UnaryExpression.Operand.Type()
	if g.isStaticSize(n, st) {
		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s copy %d", r, g.baseType(n, t), n.Operand.Value())
		return r
	}

	panic(todo("%v: sizeof %v %v", n.Position(), st, st.Kind()))
}

func (g *gen) sizeofType(n cc.Node, st, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	if zero > 0 {
		defer g.zeroModePost(n, &r, t, zero)
	}

	if g.isStaticSize(n, st) {
		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s copy %d", r, g.baseType(n, t), st.Size())
		return r
	}

	//TODO must introduce a local that captures the size at declaration as the VLA
	// length expression may evaluate to a different value later.
	panic(todo("%v: variable type size %v %v", n.Position(), st, st.Kind()))

	switch st.Kind() {
	case cc.Array:
		if !g.isStaticSize(n, st.Elem()) {
			panic(todo("%v: sizeof %v %v", n.Position(), st, st.Kind()))
		}

		len := g.assignmentExpression(nil, st.LenExpr(), true, t, cases, brk, cont, -1)
		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s mul %s, %d", r, g.baseType(n, t), len, st.Elem().Size())
		return r
	case cc.Struct:
		if nf := st.NumField(); nf == 1 {
			return g.sizeofType(n, st.FieldByIndex([]int{0}).Type(), t, cases, brk, cont, -1)
		}
	}

	panic(todo("%v: sizeof %v %v", n.Position(), st, st.Kind()))
}

func (g *gen) isStaticSize(n cc.Node, t cc.Type) bool {
	if t.IsScalarType() {
		return true
	}

	switch t.Kind() {
	case cc.Array:
		if g.isVLA(t) {
			return false
		}

		if g.isStaticSize(n, t.Elem()) {
			return true
		}

		panic(todo("%v: %v %v, %v", n.Position(), t, t.Kind(), t.Elem()))
	case cc.Struct, cc.Union:
		nf := t.NumField()
		for i := []int{0}; i[0] < nf; i[0]++ {
			if f := t.FieldByIndex(i); !g.isStaticSize(n, f.Type()) {
				return false
			}
		}

		return true
	case cc.Function, cc.Void:
		return true
	case cc.Vector:
		panic(todo("vector"))
	}

	panic(todo("%v: %v %v", n.Position(), t, t.Kind()))
}

func (g *gen) isVLA(t cc.Type) bool { return t.Kind() == cc.Array && t.IsVLA() }

func (g *gen) val(n cc.Node, op operand, t cc.Type) operand {
	switch x := op.(type) {
	case *val:
		return op
	case *pval:
		return g.load(n, op, t)
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
}

func (g *gen) pval(n cc.Node, op operand) operand {
	switch x := op.(type) {
	case *val:
		return &pval{x}
	case *pval:
		return op
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
}

func (g *gen) preInc(n *cc.UnaryExpression, op string, t cc.Type, value bool, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	// "++" UnaryExpression
	ue := g.unaryExpression(nil, n.UnaryExpression, false, nil, cases, brk, cont, -1)
	uet := n.UnaryExpression.Operand.Type()
	delta := uintptr(1)
	if uet.Kind() == cc.Ptr {
		delta = uet.Elem().Size()
	}
	d := g.intLit(n, cc.Int64Value(delta), n.UnaryExpression.Operand.Type(), uet, uet)
	switch x := ue.(type) {
	case *val:
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s %s %s, %s", ue, g.baseType(n, uet), op, ue, d)
		if !value {
			return ue
		}

		return g.zeroMode(n, g.convert(n, ue, uet, t), t, zero)
	case *pval:
		r := g.load(n, x, n.Operand.Type())
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s %s %s, %s", r, g.baseType(n, uet), op, r, d)
		r2 := g.store(n, ue, r, uet, uet, value)
		if !value {
			return r2
		}

		return g.zeroMode(n, g.convert(n, r2, uet, t), t, zero)
	default:
		panic(todo("%v: %T %v", n.Position(), x, x))
	}
}

func (g *gen) postInc(n *cc.PostfixExpression, op string, t cc.Type, value bool, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	// PostfixExpression "++"
	pe := g.postfixExpression(nil, n.PostfixExpression, false, nil, cases, brk, cont, -1)
	pet := n.PostfixExpression.Operand.Type()
	delta := uintptr(1)
	if pet.Kind() == cc.Ptr || pet.Kind() == cc.Array {
		delta = pet.Elem().Size()
	}
	d := g.intLit(n, cc.Int64Value(delta), n.PostfixExpression.Operand.Type(), pet, pet)
	r = g.newVal(nil)
	switch x := pe.(type) {
	case *val:
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s copy %s", r, g.baseType(n, pet), pe)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s %s %s, %s", pe, g.baseType(n, pet), op, pe, d)
		if !value {
			return r
		}

		return g.zeroMode(n, g.convert(n, r, pet, t), t, zero)
	case *pval:
		r := g.load(n, x, n.Operand.Type())
		s := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s %s %s, %s", s, g.baseType(n, pet), op, r, d)
		g.store(n, pe, s, pet, pet, false)
		if !value {
			return r
		}

		return g.zeroMode(n, g.convert(n, r, pet, t), t, zero)
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
}

func (g *gen) postfixExpression(ret operand, n *cc.PostfixExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	switch n.Case {
	case cc.PostfixExpressionPrimary: // PrimaryExpression
		return g.primaryExpression(ret, n.PrimaryExpression, value, t, cases, brk, cont, zero)
	case cc.PostfixExpressionIndex: // PostfixExpression '[' Expression ']'
		d := n.PostfixExpression.Declarator()
		if d != nil {
			arrT := d.Type()
			switch {
			case arrT.Decay().Kind() == cc.Ptr:
				pe := g.postfixExpression(nil, n.PostfixExpression, false, nil, cases, brk, cont, -1)
				switch x := pe.(type) {
				case *val:
					// ok
				case *pval:
					pe = g.val(n, pe, g.cPtr)
				default:
					panic(todo("%v: %T %s %v", n.Token.Position(), x, x, d.Type()))
				}
				r = g.postfixExpressionIndex(pe, n, cases, brk, cont, -1)
			default:
				panic(todo("%v: %v", n.Position(), d.Position()))
			}

			if !value || arrT.Elem().Kind() == cc.Array {
				if zero > 0 {
					panic(todo("", n.Position()))
				}

				return r
			}

			t = t.Decay()
			if !t.IsScalarType() {
				panic(todo("%v: %v -> %v", n.Token.Position(), d.Type(), t))
			}

			et := n.PostfixExpression.Operand.Type().Elem()
			r = g.load(n, r, et)
			return g.zeroMode(n, g.convert(n, r, et, t), t, zero)
		}

		if arrT := n.PostfixExpression.Operand.Type(); arrT.Decay().Kind() == cc.Ptr {
			p := g.postfixExpression(nil, n.PostfixExpression, false, nil, cases, brk, cont, -1)
			switch x := p.(type) {
			case *val:
				// ok
			case *pval:
				switch {
				case n.PostfixExpression.Operand.Type().Kind() == cc.Array:
					// ok
				default:
					p = g.val(n, p, g.cPtr)
				}
			default:
				panic(todo("%v: %T %s", n.Position(), x, x))
			}
			r = g.postfixExpressionIndex(p, n, cases, brk, cont, -1)
			if !value || arrT.Elem().Kind() == cc.Array {
				if zero > 0 {
					panic(todo("", n.Position()))
				}

				return r
			}

			if !t.IsScalarType() {
				panic(todo("", n.Position()))
			}

			et := n.PostfixExpression.Operand.Type().Elem()
			r = g.load(n, r, et)
			return g.zeroMode(n, g.convert(n, r, et, t), t, zero)
		}

		if arrT := n.Expression.Operand.Type(); n.PostfixExpression.Operand.Type().IsIntegerType() && arrT.Decay().Kind() == cc.Ptr {
			p := g.expression(nil, n.Expression, false, nil, cases, brk, cont, -1)
			switch x := p.(type) {
			case *val:
				// ok
			case *pval:
				panic(todo("%v:", n.Token.Position()))
			default:
				panic(todo("%v: %T %s", n.Position(), x, x))
			}
			r = g.postfixExpressionIndex2(p, n, cases, brk, cont, -1)
			if !value || arrT.Elem().Kind() == cc.Array {
				if zero > 0 {
					panic(todo("", n.Position()))
				}

				return r
			}

			if !t.IsScalarType() {
				panic(todo("", n.Position()))
			}

			et := n.Expression.Operand.Type().Elem()
			r = g.load(n, r, et)
			r = g.convert(n, r, et, t)
			if zero > 0 {
				panic(todo("", n.Position()))
			}

			return r
		}

		panic(todo("%v:", n.Token.Position()))
	case cc.PostfixExpressionCall: // PostfixExpression '(' ArgumentExpressionList ')'
		if d := n.PostfixExpression.Declarator(); d != nil {
			switch d.Name() {
			case idBuiltinConstantPImpl:
				op := n.Operand
				if zero > 0 {
					panic(todo("", n.Position()))
				}

				return g.intLit(n, op.Value(), op.Type(), n.Operand.Type(), t)
			}
		}

		var (
			args      []operand
			intConsts []uint64
			operands  []cc.Operand
			types     []cc.Type
		)
		for list := n.ArgumentExpressionList; list != nil; list = list.ArgumentExpressionList {
			arg := list.AssignmentExpression
			operands = append(operands, arg.Operand)
			t0 := arg.Operand.Type()
			t := t0
			switch {
			case t.Kind() == cc.Ptr:
				t = g.cPtr
			case g.isVaList(t):
				switch vaInfo := g.vaInfo; {
				case vaInfo.IsPointer:
					panic(todo("", n.Position()))
				case vaInfo.IsStruct:
					// nop
				case vaInfo.IsStructArray:
					// nop
				default:
					panic(todo("", n.Position()))
				}
			case t.Kind() != cc.Array:
				t = arg.Promote()
			}
			types = append(types, t)
			v := arg.Operand.Value()
			if !t.IsIntegerType() {
				v = nil
			}
			var intConst uint64
			switch x := v.(type) {
			case cc.Int64Value:
				intConst = uint64(x)
			case cc.Uint64Value:
				intConst = uint64(x)
			default:
				v := g.assignmentExpression(nil, arg, !g.isAggregateType(t0), t0, cases, brk, cont, -1)
				args = append(args, g.convert(n, v, t0, t))
				intConsts = append(intConsts, 0)
				continue
			}

			args = append(args, nil)
			intConsts = append(intConsts, intConst)
		}
		ct := n.PostfixExpression.Operand.Type()
		isVariadic := false
		switch ct.Kind() {
		case cc.Function:
			isVariadic = ct.IsVariadic()
		case cc.Ptr:
			if t := ct.Elem(); t.Kind() == cc.Function {
				isVariadic = t.IsVariadic()
			}
		}
		callee := g.postfixExpression(nil, n.PostfixExpression, true, ct, cases, brk, cont, -1)
		switch callee.name() {
		case idVaArg:
			if len(args) != 1 {
				panic(todo("", n.Position()))
			}

			args[0].setIsVaArg()
			if zero > 0 {
				panic(todo("", n.Position()))
			}

			return args[0]
		case idVaStart:
			if zero > 0 {
				panic(todo("", n.Position()))
			}

			return g.vaStart(n.PostfixExpression, args)
		}
		g.w("\n%s\t", g.lineNL(n))
		if n.Operand != nil {
			switch t := n.Operand.Type(); {
			case t.IsScalarType():
				r = g.newVal(ret)
				g.w("%s =%s ", r, g.abiType(n, t))
			case t.Kind() == cc.Void:
				// nop
			case g.isRecordType(t):
				r = g.newVal(ret)
				g.w("%s =%s ", r, g.abiType(n, t))
			default:
				panic(todo("", n.Position(), t))
			}
		}
		if ct := n.PostfixExpression.Operand.Type(); ct.Kind() == cc.Function {
			params := ct.Parameters()
			for i, v := range params {
				if i >= len(types) {
					break
				}

				types[i] = v.Type()
			}
		}
		g.w("call %s(", callee)
		for i, v := range args {
			op := operands[i]
			ot := op.Type()
			switch {
			case g.isVaList(ot):
				g.w(":%s ", qbe.VaList)
			case ot.Kind() == cc.Ptr && g.isVaList(ot.Elem()):
				panic(todo("", n.Position()))
			case g.isCharPtr(n, ot) || g.isCharArray(n, ot):
				g.w("c ")
			default:
				g.w("%s ", g.abiType(n, types[i]))
			}
			switch {
			case v == nil:
				g.w("%v", intConsts[i])
			default:
				g.w("%s", v)
			}
			if i != len(args)-1 {
				g.w(", ")
			}
		}
		if isVariadic {
			g.w(", ...")
		}
		g.w(")")
		if !value {
			return r
		}
		if zero > 0 {
			return g.zeroMode(n, r, n.Operand.Type(), zero)
		}

		return g.convert(n, r, n.Operand.Type(), t)
	case cc.PostfixExpressionSelect: // PostfixExpression '.' IDENTIFIER
		pe := g.postfixExpression(nil, n.PostfixExpression, false, nil, cases, brk, cont, -1)
		if off := n.Field.Offset(); off != 0 {
			p2 := g.newPVal(nil)
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s add %s, %d", p2, g.ptr, pe, off)
			pe = p2
		}
		if n.Field.IsBitField() {
			v := g.newVal(nil)
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s copy %s", v, g.ptr, pe)
			bf := &bitField{n.Field, n.PostfixExpression.Operand.Type()}
			v.setBitField(bf)
			pe = v
		}
		if !value {
			return g.pval(n, pe)
		}

		switch ft := n.Field.Type(); {
		case ft.Kind() == cc.Array:
			r := g.newPVal(nil)
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s copy %s", r, g.ptr, pe)
			return g.zeroMode(n, r, g.cPtr, zero)
		case g.isRecordType(ft):
			panic(todo("", n.Position()))
		}

		r := g.load(n, pe, n.Field.Type())
		return g.zeroMode(n, g.convert(n, r, n.Field.Type(), t), t, zero)
	case cc.PostfixExpressionPSelect: // PostfixExpression "->" IDENTIFIER
		pe0 := g.postfixExpression(nil, n.PostfixExpression, false, nil, cases, brk, cont, -1)
		pe := g.val(n, pe0, n.PostfixExpression.Operand.Type()).(*val)
		if off := n.Field.Offset(); off != 0 {
			p2 := g.newVal(nil)
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s add %s, %d", p2, g.ptr, pe, off)
			pe = p2
		}
		if n.Field.IsBitField() {
			v := g.newVal(nil)
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s copy %s", v, g.ptr, pe)
			v.setBitField(&bitField{n.Field, n.PostfixExpression.Operand.Type().Elem()})
			pe = v
		}

		if !value {
			return &pval{pe}
		}

		if g.isAggregateType(n.Field.Type()) {
			if zero > 0 {
				panic(todo("", n.Position()))
			}

			return &pval{pe}
		}

		r := g.load(n, pe, n.Field.Type())
		return g.zeroMode(n, g.convert(n, r, n.Field.Type(), t), t, zero)
	case cc.PostfixExpressionInc: // PostfixExpression "++"
		return g.postInc(n, "add", t, value, cases, brk, cont, zero)
	case cc.PostfixExpressionDec: // PostfixExpression "--"
		return g.postInc(n, "sub", t, value, cases, brk, cont, zero)
	case cc.PostfixExpressionComplit: // '(' TypeName ')' '{' InitializerList ',' '}'
		t := n.TypeName.Type()
		if g.f == nil {
			panic(todo("%v: %v", n.Position(), n.Case))
		}

		op := g.compositeLiterals[n]
		//		%t =w cne(ptr) %op, 0
		//		jnz %t, a, b
		//	@b
		//		%op =p allocN, size
		//	@a
		x := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =w cne%s %s, 0", x, g.ptr, op)
		a := g.newLabel()
		b := g.newLabel()
		g.w("\n%s", g.line(n))
		g.w("\n\tjnz %s, @%d, @%d", x, a, b)
		g.w("\n@%d", b)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s alloc%d %d", op, g.ptr, g.align(n, n.Operand.Type().Align()), t.Size())
		g.w("\n@%d", a)
		g.w("\n%s", g.line(n))
		p := g.newPVal(nil)
		for _, in := range n.InitializerList.List() {
			e := in.AssignmentExpression
			et := e.Operand.Type()
			off := in.Offset
			g.w("\n%s", g.line(e))
			switch {
			case off == 0:
				g.w("\n\t%s =%s copy %s", p, g.ptr, op)
			default:
				g.w("\n\t%s =%s add %s, %d", p, g.ptr, op, off)
			}
			if f := in.Field; f != nil {
				if f.IsBitField() {
					p.setBitField(&bitField{f, t})
				}
			}
			src := g.assignmentExpression(nil, e, !g.isAggregateType(e.Operand.Type()), et, nil, -1, -1, -1)
			switch {
			case et.Decay().IsScalarType():
				g.store(n, p, src, in.Type(), et, false)
			default:
				g.w("\n\tcall $memcpy(p %s, p %s, %s %d)", p, src, g.abiType(n, g.cSizeT), et.Size())
			}
		}
		return op
	case cc.PostfixExpressionTypeCmp: // "__builtin_types_compatible_p" '(' TypeName ',' TypeName ')'
		panic(todo("%v: %v", n.Position(), n.Case))
	case cc.PostfixExpressionChooseExpr: // "__builtin_choose_expr" '(' AssignmentExpression ',' AssignmentExpression ',' AssignmentExpression ')'
		op := n.AssignmentExpression.Operand
		if op.Value() == nil {
			panic(todo("", n.Position()))
		}

		switch {
		case op.IsNonZero():
			return g.assignmentExpression(ret, n.AssignmentExpression2, value, n.AssignmentExpression2.Operand.Type(), cases, brk, cont, zero)
		case op.IsZero():
			return g.assignmentExpression(ret, n.AssignmentExpression3, value, n.AssignmentExpression3.Operand.Type(), cases, brk, cont, zero)
		default:
			panic(todo("", n.Position()))
		}
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}
func (g *gen) postfixExpressionIndex(p operand, n *cc.PostfixExpression, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	if zero > 0 {
		panic(todo("", n.Position()))
	}

	// PostfixExpression '[' Expression ']'
	if n.Expression.Operand.IsZero() {
		return g.pval(n, p)
	}

	et := n.PostfixExpression.Operand.Type().Elem()
	elemSize := et.Size()
	switch v := n.Expression.Operand.Value().(type) {
	case cc.Int64Value:
		p2 := g.newPVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s add %s, %d", p2, g.ptr, p, uintptr(v)*elemSize)
		return p2
	case cc.Uint64Value:
		p2 := g.newPVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s add %s, %d", p2, g.ptr, p, uintptr(v)*elemSize)
		return p2
	case nil:
		x := g.expression(nil, n.Expression, true, g.cPtr, cases, brk, cont, -1)
		if elemSize != 1 {
			y := g.newPVal(nil)
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s mul %s, %d", y, g.baseType(n, g.cPtr), x, elemSize)
			x = y
		}
		p2 := g.newPVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s add %s, %s", p2, g.baseType(n, g.cPtr), p, x)
		return p2
	default:
		panic(todo("%v: %T", n.Position(), v))
	}
}

func (g *gen) postfixExpressionIndex2(p operand, n *cc.PostfixExpression, cases map[*cc.LabeledStatement]int, brk, cont, zero int) operand {
	if zero > 0 {
		panic(todo("", n.Position()))
	}

	// PostfixExpression '[' Expression ']', but effectively Expression '[' PostfixExpression ']'
	if n.PostfixExpression.Operand.IsZero() {
		return g.pval(n, p)
	}

	et := n.Expression.Operand.Type().Elem()
	elemSize := et.Size()
	switch v := n.PostfixExpression.Operand.Value().(type) {
	case cc.Int64Value:
		p2 := g.newPVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s add %s, %d", p2, g.ptr, p, uintptr(v)*elemSize)
		return p2
	// case nil:
	// 	x := g.expression(nil, n.Expression, true, g.cPtr, cases, brk, cont)
	// 	if elemSize != 1 {
	// 		y := g.newPVal(nil)
	// 		g.w("\n\t%s =%s mul %s, %d", y, g.baseType(n, g.cPtr), x, elemSize)
	// 		x = y
	// 	}
	// 	p2 := g.newPVal(nil)
	// 	g.w("\n\t%s =%s add %s, %s", p2, g.baseType(n, g.cPtr), p, x)
	// 	return p2
	default:
		panic(todo("%v: %T", n.Token.Position(), v))
	}
}

func (g *gen) vaStart(n cc.Node, args []operand) operand {
	if len(args) != 1 {
		panic(todo("", n.Position()))
	}

	g.w("\n%s", g.line(n))
	g.w("\n\tvastart %s", args[0])
	return nil
}

func (g *gen) primaryExpression(ret operand, n *cc.PrimaryExpression, value bool, t cc.Type, cases map[*cc.LabeledStatement]int, brk, cont, zero int) (r operand) {
	op := n.Operand
	switch n.Case {
	case cc.PrimaryExpressionIdent: // IDENTIFIER
		return g.zeroMode(n, g.primaryExpressionIdent(n, value, n.Operand.Type(), t), t, zero)
	case cc.PrimaryExpressionInt: // INTCONST
		if zero > 0 {
			if n.Operand.IsZero() {
				g.w("\n%s", g.line(n))
				g.w("\n\tjmp @%d", zero)
				a := g.newLabel()
				g.w("\n@%d", a)
				g.w("\n%s", g.line(n))
				return nil
			}

			if n.Operand.IsNonZero() {
				return nil
			}

			panic(todo("", n.Position()))
		}

		return g.intLit(n, op.Value(), op.Type(), n.Operand.Type(), t)
	case cc.PrimaryExpressionFloat: // FLOATCONST
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		return g.floatLit(n, t)
	case cc.PrimaryExpressionEnum: // ENUMCONST
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		return g.intLit(n, op.Value(), op.Type(), n.Operand.Type(), t)
	case cc.PrimaryExpressionChar: // CHARCONST
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		return g.intLit(n, op.Value(), op.Type(), n.Operand.Type(), t)
	case cc.PrimaryExpressionLChar: // LONGCHARCONST
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		return g.intLit(n, op.Value(), op.Type(), n.Operand.Type(), t)
	case cc.PrimaryExpressionString: // STRINGLITERAL
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		switch x := n.Operand.Value().(type) {
		case cc.StringValue:
			return &val{kind: stringVal, s: safeQuoteToASCII(cc.StringID(x).String())}
		default:
			return g.stringLit(n, n.Operand.Value())
		}
	case cc.PrimaryExpressionLString: // LONGSTRINGLITERAL
		if zero > 0 {
			panic(todo("", n.Position()))
		}

		return g.longStringLit(n, n.Operand.Value())
	case cc.PrimaryExpressionExpr: // '(' Expression ')'
		return g.expression(ret, n.Expression, value, t, cases, brk, cont, zero)
	case cc.PrimaryExpressionStmt: // '(' CompoundStatement ')'
		_, r := g.compoundStatement(n.CompoundStatement, cases, brk, cont)
		return g.zeroMode(n, r, n.Operand.Type(), zero)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) primaryExpressionIdent(n *cc.PrimaryExpression, value bool, from, to cc.Type) (r operand) {
	x, ok := n.ResolvedTo().(*cc.Declarator)
	if !ok {
		s := fmt.Sprintf("__builtin_%s", n.Token)
		id := cc.String(s)
		a := g.ast.Scope[id]
		if len(a) == 0 {
			switch {
			case strings.HasPrefix(n.Token.String(), "__builtin_"):
				g.errList.err(n, "undefined/unsupported builtin: %s", n.Token)
			default:
				g.errList.err(n, "undefined: %s", n.Token)
			}
			return g.newVal(nil)
		}

		var ok bool
		if x, ok = a[0].(*cc.Declarator); !ok {
			panic(todo("%v:", n.Position()))
		}

	}
	return g.primaryExpressionDeclarator(n, x, value, from, to)
}

func (g *gen) primaryExpressionDeclarator(n *cc.PrimaryExpression, d *cc.Declarator, value bool, from, to cc.Type) (r operand) {
	switch {
	case d.Linkage == cc.External:
		r = g.tldDeclarator(d, from)
	case d.StorageClass == cc.Automatic:
		r = g.declaratorInfos[d].op
	case d.StorageClass == cc.Static:
		if n == nil {
			return g.tldDeclarator(d, from)
		}

		switch s := n.ResolvedIn(); {
		case s.Parent() == nil:
			r = g.tldDeclarator(d, from)
		default:
			r = g.declaratorInfos[d].op
		}
	default:
		panic(todo("", n.Position()))
	}
	if !value || n.Operand.Type().Kind() == cc.Function {
		return r
	}

	r = g.val(n, r, from)
	if from != nil && to != nil {
		r = g.convert(n, r, from, to)
	}
	return r
}

func (g *gen) tldDeclarator(d *cc.Declarator, t cc.Type) (r operand) {
	x := &val{n: int32(d.Name()), kind: named}
	switch {
	case t.Kind() == cc.Function || g.isAggregateType(t):
		return x
	default:
		return g.pval(d, x)
	}
}
