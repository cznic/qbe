// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cc // import "modernc.org/qbe/cc"

import (
	"bytes"
	"fmt"
	"io"
	"sort"
	"strings"

	"modernc.org/cc/v3"
	"modernc.org/qbe"
	"modernc.org/token"
)

var (
	oTrcw bool // testing
)

type errItem struct {
	pos token.Position
	err error
}

type errList []errItem

func (e errList) Err() error {
	if len(e) == 0 {
		return nil
	}

	w := 0
	prev := errItem{}
	for _, v := range e {
		if v.pos != prev.pos || v.err.Error() != prev.err.Error() {
			e[w] = v
			w++
			prev = v
		}
	}

	var a []string
	for _, v := range e {
		a = append(a, fmt.Sprintf("%v: %v", v.pos, v.err))
	}
	return fmt.Errorf("%s", strings.Join(a, "\n"))
}

func (e *errList) err(n cc.Node, msg string, args ...interface{}) {
	var p token.Position
	errs := *e
	msg = fmt.Sprintf(msg, args...)
	if n != nil {
		p = n.Position()
	}
	*e = append(errs, errItem{p, fmt.Errorf("%s (%v:)", msg, origin(2))})
}

type gen struct {
	abi                cc.ABI
	ast                *cc.AST
	cInt               cc.Type
	cLongDouble        cc.Type
	cPtr               cc.Type
	cSizeT             cc.Type
	compositeLiterals  map[*cc.PostfixExpression]operand
	declaratorInfos    map[*cc.Declarator]*declaratorInfo
	errList            errList
	f                  *cc.FunctionDefinition
	initializers       []*initializer
	lineBuf            lineBuf
	lineDirective      []byte
	longStringLiterals map[string]operand
	name               string
	nextLabel          int
	nextLocal          int32
	nextStaticGlobal   int32
	out                io.Writer
	prototypeAttrs     map[cc.StringID][]*cc.AttributeSpecifier
	ptr                string
	stringLiterals     map[string]operand
	task               *Task
	vaInfo             *qbe.VaInfo
	wchar              cc.Type

	isClosed bool
}

func newGen(t *Task, out io.Writer, name string, ast *cc.AST, cfg *cc.Config) (*gen, error) {
	vaInfo, err := qbe.VaInfoFor(t.os, t.arch)
	if err != nil {
		return nil, err
	}

	r := &gen{
		abi:            cfg.ABI,
		ast:            ast,
		cInt:           cfg.ABI.Type(cc.Int),
		cLongDouble:    cfg.ABI.Type(cc.LongDouble),
		cPtr:           cfg.ABI.Ptr(nil, cfg.ABI.Type(cc.Void)),
		cSizeT:         ast.SizeType,
		name:           name,
		out:            out,
		prototypeAttrs: map[cc.StringID][]*cc.AttributeSpecifier{},
		task:           t,
		vaInfo:         vaInfo,
		wchar:          ast.WideCharType,
	}
	r.ptr = r.baseType(nil, r.cPtr)
	return r, nil
}

func (g *gen) newVal(dst operand) (r *val) {
	switch x := dst.(type) {
	case nil:
		r = &val{n: g.nextLocal, kind: local}
		g.nextLocal++
		return r
	case *val:
		return x
	default:
		panic(todo("internal error: %T", dst))
	}
}

func (g *gen) newPVal(dst operand) (r *pval) {
	switch x := dst.(type) {
	case nil:
		r = &pval{&val{n: g.nextLocal, kind: local}}
		g.nextLocal++
		return r
	case *pval:
		return x
	default:
		panic(todo("internal error: %T", x))
	}
}

func (g *gen) newStaticGlobalArray() (r *val) {
	r = &val{n: g.nextStaticGlobal, kind: numbered}
	g.nextStaticGlobal++
	return r
}

func (g *gen) newStaticGlobal() (r *pval) {
	r = &pval{&val{n: g.nextStaticGlobal, kind: numbered}}
	g.nextStaticGlobal++
	return r
}

func (g *gen) newLabel() int {
	r := g.nextLabel
	g.nextLabel++
	return r
}

var _ io.Writer = (*lineBuf)(nil)

type lineBuf []byte

func (b *lineBuf) Write(d []byte) (int, error) {
	*b = append(*b, d...)
	return len(d), nil
}

var lineDirectivePrefix = []byte("#line")

func (g *gen) w(s string, args ...interface{}) {
	if g.isClosed {
		return
	}

	if oTrcw {
		fmt.Printf(s, args...)
	}
	fmt.Fprintf(&g.lineBuf, s, args...)
	for {
		x := bytes.IndexByte([]byte(g.lineBuf), '\n')
		if x < 0 {
			return
		}

		line := g.lineBuf[:x+1] // Include the trailing '\n'.
		switch {
		case bytes.HasPrefix(line, lineDirectivePrefix):
			g.lineDirective = append(g.lineDirective[:0], line...)
		default:
			if len(g.lineDirective) != 0 {
				if _, err := g.out.Write(g.lineDirective); err != nil {
					g.errList.err(nil, "error writing output: %v", err)
					g.isClosed = true
					return
				}

				g.lineDirective = g.lineDirective[:0]
			}
			if _, err := g.out.Write(line); err != nil {
				g.errList.err(nil, "error writing output: %v", err)
				g.isClosed = true
			}
		}
		g.lineBuf = append(g.lineBuf[:0], g.lineBuf[x+1:]...)
	}
}

func (g *gen) main() error {
	var da []*cc.Declarator
	for n := g.ast.TranslationUnit; n != nil; n = n.TranslationUnit {
		ed := n.ExternalDeclaration
		switch ed.Case {
		case cc.ExternalDeclarationAsm:
			d := ed.AsmFunctionDefinition.Declarator
			da = append(da, d)
		case cc.ExternalDeclarationDecl: // Declaration
			de := ed.Declaration
			if de == nil {
				continue
			}

			dl := de.InitDeclaratorList
			if dl == nil {
				continue
			}

			id := dl.InitDeclarator
			if id == nil {
				continue
			}

			d := id.Declarator
			if d == nil {
				continue
			}

			if t := d.Type(); t.Kind() != cc.Function {
				continue
			}

			da = append(da, d)
		}
	}
	sort.Slice(da, func(i, j int) bool {
		x, y := da[i], da[j]
		return x.Name().String() < y.Name().String()
	})
	for _, v := range da {
		s := g.prototype(v, v.Name(), v.Type(), v.Type())
		if strings.Contains(s, prototypeTODO) {
			continue
		}

		g.w("#qbec:prototype %s %s; // %v: \n", v.Name(), s, g.pos(v))
	}
	g.w("#qbec:prototype memcpy void *memcpy(void *dest, const void *src, size_t n);\n")
	if len(da) != 0 {
		g.w("\n")
	}
	var a []cc.StructInfo
	for k := range g.ast.Structs {
		a = append(a, k)
	}
	sort.Slice(a, func(i, j int) bool {
		x, y := a[i], a[j]
		if x.Align < y.Align {
			return true
		}

		if x.Align == y.Align {
			return x.Size < y.Size
		}

		return false
	})
	for i, v := range a {
		if v.Align > 16 {
			continue
		}

		if i != 0 {
			g.w("\n")
		}
		g.w("type :%d_%d = align %d { %d }", v.Align, v.Size, g.align(nil, v.Align), v.Size)
	}
	for n := g.ast.TranslationUnit; n != nil; n = n.TranslationUnit {
		g.externalDeclarationPrototypeAttrs(n.ExternalDeclaration)
	}
	for n := g.ast.TranslationUnit; n != nil; n = n.TranslationUnit {
		g.externalDeclaration(n.ExternalDeclaration)
	}
	g.emitStrings()
	g.emitLongStrings()
	g.w("\n")
	return g.errList.Err()
}

const prototypeTODO = "__TODO__" //TODO;

func (g *gen) prototype(n cc.Node, nm cc.StringID, t0, t cc.Type) string {
	r := "void"
	if rt := t.Result(); rt != nil && rt.Kind() != cc.Void {
		r = g.cType(n, rt, rt)
	}
	var a []string
	for _, v := range t.Parameters() {
		a = append(a, g.cType(n, v.Type(), v.Type()))
	}
	if t.IsVariadic() {
		a = append(a, "...")
	}
	return fmt.Sprintf("%s %s(%s)", r, nm, strings.Join(a, ", "))
}

func (g *gen) cType(n cc.Node, t0, t cc.Type) string {
	if g.isVaList(t) {
		return "va_list"
	}

	t = t.Decay()
	switch t.Kind() {
	case cc.Ptr:
		if elem := t.Elem().Alias(); elem.IsIntegerType() {
			return fmt.Sprintf("%s*", g.cType(n, t0, elem))
		}

		return "void*"
	case cc.Char:
		return "char"
	case cc.SChar:
		return "signed char"
	case cc.UChar:
		return "unsigned char"
	case cc.Short:
		return "short"
	case cc.UShort:
		return "unsigned short"
	case cc.Int:
		return "int"
	case cc.UInt:
		return "unsigned int"
	case cc.Long:
		return "long"
	case cc.ULong:
		return "unsigned long"
	case cc.LongLong:
		return "long long"
	case cc.ULongLong:
		return "unsigned long long"
	case cc.Double:
		return "double"
	case cc.LongDouble:
		return "long double"
	case cc.Float:
		return "float"
	case cc.Float32:
		return "_Float32"
	case cc.Float32x:
		return "_Float32x"
	case cc.Float64:
		return "_Float64"
	case cc.Float64x:
		return "_Float64x"
	case cc.Void:
		return "void"
	case cc.Enum:
		return fmt.Sprintf("%sint%d_t", g.u(t), t.Size()*8)
	case cc.Array:
		s := g.cType(n, t0, t.Elem())
		switch {
		case t.IsIncomplete():
			return fmt.Sprintf("%s[]", s)
		default:
			return fmt.Sprintf("%s[%d]", s, t.Len())
		}
	case cc.Function:
		return g.prototype(n, 0, t0, t)
	case cc.Struct:
		return fmt.Sprintf("struct { int8_t f[%d]; } __attribute__ ((aligned (%d)))", t.Size(), t.Align())
	case cc.Union:
		return fmt.Sprintf("union { int8_t f[%d]; } __attribute__ ((aligned (%d)))", t.Size(), t.Align())
	case cc.Float128:
		return prototypeTODO
	case cc.Bool:
		return "_Bool"
	}
	panic(todo("%v: %v, %v; %v %v", n.Position(), t0, t0.Kind(), t, t.Kind()))
}

func (g *gen) emitStrings() {
	if len(g.stringLiterals) == 0 {
		return
	}

	g.w("\n")
	var a []string
	for k := range g.stringLiterals {
		a = append(a, k)
	}
	sort.Strings(a)
	for _, v := range a {
		g.w("\ndata %s = { b %s, b 0 }", g.stringLiterals[v], safeQuoteToASCII(v))
	}
}

func (g *gen) emitLongStrings() {
	if len(g.longStringLiterals) == 0 {
		return
	}

	g.w("\n")
	var a []string
	for k := range g.longStringLiterals {
		a = append(a, k)
	}
	sort.Strings(a)
	t := g.storeType(nil, g.wchar)
	for _, v := range a {
		g.w("\ndata %s = { %s", g.longStringLiterals[v], t)
		for _, c := range v {
			g.w(" %d", c)
		}
		g.w(", %s 0 }", t)
	}
}

func safeQuoteToASCII(s string) string {
	var b strings.Builder
	b.WriteByte('"')
	for i := 0; i < len(s); i++ {
		switch c := s[i]; c {
		case '\n':
			fmt.Fprintf(&b, "\\n")
		case '\t':
			fmt.Fprintf(&b, "\\t")
		case '\r':
			fmt.Fprintf(&b, "\\r")
		case '\v':
			fmt.Fprintf(&b, "\\v")
		case '\f':
			fmt.Fprintf(&b, "\\f")
		case '\a':
			fmt.Fprintf(&b, "\\a")
		case '\b':
			fmt.Fprintf(&b, "\\b")
		case '\\':
			fmt.Fprintf(&b, "\\\\")
		case '"':
			fmt.Fprintf(&b, "\\\"")
		default:
			switch {
			case c < ' ' || c >= 0x7f:
				fmt.Fprintf(&b, "\\x%02x", c)
			default:
				b.WriteByte(c)
			}
		}
	}
	b.WriteByte('"')
	return b.String()
}
