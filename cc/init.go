// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cc // import "modernc.org/qbe/cc"

import (
	"math"

	"modernc.org/cc/v3"
)

type ptrInitializer struct {
	op  operand
	off uintptr
}

type initializer struct {
	b    []byte
	g    *gen
	lds  map[uintptr]cc.Value
	more []*initializer
	nm   string
	ptrs map[uintptr]*ptrInitializer
	t    cc.Type
}

func (g *gen) newInitializer(t cc.Type, nm string) *initializer {
	return &initializer{
		b:  make([]byte, t.Size()),
		g:  g,
		nm: nm,
		t:  t,
	}
}

func (c *initializer) initializerArithmetic(t cc.Type, f cc.Field, n *cc.AssignmentExpression, off uintptr) {
	if n.Operand.IsZero() && n.Operand.Type().IsIntegerType() {
		return
	}

	if t.IsIntegerType() {
		var v uint64
		switch x := n.Operand.Value().(type) {
		case cc.Int64Value:
			v = uint64(x)
		case cc.Uint64Value:
			v = uint64(x)
		case cc.Float64Value:
			v = uint64(int64(x))
		default:
			panic(todo(""))
		}
		c.initializerUint64(t, f, n, v, off)
		return
	}

	switch t.Kind() {
	case cc.Float:
		switch x := n.Operand.Value().(type) {
		case cc.Float64Value:
			c.initializerUint64(t, f, n, uint64(math.Float32bits(float32(x))), off)
		case cc.Float32Value:
			c.initializerUint64(t, f, n, uint64(math.Float32bits(float32(x))), off)
		case cc.Int64Value:
			c.initializerUint64(t, f, n, uint64(math.Float32bits(float32(x))), off)
		case cc.Uint64Value:
			c.initializerUint64(t, f, n, uint64(math.Float32bits(float32(x))), off)
		default:
			panic(todo("%T", x))
		}
	case cc.Double:
		switch x := n.Operand.Value().(type) {
		case cc.Float64Value:
			c.initializerUint64(t, f, n, math.Float64bits(float64(x)), off)
		case cc.Float32Value:
			c.initializerUint64(t, f, n, math.Float64bits(float64(x)), off)
		case cc.Int64Value:
			c.initializerUint64(t, f, n, math.Float64bits(float64(x)), off)
		case cc.Uint64Value:
			c.initializerUint64(t, f, n, math.Float64bits(float64(x)), off)
		default:
			panic(todo("%T", x))
		}
	case cc.LongDouble:
		c.setLongDouble(off, n.Operand.Value())
	default:
		panic(todo("%v: %v, %v, %T(%[4]v)", n.Position(), t, t.Kind(), n.Operand.Value()))
	}
}

func (c *initializer) setLongDouble(off uintptr, v cc.Value) {
	if c.lds == nil {
		c.lds = map[uintptr]cc.Value{}
	}
	c.lds[off] = v
	// Must set the target bytes to be non zero b/c of how .emit() works
	// when finding any trailing zeros.
	for i := 0; i < int(c.g.cLongDouble.Size()); i++ {
		c.b[off+uintptr(i)] = ^byte(0)
	}
}

func (c *initializer) initializerUint64(t cc.Type, f cc.Field, n cc.Node, v uint64, off uintptr) {
	if v == 0 {
		return
	}

	sz := t.Size()
	if t.IsBitFieldType() {
		if l := uintptr(len(c.b)); off+sz > l {
			sz = l - off
		}
		var raw uint64
		switch sz {
		case 1:
			raw = uint64(c.b[off])
		case 2:
			raw = uint64(c.g.abi.ByteOrder.Uint16(c.b[off:]))
		case 4:
			raw = uint64(c.g.abi.ByteOrder.Uint32(c.b[off:]))
		case 8:
			raw = c.g.abi.ByteOrder.Uint64(c.b[off:])
		default:
			c.g.errList.err(n, "unexpected/invalid sizeof(%s): %v -> %v, off %#x, len(c.b) %v", t, t.Size(), sz, off, len(c.b))
			return
		}
		raw &^= f.Mask()
		v <<= uint(f.BitFieldOffset())
		v &= f.Mask()
		v |= raw
	}

	switch sz {
	case 1:
		c.b[off] = byte(v)
	case 2:
		c.g.abi.ByteOrder.PutUint16(c.b[off:], uint16(v))
	case 4:
		c.g.abi.ByteOrder.PutUint32(c.b[off:], uint32(v))
	case 8:
		c.g.abi.ByteOrder.PutUint64(c.b[off:], v)
	default:
		panic(todo(""))
	}
}

func (c *initializer) emit() {
	g := c.g
	sz := len(c.b)
	for n := sz; n > 0; n-- {
		if c.b[n-1] != 0 {
			break
		}

		c.b = c.b[:n-1]
	}
	switch {
	case len(c.b) == 0:
		g.w(" z %d", sz)
	default:
		typ := ""
		for off := uintptr(0); off < uintptr(len(c.b)); {
			if ptr, ok := c.ptrs[off]; ok {
				if typ != "" {
					g.w(",")
				}
				g.w(` %s %s`, g.ptr, ptr.op)
				if ptr.off != 0 {
					g.w("%+d", ptr.off)
				}
				typ = g.ptr
				off += c.g.cPtr.Size()
				continue
			}

			if ld, ok := c.lds[off]; ok {
				if typ != "" {
					g.w(",")
				}
				g.w(` ld %s`, c.g.longDoubleString(nil, ld))
				typ = "ld"
				off += c.g.cLongDouble.Size()
				continue
			}

			v := c.b[off]
			switch typ {
			case "l", "w", "ld":
				g.w(", b")
				typ = "b"
			case "":
				g.w(" b")
				typ = "b"
			}
			g.w(" %d", v)
			off++
		}
		if len(c.b) != sz {
			g.w(", z %d ", sz-len(c.b))
		}
	}
}

func (c *initializer) initializerArrayWideString(t cc.Type, n cc.Node, sid cc.StringID, off uintptr) {
	s := []rune(sid.String())
	if uintptr(len(s)) > t.Len() {
		panic(todo(""))
	}

	sz := t.Elem().Size()
	for i, r := range s {
		c.initializerUint64(t.Elem(), nil, n, uint64(r), off+uintptr(i)*sz)
	}
}

func (c *initializer) initializerPointer(t cc.Type, f cc.Field, n *cc.AssignmentExpression, off uintptr) {
	if n.Operand.IsZero() {
		return
	}

	switch x := n.Operand.Value().(type) {
	case cc.StringValue:
		switch t.Elem().Kind() {
		case cc.Char, cc.SChar, cc.UChar, cc.Void:
			c.setPtr(off, c.g.stringLit(n, x), n.Operand.Offset())
		default:
			panic(todo("", n.Position()))
		}
	case nil:
		if d := n.Operand.Declarator(); d != nil {
			switch d.Linkage {
			case cc.External, cc.Internal:
				c.setPtr(off, c.g.tldDeclarator(d, d.Type()), n.Operand.Offset())
				return
			default:
				if info := c.g.declaratorInfos[d]; info != nil && info.static {
					c.setPtr(off, info.op, n.Operand.Offset())
					return
				}

				panic(todo("%v: %v: %s, link %v", n.Position(), d.Position(), d.Name(), d.Linkage))
			}
		}

		panic(todo("%v:", n.Position()))
	case *cc.InitializerValue:
		op := c.g.newStaticGlobal()
		c.setPtr(off, op, 0)
		in := c.g.newInitializer(n.Operand.Type().Elem(), op.String())
		c.g.initializer(in, x.List(), 0)
		c.more = append(c.more, in)
	case cc.Uint64Value:
		c.initializerUint64(t, f, n, uint64(x), off)
	default:
		panic(todo("%v: %T", n.Position(), n.Operand.Value()))
	}
}

func (c *initializer) setPtr(off uintptr, ptr operand, delta uintptr) {
	if c.ptrs == nil {
		c.ptrs = map[uintptr]*ptrInitializer{}
	}
	c.ptrs[off] = &ptrInitializer{ptr, delta}
	// Must set the target bytes to be non zero b/c of how .emit() works
	// when finding any trailing zeros.
	for i := 0; i < int(c.g.cPtr.Size()); i++ {
		c.b[off+uintptr(i)] = ^byte(0)
	}
}

func (g *gen) initializer(w *initializer, list []*cc.Initializer, off0 uintptr) {
	for _, n := range list {
		expr := n.AssignmentExpression
		t := n.Type()
		off := off0 + n.Offset
		if t.IsArithmeticType() {
			w.initializerArithmetic(t, n.Field, expr, off)
			continue
		}

		switch t.Kind() {
		case cc.Ptr:
			w.initializerPointer(t, n.Field, expr, off)
		case cc.Array:
			switch e := t.Elem(); e.Kind() {
			case cc.Char, cc.SChar, cc.UChar:
				w.initializerArrayString(t, n, cc.StringID(expr.Operand.Value().(cc.StringValue)), off)
			case g.wchar.Kind():
				w.initializerArrayWideString(t, n, cc.StringID(expr.Operand.Value().(cc.WideStringValue)), off)
			default:
				panic(todo(""))
			}
		case cc.Struct, cc.Union:
			w.initializerArithmetic(t, n.Field, expr, off)
		default:
			panic(todo("%v: %v, %v", n.Position(), t, t.Kind()))
		}
	}
}

func (c *initializer) initializerArrayString(t cc.Type, n cc.Node, sid cc.StringID, off uintptr) {
	s := sid.String()
	if t.IsIntegerType() {
		panic(todo("", n.Position(), t))
	}

	if uintptr(len(s)) > t.Len() {
		s = s[:t.Len()]
	}
	for i := 0; i < len(s); i++ {
		c.initializerUint64(t.Elem(), nil, n, uint64(s[i]), off+uintptr(i))
	}
}

func (g *gen) isConstInitializer(n *cc.Initializer) bool {
	if n.IsConst() {
		return true
	}

	if e := n.AssignmentExpression; e != nil {
		if e.Operand == nil {
			panic(todo("", n.Position()))
		}

		if x, ok := e.Operand.Value().(*cc.InitializerValue); ok && x.IsConst() {
			return true
		}
	}

	for list := n.InitializerList; list != nil; list = list.InitializerList {
		if !g.isConstInitializer(list.Initializer) {
			return false
		}
	}

	e := n.AssignmentExpression
	if e == nil {
		return true
	}

	switch t := e.Operand.Type().Decay(); t.Kind() {
	case cc.Function:
		return true
	case cc.Ptr:
		if d := e.Operand.Declarator(); d != nil && d.StorageClass == cc.Static {
			return true
		}
	}

	return false
}
