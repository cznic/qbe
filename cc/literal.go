// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cc // import "modernc.org/qbe/cc"

import (
	"fmt"
	"math"
	"math/big"

	"modernc.org/cc/v3"
	"modernc.org/token"
)

func (g *gen) intLit(n cc.Node, v cc.Value, op, from, t cc.Type) operand {
	if t == nil {
		panic(todo("", n.Position()))
	}

	switch t.Kind() {
	case cc.Float, cc.Double:
		return g.floatLitFromValue(n, v, from, t)
	}

	var u uint64
	switch x := v.(type) {
	case cc.Int64Value:
		u = uint64(x)
	case cc.Uint64Value:
		u = uint64(x)
	default:
		panic(todo("%v: %T(%[2]v)", n.Position(), x))
	}

	r := g.newVal(nil)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =%s copy %d", r, g.baseType(n, t), u)
	return r
}

func (g *gen) floatLitFromValue(n cc.Node, v cc.Value, from, t cc.Type) operand {
	g.w("\n%s", g.line(n))
	switch x := v.(type) {
	case cc.Float64Value:
		r := g.newVal(nil)
		g.w("\n\t%s =d copy %d", r, math.Float64bits(float64(x)))
		return g.convert(n, r, from, t)
	case cc.Float32Value:
		r := g.newVal(nil)
		g.w("\n\t%s =s copy %d", r, math.Float32bits(float32(x)))
		return g.convert(n, r, from, t)
	case cc.Int64Value:
		switch t.Kind() {
		case cc.Float, cc.Double:
			r := g.newVal(nil)
			g.w("\n\t%s =%s sltof %d", r, g.baseType(n, t), x)
			return r
		default:
			panic(todo("%v: %v", n.Position(), t.Kind()))
		}
	case *cc.Float128Value:
		r := g.newVal(nil)
		g.w("\n\t%s =ld copy %s", r, g.longDoubleString(n, x))
		return g.convert(n, r, from, t)
	default:
		panic(todo("%v: %T(%[2]v)", n.Position(), x))
	}
}

func (g *gen) longDoubleString(n cc.Node, v cc.Value) string {
	switch x := v.(type) {
	case *cc.Float128Value:
		switch {
		case x.NaN:
			return "ld_nan"
		case x.N.IsInf():
			switch {
			case x.N.Sign() > 0:
				return "ld_inf"
			default:
				return "ld_-inf"
			}
		default:
			m := big.NewFloat(0).SetPrec(uint(g.cLongDouble.Size()) * 8).Set(x.N)
			return "ld_" + m.Text('p', 0)
		}
	case cc.Float64Value:
		switch {
		case math.IsNaN(float64(x)):
			return "ld_nan"
		case math.IsInf(float64(x), 1):
			return "ld_inf"
		case math.IsInf(float64(x), -1):
			return "ld_-inf"
		default:
			return fmt.Sprintf("ld_%g", x)
		}
	case cc.Float32Value:
		switch {
		case math.IsNaN(float64(x)):
			return "ld_nan"
		case math.IsInf(float64(x), 1):
			return "ld_inf"
		case math.IsInf(float64(x), -1):
			return "ld_-inf"
		default:
			return fmt.Sprintf("ld_%g", x)
		}
	case cc.Int64Value:
		return fmt.Sprintf("ld_%v", x)
	default:
		panic(todo("%v: %T(%[2]v)", pos(n), x))
	}
}

func pos(n cc.Node) (r token.Position) {
	if n == nil {
		return r
	}

	return n.Position()
}

func (g *gen) floatLit(n *cc.PrimaryExpression, t cc.Type) operand {
	return g.floatLitFromValue(n, n.Operand.Value(), n.Operand.Type(), t)
}

func (g *gen) longStringLit(n cc.Node, v cc.Value) operand {
	switch x := v.(type) {
	case cc.WideStringValue:
		s := cc.StringID(x).String()
		r, ok := g.longStringLiterals[s]
		if !ok {
			r = g.newStaticGlobalArray()
			if g.longStringLiterals == nil {
				g.longStringLiterals = map[string]operand{}
			}
			g.longStringLiterals[s] = r
		}
		return r
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
}

func (g *gen) stringLit(n cc.Node, v cc.Value) operand {
	switch x := v.(type) {
	case cc.StringValue:
		s := cc.StringID(x).String()
		r, ok := g.stringLiterals[s]
		if !ok {
			r = g.newStaticGlobalArray()
			if g.stringLiterals == nil {
				g.stringLiterals = map[string]operand{}
			}
			g.stringLiterals[s] = r
		}
		return r
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
}
