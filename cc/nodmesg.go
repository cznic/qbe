// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build !ecc.dmesg

package cc // import "modernc.org/qbe/cc"

const dmesgs = false

func dmesg(s string, args ...interface{}) {}
