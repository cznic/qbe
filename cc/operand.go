// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cc // import "modernc.org/qbe/cc"

import (
	"fmt"

	"modernc.org/cc/v3"
)

var (
	_ operand = (*val)(nil)
	_ operand = (*pval)(nil)
)

type operand interface {
	String() string
	bitField() *bitField
	isVaArg() bool
	name() cc.StringID
	setBitField(*bitField)
	setIsVaArg()
}

const (
	local = iota
	named
	numbered
	stringVal
)

type val struct {
	bf *bitField
	n  int32

	kind  byte
	s     string // kind: stringVal
	vaArg bool
}

func (n *val) bitField() *bitField { return n.bf }
func (n *val) isVaArg() bool       { return n.vaArg }

func (n *val) name() cc.StringID {
	if n.kind == named {
		return cc.StringID(n.n)
	}

	return 0
}

func (n *val) setBitField(bf *bitField) { n.bf = bf }
func (n *val) setIsVaArg()              { n.vaArg = true }

func (n *val) String() string {
	switch n.kind {
	case local:
		return fmt.Sprintf("%%%d", n.n)
	case named:
		return fmt.Sprintf("$%s", cc.StringID(n.n))
	case numbered:
		return fmt.Sprintf("$%d", n.n)
	case stringVal:
		return n.s
	default:
		panic(todo("", n.n, n.kind))
	}
}

type pval struct{ *val }

type bitField struct {
	f cc.Field
	t cc.Type // struct/union type
}

func (n *bitField) String() string { panic(todo("")) }
