// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cc // import "modernc.org/qbe/cc"

import (
	"fmt"

	"modernc.org/cc/v3"
)

func (g *gen) compoundStatement(n *cc.CompoundStatement, cases map[*cc.LabeledStatement]int, brk, cont int) (returns bool, r operand) {
	for list := n.BlockItemList; list != nil; list = list.BlockItemList {
		returns, r = g.blockItem(list.BlockItem, cases, brk, cont)
	}
	return returns, r
}

func (g *gen) blockItem(n *cc.BlockItem, cases map[*cc.LabeledStatement]int, brk, cont int) (returns bool, r operand) {
	switch n.Case {
	case cc.BlockItemDecl: // Declaration
		g.declaration(n.Declaration)
	case cc.BlockItemStmt: // Statement
		return g.statement(n.Statement, cases, brk, cont)
	case cc.BlockItemLabel: // LabelDeclaration
		panic(todo("%v: %v", n.Position(), n.Case))
	case cc.BlockItemFuncDef: // DeclarationSpecifiers Declarator CompoundStatement
		panic(todo("%v: %v", n.Position(), n.Case))
	case cc.BlockItemPragma: // PragmaSTDC
		panic(todo("%v: %v", n.Position(), n.Case))
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
	return false, nil
}

func (g *gen) statement(n *cc.Statement, cases map[*cc.LabeledStatement]int, brk, cont int) (returns bool, r operand) {
	switch n.Case {
	case cc.StatementLabeled: // LabeledStatement
		return g.labeledStatement(n.LabeledStatement, cases, brk, cont), nil
	case cc.StatementCompound: // CompoundStatement
		g.compoundStatement(n.CompoundStatement, cases, brk, cont)
	case cc.StatementExpr: // ExpressionStatement
		return false, g.expressionStatement(n.ExpressionStatement)
	case cc.StatementSelection: // SelectionStatement
		return g.selectionStatement(n.SelectionStatement, cases, brk, cont), nil
	case cc.StatementIteration: // IterationStatement
		g.iterationStatement(n.IterationStatement, cases, brk, cont)
	case cc.StatementJump: // JumpStatement
		return g.jumpStatement(n.JumpStatement, cases, brk, cont), nil
	case cc.StatementAsm: // AsmStatement
		panic(todo("assembler statements not suported"))
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
	return false, nil
}

func (g *gen) abortAtRuntime(n cc.Node, msg string, args ...interface{}) {
	s := fmt.Sprintf(n.Position().String()+": "+msg, args...)
	v := g.stringLit(n, cc.StringValue(cc.String(s)))
	r := g.newVal(nil)
	g.w("\n%s", g.line(n))
	g.w("\n\t%s =w call $printf(%s %s, ...)", r, g.ptr, v)
	g.w("\n%s", g.line(n))
	g.w("\n\tcall $abort()")
}

func (g *gen) statement1(n *cc.Statement, cases map[*cc.LabeledStatement]int, brk, cont int) (returns bool) {
	returns, _ = g.statement(n, cases, brk, cont)
	return returns
}

func (g *gen) labeledStatement(n *cc.LabeledStatement, cases map[*cc.LabeledStatement]int, brk, cont int) (returns bool) {
	switch n.Case {
	case cc.LabeledStatementLabel: // IDENTIFIER ':' AttributeSpecifierList Statement
		g.w("\n@%s", n.Token.Value)
		g.w("\n%s", g.line(n.Statement))
		return g.statement1(n.Statement, cases, brk, cont)
	case cc.LabeledStatementCaseLabel: // "case" ConstantExpression ':' Statement
		lbl, ok := cases[n]
		if !ok {
			panic(todo(""))
		}
		g.w("\n@%d", lbl)
		g.w("\n%s", g.line(n.Statement))
		return g.statement1(n.Statement, cases, brk, cont)
	case cc.LabeledStatementRange: // "case" ConstantExpression "..." ConstantExpression ':' Statement
		panic(todo("%v: %v", n.Position(), n.Case))
	case cc.LabeledStatementDefault: // "default" ':' Statement
		lbl, ok := cases[n]
		if !ok {
			panic(todo(""))
		}
		g.w("\n@%d", lbl)
		g.w("\n%s", g.line(n.Statement))
		return g.statement1(n.Statement, cases, brk, cont)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) selectionStatement(n *cc.SelectionStatement, cases map[*cc.LabeledStatement]int, brk, cont int) (returns bool) {
	switch n.Case {
	case cc.SelectionStatementIf: // "if" '(' Expression ')' Statement
		return g.ifStatement(n, cases, brk, cont)
	case cc.SelectionStatementIfElse: // "if" '(' Expression ')' Statement "else" Statement
		return g.ifElse(n, cases, brk, cont)
	case cc.SelectionStatementSwitch: // "switch" '(' Expression ')' Statement
		g.switchStmt(n, brk, cont)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
	return false
}

func (g *gen) switchStmt(n *cc.SelectionStatement, brk, cont int) {
	// "switch" '(' Expression ')' Statement
	m := map[*cc.LabeledStatement]int{} // LabeledStatement -> @label.
	cases := n.Cases()
	var defaultCase int
	for _, v := range cases {
		var lbl int
		switch v.Case {
		case cc.LabeledStatementLabel:
			// nop
		case cc.LabeledStatementDefault:
			lbl = g.newLabel()
			defaultCase = lbl
		default:
			lbl = g.newLabel()
		}
		m[v] = lbl
	}
	et := n.Expression.Operand.Type()
	e := g.expression(nil, n.Expression, true, et, nil, brk, cont, -1)
	cmpResult := g.newVal(nil)
	for _, v := range cases {
		if v.Case == cc.LabeledStatementDefault {
			continue
		}

		cmpValue := g.conditionalExpression(nil, v.ConstantExpression.ConditionalExpression, true, et, nil, brk, cont, -1)
		g.w("\n%s", g.line(v))
		g.w("\n\t%s =w ceq%s %s, %s", cmpResult, g.baseType(v, et), e, cmpValue)
		next := g.newLabel()
		g.w("\n%s", g.line(v))
		g.w("\n\tjnz %s, @%d, @%d", cmpResult, m[v], next)
		g.w("\n@%d", next)
	}
	z := g.newLabel()
	switch {
	case defaultCase != 0:
		g.w("\n\tjmp @%d", defaultCase)
	default:
		g.w("\n\tjmp @%d", z)
	}
	g.w("\n@%d", g.newLabel())
	g.w("\n%s", g.line(n.Statement))
	g.statement(n.Statement, m, z, cont)
	g.w("\n@%d", z)
}

func (g *gen) ifStatement(n *cc.SelectionStatement, cases map[*cc.LabeledStatement]int, brk, cont int) (returns bool) {
	// "if" '(' Expression ')' Statement
	//
	//		expr(zero)
	//		statement
	//	@zero
	zero := g.newLabel()
	g.expression(nil, n.Expression, true, n.Expression.Operand.Type(), cases, brk, cont, zero)
	g.w("\n%s", g.line(n.Statement))
	returns = g.statement1(n.Statement, cases, brk, cont)
	g.w("\n@%d", zero)
	return returns
}

// func (g *gen) ifStatement(n *cc.SelectionStatement, cases map[*cc.LabeledStatement]int, brk, cont int) (returns bool) {
// 	// "if" '(' Expression ')' Statement
// 	//
// 	//		%v = expr
// 	//		jnz %v, @a, @z
// 	//	@a
// 	//		statement
// 	//	@z
// 	a := g.newLabel()
// 	z := g.newLabel()
// 	v := g.expression(nil, n.Expression, true, n.Expression.Operand.Type(), cases, brk, cont, -1)
// 	g.w("\n%s", g.line(&n.Token))
// 	g.w("\n\tjnz %s, @%d, @%d", v, a, z)
// 	g.w("\n@%d", a)
// 	g.w("\n%s", g.line(n.Statement))
// 	returns = g.statement1(n.Statement, cases, brk, cont)
// 	g.w("\n@%d", z)
// 	return returns
// }

func (g *gen) ifElse(n *cc.SelectionStatement, cases map[*cc.LabeledStatement]int, brk, cont int) (returns bool) {
	// "if" '(' Expression ')' Statement
	//
	//		expr(zero)
	//		statement
	//		jmp @over
	//	@zero
	//		statement2
	//	@end
	zero := g.newLabel()
	end := g.newLabel()
	g.expression(nil, n.Expression, true, n.Expression.Operand.Type(), cases, brk, cont, zero)
	g.w("\n%s", g.line(n.Statement))
	returns = g.statement1(n.Statement, cases, brk, cont)
	g.w("\n%s", g.line(n.Statement))
	g.w("\n\tjmp @%d", end)
	g.w("\n@%d", zero)
	g.w("\n%s", g.line(n.Statement2))
	if !g.statement1(n.Statement2, cases, brk, cont) {
		returns = false
	}
	g.w("\n@%d", end)
	return returns
}

func (g *gen) iterationStatement(n *cc.IterationStatement, cases map[*cc.LabeledStatement]int, brk, cont int) {
	switch n.Case {
	case cc.IterationStatementWhile: // "while" '(' Expression ')' Statement
		g.while(n, cases, brk, cont)
	case cc.IterationStatementDo: // "do" Statement "while" '(' Expression ')' ';'
		g.doWhile(n, cases, brk, cont)
	case cc.IterationStatementFor: // "for" '(' Expression ';' Expression ';' Expression ')' Statement
		g.iterationStatementFor(n, cases, brk, cont)
	case cc.IterationStatementForDecl: // "for" '(' Declaration Expression ';' Expression ')' Statement
		g.iterationStatementFor2(n, cases, brk, cont)
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
}

func (g *gen) doWhile(n *cc.IterationStatement, cases map[*cc.LabeledStatement]int, brk, cont int) {
	// "do" Statement "while" '(' Expression ')' ';'
	//
	//	@a
	//		statement
	//	@b
	//		# continue
	//		expr(z)
	//		jmp @a
	//	@z
	//		# break
	a := g.newLabel()
	b := g.newLabel()
	z := g.newLabel()
	g.w("\n@%d", a)
	g.w("\n%s", g.line(n.Statement))
	g.statement(n.Statement, cases, z, b)
	g.w("\n@%d", b)
	g.w("\n%s", g.line(n.Expression))
	g.expression(nil, n.Expression, true, n.Expression.Operand.Type(), cases, z, b, z)
	g.w("\n%s", g.line(&n.Token))
	g.w("\n\tjmp @%d", a)
	g.w("\n@%d", z)
	g.w("\n%s", g.line(&n.Token5))
}

func (g *gen) while(n *cc.IterationStatement, cases map[*cc.LabeledStatement]int, brk, cont int) {
	// "while" '(' Expression ')' Statement
	//
	//	@a
	//		# continue
	//		expr(z)
	//		statement
	//		jmp @a
	//	@z
	//		# break
	a := g.newLabel()
	z := g.newLabel()
	g.w("\n@%d", a)
	g.w("\n%s", g.line(n.Expression))
	g.expression(nil, n.Expression, true, n.Expression.Operand.Type(), cases, z, a, z)
	g.w("\n%s", g.line(n.Statement))
	g.statement(n.Statement, cases, z, a)
	g.w("\n%s", g.line(&n.Token))
	g.w("\n\tjmp @%d", a)
	g.w("\n@%d", z)
}

func (g *gen) iterationStatementFor(n *cc.IterationStatement, cases map[*cc.LabeledStatement]int, brk, cont int) {
	// "for" '(' Expression ';' Expression ';' Expression ')' Statement
	//
	//		expr1
	//	@a
	//		expr2(z)
	//		statement
	//	@c
	//		# continue
	//		expr3
	//		jmp @a
	//	@z
	//		# break
	a := g.newLabel()
	c := g.newLabel()
	z := g.newLabel()
	if n.Expression != nil {
		g.expression(nil, n.Expression, false, nil, cases, brk, cont, -1)
	}
	g.w("\n@%d", a)
	if n.Expression2 != nil {
		g.w("\n%s", g.line(n.Expression2))
		g.expression(nil, n.Expression2, true, n.Expression2.Operand.Type(), cases, z, c, z)
	}
	g.statement(n.Statement, cases, z, c)
	g.w("\n@%d", c)
	if n.Expression3 != nil {
		g.w("\n%s", g.line(n.Expression3))
		g.expression(nil, n.Expression3, false, nil, cases, z, c, -1)
	}
	g.w("\n%s", g.line(&n.Token))
	g.w("\n\tjmp @%d", a)
	g.w("\n@%d", z)
}

func (g *gen) iterationStatementFor2(n *cc.IterationStatement, cases map[*cc.LabeledStatement]int, brk, cont int) {
	// "for" '(' Declaration Expression ';' Expression ')' Statement
	//
	//		%i = copy initializer
	//	@a
	//		expr(z)
	//		statement
	//	@c
	//		#continue
	//		expr2
	//		jmp @a
	//	@z
	//		# break
	a := g.newLabel()
	c := g.newLabel()
	z := g.newLabel()
	g.declaration(n.Declaration)
	g.w("\n@%d", a)
	if n.Expression != nil {
		g.w("\n%s", g.line(n.Expression))
		g.expression(nil, n.Expression, true, n.Expression.Operand.Type(), cases, z, c, z)
	}
	g.statement(n.Statement, cases, z, c)
	g.w("\n@%d", c)
	if n.Expression2 != nil {
		g.expression(nil, n.Expression2, false, nil, cases, z, c, -1)
	}
	g.w("\n%s", g.line(&n.Token))
	g.w("\n\tjmp @%d", a)
	g.w("\n@%d", z)
}

func (g *gen) jumpStatement(n *cc.JumpStatement, cases map[*cc.LabeledStatement]int, brk, cont int) (returns bool) {
	switch n.Case {
	case cc.JumpStatementGoto: // "goto" IDENTIFIER ';'
		g.w("\n%s", g.line(&n.Token2))
		g.w("\n\tjmp @%s", n.Token2.Value)
		g.w("\n@%d", g.newLabel())
		g.w("\n%s", g.line(&n.Token3))
	case cc.JumpStatementGotoExpr: // "goto" '*' Expression ';'
		panic(todo("%v: %v", n.Position(), n.Case))
	case cc.JumpStatementContinue: // "continue" ';'
		g.w("\n%s", g.line(&n.Token))
		g.w("\n\tjmp @%d", cont)
		g.w("\n@%d", g.newLabel())
		g.w("\n%s", g.line(&n.Token2))
	case cc.JumpStatementBreak: // "break" ';'
		g.w("\n%s", g.line(&n.Token))
		g.w("\n\tjmp @%d", brk)
		g.w("\n@%d", g.newLabel())
		g.w("\n%s", g.line(&n.Token2))
	case cc.JumpStatementReturn: // "return" Expression ';'
		t := g.f.Declarator.Type().Result()
		var e operand
		if n.Expression != nil {
			u := n.Expression.Operand.Type()
			switch {
			case g.isAggregateType(u):
				e = g.expression(nil, n.Expression, false, u, cases, brk, cont, -1)
			default:
				e = g.expression(nil, n.Expression, true, u, cases, brk, cont, -1)
				if t.Kind() != cc.Void {
					e = g.convert(n, e, u, t)
				}
			}
		}
		g.w("\n%s", g.line(&n.Token))
		g.w("\n\tret")
		if n.Expression != nil && t.Kind() != cc.Void {
			g.w(" %v", e)
		}
		g.w("\n@%d", g.newLabel())
		g.w("\n%s", g.line(&n.Token2))
		return true
	default:
		panic(todo("%v: %v", n.Position(), n.Case))
	}
	return false
}

func (g *gen) expressionStatement(n *cc.ExpressionStatement) (r operand) {
	if n.Expression == nil {
		return nil
	}

	return g.expression(nil, n.Expression, false, n.Expression.Operand.Type(), nil, -1, -1, -1)
}
