void x(char *s, long long i) {
	__builtin_printf(s, i);
}

#define NO_NUMBER (((long long) (~ (unsigned) 0)) + 1)

int main() {
	__builtin_printf("a) %x\n", (unsigned)0);
	__builtin_printf("b) %x\n", (~(unsigned)0));
	x("c) %lli\n", (long long)(~(unsigned)0));
	x("d) %lli\n", ((long long)(~(unsigned)0))+1);
	x("e) %lli\n", (long long)NO_NUMBER);
	if (((int) NO_NUMBER) != 0 || NO_NUMBER == 0) {
		__builtin_abort();
	}
}

