int printf (const char *, ...);

int printf2(char *s, unsigned long long a, unsigned char b, float f, double d) {
  return printf (s, a, b, f, d);
}

int foo (unsigned long long a, unsigned char b, float f, double d) {
  return printf2 ("%llu, %u, %f, %f\n", a, b, f, d);
}

int main (void) {
  int i = -3;
  float f = 3.14f;
  double d = 2.71;
  return foo (i, i, d, f);
}
