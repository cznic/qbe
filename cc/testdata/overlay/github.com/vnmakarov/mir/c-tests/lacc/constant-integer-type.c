int printf (const char *, ...);

int printf2(char *s, unsigned long long a, long long b, long long c, unsigned d, long long e) {
	return printf(s, a, b, c, d, e);
}

int main (void) {
  return printf2 ("%llu, %lld, %lld, %u, %lld\n", 0xF2C889DD98AE1F63, 0xFFD2086BDu, 0xFFD2086BD,
                 0xFFFFFFFF, 4294967295);
}
