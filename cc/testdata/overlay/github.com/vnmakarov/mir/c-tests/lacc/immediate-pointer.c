int printf (const char *, ...);

int printf2(char *s, void* p, long long i) {
	return printf(s, p, i);
}

struct point {
  long long x, y;
} p;

static long long offset = (long long) &((struct point *) 0x8)->y;

int main (void) {
  int *ptr = &*((int *) 0x601044);
#ifdef _WIN32
  return printf2 ("%p, %lld\n", ptr, offset) != 21; /* head zeros */
#else
  return printf2 ("%p, %lld\n", ptr, offset) != 13;
#endif
}
