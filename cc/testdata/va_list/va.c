#include <stdarg.h>
#include <stdio.h>

int main() {
	va_list v;

#ifdef __gnu_linux__

	// linux/amd64
	#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)

	printf("linux/amd64\n");
	printf("sizeof v %llu\n", (unsigned long long)(sizeof v));
	printf("sizeof *v %llu\n", (unsigned long long)(sizeof *v));
	//	sizeof v 24
	//	sizeof *v 24

	// linux/386
	#elif defined(i386) || defined(__i386) || defined(__i386__)

	printf("linux/386\n");
	printf("sizeof v %llu\n", (unsigned long long)(sizeof v));
	printf("sizeof *v %llu\n", (unsigned long long)(sizeof *v));
	//	sizeof v 4
	//	sizeof *v 1

	// linux/arm
	#elif defined(__arm__)

	printf("linux/arm\n");
	printf("sizeof v %llu\n", (unsigned long long)(sizeof v));
	// printf("sizeof *v %llu\n", (unsigned long long)(sizeof *v));
	//	sizeof v 4

	// linux/arm64
	#elif defined(__aarch64__)

	printf("linux/arm64\n");
	printf("sizeof v %llu\n", (unsigned long long)(sizeof v));
	// printf("sizeof *v %llu\n", (unsigned long long)(sizeof *v));
	//	sizeof v 32

	// linux/s390x
	#elif defined(__s390x__)

	printf("linux/s390x\n");
	printf("sizeof v %llu\n", (unsigned long long)(sizeof v));
	printf("sizeof *v %llu\n", (unsigned long long)(sizeof *v));
	printf("sizeof v[0] %llu\n", (unsigned long long)(sizeof v[0]));
	//	sizeof v 32
	//	sizeof *v 32


	#else
	#error unknown/usupported linux architecture
	#endif

#elif defined(__APPLE__) && defined(__MACH__)

	// darwin/amd64
	#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)

	printf("darwin/amd64\n");
	printf("sizeof v %llu\n", (unsigned long long)(sizeof v));
	printf("sizeof *v %llu\n", (unsigned long long)(sizeof *v));
	printf("sizeof v[0] %llu\n", (unsigned long long)(sizeof v[0]));
	//	sizeof v 24
	//	sizeof *v 24

	#else
	#error unknown/usupported darwin architecture
	#endif
	
#elif defined(__MINGW64__)

	// windows/amd64
	#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64)

	printf("windows/amd64\n");
	printf("sizeof v %llu\n", (unsigned long long)(sizeof v));
	printf("sizeof *v %llu\n", (unsigned long long)(sizeof *v));
	//	sizeof v 8
	//	sizeof *v 1

	#else
	#error unknown/usupported windows architecture
	#endif

#else
#error unknown/usupported operating system
#endif

	return 0;
}
