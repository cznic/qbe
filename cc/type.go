// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cc // import "modernc.org/qbe/cc"

import (
	"fmt"

	"modernc.org/cc/v3"
	"modernc.org/qbe"
)

const (
	qbeVaList = ":" + qbe.VaList
)

var (
	idBuiltinVaList = cc.String("__builtin_va_list")
	idGnuCVaList    = cc.String("__gnuc_va_list")
	idVaList        = cc.String("va_list")
)

func (g *gen) abiType(n cc.Node, t cc.Type) string {
	if t.IsIntegerType() {
		switch t.Size() {
		case 1, 2, 4:
			return "w"
		case 8:
			return "l"
		default:
			g.errList.err(n, "invalid/unsupported integer type %s, size %d", t, t.Size())
			return "l" //TODO
		}
	}

	switch t.Kind() {
	case cc.Ptr:
		if g.isCharPtr(n, t) {
			return "c"
		}

		return "p"
	case cc.Array:
		if g.isCharArray(n, t) {
			return "c"
		}

		return "p"
	case cc.Function:
		return "p"
	case cc.Double:
		return "d"
	case cc.Float:
		return "s"
	case cc.LongDouble:
		return "ld"
	case cc.Struct, cc.Union:
		return fmt.Sprintf(":%d_%d", t.Align(), t.Size())
	default:
		panic(todo("", n.Position(), t.Kind()))
	}
}

func (g *gen) baseType(n cc.Node, t cc.Type) string {
	if t.IsIntegerType() {
		switch t.Size() {
		case 1, 2, 4:
			return "w"
		case 8:
			return "l"
		default:
			g.errList.err(n, "invalid/unsupported integer type %s, size %d", t, t.Size())
			return "l" //TODO
		}
	}

	switch t.Kind() {
	case cc.Ptr, cc.Array, cc.Function:
		switch g.cPtr.Size() {
		case 4:
			return "w"
		case 8:
			return "l"
		default:
			panic(todo("%v: %v %v", n.Position(), t, t.Kind()))
		}
	case cc.Double:
		return "d"
	case cc.Float:
		return "s"
	case cc.LongDouble:
		return "ld"
	default:
		panic(todo("", n.Position(), t.Kind()))
	}
}

func (g *gen) loadType(n cc.Node, t cc.Type) string {
	switch t.Decay().Kind() {
	case cc.Int:
		switch t.Size() {
		case 4:
			return "sw"
		default:
			panic(todo("%v: %v %v", n.Position(), t, t.Kind()))
		}
	case cc.UInt:
		switch t.Size() {
		case 4:
			return "uw"
		default:
			panic(todo("%v: %v %v", n.Position(), t, t.Kind()))
		}
	case cc.Long:
		switch t.Size() {
		case 4:
			return "sw"
		case 8:
			return "l"
		default:
			panic(todo("%v: %v %v", n.Position(), t, t.Kind()))
		}
	case cc.ULong:
		switch t.Size() {
		case 4:
			return "uw"
		case 8:
			return "l"
		default:
			panic(todo("%v: %v %v", n.Position(), t, t.Kind()))
		}
	case cc.LongLong, cc.ULongLong:
		return "l"
	case cc.Short:
		return "sh"
	case cc.UShort:
		return "uh"
	case cc.Char, cc.SChar, cc.UChar:
		switch {
		case t.IsSignedType():
			return "sb"
		default:
			return "ub"
		}
	case cc.Ptr, cc.Function:
		switch g.cPtr.Size() {
		case 4:
			return "uw"
		case 8:
			return "l"
		default:
			panic(todo("%v: %v %v", n.Position(), t, t.Kind()))
		}
	case cc.Double:
		return "d"
	case cc.Float:
		return "s"
	case cc.LongDouble:
		return "ld"
	case cc.Enum:
		switch t.Size() {
		case 1:
			panic(todo("%v: %v", n.Position(), t.Size()))
		case 2:
			panic(todo("%v: %v", n.Position(), t.Size()))
		case 4:
			return g.u(t) + "w"
		case 8:
			panic(todo("%v: %v", n.Position(), t.Size()))
		default:
			panic(todo("%v: %v", n.Position(), t.Size()))
		}
	case cc.Bool:
		switch t.Size() {
		case 1:
			return "ub"
		default:
			panic(todo("%v: %v", n.Position(), t.Size()))
		}
	default:
		g.errList.err(n, "invalid/unsupported type %s", t)
		return "l" //TODO
	}
}

func (g *gen) u(t cc.Type) string {
	if g.isUnsigned(t) {
		return "u"
	}

	return ""
}

func (g *gen) su(t cc.Type) string {
	if g.isSigned(t) {
		return "s"
	}

	if g.isUnsigned(t) {
		return "u"
	}

	return ""
}

func (g *gen) isSigned(t cc.Type) bool {
	return t.IsIntegerType() && t.IsSignedType()
}

func (g *gen) isUnsigned(t cc.Type) bool {
	return t.IsIntegerType() && !t.IsSignedType() || t.Decay().Kind() == cc.Ptr
}

func (g *gen) storeType(n cc.Node, t cc.Type) string {
	switch t.Kind() {
	case cc.Char, cc.UChar, cc.SChar, cc.Bool:
		return "b"
	case cc.Short, cc.UShort:
		return "h"
	case cc.Int, cc.UInt, cc.Enum, cc.ULong, cc.Long, cc.ULongLong, cc.LongLong:
		switch t.Size() {
		case 4:
			return "w"
		case 8:
			return "l"
		default:
			panic(todo("%v: %v %v", n.Position(), t, t.Kind()))
		}
	case cc.Ptr:
		switch t.Size() {
		case 4:
			return "w"
		case 8:
			return "l"
		default:
			panic(todo("%v: %v %v", n.Position(), t, t.Kind()))
		}
	case cc.Double:
		return "d"
	case cc.Float:
		return "s"
	case cc.LongDouble:
		return "ld"
	default:
		g.errList.err(n, "invalid/unsupported type %s", t)
		return "l" //TODO
	}
}

func (g *gen) convert(n cc.Node, op operand, from, to cc.Type) (r operand) {
	if from == nil || to == nil || from.String() == to.String() {
		return op
	}

	from = from.Decay()
	to = to.Decay()
	switch {
	case from.Kind() == cc.Ptr && to.Kind() == cc.Ptr:
		return op
	case from.Kind() == cc.Ptr && to.IsIntegerType():
		if from.Size() != to.Size() {
			r := g.newVal(nil)
			g.w("\n%s", g.line(n))
			g.w("\n\t%s =%s copy %s", r, g.ptr, op)
			op = r
		}
		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s copy %s", r, g.baseType(n, to), op)
		return r
	case from.IsScalarType() && to.IsScalarType():
		fromTag := g.baseType(n, from)
		toTag := g.baseType(n, to)
		if fromTag == toTag {
			switch {
			case from.Size() == to.Size():
				if to.IsIntegerType() && to.Size() < 4 && from.IsSignedType() && !to.IsSignedType() {
					m := uint(1)<<(to.Size()*8) - 1
					r := g.newVal(nil)
					g.w("\n%s", g.line(n))
					g.w("\n\t%s =%s and %s, %d", r, toTag, op, m)
					return r
				}

				return op
			case from.Size() > to.Size():
				r := g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =%s ext%s %s", r, toTag, g.loadType(n, to), op)
				return r
			}
		}

		switch fromTag {
		case "d":
			switch toTag {
			case "s":
				r := g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =s truncd %s", r, op)
				return r
			case "ld":
				r := g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =ld extd %s", r, op)
				return r
			case "w", "l":
				r := g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =%s dto%si %s", r, toTag, g.su(to), op)
				return r
			}
		case "s":
			switch toTag {
			case "d":
				r := g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =d exts %s", r, op)
				return r
			case "ld":
				r := g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =ld exts %s", r, op)
				return r
			case "w", "l":
				r := g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =%s sto%si %s", r, toTag, g.su(to), op)
				return r
			}
		case "ld":
			switch toTag {
			case "d":
				r := g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =d truncld %s", r, op)
				return r
			case "w", "l":
				r := g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =%s ldto%si %s", r, toTag, g.su(to), op)
				return r
			}
		case "w":
			switch toTag {
			case "s", "d", "ld":
				r = g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =%s %swtof %s", r, toTag, g.su(from), op)
				return r
			}
		case "l":
			switch toTag {
			case "s", "d", "ld":
				r = g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =%s %sltof %s", r, toTag, g.su(from), op)
				return r
			case "w":
				if to.Size() == 4 {
					r = g.newVal(nil)
					g.w("\n%s", g.line(n))
					g.w("\n\t%s =w copy %s", r, op)
					return r
				}

				r = g.newVal(nil)
				g.w("\n%s", g.line(n))
				g.w("\n\t%s =w ext%s %s", r, g.loadType(n, to), op)
				return r
			}
		}

		if to.Size() <= from.Size() || !from.IsIntegerType() && from.Kind() != cc.Ptr || !to.IsIntegerType() && to.Kind() != cc.Ptr {
			panic(todo("%v: %v -> %v", n.Position(), from, to))
		}

		r := g.newVal(nil)
		g.w("\n%s", g.line(n))
		g.w("\n\t%s =%s ext%s %s", r, toTag, g.loadType(n, from), op)
		return r
	case from.Kind() == cc.Function && (to.Kind() == cc.Ptr || to.Kind() == cc.Function):
		return op
	case from.Kind() == cc.Ptr && to.Kind() == cc.Function:
		return op
	case to.Kind() == cc.Void:
		return nil
	case g.isRecordType(from) || g.isRecordType(to):
		return op
	case from.Kind() == cc.Function && to.IsIntegerType():
		return g.convert(n, op, g.cPtr, to)
	default:
		panic(todo("%v: op %v, from %v -> to %v", n.Position(), op, from, to))
	}
}

func (g *gen) isAggregateType(t cc.Type) bool {
	switch t.Kind() {
	case cc.Struct, cc.Union, cc.Array:
		return true
	default:
		return false
	}
}

func (g *gen) isRecordType(t cc.Type) bool {
	switch t.Kind() {
	case cc.Struct, cc.Union:
		return true
	default:
		return false
	}
}

func (g *gen) isAggregateDeclarator(d *cc.Declarator) bool {
	switch d.Type().Kind() {
	case cc.Struct, cc.Union:
		return true
	case cc.Array:
		return !d.IsParameter
	default:
		return false
	}
}

func (g *gen) isVaList(t cc.Type) bool {
	for t.IsAliasType() {
		switch t.AliasDeclarator().Name() {
		case idVaList, idBuiltinVaList, idGnuCVaList:
			return true
		}

		if u := t.Alias(); u != t {
			t = u
			continue
		}

		return false
	}
	return false
}

func (g *gen) underlyingType(t cc.Type) cc.Type {
	for t.IsAliasType() {
		if u := t.Alias(); u == t {
			t = u
			continue
		}

		break
	}
	return t
}
