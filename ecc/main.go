package main // import "modernc.org/qbe/ecc"

import (
	"fmt"
	"os"

	"modernc.org/qbe/cc"
)

func main() {
	if err := cc.NewTask(os.Args[0], os.Args[1:], os.Stdout, os.Stderr).Main(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
