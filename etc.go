// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package qbe // import "modernc.org/qbe"

import (
	"fmt"
	"go/token"
	"os"
	"path/filepath"
	"runtime"
	"sort"
	"strings"

	"modernc.org/mathutil"
)

func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	fn = filepath.Base(fn)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
	}
	return fmt.Sprintf("%s:%d:%s", fn, fl, fns)
}

func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	pc, fn, fl, _ := runtime.Caller(1)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
	}
	r := fmt.Sprintf("%s:%d:%s: TODOTODO %s", fn, fl, fns, s) //TODOOK
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	_, fn, fl, _ := runtime.Caller(1)
	r := fmt.Sprintf("%s:%d: TRC %s", fn, fl, s)
	fmt.Fprintf(os.Stdout, "%s\n", r)
	os.Stdout.Sync()
	return r
}

func errorf(s string, args ...interface{}) error {
	e := fmt.Errorf(s, args...)
	return fmt.Errorf("%w (%s)", e, origin(2))
}

type bits []int

func newBits(n int) (r bits) {
	if mathutil.IntBits == 64 {
		return make(bits, (n+63)>>6)
	}

	return make(bits, (n+31)>>5)
}

func (b bits) len() int { return len(b) * mathutil.IntBits }

func (b bits) String() string {
	var a []int
	for i := 0; i < b.len(); i++ {
		if b.has(i) {
			a = append(a, i)
		}
	}
	return fmt.Sprint(a)
}

func (b bits) dump(f *FuncDef) []string {
	m := map[int]struct{}{}
	for i := 0; i < b.len(); i++ {
		if b.has(i) {
			m[i] = struct{}{}
		}
	}
	var r []string
	for k, v := range f.Scope.Nodes {
		if x, ok := v.(*LocalInfo); ok {
			if _, ok := m[x.N]; ok {
				r = append(r, k)
			}
		}
	}
	sort.Strings(r)
	return r
}

func (b bits) and(c bits) bits {
	b = b.clone()
	for i, v := range b {
		b[i] = v & c[i]
	}
	return b
}

func (b bits) clone() bits {
	r := make(bits, len(b))
	copy(r, b)
	return r
}

func (b bits) has(n int) bool {
	if mathutil.IntBits == 64 {
		return b != nil && b[n>>6]&(1<<uint(n&63)) != 0
	}

	return b != nil && b[n>>5]&(1<<uint(n&31)) != 0
}

func (b bits) or(c bits) bits {
	b = b.clone()
	for i, v := range b {
		b[i] = v | c[i]
	}
	return b
}

func (b bits) set(n int) {
	if mathutil.IntBits == 64 {
		b[n>>6] |= 1 << uint(n&63)
		return
	}

	b[n>>5] |= 1 << uint(n&31)
}

// C string literals differ from Go literals. One of the problems is the
// \xnn escape sequence. In Go nn must be exactly two hexadecimal digits. In C
// one or more. This means a Go string literal "\x123" represents two bytes 0x12
// and 0x03 but the same literal in C means one byte 0x23.
func safeQuoteToASCII(s string) string {
	var b strings.Builder
	b.WriteByte('"')
	mustQuoteHex := false
	for i := 0; i < len(s); i++ {
		switch c := s[i]; c {
		case '\n':
			fmt.Fprintf(&b, "\\n")
		case '\t':
			fmt.Fprintf(&b, "\\t")
		case '\r':
			fmt.Fprintf(&b, "\\r")
		case '\v':
			fmt.Fprintf(&b, "\\v")
		case '\f':
			fmt.Fprintf(&b, "\\f")
		case '\a':
			fmt.Fprintf(&b, "\\a")
		case '\b':
			fmt.Fprintf(&b, "\\b")
		case '\\':
			fmt.Fprintf(&b, "\\\\")
		case '"':
			fmt.Fprintf(&b, "\\\"")
		default:
			switch {
			case c < ' ' || c >= 0x7f || c == '"':
				fmt.Fprintf(&b, "\\x%02x", c)
				mustQuoteHex = true
			case mustQuoteHex && (c >= '0' && c <= '9' || c >= 'a' && c <= 'f' || c >= 'A' && c <= 'F'):
				fmt.Fprintf(&b, "\\x%02x", c)
			default:
				b.WriteByte(c)
				mustQuoteHex = false
			}
		}
	}
	b.WriteByte('"')
	return b.String()
}

func safePos(n Node) (r token.Position) {
	if n != nil {
		r = n.Position()
	}
	return r
}
