module modernc.org/qbe

go 1.18

require (
	github.com/pmezard/go-difflib v1.0.0
	modernc.org/cc/v3 v3.40.0
	modernc.org/ccgo/v3 v3.16.13-0.20221017192402-261537637ce8
	modernc.org/ccorpus v1.11.6
	modernc.org/mathutil v1.5.0
	modernc.org/opt v0.1.3
	modernc.org/scannertest v1.0.0
	modernc.org/token v1.1.0
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	golang.org/x/exp v0.0.0-20181106170214-d68db9428509 // indirect
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab // indirect
	golang.org/x/tools v0.0.0-20201124115921-2c860bdd6e78 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	lukechampine.com/uint128 v1.2.0 // indirect
	modernc.org/fileutil v1.0.0 // indirect
	modernc.org/httpfs v1.0.6 // indirect
	modernc.org/lex v1.1.0 // indirect
	modernc.org/lexer v1.0.0 // indirect
	modernc.org/libc v1.21.2 // indirect
	modernc.org/memory v1.4.0 // indirect
	modernc.org/strutil v1.1.3 // indirect
)
