// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build !qbec.dmesg
// +build !qbec.dmesg

package qbe // import "modernc.org/qbe"

const dmesgs = false

func dmesg(s string, args ...interface{}) {}
