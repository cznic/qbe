// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package qbe // import "modernc.org/qbe"

import (
	"bytes"
	"go/token"
	"strings"
)

var (
	_ = []Node{
		(*CSTAdd)(nil),
		(*CSTAlloc)(nil),
		(*CSTAnd)(nil),
		(*CSTBlock)(nil),
		(*CSTCall)(nil),
		(*CSTCast)(nil),
		(*CSTCmp)(nil),
		(*CSTCopy)(nil),
		(*CSTDataDef)(nil),
		(*CSTDataItemGlobal)(nil),
		(*CSTDataItemZero)(nil),
		(*CSTDataItems)(nil),
		(*CSTDiv)(nil),
		(*CSTDtosi)(nil),
		(*CSTDtoui)(nil),
		(*CSTEnvArg)(nil),
		(*CSTEnvParam)(nil),
		(*CSTExt)(nil),
		(*CSTField)(nil),
		(*CSTFields)(nil),
		(*CSTFuncDef)(nil),
		(*CSTInstPreamble)(nil),
		(*CSTIntConst)(nil),
		(*CSTJmp)(nil),
		(*CSTJnz)(nil),
		(*CSTLoad)(nil),
		(*CSTMul)(nil),
		(*CSTOr)(nil),
		(*CSTPhi)(nil),
		(*CSTRegularArg)(nil),
		(*CSTRegularParam)(nil),
		(*CSTRem)(nil),
		(*CSTRet)(nil),
		(*CSTSar)(nil),
		(*CSTShl)(nil),
		(*CSTShr)(nil),
		(*CSTSltof)(nil),
		(*CSTStore)(nil),
		(*CSTStosi)(nil),
		(*CSTStoui)(nil),
		(*CSTSub)(nil),
		(*CSTSwtof)(nil),
		(*CSTTruncd)(nil),
		(*CSTTruncld)(nil),
		(*CSTTypeDef)(nil),
		(*CSTUdiv)(nil),
		(*CSTUrem)(nil),
		(*CSTUwtof)(nil),
		(*CSTVaArg)(nil),
		(*CSTVaStart)(nil),
		(*CSTVariadicMarker)(nil),
		(*CSTXor)(nil),
	}
)

// CST represents the complete syntax tree. It contains all the tokens of the
// input. The original source, or any part of it can be reconstructed from the
// CST or some part of it, including the original whitespace/comments.
type CST struct {
	Defs     []Node
	EOF      CSTToken
	source   *source
	firstSep []byte
}

// FirstSeparator reports the white space and comments preceding first token.
// The result must not be modified.
func (n *CST) FirstSeparator() []byte {
	return n.firstSep[:len(n.firstSep):len(n.firstSep)]
}

// Parse produces a CST or an error, of any. Positions are reported as if buf
// is coming from a file named name. The buffer becomes owned by the CST and
// must not be modified after calling Parse.
func Parse(buf []byte, name string, allErrros bool) (*CST, error) {
	p, err := newParser(buf, name, allErrros)
	if err != nil {
		return nil, err
	}

	r, err := p.parse()
	if err != nil {
		return nil, err
	}

	r.firstSep = p.firstSep
	r.EOF = p.Scanner.Tok
	return r, nil
}

type parser struct {
	*Scanner
	firstSep []byte
	tok      CSTToken
}

func newParser(buf []byte, name string, allErrros bool) (*parser, error) {
	s, err := NewScanner(buf, name, allErrros)
	if err != nil {
		return nil, err
	}

	return &parser{Scanner: s}, nil
}

func (p *parser) ch() Ch                { return p.tok.Ch }
func (p *parser) consume() (r CSTToken) { defer p.next(); return p.tok }
func (p *parser) next() Ch              { p.Scan(); p.tok = p.Scanner.Tok; return p.ch() }

func (p *parser) must(ch Ch) CSTToken {
	if p.ch() != ch {
		p.err(p.tok.off, 1, "expected %s", ch.str())
	}
	defer p.next()
	return p.tok
}

func (p *parser) parse() (*CST, error) {
	p.next()
	p.firstSep = p.tok.Sep()
	r := &CST{source: p.source}
	for {
		switch p.ch() {
		case FUNCTION:
			r.Defs = append(r.Defs, p.funcDef(CSTToken{}))
		case eof:
			return r, p.Err()
		case EXPORT:
			export := p.consume()
			switch p.ch() {
			case FUNCTION:
				r.Defs = append(r.Defs, p.funcDef(export))
			case DATA:
				r.Defs = append(r.Defs, p.dataDef(export, CSTToken{}))
			case RO:
				ro := p.consume()
				switch p.ch() {
				case DATA:
					r.Defs = append(r.Defs, p.dataDef(export, ro))
				default:
					p.err(p.tok.off, 0, "expected data")
					p.consume()
				}
			default:
				p.err(p.tok.off, 0, "expected function or data")
				p.consume()
			}
		case DATA:
			r.Defs = append(r.Defs, p.dataDef(CSTToken{}, CSTToken{}))
		case TYPE:
			r.Defs = append(r.Defs, p.typeDef())
		case RO:
			ro := p.consume()
			switch p.ch() {
			case DATA:
				r.Defs = append(r.Defs, p.dataDef(CSTToken{}, ro))
			default:
				p.err(p.tok.off, 0, "expected data")
				p.consume()
			}
		default:
			p.err(p.tok.off, 0, "expected export, function, ro, data or type")
			p.consume()
		}
	}
}

// CSTTypeDef is a CST node for:
//
//  TYPEDEF :=
//      # Regular type
//      'type' :IDENT '=' ['align' NUMBER]
//      '{'
//          ( SUBTY [NUMBER] ),
//      '}'
//    | # Opaque type
//      'type' :IDENT '=' 'align' NUMBER '{' NUMBER '}'
//
//  SUBTY := EXTTY | :IDENT
type CSTTypeDef struct {
	Type   CSTToken
	Name   CSTToken
	Eq     CSTToken
	Align  CSTToken
	Number CSTToken
	Fields *CSTFields
}

// Position implements Node.
func (n *CSTTypeDef) Position() token.Position { return n.Type.Position() }

func (p *parser) typeDef() Node {
	p.attrs = nil
	r := &CSTTypeDef{
		Type: p.must(TYPE),
		Name: p.must(TYPENAME),
		Eq:   p.must('='),
	}
	hasAlign := false
	if p.ch() == ALIGN {
		r.Align = p.consume()
		r.Number = p.must(INT_LIT)
		hasAlign = true
	}
	r.Fields = p.fields(hasAlign)
	return r
}

// CSTField is a CST node for a field of a TypeDef.
type CSTField struct {
	Type   CSTToken
	Number CSTToken
	Comma  CSTToken
}

// Position implements Node.
func (n *CSTField) Position() token.Position { return n.Type.Position() }

// CSTFields is a CST node for the fields of a TypeDef.
type CSTFields struct {
	LBrace CSTToken
	Fields []Node
	RBrace CSTToken
}

// Position implements Node.
func (n *CSTFields) Position() token.Position { return n.LBrace.Position() }

func (p *parser) fields(acceptOpaqueType bool) *CSTFields {
	r := &CSTFields{LBrace: p.must('{')}
	for {
		var typ CSTToken
		switch p.ch() {
		case L, W, S, D, LD, B, H, TYPENAME: // SUBTY
			typ = p.consume()
		case '}':
			r.RBrace = p.consume()
			return r
		case INT_LIT:
			if !acceptOpaqueType {
				p.err(p.tok.off, 0, "unexpected opaque type")
			}

			if len(r.Fields) != 0 {
				p.err(p.tok.off, 0, "opaque type can specify only one number")
			}

			r.Fields = append(r.Fields, &CSTField{Number: p.consume()})
			r.RBrace = p.must('}')
			return r
		case '{':
			r.Fields = append(r.Fields, p.fields(false))
			continue
		default:
			p.err(p.tok.off, 0, "syntax error, unexpected %s", p.ch().str())
			p.consume()
			return r
		}

		m := &CSTField{Type: typ}
		if p.ch() == INT_LIT {
			m.Number = p.consume()
		}

		switch p.ch() {
		case ',':
			m.Comma = p.consume()
			r.Fields = append(r.Fields, m)
		case '}':
			r.RBrace = p.consume()
			r.Fields = append(r.Fields, m)
			return r
		default:
			p.err(p.tok.off, 0, "syntax error, unexpected %s", p.ch().str())
			p.consume()
			return r
		}
	}
}

// CSTFuncDef is a CST node for:
//
//  FUNCDEF :=
//      ['export'] 'function' [ABITY] $IDENT '(' (PARAM), ')'
//      '{'
//         BLOCK+
//      '}'
type CSTFuncDef struct {
	Export     CSTToken
	Function   CSTToken
	ABIType    CSTToken
	Global     CSTToken
	LParen     CSTToken
	Params     []Node
	RParen     CSTToken
	LBrace     CSTToken
	Blocks     []*CSTBlock
	RBrace     CSTToken
	Map        map[string]*MapInfo
	Attributes []string
}

type MapInfo struct {
	To   string
	More string
}

// Position implements Node.
func (n *CSTFuncDef) Position() token.Position {
	if n.Export.IsValid() {
		return n.Export.Position()
	}

	return n.Function.Position()
}

var (
	attributePrefix    = []byte("attribute ")
	mapDirectivePrefix = []byte("map ")
)

func (p *parser) funcDef(export CSTToken) (r *CSTFuncDef) {
	attrs := p.attrs
	p.attrs = nil
	var m map[string]*MapInfo
	p.directiveHandler = func(line []byte) (r bool) {
		switch {
		case bytes.HasPrefix(line, mapDirectivePrefix):
			line = line[len(mapDirectivePrefix) : len(line)-1]
			a := strings.SplitN(string(line), " ", 3) // %x foo more
			if m == nil {
				m = map[string]*MapInfo{}
			}
			m[a[0]] = &MapInfo{To: a[1], More: a[2]}
			return true
		}

		return false
	}

	defer func() {
		p.directiveHandler = nil
		r.Map = m
	}()

	return &CSTFuncDef{
		Export:     export,
		Function:   p.consume(),
		ABIType:    p.abiTypeOpt(),
		Global:     p.must(GLOBAL),
		LParen:     p.must('('),
		Params:     p.params(),
		RParen:     p.must(')'),
		LBrace:     p.must('{'),
		Blocks:     p.blocks(),
		RBrace:     p.must('}'),
		Attributes: attrs,
	}
}

// CSTDataDef is a CST node for:
//
//  DATADEF :=
//      ['export'] ['ro'] 'data' $IDENT '=' ['align' NUMBER]
//      '{'
//          ( EXTTY DATAITEM+
//          | 'z'   NUMBER ),
//      '}';
type CSTDataDef struct {
	Export     CSTToken
	RO         CSTToken
	Data       CSTToken
	Global     CSTToken
	Eq         CSTToken
	Align      CSTToken
	Number     CSTToken
	LBrace     CSTToken
	Items      []Node
	RBrace     CSTToken
	Attributes []string
}

// Position implements Node.
func (n *CSTDataDef) Position() token.Position {
	if n.Export.IsValid() {
		return n.Export.Position()
	}

	return n.Data.Position()
}

func (p *parser) dataDef(export, ro CSTToken) *CSTDataDef {
	attrs := p.attrs
	p.attrs = nil
	r := &CSTDataDef{
		Export:     export,
		RO:         ro,
		Data:       p.consume(),
		Global:     p.must(GLOBAL),
		Eq:         p.must('='),
		Attributes: attrs,
	}
	if p.ch() == ALIGN {
		r.Align = p.consume()
		r.Number = p.must(INT_LIT)
	}
	r.LBrace = p.must('{')
	r.Items = p.dataDefItems()
	r.RBrace = p.must('}')
	return r
}

// CSTDataItemZero is a CST node for zero data item of a data definition.
type CSTDataItemZero struct {
	Z      CSTToken
	Number CSTToken
}

// Position implements Node.
func (n *CSTDataItemZero) Position() token.Position { return n.Z.Position() }

//          ( EXTTY DATAITEM+
//          | 'z'   NUMBER ),
func (p *parser) dataDefItems() (r []Node) {
	for {
		switch p.ch() {
		case L, W, S, D, LD, B, H: // EXTTY
			r = append(r, p.dataItems(p.consume()))
		case Z:
			r = append(r, &CSTDataItemZero{p.consume(), p.must(INT_LIT)})
		case '}':
			return r
		default:
			p.err(p.tok.off, 0, "syntax error, unexpected %s", p.ch().str())
			p.consume()
			return r
		}

		if p.ch() != ',' {
			return r
		}

		t := p.consume()
		r = append(r, &t)
	}
}

// CSTDataItems is a CST node for the data items of a data definition.
type CSTDataItems struct {
	Items []Node
}

// Position implements Node.
func (n CSTDataItems) Position() token.Position { return n.Items[0].Position() }

// CSTDataItemString represents a CST node for a string literal data
// initializer.
type CSTDataItemString struct {
	Type CSTToken
	Val  CSTToken
}

// Position implements Node.
func (n *CSTDataItemString) Position() token.Position { return n.Type.Position() }

// CSTDataItemConst represents a CST node for a numeric/pointer initializer.
type CSTDataItemConst struct {
	Type  CSTToken
	Const Node
}

// Position implements Node.
func (n *CSTDataItemConst) Position() token.Position { return n.Type.Position() }

// CSTDataItemGlobal is a CST node for an address of a global with optional
// offset.
type CSTDataItemGlobal struct {
	Type   CSTToken
	Global CSTToken
	Plus   CSTToken
	Number CSTToken
}

// Position implements Node.
func (n *CSTDataItemGlobal) Position() token.Position { return n.Type.Position() }

//  DATAITEM :=
//      $IDENT ['+' NUMBER]  # Symbol and offset
//    |  '"' ... '"'         # String
//    |  CONST               # Constant
func (p *parser) dataItems(typ CSTToken) (r CSTDataItems) {
	for {
		switch p.ch() {
		case GLOBAL:
			ident := p.consume()
			switch p.ch() {
			case '+':
				r.Items = append(r.Items, &CSTDataItemGlobal{Type: typ, Global: ident, Plus: p.consume(), Number: p.must(INT_LIT)})
			default:
				r.Items = append(r.Items, &CSTDataItemGlobal{Type: typ, Global: ident})
			}
		case STRING_LIT:
			r.Items = append(r.Items, &CSTDataItemString{typ, p.consume()})
		case '-', INT_LIT, FLOAT32_LIT, FLOAT64_LIT, LONG_DOUBLE_LIT:
			r.Items = append(r.Items, &CSTDataItemConst{typ, p.constant()})
		default:
			if len(r.Items) == 0 {
				p.err(p.tok.off, 0, "expected data item")
			}
			return r
		}
	}
}

// CSTIntConst is a CST node for a integer literal with an optional negative sign.
type CSTIntConst struct {
	Sign   CSTToken
	Number CSTToken
}

// Position implements Node.
func (n *CSTIntConst) Position() token.Position {
	if n.Sign.IsValid() {
		return n.Sign.Position()
	}

	return n.Number.Position()
}

// CSTLongDoubleLit is a CST node for a long double literal.
type CSTLongDoubleLit CSTToken

// Position implements Node.
func (n CSTLongDoubleLit) Position() token.Position { return CSTToken(n).Position() }

// CSTFloat64Lit is a CST node for a float64 literal.
type CSTFloat64Lit CSTToken

// Position implements Node.
func (n CSTFloat64Lit) Position() token.Position { return CSTToken(n).Position() }

// CSTFloat32Lit is a CST node for a float32 literal.
type CSTFloat32Lit CSTToken

// Position implements Node.
func (n CSTFloat32Lit) Position() token.Position { return CSTToken(n).Position() }

//  CONST :=
//      ['-'] NUMBER  # Decimal integer
//    | 's_' FP       # Single-precision float
//    | 'd_' FP       # Double-precision float
//    | $IDENT        # Global symbol
func (p *parser) constant() Node {
	switch p.ch() {
	case '-':
		return &CSTIntConst{Sign: p.consume(), Number: p.must(INT_LIT)}
	case INT_LIT:
		return &CSTIntConst{Number: p.consume()}
	case FLOAT32_LIT:
		return CSTFloat32Lit(p.consume())
	case FLOAT64_LIT:
		return CSTFloat64Lit(p.consume())
	case LONG_DOUBLE_LIT:
		return CSTLongDoubleLit(p.consume())
	case GLOBAL:
		t := p.tok
		return &t
	default:
		p.err(p.tok.off, 0, "syntax error, unexpected %s", p.ch().str())
		p.consume()
		return nil
	}
}

// CSTBlock is a CST node for:
//
//  BLOCK :=
//      @IDENT    # Block label
//      PHI*      # Phi instructions
//      INST*     # Regular instructions
//      JUMP      # Jump or return
type CSTBlock struct {
	Label CSTToken
	Phis  []*CSTPhi
	Insts []Node
	Jump  Node
}

// Position implements Node.
func (n *CSTBlock) Position() token.Position { return n.Label.Position() }

func (p *parser) blocks() (r []*CSTBlock) {
	for {
		b := &CSTBlock{}
		switch p.ch() {
		case LABEL:
			b.Label = p.consume()
		case '}', eof:
			return r
		default:
			p.err(p.tok.off, 0, "expected label")
		}

		var x Node
		b.Phis, x = p.phis()
		b.Insts = p.insts(x)
		b.Jump = p.jump()
		r = append(r, b)
	}
}

// CSTJnz is a CST node for the jnz instruction.
type CSTJnz struct {
	Inst    CSTToken
	Val     Node
	Comma   CSTToken
	LabelNZ CSTToken
	Comma2  CSTToken
	LabelZ  CSTToken
}

// Position implements Node.
func (n *CSTJnz) Position() token.Position { return n.Inst.Position() }

// CSTRet is a CST node for the ret instruction with an optional return value.
type CSTRet struct {
	Inst CSTToken
	Val  Node
}

// Position implements Node.
func (n *CSTRet) Position() token.Position { return n.Inst.Position() }

// CSTJmp is a CST node for the jmp instruction.
type CSTJmp struct {
	Inst  CSTToken
	Label CSTToken
}

// Position implements Node.
func (n *CSTJmp) Position() token.Position { return n.Inst.Position() }

//  JUMP :=
//      'jmp' @IDENT               # Unconditional
//    | 'jnz' VAL, @IDENT, @IDENT  # Conditional
//    | 'ret' [VAL]                # Return
func (p *parser) jump() Node {
	switch p.ch() {
	case JMP:
		return &CSTJmp{p.consume(), p.must(LABEL)}
	case JNZ:
		return &CSTJnz{
			p.consume(),
			p.val(),
			p.must(','),
			p.must(LABEL),
			p.must(','),
			p.must(LABEL),
		}
	case RET:
		return &CSTRet{p.consume(), p.valOpt()}
	default:
		return nil
	}
}

// CSTStore is a CST node for the store instruction.
type CSTStore struct {
	Inst  CSTToken
	Dst   Node
	Comma CSTToken
	Src   Node
}

// Position implements Node.
func (n *CSTStore) Position() token.Position { return n.Inst.Position() }

// CSTVaStart is a CST node for the vastart instruction.
type CSTVaStart struct {
	Inst CSTToken
	Arg  Node
}

// Position implements Node.
func (n *CSTVaStart) Position() token.Position { return n.Inst.Position() }

func (p *parser) insts(x Node) (r []Node) {
	if x != nil {
		r = append(r, x)
	}
	for {
		switch p.ch() {
		case LOCAL:
			r = append(r, p.inst())
		case LABEL, JMP, JNZ, RET:
			return r
		case STOREB, STORED, STORELD, STOREH, STOREL, STORES, STOREW:
			r = append(r, &CSTStore{Inst: p.consume(), Src: p.val(), Comma: p.must(','), Dst: p.val()})
		case CALL:
			r = append(r, p.call(CSTInstPreamble{Inst: p.consume()}))
		case VASTART:
			r = append(r, &CSTVaStart{p.consume(), p.val()})
		case eof, '}':
			return r
		default:
			p.err(p.tok.off, 0, "syntax error, unexpected %s", p.ch().str())
			p.consume()
			return r
		}
	}
}

// CSTPhi is a CST node for the CSTPhi instruction.
//
//  PHI := %IDENT '=' BASETY 'phi' ( @IDENT VAL ),
type CSTPhi struct {
	CSTInstPreamble
	Args []CSTPhiArg
}

func (p *parser) phis() (r []*CSTPhi, xtra Node) {
	for p.ch() == LOCAL {
		switch x := p.inst().(type) {
		case *CSTPhi:
			r = append(r, x)
		default:
			return r, x
		}
	}
	return r, nil
}

// CSTInstPreamble represents the common part of a 3-address instruction.
type CSTInstPreamble struct {
	Dst  CSTToken
	Eq   CSTToken
	Type CSTToken
	Inst CSTToken
}

// Position implements Node.
func (n *CSTInstPreamble) Position() token.Position {
	if n.Dst.IsValid() {
		return n.Dst.Position()
	}

	return n.Inst.Position()
}

func (p *parser) inst() Node {
	pre := CSTInstPreamble{
		Dst:  p.dst(),
		Eq:   p.must('='),
		Type: p.abiType(),
	}
	return p.op(pre)
}

// CSTCopy is a CST node for the copy instruction.
type CSTCopy struct {
	CSTInstPreamble
	Val Node
}

// CSTDeclare is a CST node for the declare instruction.
type CSTDeclare struct {
	CSTInstPreamble
}

// CSTSltof is a CST node for the sltof instruction.
type CSTSltof struct {
	CSTInstPreamble
	Val Node
}

// CSTStosi is a CST node for the stosi instruction.
type CSTStosi struct {
	CSTInstPreamble
	Val Node
}

// CSTStoui is a CST node for the stoui instruction.
type CSTStoui struct {
	CSTInstPreamble
	Val Node
}

// CSTVaArg is a CST node for the vaarg instruction.
type CSTVaArg struct {
	CSTInstPreamble
	Val Node
}

// CSTTruncd is a CST node for the truncd instruction.
type CSTTruncd struct {
	CSTInstPreamble
	Val Node
}

// CSTTruncld is a CST node for the truncld instruction.
type CSTTruncld struct {
	CSTInstPreamble
	Val Node
}

// CSTSwtof is a CST node for the swtof instruction.
type CSTSwtof struct {
	CSTInstPreamble
	Val Node
}

// CSTUltof is a CST node for the ultof instruction.
type CSTUltof struct {
	CSTInstPreamble
	Val Node
}

// CSTUwtof is a CST node for the uwtof instruction.
type CSTUwtof struct {
	CSTInstPreamble
	Val Node
}

// CSTAdd is a CST node for the add instruction.
type CSTAdd struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTShl is a CST node for the shl instruction.
type CSTShl struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTShr is a CST node for the shr instruction.
type CSTShr struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTSar is a CST node for the sar instruction.
type CSTSar struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTDiv is a CST node for the div instruction.
type CSTDiv struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTUdiv is a CST node for the udiv instruction.
type CSTUdiv struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTSub is a CST node for the sub instruction.
type CSTSub struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTMul is a CST node for the mul instruction.
type CSTMul struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTAnd is a CST node for the and instruction.
type CSTAnd struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTOr is a CST node for the or instruction.
type CSTOr struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTXor is a CST node for the xor instruction.
type CSTXor struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTRem is a CST node for the rem instruction.
type CSTRem struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTUrem is a CST node for the urem instruction.
type CSTUrem struct {
	CSTInstPreamble
	CSTBinaryOperands
}

// CSTAlloc is a CST node for the various alloc* instructions.
type CSTAlloc struct {
	CSTInstPreamble
	Val Node
}

// CSTLoad is a CST node for the various load* instructions.
type CSTLoad struct {
	CSTInstPreamble
	Val Node
}

// CSTExt is a CST node for the various ext* instructions.
type CSTExt struct {
	CSTInstPreamble
	Val Node
}

// CSTCast is a CST node for the cast instruction.
type CSTCast struct {
	CSTInstPreamble
	Val Node
}

// CSTDtosi is a CST node for the dtosi instruction.
type CSTDtosi struct {
	CSTInstPreamble
	Val Node
}

// CSTLdtosi is a CST node for the ldtosi instruction.
type CSTLdtosi struct {
	CSTInstPreamble
	Val Node
}

// CSTLdtoui is a CST node for the ldtoui instruction.
type CSTLdtoui struct {
	CSTInstPreamble
	Val Node
}

// CSTDtoui is a CST node for the dtoui instruction.
type CSTDtoui struct {
	CSTInstPreamble
	Val Node
}

// CSTCmp is a CST node for the various cmp* instructions.
type CSTCmp struct {
	CSTInstPreamble
	CSTBinaryOperands
}

func (p *parser) op(pre CSTInstPreamble) Node {
	pre.Inst = p.consume()
	if pre.Type.Ch == TYPENAME && pre.Inst.Ch != CALL && pre.Inst.Ch != VAARG {
		p.err(pre.Type.off, 0, "expected base type")
	}
	switch pre.Inst.Ch {
	case ADD:
		return &CSTAdd{pre, p.binary()}
	case ALLOC16, ALLOC4, ALLOC8:
		return &CSTAlloc{pre, p.val()}
	case AND:
		return &CSTAnd{pre, p.binary()}
	case CALL:
		return p.call(pre)
	case CAST:
		return &CSTCast{pre, p.val()}
	case
		CEQD, CEQLD, CEQL, CEQS, CEQW,
		CGED, CGELD, CGES,
		CGTD, CGTLD, CGTS,
		CLED, CLELD, CLES,
		CLTD, CLTLD, CLTS,
		CNED, CNELD, CNEL, CNES, CNEW,
		CSGEL, CSGEW,
		CSGTL, CSGTW,
		CSLEL, CSLEW,
		CSLTL, CSLTW,
		CUGEL, CUGEW,
		CUGTL, CUGTW,
		CULEL, CULEW,
		CULTL, CULTW,
		CUOD, CUOS,
		COD, COS:
		return &CSTCmp{pre, p.binary()}
	case COPY:
		return &CSTCopy{pre, p.val()}
	case DECLARE:
		return &CSTDeclare{pre}
	case DIV:
		return &CSTDiv{pre, p.binary()}
	case DTOSI:
		return &CSTDtosi{pre, p.val()}
	case LDTOSI:
		return &CSTDtosi{pre, p.val()}
	case DTOUI:
		return &CSTDtoui{pre, p.val()}
	case LDTOUI:
		return &CSTDtoui{pre, p.val()}
	case EXTS, EXTD, EXTSB, EXTSH, EXTSW, EXTUB, EXTUH, EXTUW:
		return &CSTExt{pre, p.val()}
	case LOAD, LOADD, LOADL, LOADS, LOADSB, LOADSH, LOADSW, LOADUB, LOADUH, LOADUW, LOADW, LOADLD:
		return &CSTLoad{pre, p.val()}
	case MUL:
		return &CSTMul{pre, p.binary()}
	case OR:
		return &CSTOr{pre, p.binary()}
	case PHI:
		return &CSTPhi{pre, p.phiArgs()}
	case REM:
		return &CSTRem{pre, p.binary()}
	case SAR:
		return &CSTSar{pre, p.binary()}
	case SHL:
		return &CSTShl{pre, p.binary()}
	case SHR:
		return &CSTShr{pre, p.binary()}
	case SLTOF:
		return &CSTSltof{pre, p.val()}
	case SUB:
		return &CSTSub{pre, p.binary()}
	case STOSI:
		return &CSTStosi{pre, p.val()}
	case STOUI:
		return &CSTStoui{pre, p.val()}
	case SWTOF:
		return &CSTSwtof{pre, p.val()}
	case ULTOF:
		return &CSTUltof{pre, p.val()}
	case UWTOF:
		return &CSTUwtof{pre, p.val()}
	case TRUNCD:
		return &CSTTruncd{pre, p.val()}
	case TRUNCLD:
		return &CSTTruncld{pre, p.val()}
	case UDIV:
		return &CSTUdiv{pre, p.binary()}
	case UREM:
		return &CSTUrem{pre, p.binary()}
	case VAARG:
		return &CSTVaArg{pre, p.val()}
	case XOR:
		return &CSTXor{pre, p.binary()}
	default:
		p.err(pre.Inst.off, 0, "expected operation")
		p.consume()
		return nil
	}
}

// CSTCall is a CST node for the call instruction.
//
//  CALL := [%IDENT '=' ABITY] 'call' VAL '(' (ARG), ')'
type CSTCall struct {
	CSTInstPreamble
	Val    Node
	LParen CSTToken
	Args   []Node
	RParen CSTToken
}

func (p *parser) call(pre CSTInstPreamble) *CSTCall {
	return &CSTCall{
		pre,
		p.val(),
		p.must('('),
		p.args(),
		p.must(')'),
	}
}

// CSTRegularArg is a CST node for a regular argument of a call instruction.
//
//      ABITY VAL  # Regular argument
type CSTRegularArg struct {
	Type  CSTToken
	Val   Node
	Comma CSTToken
}

// Position implements Node.
func (n *CSTRegularArg) Position() token.Position { return n.Type.Position() }

// CSTEnvArg is a CST node for a environment argument of a call instruction.
//
//    | 'env' VAL     # Environment argument (first)
type CSTEnvArg struct {
	Env   CSTToken
	Val   Node
	Comma CSTToken
}

// Position implements Node.
func (n *CSTEnvArg) Position() token.Position { return n.Env.Position() }

// CSTVariadicMarker is a CST node for a variadic argument of a call instruction.
//
//    | '...'         # Variadic marker (last)
type CSTVariadicMarker struct {
	Ellipsis CSTToken
	Comma    CSTToken
}

// Position implements Node.
func (n *CSTVariadicMarker) Position() token.Position { return n.Ellipsis.Position() }

//  ARG :=
//      ABITY VAL  # Regular argument
//    | 'env' VAL     # Environment argument (first)
//    | '...'         # Variadic marker (last)
func (p *parser) args() (r []Node) {
	for {
		comma := false
		switch p.ch() {
		case W, L, P, C, S, D, LD, TYPENAME:
			r = append(r, &CSTRegularArg{p.consume(), p.val(), p.commaOpt(&comma)})
		case ENV:
			r = append(r, &CSTEnvArg{p.consume(), p.val(), p.commaOpt(&comma)})
		case ELLIPSIS:
			off := p.tok.off
			r = append(r, &CSTVariadicMarker{p.consume(), p.commaOpt(&comma)})
			if p.ch() != ')' {
				p.err(off, 0, "variadic marker must be last")
			}
		case ')':
			return r
		case eof:
			p.err(p.tok.off, 0, "unexpected end of file")
			return r
		default:
			p.err(p.tok.off, 0, "expected argument specification")
			return r
		}

		switch p.ch() {
		case ')':
			return r
		case eof:
			p.err(p.tok.off, 0, "unexpected end of file")
			return r
		}

		if !comma {
			p.err(p.off, 0, "expected ','")
			return r
		}
	}
}

// CSTRegularParam is a CST node for a regular parameter of a function
// definition.
//
//     ABITY %IDENT  # Regular parameter
type CSTRegularParam struct {
	Type  CSTToken
	Local CSTToken
	Comma CSTToken
}

// Position implements Node.
func (n *CSTRegularParam) Position() token.Position { return n.Type.Position() }

// CSTEnvParam is a CST node for the environment parameter of a function
// definition.
//
//   | 'env' %IDENT  # Environment parameter (first)
type CSTEnvParam struct {
	Env   CSTToken
	Local CSTToken
	Comma CSTToken
}

// Position implements Node.
func (n *CSTEnvParam) Position() token.Position { return n.Env.Position() }

// PARAM :=
//     ABITY %IDENT  # Regular parameter
//   | 'env' %IDENT  # Environment parameter (first)
//   | '...'         # Variadic marker (last)
func (p *parser) params() (r []Node) {
	for {
		comma := false
		switch p.ch() {
		case W, L, P, C, S, D, LD, TYPENAME:
			r = append(r, &CSTRegularParam{p.consume(), p.must(LOCAL), p.commaOpt(&comma)})
		case ENV:
			r = append(r, &CSTEnvParam{p.consume(), p.must(LOCAL), p.commaOpt(&comma)})
		case ELLIPSIS:
			off := p.tok.off
			r = append(r, &CSTVariadicMarker{p.consume(), p.commaOpt(&comma)})
			if p.ch() != ')' {
				p.err(off, 0, "variadic marker must be last")
			}
		case ')':
			return r
		case eof:
			p.err(p.tok.off, 0, "unexpected end of file")
			return r
		default:
			p.err(p.tok.off, 0, "expected parameter")
			return r
		}

		switch p.ch() {
		case ')':
			return r
		case eof:
			p.err(p.tok.off, 0, "unexpected end of file")
			return r
		}

		if !comma {
			p.err(p.off, 0, "expected ','")
			return r
		}
	}
}

func (p *parser) commaOpt(ok *bool) (r CSTToken) {
	if p.ch() == ',' {
		*ok = true
		return p.consume()
	}

	return r
}

// CSTBinaryOperands represents the operands of a binary operation.
type CSTBinaryOperands struct {
	A     Node
	Comma CSTToken
	B     Node
}

func (p *parser) binary() CSTBinaryOperands {
	return CSTBinaryOperands{p.val(), p.must(','), p.val()}
}

// CSTPhiArg is a CST node for an argument of a phi instruction.
type CSTPhiArg struct {
	Label CSTToken
	Val   Node
	Comma CSTToken
}

func (p *parser) phiArgs() (r []CSTPhiArg) {
	for {
		arg := CSTPhiArg{Label: p.must(LABEL), Val: p.val()}
		if p.ch() != ',' {
			return append(r, arg)
		}

		arg.Comma = p.consume()
		r = append(r, arg)
	}
}

func (p *parser) val() Node {
	switch p.ch() {
	case INT_LIT, LOCAL, GLOBAL, FLOAT32_LIT, FLOAT64_LIT, LONG_DOUBLE_LIT, STRING_LIT:
		tok := p.consume()
		return &tok
	case '-':
		return &CSTIntConst{p.consume(), p.must(INT_LIT)}
	default:
		p.err(p.tok.off, 0, "syntax error, unexpected %s", p.ch().str())
		p.consume()
		return nil
	}
}

func (p *parser) valOpt() Node {
	switch p.ch() {
	case INT_LIT, LOCAL, GLOBAL, FLOAT32_LIT, FLOAT64_LIT, LONG_DOUBLE_LIT, STRING_LIT:
		tok := p.consume()
		return &tok
	case '-':
		return &CSTIntConst{p.consume(), p.must(INT_LIT)}
	default:
		return nil
	}
}

func (p *parser) dst() CSTToken {
	switch p.ch() {
	case GLOBAL, LOCAL:
		// ok
	default:
		p.err(p.tok.off, 0, "expected destination operand")
	}
	return p.consume()
}

// ABITY := BASETY | :IDENT
func (p *parser) abiType() CSTToken {
	switch p.ch() {
	case W, L, P, C, S, D, LD, TYPENAME:
	default:
		p.err(p.tok.off, 0, "expected ABI type")
	}
	return p.consume()
}

// ABITY := BASETY | :IDENT
func (p *parser) abiTypeOpt() CSTToken {
	switch p.ch() {
	case W, L, P, C, S, D, LD, TYPENAME:
		return p.consume()
	}

	return CSTToken{}
}
