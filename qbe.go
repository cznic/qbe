// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate stringer -output stringer.go -linecomment -type=Ch,vmInst,vmOperandKind,vmType

// Package qbe deals with the QBE intermediate language
//
// QBE intermediate language introduction:
//
//	https://c9x.me/compile/
//
// QBE reference:
//
//	https://c9x.me/compile/doc/il.html
//
// QBE vs LLVM:
//
//	https://c9x.me/compile/doc/llvm.html
//
// Build status
//
// Available at https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fqbe
//
// Limitations
//
// The environmental function parameter/argument, as defined at
//
//	https://c9x.me/compile/doc/il.html#Functions
//
// is not supported.
package qbe // import "modernc.org/qbe"

//TODO https://wasmer.io/ ?

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"modernc.org/opt"
)

const (
	// Reserved ABI type name that maps to C va_list. For example
	//
	// A C function
	//
	//	int vprintf(const char *format, va_list ap);
	//
	// can become a QBE function
	//
	//	export function w vprintf(l format, :__qbe_va_list ap) { ... }
	VaList = "__qbe_va_list"

	// Reserved ABI type name that maps to C *va_list. For example
	//
	// A C function
	//
	//	void foo(va_list *ap);
	//
	// can become a QBE function
	//
	//	export function foo(:__qbe_va_listp ap) { ... }
	VaListPtr = "__qbe_va_listp"

	version = "0.0.8-20210725"
)

var (
	_ = trc //TODOOK
)

// VaInfo describes a va_list
type VaInfo struct {
	Size     int // sizeof va
	ElemSize int // sizeof *va or 0 if N/A

	// IsStructArray reports whether va_list if defined as, for example
	//
	//	typedef struct {
	//		unsigned int gp_offset;
	//		unsigned int fp_offset;
	//		void *overflow_arg_area;
	//		void *reg_save_area;
	//	} __builtin_va_list[1];
	IsStructArray bool
	// IsPointer reports whether va_list if defined as, for example
	//
	//	typedef char *__builtin_va_list;
	IsPointer bool
	// IsStruct reports whether va_list if defined as, for example
	//
	//	typedef struct {
	//		void *__ap;
	//	} __builtin_va_list;
	IsStruct bool
}

// VaInfoFor returns a VaInfo for os/arch or an error, if any.
func VaInfoFor(os, arch string) (*VaInfo, error) {
	switch os {
	case "darwin":
		switch arch {
		case "amd64":
			return &VaInfo{
				Size:          24,
				ElemSize:      24,
				IsStructArray: true,
			}, nil
		}
	case "freebsd", "netbsd":
		switch arch {
		case "amd64":
			return &VaInfo{
				Size:          24,
				ElemSize:      24,
				IsStructArray: true,
			}, nil
		}
	case "linux":
		switch arch {
		case "amd64":
			return &VaInfo{
				Size:          24,
				ElemSize:      24,
				IsStructArray: true,
			}, nil
		case "arm64":
			return &VaInfo{
				Size:     32,
				ElemSize: 0, // N/A
				IsStruct: true,
			}, nil
		case "arm":
			return &VaInfo{
				Size:     4,
				ElemSize: 0, // N/A
				IsStruct: true,
			}, nil
		case "386":
			return &VaInfo{
				Size:      4,
				ElemSize:  1,
				IsPointer: true,
			}, nil
		case "s390x":
			return &VaInfo{
				Size:          32,
				ElemSize:      32,
				IsStructArray: true,
			}, nil
		}
	case "windows":
		switch arch {
		case "amd64":
			return &VaInfo{
				Size:      8,
				ElemSize:  1,
				IsPointer: true,
			}, nil
		}
	}

	return nil, fmt.Errorf("unknown/usupported target %s/%s", os, arch)
}

// Version reports the version of the qbe package.
func Version() string { return fmt.Sprintf("%v %v/%v", version, runtime.GOOS, runtime.GOARCH) }

// Task represents a compilation job.
type Task struct {
	O      string // -O argument. Read only.
	arch   string
	args   []string // As passed to NewTask.
	cc     string
	name   string // As passed to NewTask.
	o      string // -o argument.
	os     string
	ptr    Type
	stderr io.Writer
	stdout io.Writer

	c       bool // -c
	keepTmp bool // -keep-tmp
}

// NewTask returns a newly created Task.
func NewTask(name string, args []string, stdout, stderr io.Writer) *Task {
	return &Task{
		arch:   env("TARGET_ARCH", env("GOARCH", runtime.GOARCH)),
		args:   args,
		cc:     env("QBEC_CC", env("CC", "gcc")),
		os:     env("TARGET_OS", env("GOOS", runtime.GOOS)),
		name:   name,
		stderr: stderr,
		stdout: stdout,
	}
}

// Main executes task.
func (t *Task) Main() (err error) {
	switch t.arch {
	case "386", "arm":
		t.ptr = VoidPointer{w}
	case "amd64", "arm64", "s390x":
		t.ptr = VoidPointer{l}
	default:
		return errorf("unknown/unsupported architecture: %q", t.arch)
	}

	cc, err := exec.LookPath(t.cc)
	if err != nil {
		return errorf("%s: cannot lookup path for %s: %v", t.name, t.cc, err)
	}

	t.cc = cc
	stop := false
	opts := opt.NewSet()
	opts.Arg("O", true, func(opt, arg string) error { t.O = arg; return nil })
	opts.Arg("o", false, func(opt, arg string) error { t.o = arg; return nil })
	opts.Opt("c", func(opt string) error { t.c = true; return nil })
	opts.Opt("keep-tmp", func(opt string) error { t.keepTmp = true; return nil })
	opts.Opt("version", func(opt string) error {
		fmt.Fprintln(t.stderr, Version())
		stop = true
		return nil
	})
	opts.Opt("-version", func(opt string) error {
		fmt.Fprintln(t.stderr, Version())
		stop = true
		return nil
	})

	var in, replace []string
	if err := opts.Parse(t.args, func(s string) error {
		if strings.HasPrefix(s, "-") {
			return nil
		}

		if strings.HasSuffix(s, ".qbe") {
			in = append(in, s)
		}

		return nil
	}); err != nil {
		return errorf("%s: %v", t.name, err)
	}
	if stop {
		return nil
	}

	if len(in) != 0 {
		produceC := false
		if strings.HasSuffix(t.o, ".c") {
			if len(in) != 1 {
				return errorf("%s: output set to a C file with multiple QBE input files", t.name)
			}

			produceC = true
		}

		var tmpDir, cPath string
		if !produceC {
			tmpDir, err = ioutil.TempDir("", "qbec-")
			if err != nil {
				return errorf("%s: cannot create temporary directory: %v", t.name, err)
			}

			defer func() {
				if t.keepTmp {
					fmt.Fprintf(os.Stderr, "keeping temporary directory %s\n", tmpDir)
					return
				}

				os.RemoveAll(tmpDir)
			}()
		}

		cNames := map[string]struct{}{}
		for i, v := range in {
			baseName := filepath.Base(v)
			switch {
			case produceC:
				cPath = t.o
			default:
				cName := baseName[:len(baseName)-len(".qbe")] + ".c"
				dir := tmpDir
				if _, ok := cNames[cName]; ok {
					dir = filepath.Join(tmpDir, fmt.Sprint(i))
					if err := os.Mkdir(dir, 0700); err != nil {
						return errorf("mkdir(%s): %v", dir, err)
					}
				}
				cNames[cName] = struct{}{}
				cPath = filepath.Join(dir, cName)
			}
			replace = append(replace, cPath)
			b, err := ioutil.ReadFile(v)
			if err != nil {
				return errorf("%s: cannot open input file: %v", t.name, err)
			}

			cst, err := Parse(b, v, false)
			if err != nil {
				return errorf("%s: %v", t.name, err)
			}

			ast, err := cst.AST(t.os, t.arch)
			if err != nil {
				return errorf("%s: %v", t.name, err)
			}

			f, err := os.Create(cPath)
			if err != nil {
				return errorf("%s: cannot create file: %v", t.name, err)
			}

			w := bufio.NewWriter(f)
			if err := ast.C(w, t.headers(), t.os, t.arch); err != nil {
				return errorf("%s: %v", t.name, err)
			}

			if err := ast.Warning(); err != nil {
				fmt.Fprintf(t.stderr, "%s\n", err)
			}

			if err := w.Flush(); err != nil {
				return errorf("%s: cannot flush file: %v", t.name, err)
			}

			if err := f.Close(); err != nil {
				return errorf("%s: cannot close file: %v", t.name, err)
			}

			if produceC {
				return nil
			}
		}
	}
	var args []string
	if t.O == "" {
		args = append(args, "-O1")
	}
	for _, v := range t.args {
		if v == "-keep-tmp" {
			continue
		}

		if len(in) != 0 && in[0] == v {
			v = replace[0]
			in = in[1:]
			replace = replace[1:]
		}
		args = append(args, v)
	}
	cmd := exec.Command(t.cc, args...)
	cmd.Stdout = t.stdout
	cmd.Stderr = t.stderr
	if err = cmd.Run(); err != nil {
		wd, err2 := os.Getwd()
		err = errorf("%s: executing %s %v (in %v, err %v): %v", t.name, t.cc, args, wd, err2, err)
	}
	return err
}

func (t *Task) headers() string {
	switch t.os {
	case "windows":
		// https://stackoverflow.com/a/58286937
		return `
#include <malloc.h>
#include <stdarg.h>
#include <stdint.h>
`
	default:
		return `
#include <alloca.h>
#include <stdarg.h>
#include <stdint.h>
`
	}
}

func env(name, deflt string) (r string) {
	if s := os.Getenv(name); s != "" {
		return s
	}

	return deflt
}
