big_u_value		\x5cU{hex_digit}{hex_digit}{hex_digit}{hex_digit}{hex_digit}{hex_digit}{hex_digit}{hex_digit}
byte_value		{octal_byte_value}|{hex_byte_value}
decimal_digit		[0-9]
decimal_digits		{decimal_digit}+
decimal_exponent	[eE][-+]?{decimal_digits}
decimal_float_lit	{decimal_digits}"."{decimal_digits}?{decimal_exponent}?|{decimal_digits}{decimal_exponent}|"."{decimal_digits}{decimal_exponent}?
escaped_char		\x5c[abfnrtv\x5c"]
floatLiteral		{decimal_float_lit}|{hex_float_lit}
hex_byte_value		\x5cx{hex_digit}{hex_digit}
hex_digit		[0-9A-Fa-f]
hex_digits		{hex_digit}+
hex_exponent		[pP][-+]?{decimal_digits}
hex_float_lit		0[xX]{hex_mantissa}{hex_exponent}
hex_mantissa		{hex_digits}"."}{hex_digits}?|{hex_digits}|"."{hex_digits}
ident			({unicodeLetter}|{unicodeDigit}|".")+
intLiteral		{decimal_digits}
lineComment		#[^\n]*
little_u_value  	\5cu{hex_digit}{hex_digit}{hex_digit}{hex_digit}
octal_byte_value	\x5c{octal_digit}{octal_digit}{octal_digit}
octal_digit		[0-7]
sep			{white}|{lineComment}
stringLiteral		\"({unicode_value}|{byte_value})*\"
unicodeDigit		\x80
unicodeLetter		\x81
unicode_char		[^\x01\x02\x03\x04\x05\x06\x07\x08\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f]
unicode_value   	{unicode_char}|{little_u_value}|{big_u_value}|{escaped_char}
white			[ \n\r\t]+

%%

"("
")"
"+"
","
"-"
"..."
"="
"{"
"}"

"$"{ident}
"$"{stringLiteral}
"%"{ident}
":"{ident}
"@"{ident}
"d_"{floatLiteral}
"ld_"{floatLiteral}
"s_"{floatLiteral}
{intLiteral}
{sep}
{stringLiteral}

add
align
alloc16
alloc4
alloc8
and
b
c
call
cast
ceqd
ceql
ceqld
ceqs
ceqw
cged
cgeld
cges
cgtd
cgtld
cgts
cled
cleld
cles
cltd
cltld
clts
cned
cnel
cneld
cnes
cnew
cod
copy
cos
csgel
csgew
csgtl
csgtw
cslel
cslew
csltl
csltw
cugel
cugew
cugtl
cugtw
culel
culew
cultl
cultw
cuod
cuos
d
data
declare
div
dtosi
dtoui
env
export
extd
exts
extsb
extsh
extsw
extub
extuh
extuw
function
h
jmp
jnz
l
ld
ldtosi
ldtoui
load
loadd
loadl
loadld
loads
loadsb
loadsh
loadsw
loadub
loaduh
loaduw
loadw
mul
or
p
phi
ro
rem
ret
s
sar
shl
shr
sltof
storeb
stored
storeld
storeh
storel
stores
storew
stosi
stoui
sub
swtof
truncd
truncld
type
udiv
ultof
urem
uwtof
vaarg
vastart
w
xor
z
