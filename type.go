// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package qbe // import "modernc.org/qbe"

import (
	"modernc.org/cc/v3"
)

var (
	_ = []Type{
		CharPointer{},
		Float32{},
		Float64{},
		Int16{},
		Int32{},
		Int64{},
		Int8{},
		LongDouble{},
		TypeName{},
		VoidPointer{},
	}

	b  Type = Int8{}
	c  Type = CharPointer{}
	d  Type = Float64{}
	h  Type = Int16{}
	l  Type = Int64{}
	ld Type = LongDouble{}
	s  Type = Float32{}
	w  Type = Int32{}
)

// Type represents a QBE type.
type Type interface {
	String() string
	isCompatible(Type) bool
	size(*cc.ABI) uintptr // In bytes.
}

// Int8 represents type 'b'.
type Int8 struct{}

func (t Int8) String() string           { return "b" }
func (t Int8) isCompatible(u Type) bool { return t == u }
func (t Int8) size(abi *cc.ABI) uintptr { return 1 }

// Int16 represents type 'h'.
type Int16 struct{}

func (t Int16) String() string           { return "h" }
func (t Int16) isCompatible(u Type) bool { return t == u }
func (t Int16) size(abi *cc.ABI) uintptr { return 2 }

// Int32 represents type 'w'.
type Int32 struct{}

func (t Int32) String() string           { return "w" }
func (t Int32) size(abi *cc.ABI) uintptr { return 4 }

func (t Int32) isCompatible(u Type) bool {
	switch x := u.(type) {
	case CharPointer:
		return t.isCompatible(x.Type)
	case VoidPointer:
		return t.isCompatible(x.Type)
	default:
		return t == u
	}
}

// Int64 represents type 'l'.
type Int64 struct{}

func (Int64) String() string           { return "l" }
func (Int64) size(abi *cc.ABI) uintptr { return 8 }

func (t Int64) isCompatible(u Type) bool {
	switch x := u.(type) {
	case CharPointer:
		return t.isCompatible(x.Type)
	case VoidPointer:
		return t.isCompatible(x.Type)
	default:
		return t == u
	}
}

type CharPointer struct {
	Type
}

func (t CharPointer) String() string           { return "c" }
func (t CharPointer) isCompatible(u Type) bool { return t.Type.isCompatible(u) }

type VoidPointer struct {
	Type
}

func (t VoidPointer) String() string           { return "p" }
func (t VoidPointer) isCompatible(u Type) bool { return t.Type.isCompatible(u) }

// Float32 represents type 's'.
type Float32 struct{}

func (t Float32) String() string           { return "s" }
func (t Float32) isCompatible(u Type) bool { return t == u }
func (r Float32) size(abi *cc.ABI) uintptr { return 4 }

// Float64 represents type 'd'.
type Float64 struct{}

func (t Float64) String() string           { return "d" }
func (t Float64) isCompatible(u Type) bool { return t == u }
func (t Float64) size(abi *cc.ABI) uintptr { return 8 }

// LongDouble represents type 'ld'.
type LongDouble struct{}

func (t LongDouble) String() string           { return "ld" }
func (t LongDouble) isCompatible(u Type) bool { return t == u }
func (t LongDouble) size(abi *cc.ABI) uintptr { return abi.Types[cc.LongDouble].Size }

// TypeName represents a named type of the form :name.
type TypeName struct {
	Name
}

func (t TypeName) String() string           { return string(t.Name.Src()) }
func (t TypeName) isCompatible(u Type) bool { return t == u }
func (t TypeName) size(abi *cc.ABI) uintptr { panic(todo("")) }
