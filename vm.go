// Copyright 2021 The QBE Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package qbe // import "modernc.org/qbe"

import (
	"encoding/binary"
	"math"
	"unsafe"
)

type vmInst int

const (
	instAdd      vmInst = iota // add
	instAlloc16                // alloc16
	instAlloc4                 // alloc4
	instAlloc8                 // alloc8
	instAnd                    // and
	instCall                   // call
	instCast                   // cast
	instCeqd                   // ceqd
	instCeql                   // ceql
	instCeqs                   // ceqs
	instCeqw                   // ceqw
	instCged                   // cged
	instCges                   // cges
	instCgtd                   // cgtd
	instCgts                   // cgts
	instCled                   // cled
	instCles                   // cles
	instCltd                   // cltd
	instClts                   // clts
	instCned                   // cned
	instCnel                   // cnel
	instCnes                   // cnes
	instCnew                   // cnew
	instCod                    // cod
	instCopy                   // copy
	instCos                    // cos
	instCsgel                  // csgel
	instCsgew                  // csgew
	instCsgtl                  // csgtl
	instCsgtw                  // csgtw
	instCslel                  // cslel
	instCslew                  // cslew
	instCsltl                  // csltl
	instCsltw                  // csltw
	instCugel                  // cugel
	instCugew                  // cugew
	instCugtl                  // cugtl
	instCugtw                  // cugtw
	instCulel                  // culel
	instCulew                  // culew
	instCultl                  // cultl
	instCultw                  // cultw
	instCuod                   // cuod
	instCuos                   // cuos
	instDiv                    // div
	instDtosi                  // dtosi
	instExts                   // exts
	instExtsb                  // extsb
	instExtsh                  // extsh
	instExtsw                  // extsw
	instExtub                  // extub
	instExtuh                  // extuh
	instExtuw                  // extuw
	instJmp                    // jmp
	instJnz                    // jnz
	instLoadd                  // loadd
	instLoadl                  // loadl
	instLoads                  // loads
	instLoadsb                 // loadsb
	instLoadsh                 // loadsh
	instLoadsw                 // loadsw
	instLoadub                 // loadub
	instLoaduh                 // loaduh
	instLoaduw                 // loaduw
	instMul                    // mul
	instOr                     // or
	instPhi                    // phi
	instRem                    // rem
	instRet                    // ret
	instRetValue               // retValue
	instSar                    // sar
	instShl                    // shl
	instShr                    // shr
	instStoreb                 // storeb
	instStored                 // stored
	instStoreh                 // storeh
	instStorel                 // storel
	instStores                 // stores
	instStorew                 // storew
	instStosi                  // stosi
	instSub                    // sub
	instSwtof                  // swtof
	instUdiv                   // udiv
	instUrem                   // urem
	instVaArg                  // vaArg
	instVaStart                // vaStart
	instVoidCall               // voidCall
	instXor                    // xor
)

type vmOperandKind int

const (
	opFloat32 vmOperandKind = iota // float32
	opFloat64                      // float64
	opGlobal                       // global
	opInt                          // int
	opLocal                        // local
)

type vmType int

const (
	typeW vmType = iota // typeW
	typeL               // typeL
	typeD               // typeD
	typeS               // typeS
	typeT               // type name
)

type mem []byte

func (m *mem) inst() vmInst                   { return vmInst(m.get()) }
func (m *mem) operandKind() vmOperandKind     { return vmOperandKind(m.get()) }
func (m *mem) putInst(n vmInst)               { m.put(uint64(n)) }
func (m *mem) putOperandKind(n vmOperandKind) { m.put(uint64(n)) }

func (m *mem) get() uint64 {
	b := []byte(*m)
	n, ln := binary.Uvarint(b)
	if ln <= 0 {
		panic(todo(""))
	}
	*m = mem(b[ln:])
	return n
}

func (m *mem) put(n uint64) {
	b := *m
	n0 := len(b)
	if cap(b)-n0 < binary.MaxVarintLen64 {
		b = append(b, make([]byte, binary.MaxVarintLen64)...)
	}
	b = b[:cap(b)]
	n1 := binary.PutUvarint(b[n0:], n)
	*m = b[:n0+n1]
}

func (m *mem) preamble() (local uint, typ vmType, id uint64) {
	local = uint(m.get())
	typ, id = m.typ()
	return local, typ, id
}

func (m *mem) typ() (vmType, uint64) {
	switch r := vmType(m.get()); r {
	case typeT:
		return r, m.get()
	default:
		return r, ^(uint64(0))
	}
}

func (m *mem) putPreamble(ctx *ctx, n *InstPreamble) {
	m.put(uint64(ctx.f.Scope.node(n.Dst.Name).(*LocalInfo).N))
	m.putType(ctx, n, n.DstType)
}

func (m *mem) putType(ctx *ctx, n Node, t Type) {
	switch x := t.(type) {
	case Int32:
		m.put(uint64(typeW))
	case Int64:
		m.put(uint64(typeL))
		m.put(uint64(typeD))
	case Float32:
		m.put(uint64(typeS))
	case TypeName:
		m.put(uint64(typeT))
		m.put(uint64(ctx.types[string(x.Name.Src())]))
	default:
		panic(todo("%v: %T", n.Position(), t))
	}
}

func (m *mem) operand(locals *[]interface{}) (interface{}, bool) {
	switch x := m.operandKind(); x {
	case opInt:
		return int64(m.get()), true
	case opLocal:
		x := m.get()
		if x < uint64(len(*locals)) {
			r := (*locals)[x]
			return r, r != nil
		}

		return nil, false
	case opGlobal:
		return nil, false //TODO enable function calls
	case opFloat64:
		return math.Float64frombits(m.get()), true
	case opFloat32:
		return math.Float32frombits(uint32(m.get())), true
	default:
		panic(todo("", x))
	}
}

func (m *mem) skipOperand() {
	switch x := m.operandKind(); x {
	case
		opFloat32,
		opFloat64,
		opGlobal,
		opInt,
		opLocal:

		m.get()
	default:
		panic(todo("", x))
	}
}

func (m *mem) putOperand(ctx *ctx, n Node) {
	switch x := n.(type) {
	case IntLit:
		m.putOperandKind(opInt)
		m.put(x.Value())
	case Local:
		m.putOperandKind(opLocal)
		m.put(uint64(ctx.f.Scope.node(x.Name).(*LocalInfo).N))
	case Global:
		m.putOperandKind(opGlobal)
		m.put(uint64(ctx.globalID(x)))
	case *RegularArg:
		m.putOperandKind(opLocal)
		m.putOperand(ctx, x.Value)
	case Float64Lit:
		m.putOperandKind(opFloat64)
		m.put(math.Float64bits(x.Value()))
	case Float32Lit:
		m.putOperandKind(opFloat32)
		m.put(uint64(math.Float32bits(x.Value())))
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
}

func (m *mem) binary(locals *[]interface{}) (a, b interface{}, ok bool) {
	a, ok1 := m.operand(locals)
	b, ok2 := m.operand(locals)
	return a, b, ok1 && ok2
}

func (m *mem) putBinary(ctx *ctx, x binaryOp) {
	m.putOperand(ctx, x.a())
	m.putOperand(ctx, x.b())
}

type function struct {
	blocks map[int32]mem // 0-based, block #0 = entry block.
}

func newFunction(ctx *ctx, n *FuncDef, b *[]byte) *function {
	ctx.f = n
	var m *mem
	switch {
	case b == nil:
		m = &mem{}
	default:
		m = (*mem)(b)
	}
	r := &function{
		blocks: map[int32]mem{},
	}
	r.block(ctx, n.NewCFG(), m)
	return r
}

func (f *function) block(ctx *ctx, n *CFGNode, m *mem) {
	if _, ok := f.blocks[n.Block.num]; ok {
		return
	}

	n0 := len(*m)
	for _, v := range n.Block.Phis {
		f.phi(ctx, v, m)
	}
	for _, v := range n.Block.Insts {
		f.inst(ctx, v, m)
	}
	switch x := n.Block.Jump.(type) {
	case nil, Jmp:
		m.putInst(instJmp)
		m.put(uint64(n.Out1.Block.num))
		f.blocks[n.Block.num] = (*m)[n0:len(*m)]
		f.block(ctx, n.Out1, m)
	case Jnz:
		m.putInst(instJnz)
		m.putOperand(ctx, x.Value)
		m.put(uint64(n.Out1.Block.num))
		m.put(uint64(n.Out2.Block.num))
		f.blocks[n.Block.num] = (*m)[n0:len(*m)]
		f.block(ctx, n.Out1, m)
		f.block(ctx, n.Out2, m)
	case Ret:
		switch {
		case x.Value == nil:
			m.putInst(instRet)
		default:
			m.putInst(instRetValue)
			m.putOperand(ctx, x.Value)
		}
		f.blocks[n.Block.num] = (*m)[n0:len(*m)]
	default:
		panic(todo("%v: %T", n.Block.Jump.Position(), x))
	}
}

func (f *function) inst(ctx *ctx, n Node, m *mem) {
	switch x := n.(type) {
	case *Copy:
		m.putInst(instCopy)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Alloc4:
		m.putInst(instAlloc4)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Alloc8:
		m.putInst(instAlloc8)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Alloc16:
		m.putInst(instAlloc16)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Call:
		m.putInst(instCall)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.VoidCall.Value)
		m.put(uint64(len(x.VoidCall.Args)))
		for _, v := range x.VoidCall.Args {
			m.putOperand(ctx, v)
		}
	case *VoidCall:
		m.putInst(instVoidCall)
		m.putOperand(ctx, x.Value)
		m.put(uint64(len(x.Args)))
		for _, v := range x.Args {
			m.putOperand(ctx, v)
		}
	case *Div:
		m.putInst(instDiv)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Udiv:
		m.putInst(instUdiv)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Sub:
		m.putInst(instSub)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *And:
		m.putInst(instAnd)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Or:
		m.putInst(instOr)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Xor:
		m.putInst(instXor)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Add:
		m.putInst(instAdd)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Mul:
		m.putInst(instMul)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Rem:
		m.putInst(instRem)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Urem:
		m.putInst(instUrem)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Loadd:
		m.putInst(instLoadd)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Loads:
		m.putInst(instLoads)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Loadsb:
		m.putInst(instLoadsb)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Loadsh:
		m.putInst(instLoadsh)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Loadsw:
		m.putInst(instLoadsw)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Loadl:
		m.putInst(instLoadl)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Loadub:
		m.putInst(instLoadub)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Loaduh:
		m.putInst(instLoaduh)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Loaduw:
		m.putInst(instLoaduw)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Storel:
		m.putInst(instStorel)
		m.putOperand(ctx, x.Src)
		m.putOperand(ctx, x.Dst)
	case *Storeb:
		m.putInst(instStoreb)
		m.putOperand(ctx, x.Src)
		m.putOperand(ctx, x.Dst)
	case *Storeh:
		m.putInst(instStoreh)
		m.putOperand(ctx, x.Src)
		m.putOperand(ctx, x.Dst)
	case *Storew:
		m.putInst(instStorew)
		m.putOperand(ctx, x.Src)
		m.putOperand(ctx, x.Dst)
	case *Stored:
		m.putInst(instStored)
		m.putOperand(ctx, x.Src)
		m.putOperand(ctx, x.Dst)
	case *Stores:
		m.putInst(instStores)
		m.putOperand(ctx, x.Src)
		m.putOperand(ctx, x.Dst)
	case *Ceqd:
		m.putInst(instCeqd)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Ceql:
		m.putInst(instCeql)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Ceqs:
		m.putInst(instCeqs)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Ceqw:
		m.putInst(instCeqw)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cged:
		m.putInst(instCged)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cges:
		m.putInst(instCges)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cgtd:
		m.putInst(instCgtd)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cgts:
		m.putInst(instCgts)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cled:
		m.putInst(instCled)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cles:
		m.putInst(instCles)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cltd:
		m.putInst(instCltd)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Clts:
		m.putInst(instClts)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cned:
		m.putInst(instCned)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cnel:
		m.putInst(instCnel)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cnes:
		m.putInst(instCnes)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cnew:
		m.putInst(instCnew)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Csgel:
		m.putInst(instCsgel)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Csgew:
		m.putInst(instCsgew)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Csgtl:
		m.putInst(instCsgtl)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Csgtw:
		m.putInst(instCsgtw)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cslel:
		m.putInst(instCslel)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cslew:
		m.putInst(instCslew)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Csltl:
		m.putInst(instCsltl)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Csltw:
		m.putInst(instCsltw)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cugel:
		m.putInst(instCugel)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cugew:
		m.putInst(instCugew)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cugtl:
		m.putInst(instCugtl)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cugtw:
		m.putInst(instCugtw)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Culel:
		m.putInst(instCulel)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Culew:
		m.putInst(instCulew)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cultl:
		m.putInst(instCultl)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cultw:
		m.putInst(instCultw)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cuod:
		m.putInst(instCuod)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cuos:
		m.putInst(instCuos)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cod:
		m.putInst(instCod)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Cos:
		m.putInst(instCos)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Exts:
		m.putInst(instExts)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Extsb:
		m.putInst(instExtsb)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Extsh:
		m.putInst(instExtsh)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Extsw:
		m.putInst(instExtsw)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Extub:
		m.putInst(instExtub)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Extuh:
		m.putInst(instExtuh)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Extuw:
		m.putInst(instExtuw)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Sar:
		m.putInst(instSar)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Shr:
		m.putInst(instShr)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Shl:
		m.putInst(instShl)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putBinary(ctx, x)
	case *Swtof:
		m.putInst(instSwtof)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Stosi:
		m.putInst(instStosi)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Cast:
		m.putInst(instCast)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *Dtosi:
		m.putInst(instDtosi)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	case *VaStart:
		m.putInst(instVaStart)
		m.putOperand(ctx, x.Value)
	case *VaArg:
		m.putInst(instVaArg)
		m.putPreamble(ctx, &x.InstPreamble)
		m.putOperand(ctx, x.Value)
	default:
		panic(todo("%v: %T", n.Position(), x))
	}
}

func (f *function) phi(ctx *ctx, n *Phi, m *mem) {
	m.putInst(instPhi)
	m.putPreamble(ctx, &n.InstPreamble)
	m.put(uint64(len(n.Args)))
	for _, v := range n.Args {
		m.put(uint64(ctx.f.Scope.node(v.Name).(*Block).num))
		m.putOperand(ctx, v.Value)
	}
}

type vm struct {
	done bool
}

func newVM() *vm {
	return &vm{}
}

func (m *vm) runFunc(budget *int, f *function, args []interface{}) (interface{}, bool) {
	return m.runBlock(budget, f, f.blocks[0], &args, 0, -1)
}

func (m *vm) runBlock(budget *int, f *function, block mem, locals *[]interface{}, thisBlock, prevBlock int) (interface{}, bool) {
	var allocs []byte

	defer func() { allocs = nil }()

	//trc("==== prev %v, this %v", prevBlock, thisBlock)
	for ip := 0; !m.done; ip++ {
		if *budget <= 0 {
			return nil, false
		}

		*budget--
		inst := block.inst()
		//trc("prev %v, this %v, ip %v: inst %v", prevBlock, thisBlock, ip, inst)
		switch inst {
		case instAdd:
			m.add(&block, locals)
		case instSub:
			m.sub(&block, locals)
		case instMul:
			m.mul(&block, locals)
		case instDiv:
			m.div(&block, locals)
		case instUdiv:
			m.udiv(&block, locals)
		case instRem:
			m.rem(&block, locals)
		case instUrem:
			m.urem(&block, locals)
		case instAlloc16:
			m.alloc(&allocs, 16, &block, locals)
		case instAlloc4:
			m.alloc(&allocs, 4, &block, locals)
		case instAlloc8:
			m.alloc(&allocs, 8, &block, locals)
		case instAnd:
			panic(todo(""))
		case instVoidCall:
			fn, ok := block.operand(locals)
			if !ok {
				return nil, false
			}

			_ = fn
			panic(todo(""))
		case instCall:
			pre, typ, id := block.preamble()
			fn, ok := block.operand(locals)
			if !ok {
				return nil, false
			}

			_ = pre
			_ = typ
			_ = id
			_ = fn
			panic(todo(""))
		case instCast:
			m.cast(&block, locals)
		case instCeqd:
			panic(todo(""))
		case instCeql:
			panic(todo(""))
		case instCeqs:
			panic(todo(""))
		case instCeqw:
			m.ceqw(&block, locals)
		case instCged:
			panic(todo(""))
		case instCges:
			panic(todo(""))
		case instCgtd:
			panic(todo(""))
		case instCgts:
			panic(todo(""))
		case instCled:
			m.cled(&block, locals)
		case instCles:
			panic(todo(""))
		case instCltd:
			panic(todo(""))
		case instClts:
			panic(todo(""))
		case instCned:
			panic(todo(""))
		case instCnel:
			m.cnel(&block, locals)
		case instCnes:
			panic(todo(""))
		case instCnew:
			m.cnew(&block, locals)
		case instCod:
			panic(todo(""))
		case instCopy:
			local, typ, _ := block.preamble()
			op, ok := block.operand(locals)
			if !ok {
				return nil, false
			}

			m.setLocal(locals, local, typ, op)
		case instCos:
			panic(todo(""))
		case instCsgel:
			panic(todo(""))
		case instCsgew:
			panic(todo(""))
		case instCsgtl:
			panic(todo(""))
		case instCsgtw:
			m.csgtw(&block, locals)
		case instCslel:
			m.cslel(&block, locals)
		case instCslew:
			m.cslew(&block, locals)
		case instCsltl:
			panic(todo(""))
		case instCsltw:
			m.csltw(&block, locals)
		case instCugel:
			panic(todo(""))
		case instCugew:
			panic(todo(""))
		case instCugtl:
			panic(todo(""))
		case instCugtw:
			panic(todo(""))
		case instCulel:
			panic(todo(""))
		case instCulew:
			panic(todo(""))
		case instCultl:
			panic(todo(""))
		case instCultw:
			panic(todo(""))
		case instCuod:
			panic(todo(""))
		case instCuos:
			panic(todo(""))
		case instDtosi:
			m.dtosi(&block, locals)
		case instExts:
			panic(todo(""))
		case instExtsb:
			panic(todo(""))
		case instExtsh:
			panic(todo(""))
		case instExtsw:
			panic(todo(""))
		case instExtub:
			panic(todo(""))
		case instExtuh:
			panic(todo(""))
		case instExtuw:
			panic(todo(""))
		case instJmp:
			prevBlock = thisBlock
			thisBlock = int(block.get())
			block = f.blocks[int32(thisBlock)]
			ip = 0
		case instJnz:
			op, ok := block.operand(locals)
			if !ok {
				return nil, false
			}

			nz := block.get()
			z := block.get()
			prevBlock = thisBlock
			switch {
			case isNonZero(op):
				thisBlock = int(nz)
				block = f.blocks[int32(nz)]
			default:
				thisBlock = int(z)
				block = f.blocks[int32(z)]
			}
			ip = 0
		case instLoadd:
			m.load(d, false, &block, locals)
		case instLoadl:
			m.load(l, true, &block, locals)
		case instLoads:
			m.load(s, false, &block, locals)
		case instLoadsb:
			m.load(b, true, &block, locals)
		case instLoadsh:
			m.load(h, true, &block, locals)
		case instLoadsw:
			m.load(w, true, &block, locals)
		case instLoadub:
			m.load(b, false, &block, locals)
		case instLoaduh:
			m.load(h, false, &block, locals)
		case instLoaduw:
			m.load(w, false, &block, locals)
		case instOr:
			panic(todo(""))
		case instPhi:
			local, typ, _ := block.preamble()
			n := int(block.get())
			for i := 0; i < n; i++ {
				switch int(block.get()) {
				case prevBlock:
					op, ok := block.operand(locals)
					if !ok {
						return nil, false
					}

					m.setLocal(locals, local, typ, op)
				default:
					block.skipOperand()
				}
			}
		case instRet:
			return nil, true
		case instRetValue:
			return block.operand(locals)
		case instSar:
			m.sar(&block, locals)
		case instShl:
			panic(todo(""))
		case instShr:
			m.shr(&block, locals)
		case instStoreb:
			m.store(b, &block, locals)
		case instStored:
			m.store(d, &block, locals)
		case instStoreh:
			m.store(h, &block, locals)
		case instStorel:
			m.store(l, &block, locals)
		case instStores:
			m.store(s, &block, locals)
		case instStorew:
			m.store(w, &block, locals)
		case instStosi:
			panic(todo(""))
		case instSwtof:
			panic(todo(""))
		case instVaArg:
			panic(todo(""))
		case instVaStart:
			return nil, false //TODO
		case instXor:
			panic(todo(""))
		default:
			panic(todo("", inst))
		}
	}
	return nil, false
}

func (m *vm) dtosi(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	op, ok := block.operand(locals)
	if !ok {
		m.done = true
		return
	}

	_ = local
	_ = typ
	_ = op
	panic(todo(""))
}

func (m *vm) cast(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	op, ok := block.operand(locals)
	if !ok {
		m.done = true
		return
	}

	_ = local
	_ = typ
	_ = op
	panic(todo(""))
}

func (m *vm) shr(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int64:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(uint32(x)>>int32(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) sar(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int64:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)>>int32(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) cnew(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int32:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(int32(x) != int32(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) csltw(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int32:
		switch y := b.(type) {
		case int32:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(int32(x) < int32(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) cslew(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int32:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(int32(x) <= int32(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) cled(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case float64:
		switch y := b.(type) {
		case float64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(float64(x) <= float64(y)))
			default:
				panic(todo("", typ))
			}
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(float64(x) <= math.Float64frombits(uint64(y))))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		_ = local
		_ = typ
		panic(todo("%T", x))
	}
}

func (m *vm) cslel(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int64:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(int64(x) <= int64(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		_ = local
		_ = typ
		panic(todo("%T", x))
	}
}

func (m *vm) csgtw(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int32:
		switch y := b.(type) {
		case int32:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(int32(x) > int32(y)))
			default:
				panic(todo("", typ))
			}
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(int32(x) > int32(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) cnel(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case allocPtr:
		switch y := b.(type) {
		case allocPtr:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(x.allocs != y.allocs || x.x != y.x))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
	_ = local
	_ = typ
}

func (m *vm) ceqw(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int32:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(int32(x) == int32(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	case int64:
		switch y := b.(type) {
		case int32:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, bool32(int32(x) == int32(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) urem(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok || isZero(b) {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int32:
		switch y := b.(type) {
		case int32:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(uint32(x)%uint32(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	case int64:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(uint32(x)%uint32(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) rem(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok || isZero(b) {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int32:
		switch y := b.(type) {
		case int32:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)%int32(y))
			default:
				panic(todo("", typ))
			}
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)%int32(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	case int64:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)%int32(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	case allocPtr:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x.x)%int32(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) udiv(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok || isZero(b) {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int64:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(uint32(x)/uint32(y)))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) div(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok || isZero(b) {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int64:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)/int32(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) mul(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int32:
		switch y := b.(type) {
		case int32:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)*int32(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) sub(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int32:
		switch y := b.(type) {
		case int32:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)-int32(y))
			default:
				panic(todo("", typ))
			}
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)-int32(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	case int64:
		switch y := b.(type) {
		case int32:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)-int32(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	case float64:
		switch y := b.(type) {
		case float64:
			switch typ {
			case typeD:
				m.setLocal(locals, local, typ, float64(x)-float64(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) add(block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	a, b, ok := block.binary(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := a.(type) {
	case int32:
		switch y := b.(type) {
		case int32:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)+int32(y))
			default:
				panic(todo("", typ))
			}
		case int64:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)+int32(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	case int64:
		switch y := b.(type) {
		case int32:
			switch typ {
			case typeW:
				m.setLocal(locals, local, typ, int32(x)+int32(y))
			default:
				panic(todo("", typ))
			}
		case int64:
			switch typ {
			case typeL:
				m.setLocal(locals, local, typ, int64(x)+int64(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	case float64:
		switch y := b.(type) {
		case float64:
			switch typ {
			case typeD:
				m.setLocal(locals, local, typ, float64(x)+float64(y))
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	case allocPtr:
		switch y := b.(type) {
		case int64:
			switch typ {
			case typeL:
				x.x += uintptr(y)
				m.setLocal(locals, local, typ, x)
			default:
				panic(todo("", typ))
			}
		default:
			panic(todo("%T", y))
		}
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) load(memType Type, signed bool, block *mem, locals *[]interface{}) {
	local, dstType, _ := block.preamble()
	addr, ok := block.operand(locals)
	if !ok {
		m.done = true
		return
	}

	switch x := addr.(type) {
	case allocPtr:
		switch memType.(type) {
		case Int32:
			if x.x+4 <= uintptr(len(*x.allocs)) {
				m.setLocal(locals, local, dstType, *(*int32)(unsafe.Pointer(&(*x.allocs)[x.x])))
				return
			}
		default:
			panic(todo("%T(%[1]v)", memType))
		}
	default:
		panic(todo("%T(%[1]v) =%T(%[2]v) %T(%[3]v)", local, dstType, x))
	}
	m.done = true
}

func (m *vm) store(memType Type, block *mem, locals *[]interface{}) {
	src, ok1 := block.operand(locals)
	addr, ok2 := block.operand(locals)
	if !ok1 || !ok2 {
		m.done = true
		return
	}

	switch x := addr.(type) {
	case allocPtr:
		switch memType.(type) {
		case Int32:
			if x.x+4 <= uintptr(len(*x.allocs)) {
				switch y := src.(type) {
				case int32:
					*(*int32)(unsafe.Pointer(&(*x.allocs)[x.x])) = int32(y)
				case int64:
					*(*int32)(unsafe.Pointer(&(*x.allocs)[x.x])) = int32(y)
				default:
					panic(todo("%T", y))
				}
				return
			}
		case Int64:
			if x.x+8 <= uintptr(len(*x.allocs)) {
				switch y := src.(type) {
				case int64:
					*(*int64)(unsafe.Pointer(&(*x.allocs)[x.x])) = int64(y)
				default:
					panic(todo("%T", y))
				}
				return
			}
		case Int8:
			if x.x+1 <= uintptr(len(*x.allocs)) {
				switch y := src.(type) {
				case int32:
					*(*int8)(unsafe.Pointer(&(*x.allocs)[x.x])) = int8(y)
				case int64:
					*(*int8)(unsafe.Pointer(&(*x.allocs)[x.x])) = int8(y)
				default:
					panic(todo("%T", y))
				}
				return
			}
		default:
			panic(todo("%T(%[1]v)", memType))
		}
	case int64, int32:
		// nop
	default:
		panic(todo("%T", x))
	}
	m.done = true
}

func bool32(b bool) int32 {
	if b {
		return 1
	}

	return 0
}

type allocPtr struct {
	allocs *[]byte
	x      uintptr
}

func (m *vm) alloc(allocs *[]byte, align int, block *mem, locals *[]interface{}) {
	local, typ, _ := block.preamble()
	op, ok := block.operand(locals)
	if !ok {
		m.done = true
		return
	}

	var n uintptr
	switch x := op.(type) {
	case int64:
		if x < 0 || x > 1024 {
			m.done = true
			return
		}

		n = uintptr(x)
	default:
		panic(todo("%T(%v)", x, x))
	}

	if n == 0 {
		n = 1
	}
	a := *allocs
	x := roundup(uintptr(len(a)), uintptr(align))
	y := x + n
	if d := y - uintptr(len(a)); d > 0 {
		a = append(a, make([]byte, d)...)
		*allocs = a
	}
	m.setLocal(locals, local, typ, allocPtr{allocs, x})
}

func roundup(n, to uintptr) uintptr {
	if r := n % to; r != 0 {
		return n + to - r
	}

	return n
}

func isZero(op interface{}) bool {
	switch x := op.(type) {
	case int32:
		return x == 0
	case int64:
		return x == 0
	case IntLit:
		return x.Value() == 0
	default:
		panic(todo("%T", x))
	}
}

func isNonZero(op interface{}) bool {
	switch x := op.(type) {
	case int32:
		return x != 0
	case int64:
		return x != 0
	case IntLit:
		return x.Value() != 0
	default:
		panic(todo("%T", x))
	}
}

func (m *vm) setLocal(locals *[]interface{}, local uint, typ vmType, op interface{}) {
	if op == nil {
		panic(todo("internal error"))
	}

	a := *locals
	if d := int(local) - len(a); d > -1 {
		a = append(a, make([]interface{}, d+1)...)
		*locals = a
	}
	switch typ {
	case typeW:
		switch x := op.(type) {
		case int32:
			a[local] = int32(x)
		case int64:
			a[local] = int32(x)
		case allocPtr:
			a[local] = x
		default:
			panic(todo("%T", x))
		}
	case typeL:
		switch x := op.(type) {
		case int32:
			a[local] = int64(x)
		case int64:
			a[local] = int64(x)
		case allocPtr:
			a[local] = x
		default:
			panic(todo("%T", x))
		}
	case typeD:
		switch x := op.(type) {
		case float64:
			a[local] = float64(x)
		default:
			panic(todo("%T", x))
		}
	default:
		panic(todo("", typ))
	}
}
